$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/fature/pre_fatura/pre_fatura.feature");
formatter.feature({
  "name": "Gerar Pre-Fatura",
  "description": "",
  "keyword": "Funcionalidade",
  "tags": [
    {
      "name": "@atendimento_funcionalidade01"
    },
    {
      "name": "@PrioridadeAlta"
    }
  ]
});
formatter.scenario({
  "name": "Gerar um pré-fatura de serviços ambulatoriais por paciente",
  "description": "",
  "keyword": "Cenario",
  "tags": [
    {
      "name": "@atendimento_funcionalidade01"
    },
    {
      "name": "@PrioridadeAlta"
    },
    {
      "name": "@ambulatorial_paciente"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "que tenho acesso a aplicação Smart Atende",
  "keyword": "Dado "
});
formatter.match({
  "location": "DefinitionStepsLogin.que_tenho_acesso_a_aplicação_Smart_Atende()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "acesso o menu \"Arquivo;Abrir;Atendimento\" no Home do modulo Atende",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsHome.acesso_o_menu_no_Home_do_modulo_Atende(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "informa o item de busca \"ISAIAS SANTOS SILVA\" e busco no modulo Atende",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsAtendimentoBuscaPaciente.informa_o_item_de_busca_e_busco_no_modulo_Atende(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botão avancar  por \"1\" vezes do modulo Atende",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsCommonToAll.clicar_no_botão_avancar_por_vezes_do_modulo_Atende(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "informar dados da OS, tipo Atendimento \"Assistencial\" e Setor \"\"",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsAtendimentoOrdemDeServico.informar_dados_da_OS_tipo_Atendimento_e_Setor(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botão gravar do modulo Atende",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsCommonToAll.clicar_no_botão_gravar_do_modulo_Atende()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "informar dados do serviço \"Glico\",\"1\", \"ADRIANO\"",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsAtendimentoOrdemDeServico.informar_dados_do_serviço(String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicar no botão gravar do modulo Atende",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsCommonToAll.clicar_no_botão_gravar_do_modulo_Atende()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "que tenho acesso a aplicação Smart Fature",
  "keyword": "Dado "
});
formatter.match({
  "location": "DefinitionStepsCommonToAll.que_tenho_acesso_a_aplicação_Smart_Fature()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "acesso o menu \"Arquivo;Abrir;Pré-Fatura\" no Home do modulo Fature",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsHome.acesso_o_menu_no_Home_do_modulo_Fature(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico na aba \"Ambulatório\"",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_na_aba(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "busco pelo paciente \"ISAIAS SANTOS SILVA\"",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.busco_pelo_paciente(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico no botão \"Avançar\" e verifico o cabeçalho da pré-fatura",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_no_botão_e_verifico_o_cabeçalho_da_pré_fatura(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico no botão \"Novo\" para gerar uma nova pré-fatura",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_no_botão_para_gerar_uma_nova_pré_fatura(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "seleciono um periodo de trinta dias de \"01112019\" a \"30112019\"",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.seleciono_um_periodo_de_trinta_dias_de_a(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico no botão \"Gravar\" do módulo Fature",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_no_botão_do_módulo_Fature(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "visualizo uma lista de OS geradas no periodo pesquisado",
  "keyword": "Entao "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.visualizo_uma_lista_de_OS_geradas_no_periodo_pesquisado()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico no botao \"Processar\" da aba ambulatorio da tela de pre-fature do modulo fature",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_no_botao_da_aba_ambulatorio_da_tela_de_pre_fature_do_modulo_fature(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "confirmo em \"SIM\" o processamento da pré-fatura",
  "keyword": "E "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.confirmo_em_o_processamento_da_pré_fatura(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a pré fatura é processada e uma mensagem informando que o \"Processo concluído\" é exibido",
  "keyword": "Entao "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.a_pré_fatura_é_processada_e_uma_mensagem_informando_que_o_é_exibido(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clico no botao \"OK\" da aba ambulatorio da tela de pre-fature do modulo fature",
  "keyword": "Quando "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.clico_no_botao_da_aba_ambulatorio_da_tela_de_pre_fature_do_modulo_fature(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sou redirecionado para a aba de \"Log de Processamento\" onde visualizo a pré-fatura criada",
  "keyword": "Entao "
});
formatter.match({
  "location": "DefinitionStepsFatureAbaAmbulatorio.sou_redirecionado_para_a_aba_de_onde_visualizo_a_pré_fatura_criada(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});