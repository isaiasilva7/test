#language:pt
#encoding:utf-8

Funcionalidade: Calculadora

  @desktop @regressivo1
  Esquema do Cenário: Mostrar resultado após operação na calculadora
    Dado que estou na calculadora
    E que a calculadora está zerada
    Quando eu pressionar o numero "<numero_um>"
    E e escolher operação de "<operacao>"
    E eu pressionar o numero "<numero_dois>"
    Então eu devo visualizar o resultado "<resultado>"

    Exemplos:
      | numero_um | numero_dois | operacao      | resultado |
      | 4         | 2           | multiplicacao | 8         |
      | 7         | 3           | subtracao     | 4         |
      | 3         | 9           | soma          | 11        |

