Feature: Lookup a definition
  In order to talk better
  As an English student
  I want to look up word definitions

  @apple @regressivo1
  Scenario: Looking up the definition of 'apple'
    Given the user is on the Wikionary home page
    When the user looks up the definition of the word
      | word  |
      | apple |
    Then they should see the definition
      | definition                                                                                    |
      | A common, round fruit produced by the tree Malus domestica, cultivated in temperate climates. |
      | Any of various tree-borne fruits or vegetables especially considered as resembling an apple   |

  @pear @regressivo1
  Scenario: Looking up the definition of 'pear'
    Given the user is on the Wikionary home page
    When the user looks up the definition of the word
      | word |
      | pear |
    Then they should see the definition
      | definition                                                                                     |
      | An edible fruit produced by the pear tree, similar to an apple but elongated towards the stem. |
