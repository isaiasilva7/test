#language:pt
#encoding:utf-8

@laboratorio_funcionalidade04
Funcionalidade: Gerar Ordem de serviços Laboratorio - Funcionalidade 04

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario: Gerar OS e validar valor do serviço, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1" no modulo Atende Laboratorio
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario: Gerar OS e validar valor do serviço, Sexo F e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1" no modulo Atende Laboratorio
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario: Gerar OS e validar valor do serviço, Sexo M e com o convênio BRADESCO F (BF)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1" no modulo Atende Laboratorio
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario: Gerar OS e validar valor do serviço, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1" no modulo Atende Laboratorio
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no modulo Atende Laboratorio


#-------------------------------------Teste de Campo valdar se no sistema apresenta falha geral----------------------------------------------------------------------------
  @desktop @regressivo @smart
  Cenario: Clicar no botão Questionarios na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão "Questionario", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão Arquivos na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão "Arquivos", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão Info na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão "Info", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão reprogramar na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Reprogramar", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão deposito na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Deposito", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão Obs na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Obs", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Clicar no botão pagamento na tela Ordem de servico e validar que o sistema não apresenta falha Geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario: validar que ao ao clicar no botão gravar quando não existem nenhum exame a ser gravado, na tela ordem de serviço do Laboratorio no modulo Atende, o sistema não apresenta falha geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão cancelar do modulo Atende
    Quando clicar no botão "OK" do PopUp
    Quando clicar no botão gravar do modulo Atende
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo @SMART-14360
  Cenario: validar que ao ao tenta imprimir Guia, na tela de laboratório no modulo Atende quando os dados do exame não está salvo o sistema não apresenta falha geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario: validar que ao ao tenta imprimir Guia, na tela de laboratório no modulo Atende, o sistema não apresenta falha geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    E clicar no botão cancelar do modulo Atende
    Quando clicar no botão "OK" do PopUp
    E clicar no botão imprirmir do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Entao o sistema não apresenta falha geral