#language:pt
#encoding:utf-8

@laboratorio_funcionalidade03.1
Funcionalidade:  Adicionar e consultar paciente na fila de espera Laboratorio - Funcionalidade 03-1

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende
    Quando clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende Laboratorio
    E clicar no botão de encerrar  no modulo Atende Laboratorio
    Quando informa o nome do Médico "ABILIO" para consulta a Fila na tela de fila de Espera no modulo Atende Laboratorio
    E clicar no botão de encerrar  no modulo Atende Laboratorio
    Quando que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende

  @desktop @regressivo
  Cenario: Adicionar paciente na fila de espera Laboratorio com o convênios BRADESCO F (BF) e confirmado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão Adicionar na Fila tela de cadastro do paciente no modulo Atende Laboratorio
    E informo a Senha "senhaAleatoria" da tela Adicionar paciente a Fila de Espera no modulo Atende Laboratorio
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera no modulo Atende Laboratorio
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento no modulo Atende Laboratorio
    E clicar no botão detalhes na tela fila de espera atendimento no modulo Atende Laboratorio
    E clicar no botão Retorna pra Fila na tela Detalhes no modulo Atende Laboratorio
    Quando informar o nome do paciente "" na tela fila de espera atendimento no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio

  @desktop @smart @regressivo
  Cenario: Adicionar paciente na fila de espera  Laboratorio com o convênios PARTICULAR 1 e confirmado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão Adicionar na Fila tela de cadastro do paciente no modulo Atende Laboratorio
    E informo a Senha "senhaAleatoria" da tela Adicionar paciente a Fila de Espera no modulo Atende Laboratorio
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera no modulo Atende Laboratorio
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento no modulo Atende Laboratorio
    E clicar no botão detalhes na tela fila de espera atendimento no modulo Atende Laboratorio
    E clicar no botão Retorna pra Fila na tela Detalhes no modulo Atende Laboratorio
    Quando informar o nome do paciente "" na tela fila de espera atendimento no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario:Adicionar paciente na fila de espera Laboratorio com o convênios PARTICULAR 1 (PAR) e confirmado que o mesmo foi adicionado, transferir de Fila e confirmar que o mesmo foi transferido por fim remover da fila
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão Adicionar na Fila tela de cadastro do paciente no modulo Atende Laboratorio
    E informo a Senha "senhaAleatoria" da tela Adicionar paciente a Fila de Espera no modulo Atende Laboratorio
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera no modulo Atende Laboratorio
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    Quando clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende Laboratorio
    E informar a fila "ABILIO" na tela de trasfêrencia de paciente para a fila no modulo Atende Laboratorio
    Quando clicar no botão Transferir da tela trasferir paciente para a fila no modulo Atende Laboratorio
    Quando informa o nome do Médico "ABILIO" para consulta a Fila na tela de fila de Espera no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario:Adicionar paciente na fila de espera Laboratorio com o convênios BRADESCO F (BF) e confirmado que o mesmo foi adicionado, transferir de Fila e confirmar que o mesmo foi transferido por fim remover da fila
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão Adicionar na Fila tela de cadastro do paciente no modulo Atende Laboratorio
    E informo a Senha "senhaAleatoria" da tela Adicionar paciente a Fila de Espera no modulo Atende Laboratorio
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera no modulo Atende Laboratorio
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    Quando clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende Laboratorio
    E informar a fila "ABILIO" na tela de trasfêrencia de paciente para a fila no modulo Atende Laboratorio
    Quando clicar no botão Transferir da tela trasferir paciente para a fila no modulo Atende Laboratorio
    Quando informa o nome do Médico "ABILIO" para consulta a Fila na tela de fila de Espera no modulo Atende Laboratorio
    Então confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio
    E clicar no botão de encerrar atendimento
    E clicar no botão de encerrar atendimento no modulo Atende Laboratorio

  @desktop @smart @regressivo
  Cenario:validar que o Botão fila de espera Laboratorio não desaparece ao adiconar uma arquivo de outro diretorio
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1" no modulo Atende Laboratorio
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no modulo Atende Laboratorio
    E clicar no botão retornar do modulo Atende
    Quando clicar no botão "Arquivos", na tela ordem de Servico no modulo Atende Laboratorio
    E clicar botão anexar o documento da tela Arquivo no modulo Atende Laboratorio
    Quando informar o nome do arquivo "F:\Arquivos_Desenv\Gilson\teste.pdf" e clicar em "Abrir" na tela de Selecione o nome do arquivo
    Quando clicar no botão fechar da tela Arquivo no modulo Atende Laboratorio
    E clicar na tecla "F2" do teclado no modulo Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende
    E clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral
    Quando fechar a tela via comando ESC do teclado
    E  informa o item de busca "PacienteNaMemoria" e busco no modulo Atende Laboratorio
    E clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

   #----------------------------------------------------------------------------------------------------------------------------------------
  @desktop @smart @regressivo
  Cenario:Confirmar que o botão Chamar Proximo da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Chamar Proximo" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Chamar Selecionado da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Chamar Selecionado" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Encerrar Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Encerrar Atendimento" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Detalhes Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Detalhes" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Imprimir Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Imprimir" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Atualizar Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Atualizar" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Foto Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Foto" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão CR Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "CR" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Historico Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Historico" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Marcacao Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Marcacao" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario:Confirmar que o botão Marcacao Atendimento da tela fila de espera Laboratorio esta presente
    Quando clicar no botão "Fila De Espera" na tela de busca no modulo Atende Laboratorio
    E clicar no botão "Rastraeabilidade" na tela de Fila de espera recepção no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

