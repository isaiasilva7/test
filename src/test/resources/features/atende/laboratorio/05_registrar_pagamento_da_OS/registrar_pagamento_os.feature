#language:pt
#encoding:utf-8

@laboratorio_funcionalidade05
Funcionalidade: Registra pagamento Da OS em Laboratorio Atende - Funcionalidade 05

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "", Código "Glico" , material "" no modulo Atende Laboratorio
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio


  @desktop  @smart @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Receber e validar o pagamento, no modulo Atende Laboratorio
    E selecionar a forma de pagamento "A Receber" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio

  @desktop  @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Epécie e validar o pagamento, no modulo Atende Laboratorio
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio

  @desktop  @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Boleto e validar o pagamento, no modulo Atende Laboratorio
    E selecionar a forma de pagamento "Boleto" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio

  @desktop @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Desconto e validar o pagamento, no modulo Atende Laboratorio
    E selecionar a forma de pagamento "Desconto" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop  @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Cheque pré Datado e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Cheque pré Datado" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor "15"  do forma de pagamento "Cheque pré Datado" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma", da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop @smart   @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Cheque  e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Cheque" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor "15"  do forma de pagamento "Cheque pré Datado" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma", da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop  @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Nota Promissoria  e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Nota Promissoria" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop  @smart  @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Imposto  e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Imposto" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop  @smart @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Cartão  e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio


  @desktop  @smart @regressivo
  Cenario:Registra pagamento Da OS com a forma de pagamento Externo  e validar o pagamento, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor de "15" do pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio
    Quando clicar no botão "Pagamento", na tela ordem de Servico no modulo Atende Laboratorio
    Entao validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio

  @desktop  @smart @regressivo
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão novo na tela Recebimento Direto, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor de "15" do pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão Novo na tela de Recebimento Direto
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão Excluir na tela Recebimento Direto, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor de "15" do pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão Excluir na tela de Recebimento Direto
    Entao o sistema não apresenta falha geral

  @desktop  @smart @regressivo
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão Fechar dna tela Recebimento Direto, no modulo Atende Laboratorio
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E informa Valor de "15" do pagamento "Externo" da tela de Recebimento Direto, no modulo Atende Laboratorio
    E clicar no botão Fechar na tela de Recebimento Direto
    Entao o sistema não apresenta falha geral



