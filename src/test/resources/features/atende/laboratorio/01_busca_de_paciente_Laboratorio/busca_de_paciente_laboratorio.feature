#language:pt
#encoding:utf-8

@laboratorio_funcionalidade01
Funcionalidade: busca de paciente atendimento Laboratorio - Funcionalidade 01

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por Nome e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "ROBO TESTE AUTOMATIZADO" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Mostrar resultado da Busca do paciente por Nome Social e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "TXZWQGHJSKL" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por Registro e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "108353" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por Nome da Mae e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "MAE DO ROBO" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por Prontuário e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "647463" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer uma busca com número do prontuario  que o não exista na base e confirma que o sistema não apresenta falha Geral
    E  informa o item de busca "45646347777777777647463" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por RG e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "3254390855" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por CPF e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "30081461550" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Mostrar resultado da Busca do paciente por Matrícula e confirma o nome do Paciente ROBO TESTE AUTOMATIZADO que é o esperado
    E  informa o item de busca "024662395007" e busco no modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: Mostrar resultado da Busca do paciente por Numero da ordem de Serviço
    E que seleciono busca paciente por Os no Atendimento busca modulo Atende
    E informor a numero de serie da OS "3" e confrimo a busca em informar o numero da OS modulo Atende
    Entao o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO na tela de Ordem de serviço  na busca no modulo Atende

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Gravar do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnGravar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Cancelar do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnCancelar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Busca do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnbusca"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Excluir do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnExcluir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Avancar do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnAvancar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Imprimir do sistema  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnImprimir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Buscar Paceinte por OS na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnBuscarPacientePorOS" na tela de busca
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Trat. Ambulatorial na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnTratAmbulatorial" na tela de busca
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Fila de Espera  na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnFilaDeEspera" na tela de busca
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Ler cartão na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnLerCartao" na tela de busca
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Biometria na tela de busca o sistema não apresenta falha Geral
    E clicar nos botoes "btnBiometria" na tela de busca
    Entao o sistema não apresenta falha geral