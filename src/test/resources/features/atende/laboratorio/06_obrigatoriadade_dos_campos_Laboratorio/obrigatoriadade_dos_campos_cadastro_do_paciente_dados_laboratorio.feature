#language:pt
#encoding:utf-8

@atendimento_funcionalidade_laboatorio0602
Funcionalidade:  Valida a obrigatoriedade das informaçoes dados do paciente Laboratorio- Funcionalidade 06-02

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Laboratório" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende Laboratorio
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão zoom do modulo Atende

  @desktop @regressivo @smart @newHoje
  Cenario: validar que ao clicar no botão Cadastro biometria na tela de cadastro do paciente o sistema não apresenta falha Geral no modulo Atende Laboratorio
    E clicar nos botões "Mais" na tela Dados Cadastrais do paciente no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral


  @desktop @regressivo @smart @newHoje
  Cenario: validar que ao clicar no botão prontuário na tela de cadastro do paciente o sistema não apresenta falha Geral no modulo Atende Laboratorio
    E clicar nos botões "Conv. Secuncadário" na tela Dados Cadastrais do paciente no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral


  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Conv. Secundário na tela de cadastro do paciente o sistema não apresenta falha Geral no modulo Atende Laboratorio
    E clicar nos botões "Deficiências" na tela Dados Cadastrais do paciente no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: validar que ao clicar no botão Acompanhante na tela de cadastro do paciente o sistema não apresenta falha Geral no modulo Atende Laboratorio
    E clicar nos botões "RN" na tela Dados Cadastrais do paciente no modulo Atende Laboratorio
    Entao o sistema não apresenta falha geral



