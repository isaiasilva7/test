#language:pt
#encoding:utf-8

@atendimento_funcionalidade04
Funcionalidade: Gerar Ordem de serviços atendimento - Funcionalidade 04

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Assistencial, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Assistencial, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Assistencial, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Assistencial, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Assistencial, Sexo F e com o convênio BRADESCO F adiconar o codigo do serviço via ctrl enter
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E seleciono o Serviço via tecla de atalho "CTRL+ENTER" do teclado
    Quando seleciono a busca por "1-Código"  informo "Glico", confirmo través enter do telcado e  clico em OK
    E informar dados do serviço "","", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende



  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Assistencial, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Assistencial, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Assistencial, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Cirugia, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Cirugia, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Cirugia" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor Padrão e o tipo de atendimento Cirugia, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Cirugia, Sexo F e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor AAA e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor "AAA"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Cirugia, Sexo M e com o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "10,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS  e validar valor do serviço de acordo com o setor ABC e o tipo de atendimento Cirugia, Sexo M e com o convênio BRADESCO F
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Emergência" e Setor "ABC"
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "25,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo  @quarentena @smart @sem_pre_Condicao @SMART-16622
  Cenario: Gerar uma Os através da Guia de atendimento, Fluxo  zoom botão Guias
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    E informar o número de Dias "2" na tela de Guias
    E informar a senha "2" na tela de Guias
    Quando informar o Setor Executante "APARTAMENTO" na tela de Guias
    E clicar no botão gravar do modulo Atende
    Quando Clicar no botão Gerar OS na tela de Guias
    E informar dados do serviço "GLICO","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende


  @desktop  @smart @regressivo @SMART-14371
  Cenario: validar que ao ao clicar no botão gravar quando não existem nenhum exame a ser gravado, na tela ordem de serviço do Atendimtno no modulo Atende, o sistema não apresenta falha geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    E informar o número de Dias "2" na tela de Guias
    E informar a senha "2" na tela de Guias
    Quando informar o Setor Executante "APARTAMENTO" na tela de Guias
    E clicar no botão gravar do modulo Atende
    Quando Clicar no botão Gerar OS na tela de Guias
    E informar dados do serviço "GLICO","1", "ADRIANO"
    E clicar no botão cancelar do modulo Atende
    Quando clicar no botão "OK" do PopUp
    Quando clicar no botão gravar do modulo Atende
    Entao o sistema não apresenta falha geral

  @desktop @regressivo  @quarentena @smart @sem_pre_Condicao @SMART-16622
  Cenario: Gerar uma Os através da Guia de atendimento,Fluxo cadastro botão Guias
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão "Guias" na tela de busca
    E clicar no botão gravar do modulo Atende
    Quando Clicar no botão Gerar OS na tela de Guias
    E informar dados do serviço "GLICO","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende


  @desktop  @smart @regressivo
  Cenario: validar que ao ao tenta imprimir Guia, na tela de laboratório no modulo Atende, o sistema não apresenta falha geral
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende Laboratorio
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    E informar o número de Dias "2" na tela de Guias
    E informar a senha "2" na tela de Guias
    Quando informar o Setor Executante "APARTAMENTO" na tela de Guias
    E clicar no botão gravar do modulo Atende
    Quando Clicar no botão Gerar OS na tela de Guias
    E informar dados do serviço "GLICO","1", "ADRIANO"
    E clicar no botão cancelar do modulo Atende
    Quando clicar no botão "OK" do PopUp
    E clicar no botão imprirmir do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    Entao o sistema não apresenta falha geral


