#language:pt
#encoding:utf-8

@atendimento_funcionalidade06-03
Funcionalidade:  Valida a obrigatoriedade das informaçoes - Funcionalidade 06-03

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 2 (ZZ1)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão "Sim" do PopUp
    E informar o Observação "teste Observacao" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    E infarma o motivo "OUTRO" , a observação "" e clicar no botão "OK" da tela de Motivo
    Quando clicar no botão gravar do modulo Atende
    Quando clicar nos botoes "Dados Complmentares" na tela cadastro do paciente

  @desktop@regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Valida a obrigatoriedade de dados no campo nome do Pai
    E informar nome do Pai "", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop@regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade de dados no campo Religião
    E informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Valida a obrigatoriedade de dados no campo Apelido
    E informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    E informar Religião "Catolicismo", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Valida a obrigatoriedade de dados no campo CEP
    E informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    Quando informar Religião "Catolicismo", na tela de dados Complementares
    E informar Apelido "Paulo", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade de dados no campo RG
    E informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    Quando informar Apelido "Paulo", na tela de dados Complementares
    E informar Religião "Catolicismo", na tela de dados Complementares
    E informar CEP "41770030", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Valida a obrigatoriedade de dados no campo Número endereço
    Quando informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    E informar Apelido "Paulo", na tela de dados Complementares
    Quando informar Religião "Catolicismo", na tela de dados Complementares
    E informar CEP "41770030", na tela de dados Complementares
    E informar RG "1221331", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida que ao inserir todos os dados de todos os campo obrigatoriedade o sistema não altera o comportamento
    Quando informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    E informar Religião "Catolicismo", na tela de dados Complementares
    Quando informar Apelido "Paulo", na tela de dados Complementares
    E informar CEP "41770030", na tela de dados Complementares
    E informar RG "1221312", na tela de dados Complementares
    E informar Número endereco "41770", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende
