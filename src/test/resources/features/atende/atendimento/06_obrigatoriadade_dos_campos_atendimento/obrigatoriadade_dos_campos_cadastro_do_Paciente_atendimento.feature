#language:pt
#encoding:utf-8

@atendimento_funcionalidade0602
Funcionalidade:  Valida a obrigatoriedade das informaçoes - Funcionalidade 06-02

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão zoom do modulo Atende

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Cadastro biometria na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Cadastro biometria" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Foto na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Foto" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Arq na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Arq" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão prontuário na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Prontuario" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Questionario na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Questionario" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Guias 1 na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Guias 1" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Conv. Secundário na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Conv. Secundário" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Acompanhante na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Acompanhante" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Dados Complmentares na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Dados Complmentares" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Tratamento Ambulatorial na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Tratamento Ambulatorial" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Guias na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Guias" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Fila de Espera na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Fila de Espera" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @Sem_pre_Condicao
  Cenario: validar que ao clicar no botão Adionionar na fila na tela de cadastro do paciente o sistema não apresenta falha Geral
    E clicar nos botoes "Adionionar na fila" na tela cadastro do paciente
    Entao o sistema não apresenta falha geral