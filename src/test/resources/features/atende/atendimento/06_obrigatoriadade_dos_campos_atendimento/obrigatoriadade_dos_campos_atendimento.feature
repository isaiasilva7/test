#language:pt
#encoding:utf-8

@atendimento_funcionalidade0601
Funcionalidade:  Valida a obrigatoriedade das informaçoes - Funcionalidade 06-1

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Telefone Invalido
    Quando informar dados do cadasto do paciente "M","10052000","xdfsfsf","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Telefone Invalido" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem CPF Invalido
    Quando informar dados do cadasto do paciente "M","10052000","xdfsfsf","PARTICULAR 1 (PAR)", "AA", "", "" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "CPF Invalido" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Você deve preencher a data de nascimento do paciente
    Quando informar dados do cadasto do paciente "M","","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Você deve preencher a data de nascimento do paciente" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Paciente não gravado. Informe o sexo do paciente
    Quando informar dados do cadasto do paciente "","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "paciente não gravado. Informe o sexo do paciente" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem O convênio do Paciente precisa ser informado
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "O convênio do Paciente precisa ser informado" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem não é valido pra o campo Convênio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","Teste Hoje ", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "não é valido pra o campo Convênio" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Digite M - Masculino ou Feminino
    Quando informar dados do cadasto do paciente "Z","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Digite M - Masculino ou Feminino" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Data Invalida
    Quando informar dados do cadasto do paciente "M","10050000","","", "", "", "" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Data Invalida" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Data Invalida 30 de fevereiro
    Quando informar dados do cadasto do paciente "M","30022000","","", "", "", "" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Data Invalida" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem não é valido pra o campo Convênio
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","Teste Hoje", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "não é valido pra o campo Convênio" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem Telefone Invalido
    Quando informar dados do cadasto do paciente "M","10052000","122","PARTICULAR 1 (PAR) ", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "Telefone Invalido" de obrigatoriedade de informação no campo no cadastro do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade da informaçao em algums campos do cadastro do paciente, mensagem CPF Invalido
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR) ", "12121", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Então validar a mensagem "CPF Invalido" de obrigatoriedade de informação no campo no cadastro do paciente