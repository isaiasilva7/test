#language:pt
#encoding:utf-8

@atendimento_funcionalidade06-04
Funcionalidade:  Valida a obrigatoriedade das informaçoes - Funcionalidade 06-04

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 2 (ZZ1)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Peso
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Altura
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Bairro
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo CEP
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    Quando informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    E informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    Quando informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Cidade
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Estado Civil
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Nome da Mae
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Grupo Sanguinio
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo número do endereço
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    E informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo email
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Medico
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    E informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo RG
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    E informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Valida a obrigatoriedade do campo Telefone Celular
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    E informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    E informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão "Sim" do PopUp
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok


  @desktop @regressivo  @PrioridadeAlta
  Cenario: Valida a obrigatoriedade do campo
    E clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão "Sim" do PopUp
    Quando clicar no botão gravar do modulo Atende
    Então o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok

