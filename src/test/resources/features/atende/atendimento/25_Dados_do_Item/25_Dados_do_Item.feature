#language:pt
#encoding:utf-8

@atendimento_funcionalidade25
Funcionalidade:  Dados do Item - Funcionalidade 25

  Como um usuário;
  Gostaria de adicionar um registro de ocorrÇencia
  Afim de validar

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    E clicar no botão imprirmir do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão iniciar da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão concluir da tela de questionario
    E clicar no botão "Sim" do PouP Concluir
    Quando fechar a tela via comando ESC do teclado
    E clicar no botão zoom do modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Alterar Data Do Resultados, na tela Dados do Item
    Quando clicar botão Alterar Data Do Resultados, na tela Dados do Item
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Diligenciamento, na tela Dados do Item
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Informações, na tela Dados do Item
    Quando clicar botão Informações, na tela Dados do Item
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão OK, na tela Dados do Item
    Quando clicar botão OK, na tela Dados do Item
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Fechar, na tela Dados do Item
    Quando clicar botão Fechar, na tela Dados do Item
    Entao o sistema não apresenta falha geral
