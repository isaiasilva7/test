#language:pt
#encoding:utf-8

@atendimento_funcionalidade21
Funcionalidade:  Guia de Atendimento - Funcionalidade 21

  Como um usuário;
  Gostaria de adicionar Guia de Atendimento para o paciente
  Afim de validar sistema não apresenta erro ao gravar OS e no arquivo.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS com o convênio PARTICULAR 1 (PAR) e com serviço, selecionar o atendimento, visualizar  impressão e validar que ao gravar o atendimento no formato txt os dados do arquivo estão correto
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende
    E clicar no botão retornar do modulo Atende
    Quando clicar no botão atendimento da tela Ordem de servico
    E clicar na seta da tela de atendimento
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão visualizar da tela no modulo Atende
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "Txt" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=15,00;valor=GLICEMIA" estão no arquivo como o formato ".TXT"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS com o convênio BRADESCO F (BF) e com serviço, selecionar o atendimento, visualizar  impressão e validar que ao gravar o atendimento no formato txt os dados do arquivo estão correto
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende
    E clicar no botão retornar do modulo Atende
    Quando clicar no botão atendimento da tela Ordem de servico
    E clicar na seta da tela de atendimento
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão visualizar da tela no modulo Atende
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "Txt" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=15,00;valor=GLICEMIA" estão no arquivo como o formato ".TXT"

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar OS com o convênio PARTICULAR 1 (PAR) com serviço e selecionar o atendimento validar que ao imprimir o atendimento os dados estão correto
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende
    E clicar no botão retornar do modulo Atende
    Quando clicar no botão atendimento da tela Ordem de servico
    E clicar na seta da tela de atendimento
    Quando clicar no botão imprirmir do modulo Atende
    Quando clicar no botão "Visualizar" na tela Atendimento Ambulatorial
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=15,00;valor=2º POSTO;valor=PARTICULAR 1" estão no arquivo como o formato ".PDF"

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Gerar OS com o convênio BRADESCO F (BF) com serviço e selecionar o atendimento validar que ao imprimir o atendimento os dados estão correto
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende
    E clicar no botão retornar do modulo Atende
    Quando clicar no botão atendimento da tela Ordem de servico
    E clicar na seta da tela de atendimento
    Quando clicar no botão imprirmir do modulo Atende
    Quando clicar no botão "Visualizar" na tela Atendimento Ambulatorial
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=24,00;valor=2º POSTO;valor=BRADESCO F" estão no arquivo como o formato ".PDF"