#language:pt
#encoding:utf-8

@atendimento_funcionalidade02
Funcionalidade: incluir paciente atraves do atendimento - Funcionalidade 02

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart
  Cenario:validar que ao clicar no botão Gravar no momento de inserir os dados do paciente no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnGravar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario:validar que ao clicar no botão Cancelar no momento de inserir os dados do paciente  no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnCancelar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar que ao clicar no botão Busca no momento de inserir os dados do paciente  no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnbusca"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar que ao clicar no botão Excluir no momento de inserir os dados do paciente  no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnExcluir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar que ao clicar no botão Avanca no momento de inserir os dados do paciente  no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnAvancar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar que ao clicar no botãoImprimir no momento de inserir os dados do paciente  no sistema, validadar que não apresenta falha Geral
    E clicar nos botoes "btnImprimir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @PrioridadeAlta
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 01
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 02
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 03
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 04
    Quando informar dados do cadasto do paciente "M","10052000","","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 05
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 07
    Quando informar dados do cadasto do paciente "F","10052000","","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 08
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 09
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 10
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 11
    Quando informar dados do cadasto do paciente "F","10052000","","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 12
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Incluir paciente através da funcionalidade de Atendimento e confirmar que Paciente foi gravado com sucesso 13
    Quando informar dados do cadasto do paciente "M","10052000","","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.









