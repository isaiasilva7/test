#language:pt
#encoding:utf-8

@atendimento_funcionalidade22
Funcionalidade:  Registro_de_Ocorrencia - Funcionalidade 22

  Como um usuário;
  Gostaria de adicionar um registro de ocorrÇencia
  Afim de validar

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop  @smart
  Cenario: Gerar OS com o convênio PARTICULAR 1 (PAR) e com serviço, selecionar o atendimento, visualizar  impressão e validar que ao gravar o atendimento no formato txt os dados do arquivo estão correto
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão registro de Ocorrencia na tela de Ordem de Serviço
    Quando informar a origem "", a unidade "", o setor "ABC", a categoria "", o tipo "AMOSTRA: COAGULADA" e a descrição "Teste OK", na tela Registro de Ocorrencia
    E clicar no botão Ok, na tela Registro de Ocorrencia
    Quando clicar no botão Ok, na tela Enviar mensagem para
    E clicar no botão "OK" do PopUp
    Quando clicar no botão retornar do modulo Atende
    E clicar no botão Ocorrencia, na tela de Ordem de Serviço


  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no Anônimo, na tela de Registro de Ocorrência
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão registro de Ocorrencia na tela de Ordem de Serviço
    Quando informar a origem "", a unidade "", o setor "ABC", a categoria "", o tipo "AMOSTRA: COAGULADA" e a descrição "Teste OK", na tela Registro de Ocorrencia
    Quando clicar no botão Anônimo, na tela Registro de Ocorrencia
    Entao o sistema não apresenta falha geral