#language:pt
#encoding:utf-8

@atendimento_funcionalidade12
Funcionalidade: Recibo de pagamento - Funcionalidade 12

  Contexto:Registra pagamento Da OS
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico

  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de pagamento foi a Receber, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    E selecionar a forma de pagamento "A Receber" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"

  @desktop  @regressivo  @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario:Registra o pagamento da OS que a forma de pagamento foi  Epécie, confirmar o pagamento e ve validar que os dados nos relatórios estão corretos.
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"


  @desktop  @regressivo  @smart @sem_pre_Condicao
  Cenario:Registra o  pagamento da OS que a forma de pagamento foi  Boleto, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    E selecionar a forma de pagamento "Boleto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"


  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de pagamento foi  Desconto, confirmar o pagamento  e validar que os dados nos relatórios estão corretos.
    E selecionar a forma de pagamento "Desconto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"


  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de pagamento foi  Cheque, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    Quando selecionar a forma de pagamento "Cheque" da tela de Recebimento Direto
    E informa Valor "15"  do forma de pagamento "Cheque" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"


  @desktop   @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de pagamento foi Nota Promissoria, confirmar o pagamento e e validar que os dados nos relatórios estão corretos.
    Quando selecionar a forma de pagamento "Nota Promissoria" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Nota Promissoria"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"


  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de pagamento foi Imposto, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    Quando selecionar a forma de pagamento "Imposto" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Imposto"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"

  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de Cartão, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"

  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario:Registra o pagamento da OS que a forma de Externo, confirmar o pagamento e validar que os dados nos relatórios estão corretos.
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Quando clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão Fechar da tela de visualização de impressão
    Então confirmar  dados valor "15,00", nome do paciente,número da OS no arquivo com o formato ".RTF"

  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Imprimir da tela de Visualização de impressão
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    Quando clicar no botão imprimir da tela de visualização de impressão
    Entao o sistema não apresenta falha geral

  @desktop  @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Fechar da tela de Visualização de impressão
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão Documento na tela recebimento Direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    Quando clicar no botão Fechar da tela de visualização de impressão
    Entao o sistema não apresenta falha geral






