#language:pt
#encoding:utf-8

@atendimento_funcionalidade14
Funcionalidade: Relario de internação - Funcionalidade 14


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar relario de internação convênio para  PARTICULAR 1 (PAR)
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão Cancelar da tela de questionario
    E clicar no botão Relatorio internacao da tela impimir ordem de serviço
    Quando selecionar o relatorio Alta a pedido da tela de relatorio de Internacao
    E clicar no botão visualizar da tela de relatorio de Internacao
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmar que o nome do paciente, a data esta no arquivo com o formato ".RTF"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar relario de internação para convênio BRADESCO F
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão Cancelar da tela de questionario
    E clicar no botão Relatorio internacao da tela impimir ordem de serviço
    Quando selecionar o relatorio Alta a pedido da tela de relatorio de Internacao
    E clicar no botão visualizar da tela de relatorio de Internacao
    E clicar no botão Salvar Como na tela de Visualização de impressão
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmar que o nome do paciente, a data esta no arquivo com o formato ".RTF"