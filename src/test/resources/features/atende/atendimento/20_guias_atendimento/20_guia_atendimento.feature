#language:pt
#encoding:utf-8

@atendimento_funcionalidade20
Funcionalidade:  Guia de Atendimento - Funcionalidade 20

  Como um usuário;
  Gostaria de adicionar Guia de Atendimento para o paciente
  Afim de validar sistema não apresenta erro ao gravar OS.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende


  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Gerar OS da tela de Guias, fluxo zoom, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    E clicar no botão gravar do modulo Atende
    Quando Clicar no botão Gerar OS na tela de Guias
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Gerar OS da tela de Guias, fluxo Cadastro, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão "Guias" na tela de busca
    Quando clicar no botão gravar do modulo Atende
    E Clicar no botão Gerar OS na tela de Guias
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Exibir Apenas Não Vencidos da tela de Guias, fluxo Cadastro, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão "Guias" na tela de busca
    Quando clicar no botão gravar do modulo Atende
    E Clicar no botão Exibir Apenas Não Vencidos na tela de Guias
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Exibir Apenas Não Vencidos da tela de Guias, fluxo zoom, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    Quando clicar no botão gravar do modulo Atende
    E Clicar no botão Exibir Apenas Não Vencidos na tela de Guias
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Proced da tela de Guias,  fluxo Cadastro, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    Quando clicar no botão gravar do modulo Atende
    E Clicar no botão Proced na tela de Guias
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Proced da tela de Guias, fluxo zoom, clicar em Guias
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Guias 1" na tela cadastro do paciente
    Quando clicar no botão gravar do modulo Atende
    E Clicar no botão Proced na tela de Guias
    Entao o sistema não apresenta falha geral
