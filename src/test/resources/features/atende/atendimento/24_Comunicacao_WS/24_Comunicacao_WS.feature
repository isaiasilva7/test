#language:pt
#encoding:utf-8

@atendimento_funcionalidade24
Funcionalidade: Realizar Comunicacao Ws - Funcionalidade 24

  Como um usuário;
  Gostaria de realizar Comunicacao Ws
  Afim de validar o arquivo xml

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop @smart  @regressivo
  Cenario: Validar arquivo de Comunicação WS,  xml de enviou, com  o Laboratório Alvaro  com o convênio  PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    E clicar no botão imprirmir do modulo Atende
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Requisição Alvoro"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmo que no arquivo xml na tag "paciente", no  parâmetro "sexo" contem o valor "M"
    E confirmo que no arquivo xml na tag "paciente", no  parâmetro "datanasc" contem o valor "2000-05-10"
    E confirmo que no arquivo xml na tag "solicitacoes", no  parâmetro "operador" contem o valor "medicware"

  @desktop  @smart  @regressivo
  Cenario: Validar arquivo de Comunicação WS, xml de  rotorno, via banco de dados com o Laboratório Alvaro  com  o convênio PARTICULAR 1
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    E clicar no botão imprirmir do modulo Atende
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Requisição Alvoro"
    Quando clicar no link Xml de Retorno, na tela xml de Envio e Retorno
    Então confirmar que o arquivo XML de retorno contem "descricaoAcondicionamento='R' descricaoMaterial='SANGUE TOT'"
    E confirmar que o arquivo XML de retorno contem "exame='CARIB' incluido='true' informacao='Exame(s): CARIB - Incluído(s) com sucesso!'"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo de Comunicação WS,  xml de enviou com o Laboratório Alvaro  com o convênio SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Requisição Alvoro"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então confirmo que no arquivo xml na tag "paciente", no  parâmetro "sexo" contem o valor "M"
    E confirmo que no arquivo xml na tag "paciente", no  parâmetro "datanasc" contem o valor "2000-05-10"
    E confirmo que no arquivo xml na tag "solicitacoes", no  parâmetro "operador" contem o valor "medicware"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo de Comunicação WS,  xml de  rotorno, via banco de dados com o Laboratório Alvaro com o convênio  SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Requisição Alvoro"
    Quando clicar no link Xml de Retorno, na tela xml de Envio e Retorno
    Então confirmar que o arquivo XML de retorno contem "descricaoAcondicionamento='R' descricaoMaterial='SANGUE TOT'"
    E confirmar que o arquivo XML de retorno contem "exame='CARIB' incluido='true' informacao='Exame(s): CARIB - Incluído(s) com sucesso!"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo de Comunicação WS,  xml de enviou com o Laboratório DB DIAGNOSTICOS com o convênio SUL AMERICA IND (SIN)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IND (SIN)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "ANCA","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "OK" do PopUp
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "ReceberAtendimento"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=<diag:CodigoExameDB>ANCA</diag:CodigoExameDB>" estão no arquivo como o formato ".XML"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo de Comunicação WS,  xml de Retorno com o Laboratório DB DIAGNOSTICOS com o convênio SUL AMERICA IND (SIN)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IND (SIN)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "ANCA","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "OK" do PopUp
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "EnviarAmostras"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "<diag:CodigoSenhaIntegracao>jevafi59</diag:CodigoSenhaIntegracao>" estão no arquivo como o formato ".XML"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo de Comunicação WS,  xml de enviou com o Laboratório Hermes pardini com o convênio SUL AMERICA IND (SIN)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IND (SIN)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "F1195","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "OK" do PopUp
    Quando fechar a tela via comando ESC do teclado
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "getPeDido"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;<CodExmLab>F1195</CodExmLab>" estão no arquivo como o formato ".XML"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo Retorno de Comunicação WS, xml de Retorno com o Laboratório Hermes pardini com o convênio SUL AMERICA IND (SIN)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IND (SIN)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "F1195","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    Quando clicar no botão "Cancelar" na tela imprirmir ordem de Serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "getPeDido"
    Quando clicar no link Xml de Retorno, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DN 10/05/2000;'SORO'" estão no arquivo como o formato ".XML"

