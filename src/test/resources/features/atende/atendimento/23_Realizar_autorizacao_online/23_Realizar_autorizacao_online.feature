#language:pt
#encoding:utf-8

@atendimento_funcionalidade23
Funcionalidade: Realizar autorizacao online - Funcionalidade 23

  Como um usuário;
  Gostaria de Realizar autorizacao online
  Afim de validar o arquivo XML

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop @smart  @regressivo  @SMART-15040
  Cenario:Validar arquivo xml de enviou Soliciação Procedimemto Laboratório Alvaro,autorizacao online com convênio SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Entao  dá um click duplo foram do item e valdiar se o sistema não apresenta falha geral
    E o sistema não apresenta falha geral



  @desktop @smart  @regressivo
  Cenario:Validar arquivo xml de enviou Soliciação Procedimemto Laboratório Alvaro,autorizacao online com convênio SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Solicitação_Procedimemto"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=<sch:tipoTransacao>SOLICITACAO_PROCEDIMENTOS</sch:tipoTransacao>" estão no arquivo como o formato ".XML"
    E Confirmo que os dados "valor=<sch:Padrao>3.02.02</sch:Padrao>;valor=sch:descricaoProcedimento>CARIOTIPO BANDA G CBG</sch:descricaoProcedimento>" estão no arquivo como o formato ".XML"

  @desktop @smart  @regressivo
  Cenario:Validar arquivo xml de enviou Soliciação Procedimemto Laboratório Alvaro, autorizacao online com convênio SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Solicitação_Procedimemto"
    Quando clicar no link Xml de Retorno, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "O nome remoto não pôde ser resolvido: 'guiana.trixti.com.br'" estão no arquivo como o formato ".XML"


  @desktop @smart  @regressivo
  Cenario:Validar arquivo xml de enviou Soliciação Procedimemto Laboratório Alvaro,autorizacao online com 2 Serviço, com convênio SUL AMERICA IVIS (SIV)
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "LCARI","1", ""
    Quando clicar no botão gravar do modulo Atende
    E  informar dados do serviço da Segunda linha tipo "", Código "28010175", Setor "", na tela Ordem de serviços
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PopUp
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    E clicar no botão zoom do modulo Atende
    Quando clicar botão Diligenciamento, na tela Dados do Item
    Quando clicar na Aba Comunicação WS da OS, na tela Diligenciamento
    Quando clicar no quando a link Xml de Envio em que a operação é "Solicitação_Procedimemto"
    Quando clicar no link Xml de Envio, na tela xml de Envio e Retorno
    Quando clicar no botão Salvar como, na tela xml de Envio e Retorno
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=<sch:tipoTransacao>SOLICITACAO_PROCEDIMENTOS</sch:tipoTransacao>" estão no arquivo como o formato ".XML"
    E Confirmo que os dados "valor=<sch:Padrao>3.02.02</sch:Padrao>;valor=sch:descricaoProcedimento>CARIOTIPO BANDA G CBG</sch:descricaoProcedimento>" estão no arquivo como o formato ".XML"
    E Confirmo que os dados "<sch:descricaoProcedimento>Acido urico</sch:descricaoProcedimento>" estão no arquivo como o formato ".XML"



