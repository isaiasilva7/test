#language:pt
#encoding:utf-8

@atendimento_funcionalidade0702
Funcionalidade: Chamar proximo paciente na fila de Espera  - Funcionalidade 07-02

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    Quando clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    E clicar no botão de encerrar atendimento
    E que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios PARTICULAR 1 (PAR) que tenha serviço e confirmado que o mesmo foi adicionado, e chama o proximo da fila
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Quando fechar modulo Atende
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Quando Clicar no botão chamar o proximo fila de Espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios BRADESCO F (BF) que tenha serviço e confirmado que o mesmo foi adicionado, e chama o proximo da fila
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Quando fechar modulo Atende
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Quando Clicar no botão chamar o proximo fila de Espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios PARTICULAR 1 (PAR) e confirmado que o mesmo foi adicionado, e chama o proximo da fila
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Quando fechar modulo Atende
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Quando Clicar no botão chamar o proximo fila de Espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios BRADESCO F (BF) e confirmado que o mesmo foi adicionado, e chama o proximo da fila
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Quando fechar modulo Atende
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Quando Clicar no botão chamar o proximo fila de Espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento
