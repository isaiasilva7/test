#language:pt
#encoding:utf-8

@atendimento_funcionalidade0701 @PrioridadeAlta
Funcionalidade:  Adicionar e consultar paciente na fila de espera - Funcionalidade 07-1

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    Quando clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    E clicar no botão de encerrar atendimento
    Quando informa o nome do Médico "ABILIO" para consulta a Fila na tela de fila de Espera
    E clicar no botão de encerrar atendimento
    Quando que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios BRADESCO F (BF) e confirmado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento
    E clicar no botão detalhes na tela fila de espera atendimento
    E clicar no botão Retorna pra Fila na tela Detalhes
    Quando informar o nome do paciente "" na tela fila de espera atendimento
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Adicionar paciente na fila de espera com o convênios PARTICULAR 1 e confirmado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento
    E clicar no botão detalhes na tela fila de espera atendimento
    E clicar no botão Retorna pra Fila na tela Detalhes
    Quando informar o nome do paciente "" na tela fila de espera atendimento
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Adicionar paciente na fila de espera com o convênios PARTICULAR 1 (PAR) que tenha serviço e confirmado que o mesmo foi adicionado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento
    E clicar no botão detalhes na tela fila de espera atendimento
    E clicar no botão Retorna pra Fila na tela Detalhes
    Quando informar o nome do paciente "" na tela fila de espera atendimento
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Adicionar paciente na fila de espera com o convênios BRADESCO F (BF) que tenha serviço e confirmado que o mesmo foi adicionado, encerrar atendimento e confirmar o encerramento do atendimento, retorna para a fila e confirma que o nome do paciente retornou para a fila.
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento
    Quando informar o nome do paciente "pacienteAtual" na tela fila de espera atendimento
    E clicar no botão detalhes na tela fila de espera atendimento
    E clicar no botão Retorna pra Fila na tela Detalhes
    Quando informar o nome do paciente "" na tela fila de espera atendimento
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Adicionar paciente na fila de espera com o convênios PARTICULAR 1 (PAR) que tenha serviço e confirmado que o mesmo foi adicionado, transferir de Fila e confirmar que o mesmo foi transferido por fim remover da fila
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    Quando clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende
    E informar a fila "ABILIO" na tela de trasfêrencia de paciente para a fila
    Quando clicar no botão Transferir da tela trasferir paciente para a fila
    E seleciono medico "IVISSON LOPES" na fila da tela fila de espera
    E seleciono medico "ABILIO" na fila da tela fila de espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Adicionar paciente na fila de espera com o convênios BRADESCO F (BF) que tenha serviço e confirmado que o mesmo foi adicionado, transferir de Fila e confirmar que o mesmo foi transferido por fim remover da fila
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Quando clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende
    E informo a Senha "senhaAleatoria" da tela Adicionar paceinte a Fila de Espera
    Quando clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Então confiramr que o panciente foi inserido na fila de espera
    Quando clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende
    E informar a fila "ABILIO" na tela de trasfêrencia de paciente para a fila
    Quando clicar no botão Transferir da tela trasferir paciente para a fila
    E seleciono medico "IVISSON LOPES" na fila da tela fila de espera
    E seleciono medico "ABILIO" na fila da tela fila de espera
    Então confiramr que o panciente foi inserido na fila de espera
    E clicar no botão de encerrar atendimento


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar que o Botão fila de espera não desaparece ao adiconar uma arquivo de outro diretorio
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão retornar do modulo Atende
    Quando  clicar botão Documento da tela oremdem de Serviço do modulo atende
    E clicar botão anexar o documento da tela oremdem de Serviço do modulo atende
    Quando informar o nome do arquivo "F:\Arquivos_Desenv\Gilson\teste.pdf" e clicar em "Abrir" na tela de Selecione o nome do arquivo
    E clicar no botão gravar do modulo Atende
    Quando clicar no botão fechar do modulo Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E clicar nos botoes "btnFilaDeEspera" na tela de busca
    Entao o sistema não apresenta falha geral
    Quando fechar a tela via comando ESC do teclado
    E  informa o item de busca "PacienteNaMemoria" e busco no modulo Atende
    E clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Chamar Selecionado da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnChamarSelecionado" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Proximo da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnChamarProximo" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Encerrar Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnEncerrarAtendimento" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao @
  Cenario:Confirmar que o botão Detalhes Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnDetalhes" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Imprimir Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnImprimir" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Atualizar Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnAtualizar" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Foto Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnFoto" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão CR Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnCR" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Historico Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnHistorico" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Marcação Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnMarcacao" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Confirmar que o botão Rastraeabilidade Atendimento da tela fila de espera esta presente
    E clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende
    E clinar nos botoes "btnRastraeabilidade" da tela de Fila de espera recepção
    Entao o sistema não apresenta falha geral


