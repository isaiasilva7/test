#language:pt
#encoding:utf-8

@atendimento_funcionalidade19
Funcionalidade:  Acompanhante - Funcionalidade 19

  Como um usuário;
  Gostaria de adicionar Acompanhante para o paciente
  Afim de validar sistema não apresenta erro ao gravar OS.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir dados do acompanhate com o convenio com convenio PARTICULAR 1 (PAR) verificar que o sistema que o sistema não apresenta erro ao gera uma OS.
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Acompanhante" na tela cadastro do paciente
    E informar o parentesco "FILHO(A)" na aba acompanhate
    Quando informar o nome do Acompanhate "Judite" na aba acompanhate
    E informar a ultima data de Comparecimento "19082019" na aba acompanhate
    Quando informar a Observação "teste hoje" na aba acompanhate
    E clicar no botão gravar do modulo Atende
    E clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir dados do acompanhate com o convenio com convenio BRADESCO F (BF), verificar que o sistema que o sistema não apresenta erro ao gera uma OS.
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Acompanhante" na tela cadastro do paciente
    E informar o parentesco "FILHO(A)" na aba acompanhate
    Quando informar o nome do Acompanhate "Judite" na aba acompanhate
    E informar a ultima data de Comparecimento "19082019" na aba acompanhate
    Quando informar a Observação "teste hoje" na aba acompanhate
    E clicar no botão gravar do modulo Atende
    E clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende
