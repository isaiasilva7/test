#language:pt
#encoding:utf-8

@atendimento_funcionalidade13
Funcionalidade: Visualizar Pagamento na OS - Funcionalidade 13

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico


  @desktop  @regressivo
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi a Receber, confirmar o pagamento e confirmar, validar trecho do recibo
    E selecionar a forma de pagamento "A Receber" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo  @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi  Epécie, confirmar o pagamento Visualizar Pagamento na OS
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo  @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi  Boleto, confirmar o pagamento e Visualizar Pagamento na OS
    E selecionar a forma de pagamento "Boleto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi  Desconto, confirmar o pagamento e Visualizar Pagamento na OS
    E selecionar a forma de pagamento "Desconto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi  Cheque, confirmar o pagamento e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Cheque" da tela de Recebimento Direto
    E informa Valor "15"  do forma de pagamento "Cheque" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop   @regressivo @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi Nota Promissoria, confirmar o pagamento e  e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Nota Promissoria" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Nota Promissoria"
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo @smart
  Cenario: Visualizar Pagamento na OS que a forma de pagamento foi Imposto, confirmar o pagamento e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Imposto" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Imposto"
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

  @desktop  @regressivo @smart
  Cenario: Visualizar Pagamento na OS que a forma de Cartão, confirmar o pagamento e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022"
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"


  @desktop  @regressivo @smart  @SMART-16678
  Cenario: Visualizar Pagamento na OS que a forma de Cartão com três parcela, confirmar o pagamento e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022", e Rede NSU igual a "00111"
    Quando clicar no botão "Parcelar" na tela de Recebimento direto
    E informar a quantidade de parcela igual a "3" clicar no botão "OK" da tela de quantidade
    Então confirmo que o número NSU rede esta em todas as parcelas
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;Valor=15,00;valor=5,00 " estão no arquivo como o formato ".PDF"


  @desktop  @regressivo @smart  @PrioridadeAlta
  Cenario: Visualizar Pagamento na OS que a forma de Externo, confirmar o pagamento e Visualizar Pagamento na OS
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando Selecionar o documento "MEDICWARE" na tela recebimento Direto
    E clicar no botão Visualizar na tela de Recibo
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;valor=Recibo" estão no arquivo como o formato ".PDF"

