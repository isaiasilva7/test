#language:pt
#encoding:utf-8

@funcionalidade11
Funcionalidade: Cancelar pagamento da OS - Funcionalidade 11

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi a Receber e validar que o cancelamento foi confirmado
    E selecionar a forma de pagamento "A Receber" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico

  @desktop @regressivo @smart @sem_pre_Condicao  @PrioridadeAlta
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi  Epécie e validar que o cancelamento foi confirmado
    E selecionar a forma de pagamento "Epécie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi  Boleto e validar que o cancelamento foi confirmado
    E selecionar a forma de pagamento "Boleto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi  Desconto e validar que o cancelamento foi confirmado
    E selecionar a forma de pagamento "Desconto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi  Cheque pré Datado e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Cheque pré Datado" da tela de Recebimento Direto
    E informa Valor "15"  do forma de pagamento "Cheque pré Datado" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi  Cheque e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Cheque" da tela de Recebimento Direto
    E informa Valor "15"  do forma de pagamento "Cheque" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico



  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi Nota Promissoria e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Nota Promissoria" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Nota Promissoria"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de pagamento foi Imposto e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Imposto" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Imposto"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario:Registra cancelamento do pagamento da OS que a forma de Cartão foi Imposto e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Registra cancelamento do pagamento da OS que a forma de Externo foi Imposto e validar que o cancelamento foi confirmado
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "15" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Entao validar que o status da fatura esta como Faturado na tela ordem de servico
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão cancelar pagamento na tela  de Recebimento Direto
    Quando clicar no botão "Sim" do PopUp
    E informo o motivo "PROBLEMA IDENTIFICADO" de canelamento , observação "" e confirmo clicando no botão "OK"
    Entao validar que o status da fatura esta como aberto na tela ordem de servico






