#language:pt
#encoding:utf-8

@atendimento_funcionalidade16
Funcionalidade:  Valida a obrigatoriedade das informaçoes - Funcionalidade 16

  Como um usuário;
  Gostaria de gerar OS (ordem de serviço) que o paciente tenha dados complementares inserido;
  Afim de validar sistema não apresenta erro ao gravar OS.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende


  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir dados complementares do Paciente, Com convenio PARTICULAR 1 (PAR) e adicionar um serviço validar que o sistema não apresenta erro
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão "Sim" do PopUp
    E informar o Observação "teste Observacao" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    E infarma o motivo "OUTRO" , a observação "" e clicar no botão "OK" da tela de Motivo
    Quando clicar no botão gravar do modulo Atende
    Quando clicar nos botoes "Dados Complmentares" na tela cadastro do paciente
    Quando informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    E informar Religião "Catolicismo", na tela de dados Complementares
    Quando informar Apelido "Paulo", na tela de dados Complementares
    E informar CEP "41770030", na tela de dados Complementares
    E informar RG "1221312", na tela de dados Complementares
    E informar Número endereco "41770", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende


  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir dados complementares do Paciente, Com convenio BRADESCO F (BF) e adicionar um serviço validar que o sistema não apresenta erro
    Quando informar dados do cadasto do paciente "F","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão "Sim" do PopUp
    E informar o Observação "teste Observacao" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    E infarma o motivo "OUTRO" , a observação "" e clicar no botão "OK" da tela de Motivo
    Quando clicar no botão gravar do modulo Atende
    Quando clicar nos botoes "Dados Complmentares" na tela cadastro do paciente
    Quando informar nome do Pai "nomePaiAleatorio", na tela de dados Complementares
    E informar Religião "Catolicismo", na tela de dados Complementares
    Quando informar Apelido "Paulo", na tela de dados Complementares
    E informar CEP "41770030", na tela de dados Complementares
    E informar RG "1221312", na tela de dados Complementares
    E informar Número endereco "41770", na tela de dados Complementares
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão avancar do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende



