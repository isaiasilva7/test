#language:pt
#encoding:utf-8

@atendimento_funcionalidade10
Funcionalidade: Registra Deposito  - Funcionalidade 10


  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão deposito tela de ordem de serviço

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito em espécie, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Especie" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito em Boleto, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Boleto" da tela de Recebimento Direto
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Fazer um deposito em Cartao, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Cartão" da tela de Recebimento Direto
    E informa bandeira do cartão "CAPPTA MASTER C", conta Chegue "", número  do cartão "5555666677778884", data de validade "122022"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito Externo, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Externo" da tela de Recebimento Direto
    E informa Valor de "500" do pagamento "Externo"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito via Desconto, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Desconto" da tela de Recebimento Direto
    E informa Valor de "500" do pagamento "Desconto"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito  atraves de Imposto, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Imposto" da tela de Recebimento Direto
    E informa Valor de "500" do pagamento "Imposto"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito com Nota Promissoria, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Nota Promissoria" da tela de Recebimento Direto
    E informa Valor de "500" do pagamento "Nota Promissoria"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Fazer um deposito em Cheque, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Cheque" da tela de Recebimento Direto
    E informa Valor "500"  do forma de pagamento "Cheque" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao  @PrioridadeAlta
  Cenario: Fazer um deposito em Cheque pré Datado, confirma e devolver
    E clicar no botão Novo na tela de Deposito
    Quando informo tipo de Deposito "ADIANTAMENTO", valor total "500"
    Quando selecionar a forma de pagamento "Cheque pré Datado" da tela de Recebimento Direto
    E informa Valor "500"  do forma de pagamento "Cheque pré Datado" , data de vencimento do cheque "1082019", agencia  "123",conta "1324", numero do chegue "131321", praça "12324", titularidade "mesma"
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é "500"
    Quando clicar no botão Devolver na tela de Deposito
    E clicar no botão "Sim" do PopUp
    Quando clicar no botão OK da tela Devolução
    E informo o responsavel "Teste hoje" e confirmo clicando no botão ok
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão deposito tela de ordem de serviço
    Então confirmar que o valor do Deposito é ""

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Devolver
    Quando clicar no botão Devolver na tela de Deposito
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Visualizar
    Quando clicar no botão Visualizar na tela de Deposito
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Cancelar
    Quando clicar no botão Cancelar na tela de Deposito
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresetna falha geral ao clicar no botão Fechar
    Quando clicar no botão Fechar na tela de Deposito
    Entao o sistema não apresenta falha geral


