#language:pt
#encoding:utf-8

 @PrioridadeAlta @atendimento_funcionalidade09
Funcionalidade: Impressão guia TISS  - Funcionalidade 09

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar uma OS com tratamento ambulatorial, com o convênio SULAMERICA - OCUPACI, validar que a Guia Tis foi gerada
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SULAMERICA - OCUPACI (SIO)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão iniciar da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão concluir da tela de questionario
    E clicar no botão "Sim" do PouP Concluir
    Quando clicar no botão "Guia Tiss" na tela imprirmir ordem de Serviço
    E selecionar a teclas "Shit+Enter" do teclado
    Então validar a existencia do Guia de Serviço
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;Numero do prestador" estão no arquivo como o formato ".PDF"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar uma OS com tratamento ambulatorial, com o convênio BRADESCO F (BF), validar que a Guia Tis foi gerada
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o numero da OS
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão iniciar da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão concluir da tela de questionario
    E clicar no botão "Sim" do PouP Concluir
    Quando clicar no botão "Guia Tiss" na tela imprirmir ordem de Serviço
    E selecionar a teclas "Shit+Enter" do teclado
    Então validar a existencia do Guia de Serviço
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;Numero do prestador" estão no arquivo como o formato ".PDF"


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Gerar uma OS com tratamento ambulatorial, com o convênio SUL AMERICA IVIS (SIV), validar que a Guia Tis foi gerada
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SUL AMERICA IVIS (SIV)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço da Primeira linha tipo "S", Código "33010154", Setor "338", na tela Ordem de serviços
    Quando clicar no botão gravar do modulo Atende
    E clicar no botão "OK" do PouP Concluir
    Quando clicar no botão Novo, no modulo Atende
    E informar dados do serviço da Segunda linha tipo "S", Código "17055040", Setor "338", na tela Ordem de serviços
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão "Sim" do PouP Concluir
    Quando clicar no botão "Guia Tiss" na tela imprirmir ordem de Serviço
    E selecionar a teclas "Shit+Enter" do teclado
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=14 - Nome do Contratado HOSPITAL MODELO MEDICWARE;valor=26 - Descrição US ABD SUPERIOR POTASSIO 1001" estão no arquivo como o formato ".PDF"
    E Confirmo que os dados "valor=22 22 3301004066 97990019 US ABD SUPERIOR POTASSIO 1001 1 1 1" estão no arquivo como o formato ".PDF"
    Então Confirmo que os dados "valor=UF 663.023.620-32 663.023.620-32 ADRIANA RAMOS 6 6 376 376 BA ADRIANA RAMOS BA" estão no arquivo como o formato ".PDF"



