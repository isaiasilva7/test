#language:pt
#encoding:utf-8

@atendimento_funcionalidade08
Funcionalidade: Impressao de Etiqueta - Funcionalidade 08

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "nome_da_mae_Gerado_Aleatorio", "nome_social_Gerado_Aleatorio" no modulo Atende
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão pagamento da tela ordem de servico
    E clicar no botão OK da tela recebimento direto
    Quando fechar a tela via comando ESC do teclado
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão Cancelar da tela de questionario


  @desktop @regressivo @smart @sem_pre_Condicao  @PrioridadeAlta
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 1, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "1" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "1" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 2, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "2" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "2" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 3, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "3" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "3" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 4, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "4" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "4" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 5, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "5" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "5" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario:validar na impressão de Etiquetas do paciente, a quantidade de etiqueta igual a 6, o nome do paciente e o convênio
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    E informar a quantidade igual a "6" de etiqueta na tela quantidade de etiquetas
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao validar a quantidade de etiqueta gerada é "6" nome do Convênio "PARTICULAR 1" na etiqueta do paciente

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão OK da tela de imprimir ordem de Serviço
    Quando Clicar no botão Ok da tela imprimir ordem de serviço
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão Cancelar da tela de imprimir ordem de Serviço
    Quando Clicar no botão Cancelar da tela imprimir ordem de serviço
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: Validar que o sistema não apresenta falha geral ao clicar no botão Impressora da tela de imprimir ordem de Serviço
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    Quando Clicar no botão imprimir na tela quantidade de etiquetas
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Validar que o sistema não apresenta falha geral ao clicar no botão Impressora  da tela Quantidade de Etiqueta
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    Quando Clicar no botão Ok da tela Quantidade de Etiqueta
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Validar que o sistema não apresenta falha geral ao clicar no botão Cancelar  da tela Quantidade de Etiqueta
    Quando clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico
    Quando Clicar no botão Cancelar da tela Quantidade de Etiqueta
    Entao o sistema não apresenta falha geral
