#language:pt
#encoding:utf-8

@atendimento_funcionalidade17
Funcionalidade:  Validar Geracao de Questionario - Funcionalidade 17

  Como um usuário;
  Gostaria de gerar OS (ordem de serviço) que o paciente tenha dados complementares inserido;
  Afim de validar sistema não apresenta erro ao gravar OS.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo  @quarentena @smart @sem_pre_Condicao @SMART-16598
  Cenario: Validar que os dados do questionario do paciente estão correto e com o convenio PARTICULAR 1 (PAR), no arquivo com formato TXT
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "TXT" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=1,000" estão no arquivo como o formato ".TXT"

  @desktop @regressivo @quarentena @smart @sem_pre_Condicao @SMART-16598
  Cenario: Validar que os dados do questionario do paciente estão correto e com o convenio PARTICULAR 1 (PAR), no arquivo com formato CSV
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "csv" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=1,000" estão no arquivo como o formato ".csv"

  @desktop @regressivo @quarentena @smart @sem_pre_Condicao @SMART-16598
  Cenario: Validar que os dados do questionario do paciente estão correto e com o convenio PARTICULAR 1 (PAR), no arquivo com formato HTMl
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "HTM" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=1,000" estão no arquivo como o formato ".HTM"

  @desktop @regressivo @quarentena  @smart @sem_pre_Condicao @SMART-16598
  Cenario: Validar que os dados do questionario do paciente estão correto e com o convenio PARTICULAR 1 (PAR), no arquivo com formato PDF
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "PDF" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=1,000" estão no arquivo como o formato ".PDF"

  @desktop @regressivo @quarentena @smart @sem_pre_Condicao @SMART-16598
  Cenario:Validar que os dados do questionario do paciente estão correto e como convenio BRADESCO F (BF) no arquivo com formato TXT
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    E informar o Peso "80" na tela de cadastro do Prontuario modulo atende
    Quando informar o Alto "80,00" na tela de cadastro do Prontuario modulo atende
    E informar o Bairro "STIEP" na tela de cadastro do Prontuario modulo atende
    Quando informar o CEP "41770030" na tela de cadastro do Prontuario modulo atende
    E informar o Estado Civil "Outro" na tela de cadastro do Prontuario modulo atende
    Quando informar o Nome da Mãe "Teste Nome da mae" na tela de cadastro do Prontuario modulo atende
    E informar o Grupo Sanguinio "O+" na tela de cadastro do Prontuario modulo atende
    Quando informar o número do endereço "50" na tela de cadastro do Prontuario modulo atende
    E informar o email "teste@tests.com.br" na tela de cadastro do Prontuario modulo atende
    Quando informar o Médico "ABEL" na tela de cadastro do Prontuario modulo atende
    E informar o RG "" na tela de cadastro do Prontuario modulo atende
    Quando informar o Telefone Celular "11960666902" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão "Sim" do PopUp
    E informar o Observação "teste Observacao" na tela de cadastro do Prontuario modulo atende
    Quando clicar no botão gravar do modulo Atende
    E infarma o motivo "OUTRO" , a observação "" e clicar no botão "OK" da tela de Motivo
    Quando clicar no botão gravar do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    E validar resposta do questionario na tela de impressão
    Quando clicar no botão Salvar Como na tela de Visualização de impressão
    E  Escolho o formato do arquivo "Txt" e clico no botão "OK", na tela de Selecionar o formato
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=1,000" estão no arquivo como o formato ".TXT"


  @desktop @regressivo  @smart @sem_pre_Condicao
  Cenario: Validar que os dados do questionario do paciente estão correto e com o convenio PARTICULAR 1 (PAR), no arquivo com formato PDF o tenta imprirmir
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Questionario" na tela cadastro do paciente
    E clicar no botão Novo da tela de questionario
    Quando informar o nome do questionario "exame paulo" na tela de questionario
    E clicar no botão iniciar da tela de questionario
    Quando informar a resposta "1" na  tela de questionario
    E clicar no botão Concluir da tela de questionario
    Quando selecionar a lupa para visulaizar da tela de questionario
    E clicar no botão impimir  da tela de questionario
    E clicar no botão imprimir da tela de visualização de impressão
    Quando clicar no botão impressora da tela de imprimir
    E selecionar a impressora "Print to PDF" e clicar no botão Ok da tela  Printer Setup
    Quando clicar no botão Ok da tela de impimir
    Quando informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo
    Então Confirmo que os dados "nomePacienteAleatorio;DataAtual;valor=MEDICWARE;valor=1,000" estão no arquivo como o formato ".PDF"


