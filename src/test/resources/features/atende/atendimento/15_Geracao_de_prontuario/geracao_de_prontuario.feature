#language:pt
#encoding:utf-8

@atendimento_funcionalidade15
Funcionalidade: Geração de prontuário - Funcionalidade 15


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Valida geração do numero do prontuario com convênio PARTICULAR 1
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Então confirmo que o número do prontuario não foi gerado na tela de cadasto do prontuario
    Quando clicar no botão prontuario na tela de cadasto do prontuario
    Quando clicar no botão prontuario na tela de cadasto do prontuario
    Então confirmo que o número do prontuario foi gerado na tela de cadasto do prontuario


  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario:Valida geração do numero do prontuario com convênio SULAMERICA - OCUPACI
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","SULAMERICA - OCUPACI (SIO)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    E gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Então confirmo que o número do prontuario não foi gerado na tela de cadasto do prontuario
    Quando clicar no botão prontuario na tela de cadasto do prontuario
    Quando clicar no botão prontuario na tela de cadasto do prontuario
    Então confirmo que o número do prontuario foi gerado na tela de cadasto do prontuario