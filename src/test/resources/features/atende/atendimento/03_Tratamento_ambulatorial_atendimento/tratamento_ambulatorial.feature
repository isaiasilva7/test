#language:pt
#encoding:utf-8

@atendimento_funcionalidade03
Funcionalidade: Tratamento ambulatorial - Funcionalidade 03

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Gerar uma OS com tratamento ambulatorial e validar que a Guia Tis foi gerada
    Quando informar dados do cadasto do paciente
    |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
    | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando clicar no botão imprirmir do modulo Atende
    E clicar no botão iniciar da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão proximo da tela de questionario
    Quando Clicar no botão concluir da tela de questionario
    E clicar no botão "Sim" do PouP Concluir
    Quando clicar no botão "Guia Tiss" na tela imprirmir ordem de Serviço
    E selecionar a teclas "Shit+Enter" do teclado
    Então validar a existencia do Guia de Serviço

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Cancelar no tela de tratamento ambulatorial o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone    |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    E clinar nos botoes "btnCancelar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Excluir no tela de tratamento ambulatorial o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone    |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    E clinar nos botoes "btnExcluir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Avancar no tela de tratamento ambulatorial o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    E clinar nos botoes "btnAvancar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Gravar no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnGravar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Cancelar no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnCancelar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Busca no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnbusca"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Excluir no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnExcluir"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Avancar no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnAvancar"
    Entao o sistema não apresenta falha geral

  @desktop @regressivo @smart @sem_pre_Condicao
  Cenario: validar que ao clicar no botão Imprimir no tela de atendimento ordem de servico o sistema e não apresenta falha Geral
    Quando informar dados do cadasto do paciente
      |sexo |data_nascimento |telefone      |convenio                       |cPF                  |
      | M   | 10052000 	   |71960666905   |SULAMERICA - OCUPACI (SIO)     |CPF_Gerado_Aleatorio |
    E gravo as informacoes
    Entao deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende.
    E seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende.
    Quando gravar o Tratamento Ambulatorial no modulo Atende
    E clicar no botão avancar  por "3" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    E clicar nos botoes "btnImprimir"
    Entao o sistema não apresenta falha geral

