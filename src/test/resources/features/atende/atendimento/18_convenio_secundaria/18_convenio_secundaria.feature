#language:pt
#encoding:utf-8

@atendimento_funcionalidade18
Funcionalidade: Convênio secundário - Funcionalidade 19

  Como um usuário;
  Gostaria de adicionar um convênio secundário para o paciente
  Afim de validar sistema não apresenta erro ao gravar OS.

  Contexto:
    Dado que tenho acesso a aplicação Smart Atende
    Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
    E  informa o item de busca "nomePacienteAleatorio" e busco no modulo Atende
    E confirmo que desejo incluir novo paciente no atendimento no modulo Atende

  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir um convenio secundaria quando o paciente já esta com o convenio com convenio PARTICULAR 1 (PAR) verificar que o sistema que o sistema não apresenta erro ao gera uma OS.
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","PARTICULAR 1 (PAR)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Convenio Secundário" na tela cadastro do paciente
    E informar o convênio Secundario "SUL AMERICA IVIS" na aba convenio secundário
    Quando informar o Titularidade "mesmaTitualirade" na aba convenio secundário
    E informar a matricula secundária "231236" na aba convenio secundário
    Quando informar a data de Validade "15022025" na aba convenio secundário
    E informar o plano "ESPECIAL" na aba convenio secundário
    Quando informar o numero de matricula na empresa "131313" na aba convenio secundário
    E informar o locação "PARALELA" na aba convenio secundário
    Quando informar a chavesline "1346131" na aba convenio secundário
    E clicar no botão gravar do modulo Atende
    E clicar no botão zoom do modulo Atende
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "15,00" na tela ordem de serviço do paciente no módulo Atende


  @desktop @regressivo @smart @sem_pre_Condicao @PrioridadeAlta
  Cenario: Validar que ao inserir um convenio secundaria quando o paciente já esta com o convenio com convenio BRADESCO F (BF), verificar que o sistema que o sistema não apresenta erro ao gera uma OS.
    Quando informar dados do cadasto do paciente "M","10052000","71960666905","BRADESCO F (BF)", "CPF_Gerado_Aleatorio", "", "" no modulo Atende
    Quando gravo as informacoes
    E clicar no botão zoom do modulo Atende
    Quando clicar nos botoes "Convenio Secundário" na tela cadastro do paciente
    E informar o convênio Secundario "SUL AMERICA IVIS" na aba convenio secundário
    Quando informar o Titularidade "mesmaTitualirade" na aba convenio secundário
    E informar a matricula secundária "231236" na aba convenio secundário
    Quando informar a data de Validade "15022025" na aba convenio secundário
    E informar o plano "ESPECIAL" na aba convenio secundário
    Quando informar o numero de matricula na empresa "131313" na aba convenio secundário
    E informar o locação "PARALELA" na aba convenio secundário
    Quando informar a chavesline "1346131" na aba convenio secundário
    E clicar no botão gravar do modulo Atende
    E clicar no botão zoom do modulo Atende
    E clicar no botão avancar  por "1" vezes do modulo Atende
    Quando clicar no botão gravar do modulo Atende
    E informar dados do serviço "Glico","1", "ADRIANO"
    Quando clicar no botão gravar do modulo Atende
    Quando capturo o valor do serviço "1"
    Entao  validar o valor do exame "24,00" na tela ordem de serviço do paciente no módulo Atende
