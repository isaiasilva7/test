#language:pt
#encoding:utf-8
@atendimento_funcionalidade01 @PrioridadeAlta
Funcionalidade: Gerar Pre-Fatura

  @ambulatorial_semItens
  Cenario: Pré-fatura de serviços ambulatoriais por paciente sem itens encontrados
    Dado que tenho acesso a aplicação Smart Fature
    Quando acesso o menu "Arquivo;Abrir;Pré-Fatura" no Home do modulo Fature
    E clico na aba "Ambulatório"
    Quando busco pelo paciente "LALINHA"
    E clico no botão "Avançar" e verifico o cabeçalho da pré-fatura
    E clico no botão "Novo" para gerar uma nova pré-fatura
    E seleciono o periodo atual de "30092019" a "30092019"
    Quando clico no botao "Gravar" da aba ambulatorio da tela de pre-fature do modulo fature
    Entao nenhuma OS gerada no periodo é exibida
    Quando clico no botao "Processar" da aba ambulatorio da tela de pre-fature do modulo fature
    E confirmo em "SIM" o processamento da pré-fatura
    Entao uma mensagem informando que nenhum item foi encontrado para esta pré-fatura é exibida


  @ambulatorial_paciente   
   Cenario: Gerar um pré-fatura de serviços ambulatoriais por paciente
			#Pré-Condição - GERAR OS - para que exista registro dentro do período de busca
			Dado que tenho acesso a aplicação Smart Atende
			Quando acesso o menu "Arquivo;Abrir;Atendimento" no Home do modulo Atende
			E  informa o item de busca "ISAIAS SANTOS SILVA" e busco no modulo Atende
			E clicar no botão avancar  por "1" vezes do modulo Atende
			Quando informar dados da OS, tipo Atendimento "Assistencial" e Setor ""
			Quando clicar no botão gravar do modulo Atende
			E informar dados do serviço "Glico","1", "ADRIANO"
			Quando clicar no botão gravar do modulo Atende
			
			Dado que tenho acesso a aplicação Smart Fature
			Quando acesso o menu "Arquivo;Abrir;Pré-Fatura" no Home do modulo Fature
			E clico na aba "Ambulatório"
			Quando busco pelo paciente "ISAIAS SANTOS SILVA"
			E clico no botão "Avançar" e verifico o cabeçalho da pré-fatura
			E clico no botão "Novo" para gerar uma nova pré-fatura				        
			E seleciono um periodo de trinta dias de "01112019" a "30112019"
			Quando clico no botão "Gravar" do módulo Fature
			Entao visualizo uma lista de OS geradas no periodo pesquisado
			Quando clico no botao "Processar" da aba ambulatorio da tela de pre-fature do modulo fature
			E confirmo em "SIM" o processamento da pré-fatura
			Entao a pré fatura é processada e uma mensagem informando que o "Processo concluído" é exibido
			Quando clico no botao "OK" da aba ambulatorio da tela de pre-fature do modulo fature
			Entao sou redirecionado para a aba de "Log de Processamento" onde visualizo a pré-fatura criada

		@ambulatorial_os
		Cenario: Gerar um pré-fatura de serviços ambulatoriais por ordem de serviço
		  Dado que tenho acesso a aplicação Smart Fature
			Quando acesso o menu "Arquivo;Abrir;Pré-Fatura" no Home do modulo Fature
		  E clico na aba "Ambulatório"
		  Quando clico no botao "Buscar Por OS" da aba ambulatorio da tela de pre-fature do modulo fature
		  E informo a OS "15619"
		  Quando clico no botao "OK" da tela de informar numero da OS
		  Entao verifico que é exibido uma tela com a pré-fatura referente a OS lançada
		  Quando clico no botao "Gravar" da aba ambulatorio da tela de pre-fature do modulo fature
		  Quando clico no botao "Processar" da aba ambulatorio da tela de pre-fature do modulo fature
		  E confirmo em "SIM" o processamento da pré-fatura
		  E confirmo o processamento da pré-fatura
		  Entao a pré fatura é processada e uma mensagem informando que o "Processo concluído" é exibido
		  Quando clico no botao "OK" da aba ambulatorio da tela de pre-fature do modulo fature
		  Entao sou redirecionado para a aba de "Log de Processamento" onde visualizo a pré-fatura criada

