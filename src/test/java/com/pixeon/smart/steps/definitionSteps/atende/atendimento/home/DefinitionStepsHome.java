package com.pixeon.smart.steps.definitionSteps.atende.atendimento.home;

import com.pixeon.smart.steps.serenity.atende.atendimento.home.HomeSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsHome {
   @Steps
   HomeSteps homeSteps;
  
   @Quando("^acesso o menu \"([^\"]*)\" no Home do modulo Atende$")
   public void acesso_o_menu_no_Home_do_modulo_Atende(String menu) {
	   homeSteps.acesso_o_menu_no_Home_do_modulo_Atende(menu);
   }

   @Quando("^acesso o menu \"([^\"]*)\" no Home do modulo Fature$")
   public void acesso_o_menu_no_Home_do_modulo_Fature(String menu) {
	   homeSteps.acesso_o_menu_no_Home_do_modulo_Fature(menu);
   }
   
   
}
