package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_fila_de_espera_recepcao_caixa;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Fila_de_espera_recepcao_caixa.DetalheAtendimentoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsAtendimentoDetalhe {
    @Steps
    DetalheAtendimentoSteps detalheAtendimentoSteps;

    @Quando("^clicar no botão Retorna pra Fila na tela Detalhes$")
    public void clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes() {
        detalheAtendimentoSteps.clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes();
    }

}
