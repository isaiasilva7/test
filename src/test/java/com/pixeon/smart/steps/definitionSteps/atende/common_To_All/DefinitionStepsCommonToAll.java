package com.pixeon.smart.steps.definitionSteps.atende.common_To_All;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.steps.serenity.atende.common_To_All.CommonToAllSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCommonToAll {

    @Steps
    CommonToAllSteps commonToAllSteps;

    @Quando("^clicar no botão imprirmir do modulo Atende$")
    public void clicar_no_botão_imprirmir_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_imprirmir_do_modulo_Atende();
    }


    @Quando("^clicar no botão gravar do modulo Atende$")
    public void clicar_no_botão_gravar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_gravar_do_modulo_Atende();
    }

    @Quando("^clicar no botão cancelar do modulo Atende$")
    public void clicar_no_botão_cancelar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_cancelar_do_modulo_Atende();
    }

    @Quando("^clicar no botão novo do modulo Atende$")
    public void clicar_no_botão_novo_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_novo_do_modulo_Atende();
    }

    @Quando("^clicar no botão buscar do modulo Atende$")
    public void clicar_no_botão_buscar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_buscar_do_modulo_Atende();
    }

    @Quando("^clicar no botão fechar do modulo Atende$")
    public void clicar_no_botão_fechar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_fechar_do_modulo_Atende();

    }

    @Quando("^clicar no botão retornar do modulo Atende$")
    public void clicar_no_botão_retornar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_retornar_do_modulo_Atende();
    }

    @Quando("^clicar no botão avancar do modulo Atende$")
    public void clicar_no_botão_avancar_do_modulo_Atende() {
        commonToAllSteps.clicar_no_botão_avancar_do_modulo_Atende();
    }

    @Quando("^clicar no botão zoom do modulo Atende$")
    public void clicar_no_botão_zoom_do_modulo_Atende() {

        commonToAllSteps.clicar_no_botão_zoom_do_modulo_Atende();
    }

    @Quando("^clicar no botão avancar  por \"([^\"]*)\" vezes do modulo Atende$")
    public void clicar_no_botão_avancar_por_vezes_do_modulo_Atende(String vezes) {
        commonToAllSteps.clicar_no_botão_avancar_por_vezes_do_modulo_Atende( vezes);
    }


    @Quando("^clicar nos botoes \"([^\"]*)\"$")
    public void clicar_nos_botoes(String nomeDoBotao) {
        commonToAllSteps.clicar_nos_botoes(nomeDoBotao);
    }

    @Quando("^fechar modulo Atende$")
    public void fechar_modulo_Atende() {
        commonToAllSteps.fechar_modulo_Atende();
    }

    @Quando("^fechar a tela via comando ESC do teclado$")
    public void fechar_a_tela_via_comando_ESC_do_teclado() {
        commonToAllSteps.fechar_a_tela_via_comando_ESC_do_teclado();
    }

    @Então("^o sistema apresenta a mensagem Gravação realizada com sucesso$")
    public void o_sistema_apresenta_a_mensagem_Gravação_realizada_com_sucesso() {
        commonToAllSteps.o_sistema_apresenta_a_mensagem_Gravação_realizada_com_sucesso();
    }

    @Quando("^clicar no botão Novo, no modulo Atende$")
    public void clicar_no_botão_Novo_no_modulo_Atende() { commonToAllSteps.clicar_no_botão_novo_do_modulo_Atende(); }


    @Quando("^clicar na tecla \"([^\"]*)\" do teclado no modulo Atende$")
    public void clicar_na_tecla_do_teclado_no_modulo_Atende(String tecla) {
        commonToAllSteps.clicar_na_tecla_do_teclado_no_modulo_Atende(tecla);
    }


}
