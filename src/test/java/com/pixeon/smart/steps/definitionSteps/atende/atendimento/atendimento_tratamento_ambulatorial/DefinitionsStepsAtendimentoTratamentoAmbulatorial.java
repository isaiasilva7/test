package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_tratamento_ambulatorial;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_tratamento_ambulatorial.AtendimentoTratamentoAmbulatorialSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsAtendimentoTratamentoAmbulatorial {

    @Steps
    AtendimentoTratamentoAmbulatorialSteps atendimentoTratamentoAmbulatorialSteps;

    @Quando("^gravar o Tratamento Ambulatorial no modulo Atende$")
    public void gravar_o_Tratamento_Ambulatorial_no_modulo_Atende() {
        atendimentoTratamentoAmbulatorialSteps.gravar_o_Tratamento_Ambulatorial_no_modulo_Atende();
    }

}
