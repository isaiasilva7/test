package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Dados_do_item;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Dados_do_item.AtendimentoDadosDoItemSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoDadosDoItem {

    @Steps
    AtendimentoDadosDoItemSteps atendimentoDadosDoItemSteps;

    @Quando("^clicar botão Alterar Data Do Resultados, na tela Dados do Item$")
    public void clicar_botão_Alterar_Data_Do_Resultados_na_tela_Dados_do_Item() {
        atendimentoDadosDoItemSteps.clicar_botão_Alterar_Data_Do_Resultados_na_tela_Dados_do_Item();
    }


    @Quando("^clicar botão Diligenciamento, na tela Dados do Item$")
    public void clicar_botão_Diligenciamento_na_tela_Dados_do_Item() {
        atendimentoDadosDoItemSteps.clicar_botão_Diligenciamento_na_tela_Dados_do_Item();
    }

    @Quando("^clicar botão Informações, na tela Dados do Item$")
    public void clicar_botão_Informações_na_tela_Dados_do_Item() {
        atendimentoDadosDoItemSteps.clicar_botão_Informações_na_tela_Dados_do_Item();
    }

    @Quando("^clicar botão OK, na tela Dados do Item$")
    public void clicar_botão_OK_na_tela_Dados_do_Item() {
        atendimentoDadosDoItemSteps.clicar_botão_OK_na_tela_Dados_do_Item();
    }

    @Quando("^clicar botão Fechar, na tela Dados do Item$")
    public void clicar_botão_Fechar_na_tela_Dados_do_Item() {
        atendimentoDadosDoItemSteps.clicar_botão_Fechar_na_tela_Dados_do_Item();
    }

}
