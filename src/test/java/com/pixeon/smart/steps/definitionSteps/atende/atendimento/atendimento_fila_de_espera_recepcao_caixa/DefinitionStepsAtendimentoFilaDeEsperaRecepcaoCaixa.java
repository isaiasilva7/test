package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_fila_de_espera_recepcao_caixa;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Fila_de_espera_recepcao_caixa.AtendimentoFilaDeEsperaRecepcaoCaixaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoFilaDeEsperaRecepcaoCaixa {

    @Steps
    AtendimentoFilaDeEsperaRecepcaoCaixaSteps atendimentoFilaDeEsperaRecepcaoCaixaSteps;

    @Então("^confiramr que o panciente foi inserido na fila de espera$")
    public void confiramr_que_o_panciente_foi_inserido_na_fila_de_espera() {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.confiramr_que_o_panciente_foi_inserido_na_fila_de_espera();
    }

    @Então("^clicar no botão de encerrar atendimento$")
    public void clicar_no_botão_de_encerrar_atendimento() {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_de_encerrar_atendimento();
    }

    @Quando("^informar o nome do paciente \"([^\"]*)\" na tela fila de espera atendimento$")
    public void informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento(String nome) {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento(nome);
    }

    @Quando("^clicar no botão detalhes na tela fila de espera atendimento$")
    public void clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento() {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento();
    }



    @Quando("^clinar nos botoes \"([^\"]*)\" da tela de Fila de espera recepção$")
    public void clinar_nos_botoes_da_tela_de_Fila_de_espera_recepção(String nomeBotoes) {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.clinar_nos_botoes_da_tela_de_Fila_de_espera_recepção(nomeBotoes);
    }

    @Quando("^clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende$")
    public void clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende();
    }

    @Quando("^seleciono medico \"([^\"]*)\" na fila da tela fila de espera$")
    public void seleciono_medico_na_fila_da_tela_fila_de_espera(String medico) {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.seleciono_medico_na_fila_da_tela_fila_de_espera(medico);
    }


    @Quando("^Clicar no botão chamar o proximo fila de Espera$")
    public void clicar_no_botão_chamar_o_proximo_fila_de_Espera() {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_chamar_o_proximo_fila_de_Espera();
    }

    @Quando("^informa o nome do Médico \"([^\"]*)\" para consulta a Fila na tela de fila de Espera$")
    public void informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera(String nomeDomedico) {
        atendimentoFilaDeEsperaRecepcaoCaixaSteps.informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera(nomeDomedico);
    }


}
