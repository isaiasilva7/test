package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_XML_de_Enviar_e_retorno.atendimento_recibo;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_XML_de_Enviar_e_retorno.AtendimentoXmlDeEnvioERetornoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoXmlDeEnvioERetorno {

    @Steps
    AtendimentoXmlDeEnvioERetornoSteps atendimentoXmlDeEnvioERetornoSteps;

    @Quando("^clicar no link Xml de Envio, na tela xml de Envio e Retorno$")
    public void clicar_no_link_Xml_de_Envio_na_tela_xml_de_Envio_e_Retorno() {
        atendimentoXmlDeEnvioERetornoSteps.clicar_no_link_Xml_de_Envio_na_tela_xml_de_Envio_e_Retorno();
    }


    @Quando("^clicar no link Xml de Retorno, na tela xml de Envio e Retorno$")
    public void clicar_no_link_Xml_de_Retorno_na_tela_xml_de_Envio_e_Retorno() {
        atendimentoXmlDeEnvioERetornoSteps.clicar_no_link_Xml_de_Retorno_na_tela_xml_de_Envio_e_Retorno();
    }

    @Quando("^clicar no botão Salvar como, na tela xml de Envio e Retorno$")
    public void clicar_no_botão_Salvar_como_na_tela_xml_de_Envio_e_Retorno() {
        atendimentoXmlDeEnvioERetornoSteps.clicar_no_botão_Salvar_como_na_tela_xml_de_Envio_e_Retorno();
    }


    @Quando("^clicar no botão Copiar como, na tela xml de Envio e Retorno$")
    public void clicar_no_botão_Copiar_como_na_tela_xml_de_Envio_e_Retorno() {
        atendimentoXmlDeEnvioERetornoSteps.clicar_no_botão_Copiar_como_na_tela_xml_de_Envio_e_Retorno();

    }

    @Então("^confirmo que no arquivo xml na tag \"([^\"]*)\", no  parâmetro \"([^\"]*)\" contem o valor \"([^\"]*)\"$")
    public void confirmo_que_no_arquivo_xml_na_tag_no_parâmetro_contem_o_valor(String tag, String paramentro, String valor) {
        atendimentoXmlDeEnvioERetornoSteps.confirmo_que_no_arquivo_xml_na_tag_no_parâmetro_contem_o_valor(tag,paramentro,valor);
    }

    @Então("^confirmar que o arquivo XML de retorno contem \"([^\"]*)\"$")
    public void confirmar_que_o_arquivo_XML_de_retorno_contem(String texto) {
        atendimentoXmlDeEnvioERetornoSteps.confirmar_o_arquivo_XML_de_retorno_contem(texto);
    }





}
