package  com.pixeon.smart.steps.definitionSteps.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados.DadosComplementaresSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsDadosComplementares {

    @Steps
    DadosComplementaresSteps dadosComplementaresSteps;

    @Quando("^informar nome do Pai \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_nome_do_Pai_na_tela_de_dados_Complementares(String nomeDoPai) {
        dadosComplementaresSteps.informar_nome_do_Pai_na_tela_de_dados_Complementares(nomeDoPai);
    }


    @Quando("^informar Apelido \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_Apelido_na_tela_de_dados_Complementares(String apelido) {
        dadosComplementaresSteps.informar_Apelido_na_tela_de_dados_Complementares(apelido);
    }

    @Quando("^informar Religião \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_Religião_na_tela_de_dados_Complementares(String religiao) {
        dadosComplementaresSteps.informar_Religião_na_tela_de_dados_Complementares(religiao);
    }

    @Quando("^informar Cor \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_Cor_na_tela_de_dados_Complementares(String cor) {
        dadosComplementaresSteps.informar_Cor_na_tela_de_dados_Complementares(cor);
    }

    @Quando("^informar RG \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_RG_na_tela_de_dados_Complementares(String rg) {
        dadosComplementaresSteps.informar_RG_na_tela_de_dados_Complementares(rg);
    }

    @Quando("^informar CEP \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_CEP_na_tela_de_dados_Complementares(String cep) {
        dadosComplementaresSteps.informar_CEP_na_tela_de_dados_Complementares(cep);
    }

    @Quando("^informar Número endereco \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_Número_endereco_na_tela_de_dados_Complementares(String numeroEndereco) {
        dadosComplementaresSteps.informar_Número_endereco_na_tela_de_dados_Complementares(numeroEndereco);
    }

    @Quando("^informar bairro \"([^\"]*)\", na tela de dados Complementares$")
    public void informar_bairro_na_tela_de_dados_Complementares(String bairro) {
        dadosComplementaresSteps.informar_bairro_na_tela_de_dados_Complementares(bairro);
    }


}
