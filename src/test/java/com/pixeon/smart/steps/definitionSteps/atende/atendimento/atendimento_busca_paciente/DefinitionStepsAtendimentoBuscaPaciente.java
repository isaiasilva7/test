package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_busca_paciente;

import com.pixeon.smart.datamodel.atende.atendimento_busca_paciente.DataAtendeAtendimentoBuscaPaciente;
import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_busca_paciente.AtendimentoBuscaPacienteSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsAtendimentoBuscaPaciente {

    @Steps
    AtendimentoBuscaPacienteSteps atendimentoBuscaPacienteSteps;

    @Quando("^informa o item de busca \"([^\"]*)\" e busco no modulo Atende$")
    public void informa_o_item_de_busca_e_busco_no_modulo_Atende(String itemDeBusca) {
         atendimentoBuscaPacienteSteps.informa_o_item_de_busca_e_busco_no_modulo_Atende(itemDeBusca);
    }

    @Entao("^o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO  na busca no modulo Atende$")
    public void o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_busca_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_busca_no_modulo_Atende();
    }

    @Quando("^que seleciono busca paciente por Os no Atendimento busca modulo Atende$")
    public void que_seleciono_busca_paciente_por_Os_no_Atendimento_busca_modulo_Atende() {
        atendimentoBuscaPacienteSteps.que_seleciono_busca_paciente_por_Os_no_Atendimento_busca_modulo_Atende();
    }

    @Quando("^confirmo que desejo incluir novo paciente no atendimento no modulo Atende$")
    public void confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende();
    }

    @Quando("^informar dados do cadasto do paciente$")
    public void informar_dados_do_cadasto_do_paciente(List<DataAtendeAtendimentoBuscaPaciente> dataAtendeAtendimentoBuscaPaciente) {
        atendimentoBuscaPacienteSteps.informar_dados_do_cadasto_do_paciente(dataAtendeAtendimentoBuscaPaciente);
    }


    @Quando("^gravo as informacoes$")
    public void gravo_as_informacoes() {
        atendimentoBuscaPacienteSteps.gravo_as_informacoes();
    }


    @Entao("^deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende\\.$")
    public void deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende();
    }



    @Quando("^informar dados do cadasto do paciente \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" no modulo Atende$")
    public void informar_dados_do_cadasto_do_paciente_no_modulo_Atende(String sexo, String data_nascimento, String telefone, String convenio, String cPF, String nome_da_mae, String nome_social) {
        atendimentoBuscaPacienteSteps.informar_dados_do_cadasto_do_paciente(sexo,data_nascimento,telefone, convenio, cPF, nome_da_mae, nome_social);
    }



    @Entao("^seleciono o botão Tratamento Ambulatorial na tela atendimento busca paciente no modulo Atende\\.$")
    public void seleciono_o_botao_Tratamento_Ambulatorial_na_tela_atendimento_busca_paciente_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.seleciono_o_botao_Tratamento_Ambulatorial_na_tela_atendimento_busca_paciente_no_modulo_Atende();
    }

    @Quando("^clicar no botão Adiconar na Fila tela de cadastro do paciente no modulo Atende$")
    public void clicar_no_botão_Adiconar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.clicar_no_botão_Adiconar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende();
    }


    @Quando("^clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende$")
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        atendimentoBuscaPacienteSteps.clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende();
    }



    @Quando("^clicar nos botoes \"([^\"]*)\" na tela de busca$")
    public void clicar_nos_botoes_na_tela_de_busca(String nomeDoBotoes) {
        atendimentoBuscaPacienteSteps.clicar_nos_botoes_na_tela_de_busca(nomeDoBotoes);
    }
    @Quando("^clicar no botão \"([^\"]*)\" na tela de busca$")
    public void clicar_no_botão_na_tela_de_busca(String nomeDoBotoes) {
        atendimentoBuscaPacienteSteps.clicar_nos_botoes_na_tela_de_busca(nomeDoBotoes);
    }
}
