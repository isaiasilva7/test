package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Ordem_de_servicos_atendimentos;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Ordem_de_servicos_atendimentos.AtendimentoOrdemDeServicoAtendimentosSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoOrdemDeServicoAtendimentos {

    @Steps
    AtendimentoOrdemDeServicoAtendimentosSteps atendimentoOrdemDeServicoAtendimentosSteps;

    @Quando("^clicar na seta da tela de atendimento$")
    public void clicar_na_seta_da_tela_de_atendimento() {
        atendimentoOrdemDeServicoAtendimentosSteps.clicar_na_seta_da_tela_de_atendimento();
    }

}
