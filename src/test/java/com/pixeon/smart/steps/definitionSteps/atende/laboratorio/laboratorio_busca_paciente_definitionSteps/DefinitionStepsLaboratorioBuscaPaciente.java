package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_busca_paciente_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_busca_paciente_Steps.LaboratorioBuscaPacienteSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioBuscaPaciente {
    @Steps
    LaboratorioBuscaPacienteSteps laboratorioBuscaPacienteSteps;

    @Quando("^clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende Laboratorio$")
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        laboratorioBuscaPacienteSteps.clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio();
    }

    @Quando("^informa o item de busca \"([^\"]*)\" e busco no modulo Atende Laboratorio$")
    public void informa_o_item_de_busca_e_busco_no_modulo_Atende_Laboratorio(String itemBusca) {
        laboratorioBuscaPacienteSteps.informa_o_item_de_busca_e_busco_no_modulo_Atende_Laboratorio(itemBusca);
    }

    @Quando("^confirmo que desejo incluir novo paciente no atendimento no modulo Atende Laboratorio$")
    public void confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende_Laboratorio() {
        laboratorioBuscaPacienteSteps.confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende_Laboratorio();
    }

    @Quando("^informar dados do cadasto do paciente \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" no modulo Atende Laboratorio$")
    public void informar_dados_do_cadasto_do_paciente_no_modulo_Atende_Laboratorio(String sexo, String data_nascimento, String telefone, String convenio, String cPF, String nome_da_mae, String nome_social) {
        laboratorioBuscaPacienteSteps.informar_dados_do_cadasto_do_paciente_no_modulo_Atende_Laboratorio(sexo,data_nascimento,telefone,convenio,cPF,nome_da_mae,nome_social);
    }

    @Quando("^clicar no botão Adicionar na Fila tela de cadastro do paciente no modulo Atende Laboratorio$")
    public void clicar_no_botão_Adicionar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        laboratorioBuscaPacienteSteps.clicar_no_botão_Adicionar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão Fila de Espera tela de cadastro do paciente no modulo Atende no modulo Atende Laboratorio$")
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_no_modulo_Atende_Laboratorio() {
        laboratorioBuscaPacienteSteps.clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_no_modulo_Atende_Laboratorio();
    }


    @Entao("^deve apresentar a mensagem Paciente gravado com sucesso no modulo Atende Laboratorio$")
    public void deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende_Laboratorio() {
        laboratorioBuscaPacienteSteps.deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão \"([^\"]*)\" na tela de busca no modulo Atende Laboratorio$")
    public void clicar_no_botão_na_tela_de_busca_no_modulo_Atende_Laboratorio(String nomeDoBotao) {
        laboratorioBuscaPacienteSteps.clicar_no_botão_na_tela_de_busca_no_modulo_Atende_Laboratorio(nomeDoBotao);
    }

}
