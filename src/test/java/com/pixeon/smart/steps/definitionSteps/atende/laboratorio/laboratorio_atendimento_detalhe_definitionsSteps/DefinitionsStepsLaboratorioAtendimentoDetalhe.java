package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_atendimento_detalhe_definitionsSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_atendimento_detalhe_Steps.LaboratorioAtendimentoDetalheSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsLaboratorioAtendimentoDetalhe {
    @Steps
    LaboratorioAtendimentoDetalheSteps laboratorioAtendimentoDetalheSteps;

    @Quando("^clicar no botão Retorna pra Fila na tela Detalhes no modulo Atende Laboratorio$")
    public void clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes_no_modulo_Atende_Laboratorio() {
        laboratorioAtendimentoDetalheSteps.clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes_no_modulo_Atende_Laboratorio();
    }

}
