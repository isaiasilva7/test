package  com.pixeon.smart.steps.definitionSteps.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados.CadastroDadosPacienteProntuarioSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCadastroDadosPacienteProntuario {

    @Steps
    CadastroDadosPacienteProntuarioSteps cadastroDadosPacienteProntuarioSteps;


    @Então("^confirmo que o número do prontuario não foi gerado na tela de cadasto do prontuario$")
    public void confirmo_que_o_número_do_prontuario_não_foi_gerado_na_tela_de_cadasto_do_prontuario() {
        cadastroDadosPacienteProntuarioSteps.confirmo_que_o_número_do_prontuario_não_foi_gerado_na_tela_de_cadasto_do_prontuario();
    }


    @Quando("^clicar no botão prontuario na tela de cadasto do prontuario$")
    public void clicar_no_botão_prontuario_na_tela_de_cadasto_do_prontuario() {
        cadastroDadosPacienteProntuarioSteps.clicar_no_botão_prontuario_na_tela_de_cadasto_do_prontuario();
    }

    @Então("^confirmo que o número do prontuario foi gerado na tela de cadasto do prontuario$")
    public void confirmo_que_o_número_do_prontuario_foi_gerado_na_tela_de_cadasto_do_prontuario() {
        cadastroDadosPacienteProntuarioSteps.confirmo_que_o_número_do_prontuario_foi_gerado_na_tela_de_cadasto_do_prontuario();
    }


    @Quando("^informar o Peso \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Peso_na_tela_de_cadastro_do_Prontuario_modulo_atende(String pseo) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Peso_na_tela_de_cadastro_do_Prontuario_modulo_atende(pseo);
    }


    @Quando("^informar o Alto \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Alto_na_tela_de_cadastro_do_Prontuario_modulo_atende(String alta) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Alto_na_tela_de_cadastro_do_Prontuario_modulo_atende(alta);
    }

    @Quando("^informar o Bairro \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Bairro_na_tela_de_cadastro_do_Prontuario_modulo_atende(String bairro) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Bairro_na_tela_de_cadastro_do_Prontuario_modulo_atende(bairro);
    }

    @Quando("^informar o CEP \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_CEP_na_tela_de_cadastro_do_Prontuario_modulo_atende(String cEP) {
        cadastroDadosPacienteProntuarioSteps.informar_o_CEP_na_tela_de_cadastro_do_Prontuario_modulo_atende(cEP);
    }

    @Quando("^informar o Estado Civil \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Estado_Civil_na_tela_de_cadastro_do_Prontuario_modulo_atende(String estadoCivil) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Estado_Civil_na_tela_de_cadastro_do_Prontuario_modulo_atende(estadoCivil);
    }

    @Quando("^informar o Nome da Mãe \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Nome_da_Mãe_na_tela_de_cadastro_do_Prontuario_modulo_atende(String nomeDaMae) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Nome_da_Mãe_na_tela_de_cadastro_do_Prontuario_modulo_atende(nomeDaMae);
    }

    @Quando("^informar o Grupo Sanguinio \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Grupo_Sanguinio_na_tela_de_cadastro_do_Prontuario_modulo_atende(String grupoSanguineo) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Grupo_Sanguinio_na_tela_de_cadastro_do_Prontuario_modulo_atende(grupoSanguineo);
    }

    @Quando("^informar o número do endereço \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_número_do_endereço_na_tela_de_cadastro_do_Prontuario_modulo_atende(String numeroEndereco) {
        cadastroDadosPacienteProntuarioSteps.informar_o_número_do_endereço_na_tela_de_cadastro_do_Prontuario_modulo_atende(numeroEndereco);
    }

    @Quando("^informar o email \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_email_na_tela_de_cadastro_do_Prontuario_modulo_atende(String email) {
        cadastroDadosPacienteProntuarioSteps.informar_o_email_na_tela_de_cadastro_do_Prontuario_modulo_atende(email);
    }

    @Quando("^informar o Médico \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Médico_na_tela_de_cadastro_do_Prontuario_modulo_atende(String medico) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Médico_na_tela_de_cadastro_do_Prontuario_modulo_atende(medico);
    }

    @Quando("^informar o RG \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_RG_na_tela_de_cadastro_do_Prontuario_modulo_atende(String rg) {
        cadastroDadosPacienteProntuarioSteps.informar_o_RG_na_tela_de_cadastro_do_Prontuario_modulo_atende(rg);
    }

    @Quando("^informar o Telefone Celular \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Telefone_Celular_na_tela_de_cadastro_do_Prontuario_modulo_atende(String TelefoneCelular) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Telefone_Celular_na_tela_de_cadastro_do_Prontuario_modulo_atende(TelefoneCelular);
    }

    @Quando("^informar o Observação \"([^\"]*)\" na tela de cadastro do Prontuario modulo atende$")
    public void informar_o_Observação_na_tela_de_cadastro_do_Prontuario_modulo_atende(String observacao) {
        cadastroDadosPacienteProntuarioSteps.informar_o_Observação_na_tela_de_cadastro_do_Prontuario_modulo_atende(observacao);
    }




}
