package com.pixeon.smart.steps.definitionSteps.atende.login;

import com.pixeon.smart.steps.serenity.atende.login.LoginSteps;
import cucumber.api.java.pt.Dado;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLogin {

	@Steps
	LoginSteps loginSteps;

	@Dado("^que tenho acesso a aplicação Smart Atende$")
	public void que_tenho_acesso_a_aplicação_Smart_Atende() {
		loginSteps.que_tenho_acesso_a_aplicação_Smart_Atende();
	}

}
