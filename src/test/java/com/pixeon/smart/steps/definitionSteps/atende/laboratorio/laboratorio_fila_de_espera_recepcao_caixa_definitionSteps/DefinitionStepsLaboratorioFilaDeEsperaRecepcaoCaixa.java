package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_fila_de_espera_recepcao_caixa_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_fila_de_espera_recepcao_caixa_Steps.LaboratorioFilaDeEsperaRecepcaoCaixaSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioFilaDeEsperaRecepcaoCaixa {
    @Steps
    LaboratorioFilaDeEsperaRecepcaoCaixaSteps laboratorioFilaDeEsperaRecepcaoCaixaSteps;


    @Quando("^clicar no botão de encerrar  no modulo Atende Laboratorio$")
    public void clicar_no_botão_de_encerrar_no_modulo_Atende_Laboratorio() {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_de_encerrar_no_modulo_Atende_Laboratorio();
    }

    @Quando("^informa o nome do Médico \"([^\"]*)\" para consulta a Fila na tela de fila de Espera no modulo Atende Laboratorio$")
    public void informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera_no_modulo_Atende_Laboratorio(String nomeMedico) {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera_no_modulo_Atende_Laboratorio(nomeMedico);
    }

    @Então("^clicar no botão de encerrar atendimento no modulo Atende Laboratorio$")
    public void clicar_no_botão_de_encerrar_atendimento_no_modulo_Atende_Laboratorio() {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_de_encerrar_no_modulo_Atende_Laboratorio();
    }

    @Quando("^informar o nome do paciente \"([^\"]*)\" na tela fila de espera atendimento no modulo Atende Laboratorio$")
    public void informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio(String nome) {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio(nome);
    }

    @Quando("^clicar no botão detalhes na tela fila de espera atendimento no modulo Atende Laboratorio$")
    public void clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio() {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão Trasfêrencia tela de cadastro do paciente no modulo Atende Laboratorio$")
    public void clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio();
    }

    @Então("^confirmar que o panciente foi inserido na fila de espera no modulo Atende Laboratorio$")
    public void confirmar_que_o_panciente_foi_inserido_na_fila_de_espera_no_modulo_Atende_Laboratorio() {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.confirmar_que_o_panciente_foi_inserido_na_fila_de_espera_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão \"([^\"]*)\" na tela de Fila de espera recepção no modulo Atende Laboratorio$")
    public void clicar_no_botão_na_tela_de_Fila_de_espera_recepção_no_modulo_Atende_Laboratorio(String nomeDoBotao) {
        laboratorioFilaDeEsperaRecepcaoCaixaSteps.clicar_no_botão_na_tela_de_Fila_de_espera_recepção_no_modulo_Atende_Laboratorio(nomeDoBotao);
    }


}
