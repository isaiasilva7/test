package com.pixeon.smart.steps.definitionSteps.atende.menu;

import com.pixeon.smart.steps.serenity.atende.menu.MenuSteps;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.pt.Quando;

public class DefinitionStepsMenu {
    @Steps
    MenuSteps menuSteps;

    @Quando("^que acesso o menu \"([^\"]*)\" no modulo Atende$")
    public void que_acesso_o_menu_e_sub_menu_o_Home_do_modulo_Atende(String menu) {
        menuSteps.selecionarMenu(menu);
    }

}
