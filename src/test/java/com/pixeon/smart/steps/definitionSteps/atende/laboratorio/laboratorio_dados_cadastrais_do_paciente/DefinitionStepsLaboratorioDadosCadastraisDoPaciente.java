package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_dados_cadastrais_do_paciente;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_Dados_Cadastrais_do_Paciente.LaboratorioDadosCadastraisDoPacienteSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioDadosCadastraisDoPaciente {
    @Steps
    LaboratorioDadosCadastraisDoPacienteSteps laboratorioDadosCadastraisDoPacienteSteps;

    @Quando("^clicar nos botões \"([^\"]*)\" na tela Dados Cadastrais do paciente no modulo Atende Laboratorio$")
    public void clicar_nos_botões_na_tela_Dados_Cadastrais_do_paciente_no_modulo_Atende_Laboratorio(String botoes) {
        laboratorioDadosCadastraisDoPacienteSteps.clicar_nos_botões_na_tela_Dados_Cadastrais_do_paciente_no_modulo_Atende_Laboratorio(botoes);
    }

}
