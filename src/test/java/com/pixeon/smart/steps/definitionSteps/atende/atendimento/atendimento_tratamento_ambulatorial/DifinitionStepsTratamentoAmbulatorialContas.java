package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_tratamento_ambulatorial;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_tratamento_ambulatorial.TratamentoAmbulatorialContasSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DifinitionStepsTratamentoAmbulatorialContas {

    @Steps
    TratamentoAmbulatorialContasSteps tratamentoAmbulatorialContasSteps;

    @Quando("^clicar na GuiaTis do modulo Atende$")
    public void clicar_na_GuiaTis_do_modulo_Atende() {
        tratamentoAmbulatorialContasSteps.clicar_na_GuiaTis_do_modulo_Atende();
    }

}