package com.pixeon.smart.steps.definitionSteps.atende.atendimento.formulario;


import com.pixeon.smart.steps.serenity.atende.atendimento.formulario.FormularioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFormulario {
    @Steps
    FormularioSteps formularioSteps;

    @Quando("^selecionar a teclas \"([^\"]*)\" do teclado$")
    public void selecionar_a_teclas_do_teclado(String teclasTeclado) {
        formularioSteps.selecionar_a_teclas_do_teclado(teclasTeclado);
    }
}
