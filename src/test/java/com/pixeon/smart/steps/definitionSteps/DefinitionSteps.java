package com.pixeon.smart.steps.definitionSteps;

import com.pixeon.smart.datamodel.DataDictionary;
import com.pixeon.smart.datamodel.DataWord;
import com.pixeon.smart.steps.serenity.EndUserSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import java.util.List;

public class DefinitionSteps {

    @Steps
    EndUserSteps anna;

    @Given("the user is on the Wikionary home page")
    public void givenTheUserIsOnTheWikionaryHomePage() {
        anna.is_the_home_page();
    }

    @When("^the user looks up the definition of the word$")
    public void the_user_looks_up_the_definition_of_the_word(List<DataWord> dataWord) {
    	anna.looks_for(dataWord);
    }

    @Then("^they should see the definition$")
    public void they_should_see_the_definition(List<DataDictionary> dataDefinition) {
        anna.should_see_definition(dataDefinition);
    }

}
