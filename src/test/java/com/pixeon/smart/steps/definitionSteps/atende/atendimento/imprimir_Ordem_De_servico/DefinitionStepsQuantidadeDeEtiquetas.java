package com.pixeon.smart.steps.definitionSteps.atende.atendimento.imprimir_Ordem_De_servico;

import com.pixeon.smart.steps.serenity.atende.atendimento.imprimir_Ordem_De_servico.QuantidadeDeEtiquetasSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsQuantidadeDeEtiquetas {
    @Steps
    QuantidadeDeEtiquetasSteps quantidadeDeEtiquetasSteps;

    @Quando("^informar a quantidade igual a \"([^\"]*)\" de etiqueta na tela quantidade de etiquetas$")
    public void informar_a_quantidade_igual_a_de_etiqueta_na_tela_quantidade_de_etiquetas(String quantidade) {
        quantidadeDeEtiquetasSteps.informar_a_quantidade_igual_a_de_etiqueta_na_tela_quantidade_de_etiquetas(quantidade);
    }

    @Quando("^Clicar no botão imprimir na tela quantidade de etiquetas$")
    public void clicar_no_botão_imprimir_na_tela_quantidade_de_etiquetas() {
        quantidadeDeEtiquetasSteps.clicar_no_botão_imprimir_na_tela_quantidade_de_etiquetas();
    }


    @Entao("^validar a quantidade de etiqueta gerada é \"([^\"]*)\" nome do Convênio \"([^\"]*)\" na etiqueta do paciente$")
    public void validar_a_quantidade_de_etiqueta_gerada_é_nome_do_Convênio_na_etiqueta_do_paciente(String quantidade, String nome_do_convenio) {
        quantidadeDeEtiquetasSteps.validar_a_quantidade_de_etiqueta_gerada_é_nome_do_Convênio_na_etiqueta_do_paciente(quantidade,nome_do_convenio);
    }


    @Quando("^Clicar no botão Ok da tela Quantidade de Etiqueta$")
    public void clicar_no_botão_Ok_da_tela_Quantidade_de_Etiqueta() {
        quantidadeDeEtiquetasSteps.clicar_no_botão_Ok_da_tela_Quantidade_de_Etiqueta();
    }

    @Quando("^Clicar no botão Cancelar da tela Quantidade de Etiqueta$")
    public void clicar_no_botão_Cancelar_da_tela_Quantidade_de_Etiqueta() {
        quantidadeDeEtiquetasSteps.clicar_no_botão_Cancelar_da_tela_Quantidade_de_Etiqueta();
    }

}
