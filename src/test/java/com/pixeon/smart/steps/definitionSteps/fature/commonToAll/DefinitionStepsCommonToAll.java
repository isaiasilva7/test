package com.pixeon.smart.steps.definitionSteps.fature.commonToAll;

import com.pixeon.smart.steps.serenity.fature.commonToAll.CommonToAllSteps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCommonToAll {
	
	 @Steps
	CommonToAllSteps commonToAllStepsFature;

	
	@Dado("^que tenho acesso a aplicação Smart Fature$")
	public void que_tenho_acesso_a_aplicação_Smart_Fature() {
		commonToAllStepsFature.que_tenho_acesso_a_aplicação_Smart_Fature();
	}


}
