package com.pixeon.smart.steps.definitionSteps.atende.atendimento.imprimir_Ordem_De_servico;

import com.pixeon.smart.steps.serenity.atende.atendimento.imprimir_Ordem_De_servico.ImprimirOrdemDeServicoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImprimirOrdemDeServico {
    @Steps
    ImprimirOrdemDeServicoSteps imprimirOrdemDeServicoSteps;

    @Quando("^clicar no botão \"([^\"]*)\" na tela imprirmir ordem de Serviço$")
    public void clicar_no_botão_na_tela_imprirmir_ordem_de_Serviço(String nomeDoBotao) {
        imprimirOrdemDeServicoSteps.clicar_no_botão_na_tela_imprirmir_ordem_de_Serviço(nomeDoBotao);
    }


    @Quando("^clicar no botão \"([^\"]*)\" na tela Atendimento Ambulatorial$")
    public void clicar_no_botão_na_tela_Atendimento_Ambulatorial$(String nomeDoBotao) {
        imprimirOrdemDeServicoSteps.clicar_no_botão_na_tela_imprirmir_ordem_de_Serviço(nomeDoBotao);
    }



    @Quando("^clinar no botão quantidade de etiquetas na tela de imprimir  Ordem de servico$")
    public void clinar_no_botão_quantidade_de_etiquetas_na_tela_de_imprimir_Ordem_de_servico() {
        imprimirOrdemDeServicoSteps.clinar_no_botão_quantidade_de_etiquetas_na_tela_de_imprimir_Ordem_de_servico();
    }


   @Quando("^Clicar no botão Ok da tela imprimir ordem de serviço$")
  public void clicar_no_botão_Ok_da_tela_impimir_ordem_de_serviço() {
     imprimirOrdemDeServicoSteps.clicar_no_botão_Ok_da_tela_impimir_ordem_de_serviço();
  }


    @Quando("^Clicar no botão Cancelar da tela imprimir ordem de serviço$")
    public void clicar_no_botão_Cancelar_da_tela_imprimir_ordem_de_serviço() {
        imprimirOrdemDeServicoSteps.clicar_no_botão_Cancelar_da_tela_imprimir_ordem_de_serviço();
   }


    @Quando("^clicar no botão Relatorio internacao da tela impimir ordem de serviço$")
    public void clicar_no_botão_Relatorio_internacao_da_tela_impimir_ordem_de_serviço() {
        imprimirOrdemDeServicoSteps.clicar_no_botão_Relatorio_internacao_da_tela_impimir_ordem_de_serviço();
    }


}
