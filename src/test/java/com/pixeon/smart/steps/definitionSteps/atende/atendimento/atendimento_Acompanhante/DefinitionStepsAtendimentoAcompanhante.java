package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Acompanhante;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Acompanhante.AtendimentoAcompanhanteSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoAcompanhante {

    @Steps
    AtendimentoAcompanhanteSteps atendimentoAcompanhanteSteps;


    @Quando("^informar o parentesco \"([^\"]*)\" na aba acompanhate$")
    public void informar_o_parentesco_na_aba_acompanhate(String tipoDeParentesco) {
        atendimentoAcompanhanteSteps.informar_o_parentesco_na_aba_acompanhate(tipoDeParentesco);
    }


    @Quando("^informar o nome do Acompanhate \"([^\"]*)\" na aba acompanhate$")
    public void informar_o_nome_do_Acompanhate_na_aba_acompanhate(String nomeDoAcompanhante) {
        atendimentoAcompanhanteSteps.informar_o_nome_do_Acompanhate_na_aba_acompanhate(nomeDoAcompanhante);
    }

    @Quando("^informar a ultima data de Comparecimento \"([^\"]*)\" na aba acompanhate$")
    public void informar_a_ultima_data_de_Comparecimento_na_aba_acompanhate(String ultimaDataCompareciemtno) {
        atendimentoAcompanhanteSteps.informar_a_ultima_data_de_Comparecimento_na_aba_acompanhate(ultimaDataCompareciemtno);
    }

    @Quando("^informar a Observação \"([^\"]*)\" na aba acompanhate$")
    public void informar_a_Observação_na_aba_acompanhate(String observacao) {
        atendimentoAcompanhanteSteps.informar_a_Observação_na_aba_acompanhate(observacao);
    }


}
