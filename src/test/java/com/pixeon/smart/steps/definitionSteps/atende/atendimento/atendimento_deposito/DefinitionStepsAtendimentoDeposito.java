package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_deposito;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_deposito.AtendimentoDepositoSteps;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoDeposito {

    @Steps
    AtendimentoDepositoSteps atendimentoDepositoSteps;

    @Quando("^clicar no botão Novo na tela de Deposito$")
    public void clicar_no_botão_Novo_na_tela_de_Deposito() {
        atendimentoDepositoSteps.clicar_no_botão_Novo_na_tela_de_Deposito();
    }

    @Então("^confirmar que o valor do Deposito é \"([^\"]*)\"$")
    public void confirmar_que_o_valor_do_Deposito_é(String valorDeposito) {
        atendimentoDepositoSteps.confirmar_que_o_valor_do_Deposito_é(valorDeposito);
    }


    @Quando("^clicar no botão Devolver na tela de Deposito$")
    public void clicar_no_botão_Devolver_na_tela_de_Deposito() {
        atendimentoDepositoSteps.clicar_no_botão_Devolver_na_tela_de_Deposito();
    }

    @Quando("^clicar no botão Visualizar na tela de Deposito$")
    public void clicar_no_botão_Visualizar_na_tela_de_Deposito() {
        atendimentoDepositoSteps.clicar_no_botão_Visualizar_na_tela_de_Deposito();
    }

    @Quando("^clicar no botão Cancelar na tela de Deposito$")
    public void clicar_no_botão_Cancelar_na_tela_de_Deposito() {
        atendimentoDepositoSteps.clicar_no_botão_Cancelar_na_tela_de_Deposito();
    }

    @Quando("^clicar no botão Fechar na tela de Deposito$")
    public void clicar_no_botão_Fechar_na_tela_de_Deposito() {
        atendimentoDepositoSteps.clicar_no_botão_Fechar_na_tela_de_Deposito();
    }

}
