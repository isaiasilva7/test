package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Convenio_segundaria_do_paciente;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Convenio_segundaria_do_paciente.AtendimentoConvenioSecundarioSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.util.List;

public class DefinitionStepsAtendimentoConvenioSecundario {

    @Steps
    AtendimentoConvenioSecundarioSteps atendimentoConvenioSecundarioSteps;

    @Quando("^informar o convênio Secundario \"([^\"]*)\" na aba convenio secundário$")
    public void informar_o_convênio_Secundario_na_aba_convenio_secundário(String nomeDoConvenioSecundario ) {
        atendimentoConvenioSecundarioSteps.informar_o_convênio_Secundario_na_aba_convenio_secundário(nomeDoConvenioSecundario);
    }

    @Quando("^informar o Titularidade \"([^\"]*)\" na aba convenio secundário$")
    public void informar_o_Titularidade_na_aba_convenio_secundário(String titularidade) {
        atendimentoConvenioSecundarioSteps.informar_o_Titularidade_na_aba_convenio_secundário(titularidade);
    }


    @Quando("^informar a matricula secundária \"([^\"]*)\" na aba convenio secundário$")
    public void informar_a_matricula_secundária_na_aba_convenio_secundário(String matriculaSecundaria) {
        atendimentoConvenioSecundarioSteps.informar_a_matricula_secundária_na_aba_convenio_secundário(matriculaSecundaria);
    }


    @Quando("^informar a data de Validade \"([^\"]*)\" na aba convenio secundário$")
    public void informar_a_data_de_Validade_na_aba_convenio_secundário(String dataValidade) {
        atendimentoConvenioSecundarioSteps.informar_a_data_de_Validade_na_aba_convenio_secundário(dataValidade);
    }

    @Quando("^informar o plano \"([^\"]*)\" na aba convenio secundário$")
    public void informar_o_plano_na_aba_convenio_secundário(String plano) {
        atendimentoConvenioSecundarioSteps.informar_o_plano_na_aba_convenio_secundário(plano);
    }

    @Quando("^informar o numero de matricula na empresa \"([^\"]*)\" na aba convenio secundário$")
    public void informar_o_numero_de_matricula_na_empresa_na_aba_convenio_secundário(String matriculaNaEmpresa) {
        atendimentoConvenioSecundarioSteps.informar_o_numero_de_matricula_na_empresa_na_aba_convenio_secundário(matriculaNaEmpresa);
    }

    @Quando("^informar o locação \"([^\"]*)\" na aba convenio secundário$")
    public void informar_o_locação_na_aba_convenio_secundário(String locacao) {
        atendimentoConvenioSecundarioSteps.informar_o_locação_na_aba_convenio_secundário(locacao);
    }

    @Quando("^informar a chavesline \"([^\"]*)\" na aba convenio secundário$")
    public void informar_a_chavesline_na_aba_convenio_secundário(String chaveSLine) {
        atendimentoConvenioSecundarioSteps.informar_a_chavesline_na_aba_convenio_secundário(chaveSLine);
    }
}
