package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_Ordem_de_servicos_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_Ordem_de_servicos_Steps.LaboratorioOrdemDeServicosSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioOrdemDeServicos {
    @Steps
    LaboratorioOrdemDeServicosSteps laboratorioOrdemDeServicosSteps;

    @Quando("^informar dados do serviço da Primeira linha tipo \"([^\"]*)\", Código \"([^\"]*)\" , material \"([^\"]*)\" no modulo Atende Laboratorio$")
    public void informar_dados_do_serviço_da_Primeira_linha_tipo_Código_material_no_modulo_Atende_Laboratorio(String tipo, String codigo, String material) {
        laboratorioOrdemDeServicosSteps.informar_dados_do_serviço_da_Primeira_linha_tipo_Código_material_no_modulo_Atende_Laboratorio(tipo,codigo,material);
    }


    /*
    @Quando("^clicar no botão Arquivos, no modulo Atende Laboratorio$")
    public void clicar_no_botão_Arquivos_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_Arquivos_no_modulo_Atende_Laboratorio();
    }


    @Quando("^clicar no botão Questionarios, no modulo Atende Laboratorio$")
    public void clicar_no_botão_Questionarios_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_Questionarios_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão Obs, no modulo Atende Laboratorio$")
    public void clicar_no_botão_Obs_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_Obs_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão reprogramar, no modulo Atende Laboratorio$")
    public void clicar_no_botão_reprogramar_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_reprogramar_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão deposito, no modulo Atende Laboratorio$")
    public void clicar_no_botão_deposito_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_deposito_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão pagamento, no modulo Atende Laboratorio$")
    public void clicar_no_botão_pagamento_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_pagamento_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão info, no modulo Atende Laboratorio$")
    public void clicar_no_botão_info_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_info_no_modulo_Atende_Laboratorio();
    }
*/

    @Quando("^capturo o valor do serviço \"([^\"]*)\" no modulo Atende Laboratorio$")
    public void capturo_o_valor_do_serviço_no_modulo_Atende_Laboratorio(String capturaValor) {
        laboratorioOrdemDeServicosSteps.capturo_o_valor_do_serviço_no_modulo_Atende_Laboratorio(capturaValor);
    }


    @Entao("^validar o valor do exame \"([^\"]*)\" na tela ordem de serviço do paciente no modulo Atende Laboratorio$")
    public void validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_modulo_Atende_Laboratorio(String valdiarValor) {
        laboratorioOrdemDeServicosSteps.validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_modulo_Atende_Laboratorio(valdiarValor);
    }


    @Quando("^clicar no botão \"([^\"]*)\", na tela ordem de Servico no modulo Atende Laboratorio$")
    public void clicar_no_botão_na_tela_ordem_de_Servico_no_modulo_Atende_Laboratorio(String nomeDoBotao) {
        laboratorioOrdemDeServicosSteps.clicar_no_botão_na_tela_ordem_de_Servico_no_modulo_Atende_Laboratorio(nomeDoBotao);
    }


    @Entao("^validar que o status da fatura esta como Faturado na tela ordem de servico, no modulo Atende Laboratorio$")
    public void validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico_no_modulo_Atende_Laboratorio() {
        laboratorioOrdemDeServicosSteps.validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico_no_modulo_Atende_Laboratorio();
    }

}
