package com.pixeon.smart.steps.definitionSteps.atende.atendimento.questionario;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.steps.serenity.atende.atendimento.questionario.QuestionarioSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsQuestionario {
    @Steps
    QuestionarioSteps questionarioSteps;

    @Quando("^clicar no botão iniciar da tela de questionario$")
    public void clicar_no_botão_iniciar_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_iniciar_da_tela_de_questionario();
    }


    @Quando("^Clicar no botão proximo da tela de questionario$")
    public void clicar_no_botão_proximo_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_proximo_da_tela_de_questionario();
    }

    @Quando("^Clicar no botão concluir da tela de questionario$")
    public void clicar_no_botão_concluir_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_concluir_da_tela_de_questionario();

    }

    @Quando("^clicar no botão Cancelar da tela de questionario$")
    public void clicar_no_botão_Cancelar_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_Cancelar_da_tela_de_questionario();
    }


    @Quando("^clicar no botão Novo da tela de questionario$")
    public void clicar_no_botão_Novo_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_Novo_da_tela_de_questionario();
    }


    @Quando("^informar o nome do questionario \"([^\"]*)\" na tela de questionario$")
    public void informar_o_nome_do_questionario_na_tela_de_questionario(String nomeDOQuestinario) {
        questionarioSteps.informar_o_nome_do_questionario_na_tela_de_questionario(nomeDOQuestinario);
    }

    @Quando("^informar a resposta \"([^\"]*)\" na  tela de questionario$")
    public void informar_a_resposta_na_tela_de_questionario(String resposta) {
        questionarioSteps.informar_a_resposta_na_tela_de_questionario(resposta);
    }

    @Quando("^clicar no botão Concluir da tela de questionario$")
    public void clicar_no_botão_Concluir_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_concluir_da_tela_de_questionario();
    }

    @Quando("^selecionar a lupa para visulaizar da tela de questionario$")
    public void selecionar_a_lupa_para_visulaizar_da_tela_de_questionario() {
        questionarioSteps.selecionar_a_lupa_para_visulaizar_da_tela_de_questionario();
    }

    @Quando("^clicar no botão impimir  da tela de questionario$")
    public void clicar_no_botão_impimir_da_tela_de_questionario() {
        questionarioSteps.clicar_no_botão_impimir_da_tela_de_questionario();
    }

    @Quando("^validar resposta do questionario na tela de impressão$")
    public void validar_resposta_do_questionario_na_tela_de_impressão() {
        questionarioSteps.validar_resposta_do_questionario_na_tela_de_impressão();
    }

}
