package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_guias;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_guias.AtendimentoGuiasSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoGuias {

    @Steps
    AtendimentoGuiasSteps atendimentoGuiasSteps;

    @Quando("^informar o número da Guia \"([^\"]*)\" na tela de Guias$")
    public void informar_o_número_da_Guia_na_tela_de_Guias(String numeroDaGuia) {
        atendimentoGuiasSteps.informar_o_número_da_Guia_na_tela_de_Guias(numeroDaGuia);
    }

    @Quando("^informar o Data Início \"([^\"]*)\" na tela de Guias$")
    public void informar_o_Data_Início_na_tela_de_Guias(String dataInicio) {
        atendimentoGuiasSteps.informar_o_Data_Início_na_tela_de_Guias(dataInicio);
    }

    @Quando("^informar o número de Dias \"([^\"]*)\" na tela de Guias$")
    public void informar_o_número_de_Dias_na_tela_de_Guias(String numeroDeDias) {
        atendimentoGuiasSteps.informar_o_número_de_Dias_na_tela_de_Guias(numeroDeDias);
    }

    @Quando("^informar o Data Fim \"([^\"]*)\" na tela de Guias$")
    public void informar_o_Data_Fim_na_tela_de_Guias(String dataFim) {
        atendimentoGuiasSteps.informar_o_Data_Fim_na_tela_de_Guias(dataFim);
    }

    @Quando("^informar a senha \"([^\"]*)\" na tela de Guias$")
    public void informar_a_senha_na_tela_de_Guias(String senha) {
        atendimentoGuiasSteps.informar_a_senha_na_tela_de_Guias(senha);
    }

    @Quando("^informar o Setor Executante \"([^\"]*)\" na tela de Guias$")
    public void informar_o_Setor_Executante_na_tela_de_Guias(String setorExecutante) {
        atendimentoGuiasSteps.informar_o_Setor_Executante_na_tela_de_Guias(setorExecutante);
    }

    @Quando("^informar o convenio \"([^\"]*)\" na tela de Guias$")
    public void informar_o_convenio_na_tela_de_Guias(String convenio) {
        atendimentoGuiasSteps.informar_o_convenio_na_tela_de_Guias(convenio);
    }

    @Quando("^Clicar no botão Gerar OS na tela de Guias$")
    public void clicar_no_botão_Gerar_OS_na_tela_de_Guias() {
        atendimentoGuiasSteps.clicar_no_botão_Gerar_OS_na_tela_de_Guias();
    }

    @Quando("^Clicar no botão Exibir Apenas Não Vencidos na tela de Guias$")
    public void clicar_no_botão_Exibir_Apenas_Não_Vencidos_na_tela_de_Guias() {
        atendimentoGuiasSteps.clicar_no_botão_Exibir_Apenas_Não_Vencidos_na_tela_de_Guias();
    }

    @Quando("^Clicar no botão Proced na tela de Guias$")
    public void clicar_no_botão_Proced_na_tela_de_Guias() {
        atendimentoGuiasSteps.clicar_no_botão_Proced_na_tela_de_Guias();
    }

}
