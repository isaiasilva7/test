package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_recebimento_Direto_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_recebimento_Direto_Steps.LaboratorioRecebimentoDiretoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioRecebimentoDireto {
    @Steps
    LaboratorioRecebimentoDiretoSteps laboratorioRecebimentoDiretoSteps;

    @Quando("^selecionar a forma de pagamento \"([^\"]*)\" da tela de Recebimento Direto, no modulo Atende Laboratorio$")
    public void selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String formaDePagamento) {
        laboratorioRecebimentoDiretoSteps.selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(formaDePagamento);
    }

    @Quando("^clicar no botão OK da tela recebimento direto, no modulo Atende Laboratorio$")
    public void clicar_no_botão_OK_da_tela_recebimento_direto_no_modulo_Atende_Laboratorio() {
        laboratorioRecebimentoDiretoSteps.clicar_no_botão_OK_da_tela_recebimento_direto_no_modulo_Atende_Laboratorio();
    }

    @Entao("^validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto, no modulo Atende Laboratorio$")
    public void validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio() {
        laboratorioRecebimentoDiretoSteps.validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio();
    }


    @Quando("^informa Valor \"([^\"]*)\"  do forma de pagamento \"([^\"]*)\" , data de vencimento do cheque \"([^\"]*)\", agencia  \"([^\"]*)\",conta \"([^\"]*)\", numero do chegue \"([^\"]*)\", praça \"([^\"]*)\", titularidade \"([^\"]*)\", da tela de Recebimento Direto, no modulo Atende Laboratorio$")
    public void informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String valorPagamento, String fomraDePagamento, String dataVencimento, String agencia, String conta, String numeroDoChequeOuCartao, String praca, String titularidade) {
        laboratorioRecebimentoDiretoSteps.informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(valorPagamento,fomraDePagamento,dataVencimento,agencia,conta,numeroDoChequeOuCartao,praca,titularidade);
    }

    @Quando("^informa bandeira do cartão \"([^\"]*)\", conta Chegue \"([^\"]*)\", número  do cartão \"([^\"]*)\", data de validade \"([^\"]*)\" da tela de Recebimento Direto, no modulo Atende Laboratorio$")
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data) {
        laboratorioRecebimentoDiretoSteps.informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(bandeiraDoCartao,ContaChegue,numeroDoCartao,data);
    }


    @Quando("^informa Valor de \"([^\"]*)\" do pagamento \"([^\"]*)\" da tela de Recebimento Direto, no modulo Atende Laboratorio$")
    public void informa_Valor_de_do_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String valor, String formaDePagamento) {
        laboratorioRecebimentoDiretoSteps.informa_Valor_de_do_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(valor,formaDePagamento);
    }

}
