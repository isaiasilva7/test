package com.pixeon.smart.steps.definitionSteps.atende.atendimento.busca_paciente_por_OS;

import com.pixeon.smart.steps.serenity.atende.atendimento.busca_paciente_por_OS.BuscaPacientePorOsSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;


public class DefinitionStepsBuscaPacientePorOS {

    @Steps
    BuscaPacientePorOsSteps buscaPacientePorOsSteps;

    @Quando("^informor a numero de serie da OS \"([^\"]*)\" e confrimo a busca em informar o numero da OS modulo Atende$")
    public void informor_a_numero_de_serie_da_OS_e_confrimo_a_busca_em_informar_o_numero_da_OS_modulo_Atende(String numeroSerieOS) {
        buscaPacientePorOsSteps.informor_a_numero_de_serie_da_OS_e_confrimo_a_busca_em_informar_o_numero_da_OS_modulo_Atende(numeroSerieOS);
    }
}
