package com.pixeon.smart.steps.definitionSteps.atende.atendimento.devolucao;

import com.pixeon.smart.steps.serenity.atende.atendimento.Devolucao.DevolucaoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsDevolucao {
    @Steps
    DevolucaoSteps devolucaoSteps;


    @Quando("^clicar no botão OK da tela Devolução$")
    public void clicar_no_botão_OK_da_tela_Devolucao() {
        devolucaoSteps.clicar_no_botão_OK_da_tela_Devolucao();
    }

    @Quando("^informo o responsavel \"([^\"]*)\" e confirmo clicando no botão ok$")
    public void informo_o_responsavel_e_confirmo_clicando_no_botão_ok(String nomeDoRepsoanvel) {
        devolucaoSteps.informo_o_responsavel_e_confirmo_clicando_no_botão_ok(nomeDoRepsoanvel);
    }

}
