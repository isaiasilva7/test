package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Trasferir_Paciente_Para_Fila;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_trasferir_paciente_para_fila.AtendimentoTrasferirPacienteParaFilaSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoTrasferirPacienteParaFila {
    @Steps
    AtendimentoTrasferirPacienteParaFilaSteps atendimentoTrasferirPacienteParaFilaSteps;


    @Quando("^clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera$")
    public void clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera() {
        atendimentoTrasferirPacienteParaFilaSteps.clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera();
    }

    @Quando("^informo a Senha \"([^\"]*)\" da tela Adicionar paceinte a Fila de Espera$")
    public void informo_a_Senha_da_tela_Adicionar_paceinte_a_Fila_de_Espera(String senha) {
        atendimentoTrasferirPacienteParaFilaSteps.informo_a_Senha_da_tela_Adicionar_paceinte_a_Fila_de_Espera(senha);
    }

    @Quando("^informar a fila \"([^\"]*)\" na tela de trasfêrencia de paciente para a fila$")
    public void informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila(String fila) {
        atendimentoTrasferirPacienteParaFilaSteps.informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila(fila);
    }

    @Quando("^clicar no botão Transferir da tela trasferir paciente para a fila$")
    public void clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila() {
        atendimentoTrasferirPacienteParaFilaSteps.clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila();
    }




}


