package com.pixeon.smart.steps.definitionSteps.atende.pop_up;

import com.pixeon.smart.steps.serenity.atende.pop_up.PouUpSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsPouUp {
    @Steps
    PouUpSteps  pouUpSteps;

    @Quando("^clicar no botão \"([^\"]*)\" do PouP Concluir$")
    public void clicar_no_botão_do_PouP_Concluir(String nomeDoBotao) {
        pouUpSteps.clicar_no_botão_do_PouP_Concluir(nomeDoBotao);
    }

    @Entao("^o sistema não apresenta falha geral$")
    public void o_sistema_não_apresenta_falha_geral() {
        pouUpSteps.o_sistema_não_apresenta_falha_geral();
    }

    @Entao("^validar a mensagem \"([^\"]*)\" de obrigatoriedade de informação no campo no cadastro do paciente$")
    public void validar_a_mensagem_de_obrigatoriedade_de_informação_no_campo_no_cadastro_do_paciente(String mensagem_alerta) {
        pouUpSteps.validar_a_mensagem_de_obrigatoriedade_de_informação_no_campo_no_cadastro_do_paciente(mensagem_alerta);
    }

    @Quando("^clicar no botão \"([^\"]*)\" do PopUp$")
    public void clicar_no_botão_do_PopUp(String nomeDoBotão) {
        pouUpSteps.clicar_no_botão_do_PouP_Concluir(nomeDoBotão);
    }


    @Entao("^o sistema alerta através de PoPup que o campo é obrigatorio e clico botão ok$")
    public void o_sistema_alerta_através_de_PoPup_que_o_campo_é_obrigatorio_e_clico_botão_ok() {
        pouUpSteps.o_sistema_alerta_através_de_PoPup_que_o_campo_é_obrigatorio_e_clico_botão_ok();
    }

    @Quando("^informar a quantidade de parcela igual a \"([^\"]*)\" clicar no botão \"([^\"]*)\" da tela de quantidade$")
    public void informar_a_quantidade_de_parcela_igual_a_clicar_no_botão_da_tela_de_quantidade(String informaQuantidade, String nomeDobtao) {
        pouUpSteps.informar_a_quantidade_de_parcela_igual_a_clicar_no_botão_da_tela_de_quantidade(informaQuantidade,nomeDobtao);
    }
}
