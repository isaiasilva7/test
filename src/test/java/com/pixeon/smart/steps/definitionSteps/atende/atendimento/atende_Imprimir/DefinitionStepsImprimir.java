
package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atende_Imprimir;


import com.pixeon.smart.steps.serenity.atende.atendimento.atende_Imprimir.ImprimirSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsImprimir {

    @Steps
    ImprimirSteps imprimirSteps;

    @Quando("^clicar no botão impressora da tela de imprimir$")
    public void clicar_no_botão_impressora_da_tela_de_imprimir() {
        imprimirSteps.clicar_no_botão_impressora_da_tela_de_imprimir();
    }


    @Quando("^selecionar a impressora \"([^\"]*)\" e clicar no botão Ok da tela  Printer Setup$")
    public void selecionar_a_impressora_e_clicar_no_botão_Ok_da_tela_Printer_Setup(String nomeDaimpressora) {
        imprimirSteps.selecionar_a_impressora_e_clicar_no_botão_Ok_da_tela_Printer_Setup(nomeDaimpressora);
    }

    @Quando("^clicar no botão Ok da tela de impimir$")
    public void clicar_no_botão_Ok_da_tela_de_impimir() {
        imprimirSteps.clicar_no_botão_Ok_da_tela_de_impimir();
    }


}
