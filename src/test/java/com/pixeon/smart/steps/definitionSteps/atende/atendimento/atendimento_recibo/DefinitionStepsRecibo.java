package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_recibo;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_recibo.ReciboSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsRecibo {

    @Steps
    ReciboSteps reciboSteps;

    @Quando("^Selecionar o documento \"([^\"]*)\" na tela recebimento Direto$")
    public void selecionar_o_documento_na_tela_recebimento_Direto(String nomedoDocumento) {
        reciboSteps.selecionar_o_documento_na_tela_recebimento_Direto(nomedoDocumento);
    }

    @Quando("^clicar no botão Visualizar na tela de Recibo$")
    public void clicar_no_botão_Visualizar_na_tela_de_Recibo() {
        reciboSteps.clicar_no_botão_Visualizar_na_tela_de_Recibo();
    }


    @Quando("^clicar no botão visualizar da tela no modulo Atende$")
    public void clicar_no_botão_visualizar_da_tela_no_modulo_Atende() {
        reciboSteps.clicar_no_botão_Visualizar_na_tela_de_Recibo();
    }

}
