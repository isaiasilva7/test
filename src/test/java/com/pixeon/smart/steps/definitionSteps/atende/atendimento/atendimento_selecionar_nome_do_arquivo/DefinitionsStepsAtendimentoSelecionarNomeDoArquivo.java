package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_selecionar_nome_do_arquivo;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_selecionar_nome_do_arquivo.AtendimentoSelecionarNomeDoArquivoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsAtendimentoSelecionarNomeDoArquivo {
    @Steps
    AtendimentoSelecionarNomeDoArquivoSteps atendimentoSelecionarNomeDoArquivoSteps;


    @Quando("^informar o nome do arquivo Teste Automatizado e clicar em Salvar na tela de Selecione o nome do arquivo$")
    public void informar_o_nome_do_arquivo_Teste_Automatizado_e_clicar_em_Salvar_na_tela_de_Selecione_o_nome_do_arquivo() {
        atendimentoSelecionarNomeDoArquivoSteps.informar_o_nome_do_arquivo_Teste_Automatizado_e_clicar_em_Salvar_na_tela_de_Selecione_o_nome_do_arquivo();
    }

    @Entao("^confirmar  dados valor \"([^\"]*)\", nome do paciente,número da OS no arquivo com o formato \"([^\"]*)\"$")
    public void confirmar_dados_valor_nome_do_paciente_número_da_OS_no_arquivo_com_o_formato(String valorPagmento, String formatoDoArquivo) {
        atendimentoSelecionarNomeDoArquivoSteps.confirmar_dados_valor_nome_do_paciente_número_da_OS_no_arquivo_com_o_formato(valorPagmento,formatoDoArquivo);
    }

    @Entao("^Confirmar que o nome do paciente, a data esta no arquivo com o formato \"([^\"]*)\"$")
    public void confirmar_que_o_nome_do_paciente_a_data_esta_no_arquivo_com_o_formato(String formatoDoArquivo) {
        atendimentoSelecionarNomeDoArquivoSteps.confirmar_que_o_nome_do_paciente_a_data_esta_no_arquivo_com_o_formato(formatoDoArquivo);
    }


    @Quando("^informar o nome do arquivo \"([^\"]*)\" e clicar em \"([^\"]*)\" na tela de Selecione o nome do arquivo$")
    public void informar_o_nome_do_arquivo_e_clicar_em_na_tela_de_Selecione_o_nome_do_arquivo(String enderecoDoArquivo, String nomeDoBotao) {
        atendimentoSelecionarNomeDoArquivoSteps.informar_o_nome_do_arquivo_e_clicar_em_na_tela_de_Selecione_o_nome_do_arquivo(enderecoDoArquivo,nomeDoBotao);
    }

    @Quando("^Escolho o formato do arquivo \"([^\"]*)\" e clico no botão \"([^\"]*)\", na tela de Selecionar o formato$")
    public void escolho_o_formato_do_arquivo_e_clico_no_botão_na_tela_de_Selecionar_o_formato(String formatoDoArquivo, String nomeDobotao) {
        atendimentoSelecionarNomeDoArquivoSteps.escolho_o_formato_do_arquivo_e_clico_no_botão_na_tela_de_Selecionar_o_formato(formatoDoArquivo,nomeDobotao);
    }

    @Então("^Confirmo que os dados \"([^\"]*)\" estão no arquivo como o formato \"([^\"]*)\"$")
    public void confirmo_que_os_dados_estão_no_arquivo_como_o_formato(String dados, String formatoDoArquivo) {
        atendimentoSelecionarNomeDoArquivoSteps.confirmo_que_os_dados_estão_no_arquivo_como_o_formato(dados,formatoDoArquivo);
    }

}
