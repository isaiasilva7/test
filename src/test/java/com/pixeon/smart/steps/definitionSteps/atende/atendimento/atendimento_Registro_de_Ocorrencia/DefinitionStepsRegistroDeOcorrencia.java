package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Registro_de_Ocorrencia;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Registro_de_Ocorrencia.RegistroDeOcorrenciaSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsRegistroDeOcorrencia {

    @Steps
    RegistroDeOcorrenciaSteps registroDeOcorrenciaSteps;



    @Quando("^informar a origem \"([^\"]*)\", a unidade \"([^\"]*)\", o setor \"([^\"]*)\", a categoria \"([^\"]*)\", o tipo \"([^\"]*)\" e a descrição \"([^\"]*)\", na tela Registro de Ocorrencia$")
    public void informar_a_origem_a_unidade_o_setor_a_categoria_o_tipo_e_a_descrição_na_tela_Registro_de_Ocorrencia(String origem, String unidade, String setor, String categoria, String tipo, String descricao) {
        registroDeOcorrenciaSteps.informar_a_origem_a_unidade_o_setor_a_categoria_o_tipo_e_a_descrição_na_tela_Registro_de_Ocorrencia(origem,unidade,setor,categoria,tipo,descricao);
    }

    @Quando("^clicar no botão Ok, na tela Registro de Ocorrencia$")
    public void clicar_no_botão_Ok_na_tela_Registro_de_Ocorrencia() {
        registroDeOcorrenciaSteps.clicar_no_botão_Ok_na_tela_Registro_de_Ocorrencia();
    }

    @Quando("^clicar no botão Anônimo, na tela Registro de Ocorrencia$")
    public void clicar_no_botão_Anônimo_na_tela_Registro_de_Ocorrencia() {
        registroDeOcorrenciaSteps.clicar_no_botão_Anônimo_na_tela_Registro_de_Ocorrencia();
    }

}
