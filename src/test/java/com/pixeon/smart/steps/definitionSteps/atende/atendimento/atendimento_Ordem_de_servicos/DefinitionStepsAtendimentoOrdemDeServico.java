package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Ordem_de_servicos;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Ordem_de_servicos.AtendimentoOrdemDeServicoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoOrdemDeServico {

    @Steps
    AtendimentoOrdemDeServicoSteps atendimentoOrdemDeServicoSteps;

    @Entao("^o sistema retorna o paciente o  paciente ROBO TESTE AUTOMATIZADO na tela de Ordem de serviço  na busca no modulo Atende$")
    public void o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_tela_de_Ordem_de_serviço_na_busca_no_modulo_Atende() {
        atendimentoOrdemDeServicoSteps.o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_tela_de_Ordem_de_serviço_na_busca_no_modulo_Atende();
    }

    @Quando("^informar dados do serviço \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\"$")
    public void informar_dados_do_serviço(String codigo, String quantidade, String nome_do_responsavel_coleta_material) {
        atendimentoOrdemDeServicoSteps.informar_dados_do_serviço(codigo,quantidade,nome_do_responsavel_coleta_material);
    }

    @Quando("^capturo o numero da OS$")
    public void capturo_o_numero_da_OS() {
        atendimentoOrdemDeServicoSteps.capturo_o_numero_da_OS();
    }
    @Quando("^capturo o valor do serviço \"([^\"]*)\"$")
    public void capturo_o_valor_do_serviço(String idetificacaoDoservico) {
        atendimentoOrdemDeServicoSteps.capturo_o_valor_do_serviço(idetificacaoDoservico);
    }

    @Entao("^validar o valor do exame \"([^\"]*)\" na tela ordem de serviço do paciente no módulo Atende$")
    public void validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_módulo_Atende(String valorExame) {
        atendimentoOrdemDeServicoSteps.validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_módulo_Atende(valorExame);
    }

    @Quando("^informar dados da OS, tipo Atendimento \"([^\"]*)\" e Setor \"([^\"]*)\"$")
    public void informar_dados_da_OS_tipo_Atendimento_e_Setor(String tipoAtendimento, String setor) {
        atendimentoOrdemDeServicoSteps.informar_dados_da_OS_tipo_Atendimento_e_Setor(tipoAtendimento,setor);
    }

    @Quando("^clicar no botão deposito tela de ordem de serviço$")
    public void clicar_no_botão_deposito_tela_de_ordem_de_serviço() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_deposito_tela_de_ordem_de_serviço();
    }

    @Quando("^clicar no botão Adiconar na Fila tela de Ordem de servico  no modulo Atende$")
    public void  clicar_no_botão_Adiconar_na_Fila_tela_de_Ordem_de_servico_no_modulo_Atende() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_Adiconar_na_Fila_tela_de_Ordem_de_servico_no_modulo_Atende();
    }

    @Quando("^clicar no botão Fila de Espera tela de Ordem de servico  no modulo Atende$")
    public void clicar_no_botão_Fila_de_Espera_tela_de_Ordem_de_servico_no_modulo_Atende() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_Fila_de_Espera_tela_de_Ordem_de_servico_no_modulo_Atende();
    }

    @Quando("^clicar no botão pagamento da tela ordem de servico$")
    public void clicar_no_botão_pagamento_da_tela_ordem_de_servico() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_pagamento_da_tela_ordem_de_servico();
    }

    @Entao("^validar que o status da fatura esta como Faturado na tela ordem de servico$")
    public void validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico() {
        atendimentoOrdemDeServicoSteps.validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico();
    }

    @Entao("^validar que o status da fatura esta como aberto na tela ordem de servico$")
    public void validar_que_o_status_da_fatura_esta_como_aberto_na_tela_ordem_de_servico() {
        atendimentoOrdemDeServicoSteps.validar_que_o_status_da_fatura_esta_como_aberto_na_tela_ordem_de_servico();
    }

    @Quando("^clicar botão Documento da tela oremdem de Serviço do modulo atende$")
    public void clicar_botão_Documento_da_tela_oremdem_de_Serviço_do_modulo_atende() {
        atendimentoOrdemDeServicoSteps.clicar_botão_Documento_da_tela_oremdem_de_Serviço_do_modulo_atende();
    }


    @Quando("^clicar botão anexar o documento da tela oremdem de Serviço do modulo atende$")
    public void clicar_botão_anexar_o_documento_da_tela_oremdem_de_Serviço_do_modulo_atende() {
        atendimentoOrdemDeServicoSteps.clicar_botão_anexar_o_documento_da_tela_oremdem_de_Serviço_do_modulo_atende();
    }

    @Quando("^clicar no botão atendimento da tela Ordem de servico$")
    public void clicar_no_botão_atendimento_da_tela_Ordem_de_servico() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_atendimento_da_tela_Ordem_de_servico();
    }

    @Quando("^clicar no botão registro de Ocorrencia na tela de Ordem de Serviço$")
    public void clicar_no_botão_registro_de_Ocorrencia_na_tela_de_Ordem_de_Serviço() {
        atendimentoOrdemDeServicoSteps. clicar_no_botão_registro_de_Ocorrencia_na_tela_de_Ordem_de_Serviço();
    }

    @Quando("^clicar no botão Ocorrencia, na tela de Ordem de Serviço$")
    public void clicar_no_botão_Ocorrencia_na_tela_de_Ordem_de_Serviço() {
        atendimentoOrdemDeServicoSteps.clicar_no_botão_Ocorrencia_na_tela_de_Ordem_de_Serviço();
    }


    @Quando("^informar dados do serviço da Primeira linha tipo \"([^\"]*)\", Código \"([^\"]*)\", Setor \"([^\"]*)\", na tela Ordem de serviços$")
    public void informar_dados_do_serviço_da_Primeira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        atendimentoOrdemDeServicoSteps.informar_dados_do_serviço_da_Primeira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(tipo,codigo,setor);
    }

    @Quando("^informar dados do serviço da Segunda linha tipo \"([^\"]*)\", Código \"([^\"]*)\", Setor \"([^\"]*)\", na tela Ordem de serviços$")
    public void informar_dados_do_serviço_da_Segunda_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        atendimentoOrdemDeServicoSteps.informar_dados_do_serviço_da_Segunda_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(tipo,codigo,setor);
    }


    @Quando("^informar dados do serviço Terceira linha  tipo \"([^\"]*)\", Código \"([^\"]*)\", Setor \"([^\"]*)\", na tela Ordem de serviços$")
    public void informar_dados_do_serviço_Terceira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        atendimentoOrdemDeServicoSteps.informar_dados_do_serviço_Terceira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(tipo,codigo,setor);
    }

    @Quando("^seleciono o Serviço via tecla de atalho \"([^\"]*)\" do teclado$")
    public void seleciono_o_Serviço_via_tecla_de_atalho_do_teclado(String telclaAtalho) {
        atendimentoOrdemDeServicoSteps.seleciono_o_Serviço_via_tecla_de_atalho_do_teclado(telclaAtalho);
    }
}
