package com.pixeon.smart.steps.definitionSteps;

import com.pixeon.smart.steps.serenity.CalculadoraSteps;
import cucumber.api.java.es.Dado;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCalculadora {

    @Steps
    CalculadoraSteps calculadoraSteps;

    @Dado("^que estou na calculadora$")
    public void que_estou_na_calculadora() {
        calculadoraSteps.validaCalculadoraAberta();
    }

    @Dado("^que a calculadora está zerada$")
    public void que_a_calculadora_está_zerada() {
        calculadoraSteps.zeraCalculadora();
    }

    @Quando("^eu pressionar o numero \"([^\"]*)\"$")
    public void eu_pressionar_o_numero(String botao) {
        calculadoraSteps.pressionaBotao(botao);
    }

    @Quando("^e escolher operação de \"([^\"]*)\"$")
    public void e_escolher_operação_de(String botao) {
        calculadoraSteps.pressionaBotao(botao);
    }

    @Então("^eu devo visualizar o resultado \"([^\"]*)\"$")
    public void eu_devo_visualizar_o_resultado(String resultado) {
        calculadoraSteps.visualizaResultado(resultado);
    }






}
