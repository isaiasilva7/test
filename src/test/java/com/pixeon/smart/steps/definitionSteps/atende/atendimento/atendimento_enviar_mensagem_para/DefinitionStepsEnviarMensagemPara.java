package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_enviar_mensagem_para;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_enviar_mensagem_para.EnviarMensagemParaSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsEnviarMensagemPara {

    @Steps
    EnviarMensagemParaSteps enviarMensagemParaSteps;

    @Quando("^clicar no botão Ok, na tela Enviar mensagem para$")
    public void clicar_no_botão_Ok_na_tela_Enviar_mensagem_para() {
        enviarMensagemParaSteps.clicar_no_botão_Ok_na_tela_Enviar_mensagem_para();
    }
}
