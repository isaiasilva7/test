package com.pixeon.smart.steps.definitionSteps.fature.pre_fature;

import com.pixeon.smart.steps.serenity.fature.pre_fatura.AbaAmbulatorioSteps;

import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsFatureAbaAmbulatorio {
	
	@Steps
	AbaAmbulatorioSteps abaAmbulatorioSteps;
	
	@Quando("^clico na aba \"([^\"]*)\"$")
	public void clico_na_aba(String aba) {
		abaAmbulatorioSteps.clico_na_aba(aba);
	}

	@Quando("^busco pelo paciente \"([^\"]*)\"$")
	public void busco_pelo_paciente(String nome_paciente) {
		abaAmbulatorioSteps.busco_pelo_paciente(nome_paciente);		
	}

	@Quando("^clico no botão \"([^\"]*)\" e verifico o cabeçalho da pré-fatura$")
	public void clico_no_botão_e_verifico_o_cabeçalho_da_pré_fatura(String botao) {
		abaAmbulatorioSteps.clico_no_botão_e_verifico_o_cabeçalho_da_pré_fatura(botao);
	}
	
	@Quando("^clico no botão \"([^\"]*)\" para gerar uma nova pré-fatura$")
	public void clico_no_botão_para_gerar_uma_nova_pré_fatura(String btnNovo) {
		abaAmbulatorioSteps.clico_no_botão_para_gerar_uma_nova_pré_fatura(btnNovo);
	}
	
	@Quando("^seleciono o periodo atual de \"([^\"]*)\" a \"([^\"]*)\"$")
	public void seleciono_o_periodo_atual_de_a(String periodoInicial, String periodoFinal) {
		abaAmbulatorioSteps.seleciono_o_periodo_atual_de_a(periodoInicial, periodoFinal);
	}

	@Quando("^clico no botão \"([^\"]*)\" do módulo Fature$")
	public void clico_no_botão_do_módulo_Fature(String botaoGravar) {
		abaAmbulatorioSteps.clico_no_botão_do_módulo_Fature(botaoGravar);
	}
	
	@Entao("^nenhuma OS gerada no periodo é exibida$")
	public void nenhuma_OS_gerada_no_periodo_é_exibida() {
		abaAmbulatorioSteps.nenhuma_OS_gerada_no_periodo_é_exibida();
	}

	@Quando("^confirmo em \"([^\"]*)\" o processamento da pré-fatura$")
	public void confirmo_em_o_processamento_da_pré_fatura(String simProcessar) {
	   abaAmbulatorioSteps.confirmo_em_o_processamento_da_pré_fatura(simProcessar);
	}
	
	@Entao("^uma mensagem informando que nenhum item foi encontrado para esta pré-fatura é exibida$")
	public void uma_mensagem_informando_que_nenhum_item_foi_encontrado_para_esta_pré_fatura_é_exibida(String mensagem) {
		abaAmbulatorioSteps.uma_mensagem_informando_que_nenhum_item_foi_encontrado_para_esta_pré_fatura_é_exibida(mensagem);
	}
	
	@Quando("^seleciono um periodo de trinta dias de \"([^\"]*)\" a \"([^\"]*)\"$")
	public void seleciono_um_periodo_de_trinta_dias_de_a(String periodoInicial, String periodoFinal) {
		abaAmbulatorioSteps.seleciono_um_periodo_de_trinta_dias_de_a(periodoInicial, periodoFinal);  
	}

	@Entao("^visualizo uma lista de OS geradas no periodo pesquisado$")
	public void visualizo_uma_lista_de_OS_geradas_no_periodo_pesquisado() {
		abaAmbulatorioSteps.visualizo_uma_lista_de_OS_geradas_no_periodo_pesquisado();
	}
	

	@Entao("^a pré fatura é processada e uma mensagem informando que o \"([^\"]*)\" é exibido$")
	public void a_pré_fatura_é_processada_e_uma_mensagem_informando_que_o_é_exibido(String arg1) {
		abaAmbulatorioSteps.a_pré_fatura_é_processada_e_uma_mensagem_informando_que_o_é_exibido();
	}
	
	@Quando("^clico no botao \"([^\"]*)\" da aba ambulatorio da tela de pre-fature do modulo fature$")
	public void clico_no_botao_da_aba_ambulatorio_da_tela_de_pre_fature_do_modulo_fature(String nomeBotao) {
		abaAmbulatorioSteps.clico_no_botao_da_aba_ambulatorio_da_tela_de_pre_fature_do_modulo_fature(nomeBotao);
	}
	
	@Entao("^sou redirecionado para a aba de \"([^\"]*)\" onde visualizo a pré-fatura criada$")
	public void sou_redirecionado_para_a_aba_de_onde_visualizo_a_pré_fatura_criada(String abaLog) {
		abaAmbulatorioSteps.sou_redirecionado_para_a_aba_de_onde_visualizo_a_pré_fatura_criada();
	}

	@Quando("^informo a OS \"([^\"]*)\"$")
	public void informo_a_OS(String numeroOS) {
		abaAmbulatorioSteps.informo_a_OS(numeroOS);
	}

	@Quando("^clico no botao \"([^\"]*)\" da tela de informar numero da OS$")
	public void clico_no_botao_da_tela_de_informar_numero_da_OS(String botaoOk) {
		abaAmbulatorioSteps.clico_no_botao_da_tela_de_informar_numero_da_OS(botaoOk);
	}
	
}
