package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_motivo;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_motivo.MotivoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsMotivo {
    @Steps
    MotivoSteps motivoSteps;

    @Quando("^infarma o motivo \"([^\"]*)\" , a observação \"([^\"]*)\" e clicar no botão \"([^\"]*)\" da tela de Motivo$")
    public void infarma_o_motivo_a_observação_e_clicar_no_botão_da_tela_de_Motivo(String motivo, String observacao, String nomeDobotao) {
        motivoSteps.infarma_o_motivo_a_observação_e_clicar_no_botão_da_tela_de_Motivo(motivo,observacao,nomeDobotao);
    }

    
    
}
