package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_arquivos_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_arquivos_Steps.LaboratorioArquivosSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsLaboratorioArquivos {
    @Steps
    LaboratorioArquivosSteps laboratorioArquivosSteps;

    @Quando("^clicar botão anexar o documento da tela Arquivo no modulo Atende Laboratorio$")
    public void clicar_botão_anexar_o_documento_da_tela_Arquivo_no_modulo_Atende_Laboratorio() {
        laboratorioArquivosSteps.clicar_botão_anexar_o_documento_da_tela_Arquivo_no_modulo_Atende_Laboratorio();
    }

    @Quando("^clicar no botão fechar da tela Arquivo no modulo Atende Laboratorio$")
    public void clicar_no_botão_fechar_da_tela_Arquivo_no_modulo_Atende_Laboratorio() {
        laboratorioArquivosSteps.clicar_no_botão_fechar_da_tela_Arquivo_no_modulo_Atende_Laboratorio();
    }

}
