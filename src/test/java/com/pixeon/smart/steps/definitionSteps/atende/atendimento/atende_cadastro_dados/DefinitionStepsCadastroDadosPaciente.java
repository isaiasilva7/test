package  com.pixeon.smart.steps.definitionSteps.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados.CadastroDadosPacienteSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsCadastroDadosPaciente {

    @Steps
    CadastroDadosPacienteSteps cadastroDadosPacienteSteps;

    @Quando("^clicar nos botoes \"([^\"]*)\" na tela cadastro do paciente$")
    public void clicar_nos_botoes_na_tela_cadastro_do_paciente(String botao) {
        cadastroDadosPacienteSteps.clicar_nos_botoes_na_tela_cadastro_do_paciente(botao);
    }


}
