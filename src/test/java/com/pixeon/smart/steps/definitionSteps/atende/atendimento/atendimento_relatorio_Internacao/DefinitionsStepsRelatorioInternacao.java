package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_relatorio_Internacao;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Relatorio_Internacao.RelatorioInternacaoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsRelatorioInternacao {
    @Steps
    RelatorioInternacaoSteps relatorioInternacaoSteps;

    @Quando("^selecionar o relatorio Alta a pedido da tela de relatorio de Internacao$")
    public void selecionar_o_relatorio_Alta_a_pedido_da_tela_de_relatorio_de_Internacao() {
        relatorioInternacaoSteps.selecionar_o_relatorio_Alta_a_pedido_da_tela_de_relatorio_de_Internacao();
    }


    @Quando("^clicar no botão visualizar da tela de relatorio de Internacao$")
    public void clicar_no_botão_visualizar_da_tela_de_relatorio_de_Internacao() {
        relatorioInternacaoSteps.clicar_no_botão_visualizar_da_tela_de_relatorio_de_Internacao();
    }

}
