package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_Diligenciamento;

import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Diligenciamento.AtendimentoDiligenciamentoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsAtendimentoDiligenciamento {

    @Steps
    AtendimentoDiligenciamentoSteps atendimentoDiligenciamentoSteps;

    @Quando("^clicar na Aba Diligenciamento, na tela Diligenciamento$")
    public void clicar_na_Aba_Diligenciamento_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Diligenciamento_na_tela_Diligenciamento();
    }

    @Quando("^clicar na Aba Status, na tela Diligenciamento$")
    public void clicar_na_Aba_Status_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Status_na_tela_Diligenciamento();
    }

    @Quando("^clicar na Aba Rastrabilidade, na tela Diligenciamento$")
    public void clicar_na_Aba_Rastrabilidade_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Rastrabilidade_na_tela_Diligenciamento();
    }

    @Quando("^clicar na Aba Faturamento, na tela Diligenciamento$")
    public void clicar_na_Aba_Faturamento_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Faturamento_na_tela_Diligenciamento();
    }

    @Quando("^clicar no botão OK, na tela Diligenciamento$")
    public void clicar_no_botão_OK_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_no_botão_OK_na_tela_Diligenciamento();
    }

    @Quando("^clicar no botão Cancelar, na tela Diligenciamento$")
    public void clicar_no_botão_Cancelar_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_no_botão_Cancelar_na_tela_Diligenciamento();
    }

    @Quando("^clicar no botão Fechar, na tela Diligenciamento$")
    public void clicar_no_botão_Fechar_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_no_botão_Fechar_na_tela_Diligenciamento();
    }

    @Quando("^clicar na Aba Documento da OS, na tela Diligenciamento$")
    public void clicar_na_Aba_Documento_da_OS_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Documento_da_OS_na_tela_Diligenciamento();
    }

    @Quando("^clicar na Aba Comunicação WS da OS, na tela Diligenciamento$")
    public void clicar_na_Aba_Comunicação_WS_da_OS_na_tela_Diligenciamento() {
        atendimentoDiligenciamentoSteps.clicar_na_Aba_Comunicação_WS_da_OS_na_tela_Diligenciamento();
    }


    @Quando("^clicar no quando a link Xml de Envio em que a operação é \"([^\"]*)\"$")
    public void clicar_no_quando_a_link_Xml_de_Envio_em_que_a_operação_é(String operacao) {
        atendimentoDiligenciamentoSteps.clicar_no_quando_a_link_Xml_de_Envio_em_que_a_operação_é(operacao);
    }


    @Entao("^dá um click duplo foram do item e valdiar se o sistema não apresenta falha geral$")
    public void dá_um_click_duplo_foram_do_item_e_valdiar_se_o_sistema_não_apresenta_falha_geral() {
        atendimentoDiligenciamentoSteps.dá_um_click_duplo_foram_do_item_e_valdiar_se_o_sistema_não_apresenta_falha_geral();


    }

}
