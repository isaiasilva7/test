package com.pixeon.smart.steps.definitionSteps.atende.visualizacao_de_impressao;

import com.pixeon.smart.steps.serenity.atende.visualizacao_de_impressao.VisualizacaoDeImpressao;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Então;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsVisualizacaoDeImpressao {
    @Steps
    VisualizacaoDeImpressao visualizacaoDeImpressao;

    @Então("^validar a existencia do Guia de Serviço$")
    public void validar_a_existencia_do_Guia_de_Serviço() {
        visualizacaoDeImpressao.validar_a_existencia_do_Guia_de_Serviço();
    }

    @Então("^validar dados da guia tis \"([^\"]*)\"$")
    public void validar_dados_da_guia_tis(String param ){
        visualizacaoDeImpressao.validar_dados_da_guia_tis(param);
    }

    @Quando("^clicar no botão Salvar Como na tela de Visualização de impressão$")
    public void clicar_no_botão_Salvar_Como_na_tela_de_Visualização_de_impressão() {
        visualizacaoDeImpressao.clicar_no_botão_Salvar_Como_na_tela_de_Visualização_de_impressão();
    }

    @Quando("^clicar no botão imprimir da tela de visualização de impressão$")
    public void clicar_no_botão_imprimir_da_tela_de_visualização_de_impressão() {
        visualizacaoDeImpressao.clicar_no_botão_Imprimir_da_tela_de_visualização_de_impressão();
    }

    @Quando("^clicar no botão Fechar da tela de visualização de impressão$")
    public void clicar_no_botão_Fechar_da_tela_de_visualização_de_impressão() {
        visualizacaoDeImpressao.clicar_no_botão_Fechar_da_tela_de_visualização_de_impressão();
    }

    @Então("^confirmo a existencia do recibo na tela de visualização de impressão$")
    public void confirmo_a_existencia_do_recibo_na_tela_de_visualização_de_impressão() {
        visualizacaoDeImpressao.confirmo_a_existencia_do_recibo_na_tela_de_visualização_de_impressão();
    }

}
