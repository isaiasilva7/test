package com.pixeon.smart.steps.definitionSteps.atende.laboratorio.laboratorio_trasferir_paciente_para_fila_definitionSteps;

import com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_trasferir_paciente_para_fila_definitionSteps.LaboratorioTrasferirPacienteparaFilaSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionStepsLaboratorioTrasferirPacienteParaFila {
    @Steps
    LaboratorioTrasferirPacienteparaFilaSteps laboratorioTrasferirPacienteparaFilaSteps;

    @Quando("^informo a Senha \"([^\"]*)\" da tela Adicionar paciente a Fila de Espera no modulo Atende Laboratorio$")
    public void informo_a_Senha_da_tela_Adicionar_paciente_a_Fila_de_Espera_no_modulo_Atende_Laboratorio(String senha) {
        laboratorioTrasferirPacienteparaFilaSteps.informo_a_Senha_da_tela_Adicionar_paciente_a_Fila_de_Espera_no_modulo_Atende_Laboratorio(senha);
    }

    @Quando("^clicar em botão adicionar da tela Adicionar paceinte a Fila de Espera no modulo Atende Laboratorio$")
    public void clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera_no_modulo_Atende_Laboratorio() {
        laboratorioTrasferirPacienteparaFilaSteps.clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera_no_modulo_Atende_Laboratorio();
    }

    @Quando("^informar a fila \"([^\"]*)\" na tela de trasfêrencia de paciente para a fila no modulo Atende Laboratorio$")
    public void informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila_no_modulo_Atende_Laboratorio(String fila) {
        laboratorioTrasferirPacienteparaFilaSteps.informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila_no_modulo_Atende_Laboratorio(fila);
    }


    @Quando("^clicar no botão Transferir da tela trasferir paciente para a fila no modulo Atende Laboratorio$")
    public void clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila_no_modulo_Atende_Laboratorio() {
        laboratorioTrasferirPacienteparaFilaSteps.clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila_no_modulo_Atende_Laboratorio();
    }
}
