package com.pixeon.smart.steps.definitionSteps.atende.atendimento.atendimento_selecione_o_servico;


import com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_selecione_o_servico.AtendimentoSelecioneOServicoSteps;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

public class DefinitionsStepsAtendimentoSelecioneOServico {
    @Steps
    AtendimentoSelecioneOServicoSteps atendimentoSelecioneOServicoSteps;


    @Quando("^seleciono a busca por \"([^\"]*)\"  informo \"([^\"]*)\", confirmo través enter do telcado e  clico em OK$")
    public void seleciono_a_busca_por_informo_confirmo_través_enter_do_telcado_e_clico_em_OK(String busca, String texto) {
        atendimentoSelecioneOServicoSteps.seleciono_a_busca_por_informo_confirmo_través_enter_do_telcado_e_clico_em_OK(busca,texto);
    }
}
