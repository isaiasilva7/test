package com.pixeon.smart.steps.definitionSteps.atende.atendimento.recebimento_Direto;

import com.pixeon.smart.steps.serenity.atende.atendimento.recebimento_Direto.RecebimentoDiretoSteps;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Steps;

import java.sql.SQLOutput;

public class DefinitionStepsRecebimentoDireto {
    @Steps
    RecebimentoDiretoSteps recebimentoDiretoSteps;


    @Quando("^clicar no botão OK da tela recebimento direto$")
    public void clicar_no_botão_OK_da_tela_recebimento_direto() {
      recebimentoDiretoSteps.clicar_no_botão_OK_da_tela_recebimento_direto();
    }

    @Quando("^selecionar a forma de pagamento \"([^\"]*)\" da tela de Recebimento Direto$")
    public void selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto(String cboFormaDePagamento) {
        recebimentoDiretoSteps.selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto(cboFormaDePagamento);
    }

    @Entao("^validar que o botão cancelar Pagamento esta na tela  de Recebimento Direto$")
    public void validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto() {
        recebimentoDiretoSteps.validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto();
    }


    @Quando("^informo tipo de Deposito \"([^\"]*)\", valor total \"([^\"]*)\"$")
    public void informo_tipo_de_Deposito_valor_total(String tipoPagamento, String valorTotal) {
        recebimentoDiretoSteps.informo_tipoo_de_Deposito_valor_total(tipoPagamento,valorTotal);
    }

    @Quando("^informa bandeira do cartão \"([^\"]*)\", conta Chegue \"([^\"]*)\", número  do cartão \"([^\"]*)\", data de validade \"([^\"]*)\"$")
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data) {
        recebimentoDiretoSteps.informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade(bandeiraDoCartao, ContaChegue,numeroDoCartao, data);
    }

    @Quando("^informa Valor \"([^\"]*)\"  do forma de pagamento \"([^\"]*)\" , data de vencimento do cheque \"([^\"]*)\", agencia  \"([^\"]*)\",conta \"([^\"]*)\", numero do chegue \"([^\"]*)\", praça \"([^\"]*)\", titularidade \"([^\"]*)\"$")
    public void informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade(String valorCheque, String fomraDepagamento, String dataVencimentoCheque, String agencia, String conta, String numeroDoCheque, String praca, String titularidade) {
        recebimentoDiretoSteps.informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade(valorCheque,fomraDepagamento,dataVencimentoCheque,agencia,conta,numeroDoCheque,praca,titularidade);
    }

    @Quando("^informa Valor de \"([^\"]*)\" do pagamento \"([^\"]*)\"$")
    public void informa_Valor_de_do_pagamento(String valor, String formaDePagamento) {
        recebimentoDiretoSteps.informa_Valor_de_do_pagamento(valor,formaDePagamento);
    }


    @Quando("^clicar no botão Novo na tela de Recebimento Direto$")
    public void clicar_no_botão_Novo_na_tela_de_Recebimento_Direto() {
        recebimentoDiretoSteps.clicar_no_botão_Novo_na_tela_de_Recebimento_Direto();
    }


    @Quando("^clicar no botão Excluir na tela de Recebimento Direto$")
    public void clicar_no_botão_Excluir_na_tela_de_Recebimento_Direto() {
        recebimentoDiretoSteps.clicar_no_botão_Excluir_na_tela_de_Recebimento_Direto();
    }

    @Quando("^clicar no botão Fechar na tela de Recebimento Direto$")
    public void clicar_no_botão_Fechar_na_tela_de_Recebimento_Direto() {
        recebimentoDiretoSteps.clicar_no_botão_Fechar_na_tela_de_Recebimento_Direto();
    }


    @Quando("^clicar no botão cancelar pagamento na tela  de Recebimento Direto$")
    public void clicar_no_botão_cancelar_pagamento_na_tela_de_Recebimento_Direto() {
        recebimentoDiretoSteps.clicar_no_botão_cancelar_pagamento_na_tela_de_Recebimento_Direto();
    }

    @Quando("^informo o motivo \"([^\"]*)\" de canelamento , observação \"([^\"]*)\" e confirmo clicando no botão \"([^\"]*)\"$")
    public void informo_o_motivo_de_canelamento_observação_e_confirmo_clicando_no_botão(String motivo, String observacao, String nomebotao) {
        recebimentoDiretoSteps.informo_o_motivo_de_canelamento_observação_e_confirmo_clicando_no_botão(motivo,observacao, nomebotao);
    }

    @Quando("^clicar no botão Documento na tela recebimento Direto$")
    public void clicar_no_botão_Documento_na_tela_recebimento_Direto() {
        recebimentoDiretoSteps.clicar_no_botão_Documento_na_tela_recebimento_Direto();
    }


    @Quando("^clicar no botão \"([^\"]*)\" na tela de Recebimento direto$")
    public void clicar_no_botão_na_tela_de_Recebimento_direto(String nomeDoBotao) {
        recebimentoDiretoSteps.clicar_no_botão_na_tela_de_Recebimento_direto(nomeDoBotao);
    }


    @Quando("^informa bandeira do cartão \"([^\"]*)\", conta Chegue \"([^\"]*)\", número  do cartão \"([^\"]*)\", data de validade \"([^\"]*)\", e Rede NSU igual a \"([^\"]*)\"$")
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_e_Rede_NSU_igual_a(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data, String nsuRede) {
        recebimentoDiretoSteps.informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_e_Rede_NSU_igual_a(bandeiraDoCartao, ContaChegue,numeroDoCartao, data,nsuRede);
    }

    @Então("^confirmo que o número NSU rede esta em todas as parcelas$")
    public void confirmo_que_o_número_NSU_rede_esta_em_todas_as_parcelas() {
        recebimentoDiretoSteps.confirmo_que_o_número_NSU_rede_esta_em_todas_as_parcelas();
    }

}
