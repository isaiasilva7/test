package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_trasferir_paciente_para_fila;

import com.pixeon.smart.pages.atende.atendimento.atendimento_trasferir_paciente_para_fila.PageAtendimentoTrasferirPacienteParaFila;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;

public class AtendimentoTrasferirPacienteParaFilaSteps {
    PageAtendimentoTrasferirPacienteParaFila pageAtendimentoTrasferirPacienteParaFila;
    Utils utils;

    @Step
    public void clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera() {
        pageAtendimentoTrasferirPacienteParaFila.clickBtnAdicionarPacienteAFila();
    }
    @Step
    public void informo_a_Senha_da_tela_Adicionar_paceinte_a_Fila_de_Espera(String senha) {
		  if (senha.equals("senhaAleatoria")) {
	          String SenhaGeradaDaData;
	          SenhaGeradaDaData= (utils.getDataHoraMinutoSegundoEMilesimo().replace(":","")).replace(".","");
	           pageAtendimentoTrasferirPacienteParaFila.informarTxtSenha(SenhaGeradaDaData);
	       }else{
	           pageAtendimentoTrasferirPacienteParaFila.informarTxtSenha(senha);
	       }
    }
    @Step
    public void informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila(String fila) {
        pageAtendimentoTrasferirPacienteParaFila.informarTxtFila(fila);
    }

    @Step
    public void clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila() {
        pageAtendimentoTrasferirPacienteParaFila.clickBtnTransfeir();
    }



}
