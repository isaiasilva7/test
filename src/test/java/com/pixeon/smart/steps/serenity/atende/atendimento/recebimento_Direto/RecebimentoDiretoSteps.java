package com.pixeon.smart.steps.serenity.atende.atendimento.recebimento_Direto;



import com.pixeon.smart.pages.atende.atendimento.recebimento_Direto.PageRecebimentoDireto;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class RecebimentoDiretoSteps {
    PageRecebimentoDireto pageRecebimentoDireto;

    @Step
    public void clicar_no_botão_OK_da_tela_recebimento_direto() {
        pageRecebimentoDireto.clickBtnOK();
    }
    @Step
    public void selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto(String cboFormaDePagamento) {
      //  pageRecebimentoDireto.

        if (cboFormaDePagamento.equals("A Receber")){
            Utils.sleep(500);
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickCboAreceber();
        }else if (cboFormaDePagamento.equals("Boleto")||cboFormaDePagamento.equals("boleto")){
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickCboBoleto();
        }else if (cboFormaDePagamento.equals("Cheque")||cboFormaDePagamento.equals("cheque")){
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickCboCheque();
        }else if (cboFormaDePagamento.equals("Epécie")||cboFormaDePagamento.equals("Especie")){
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickcboEspece();
        }else if (cboFormaDePagamento.equals("Externo")||cboFormaDePagamento.equals("externo")){
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickCboExterno();
        }else if (cboFormaDePagamento.equals("Desconto")||cboFormaDePagamento.equals("Desconto")) {
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickCboDesconto();
        }else if (cboFormaDePagamento.equals("Cartão")||cboFormaDePagamento.equals("cartão")){
                pageRecebimentoDireto.clickcboFormaDePagamento();
                pageRecebimentoDireto.clickCboCartao();
        }else if (cboFormaDePagamento.equals("Imposto")||cboFormaDePagamento.equals("imposto")){
                pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickscrollFormaDEPagamento();
            pageRecebimentoDireto.clickscrollFormaDEPagamento();
               pageRecebimentoDireto.clickcboImposto();
        }else if (cboFormaDePagamento.equals("Nota Promissoria")||cboFormaDePagamento.equals("nota promissoria")) {
            pageRecebimentoDireto.clickcboFormaDePagamento();
            pageRecebimentoDireto.clickscrollFormaDEPagamento();
            pageRecebimentoDireto.clickscrollFormaDEPagamento();
            pageRecebimentoDireto.clickCboNotaPromissoria();
        }else if (cboFormaDePagamento.equals("Cheque pré Datado")||cboFormaDePagamento.equals("Cheque Pré-Datado")){
                pageRecebimentoDireto.clickcboFormaDePagamento();
                pageRecebimentoDireto.clickCboChequePreDatado();
        }else{
            Assert.assertFalse("Forma de pagamento não cadastrada",true);
        }
    }

    @Step
    public void validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto() {
        pageRecebimentoDireto.validarExistenciaBtnCancelarPagamento();
    }

    @Step
    public void informo_tipoo_de_Deposito_valor_total(String tipoPagamento, String valorTotal) {

        pageRecebimentoDireto.informaTxtTipo(tipoPagamento);
        pageRecebimentoDireto.informaTxtValorTotal(valorTotal);
    }

    @Step
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data) {
        pageRecebimentoDireto.informaTxtBandeiraDoCartao(bandeiraDoCartao);
        pageRecebimentoDireto.informaTxtConta(ContaChegue);
        pageRecebimentoDireto.informaTxtNumeroDoCartao(numeroDoCartao);
        pageRecebimentoDireto.informaTxtDataValidadeDoCartao(data);

    }

    @Step
    public void informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade(String valorPagamento, String formaDepagamento, String dataVencimento, String agencia, String conta, String numeroDoChequeOuCartao, String praca, String titularidade) {

        if (formaDepagamento.equals("Cheque")||formaDepagamento.equals("Cheque pré Datado")){
            pageRecebimentoDireto.informaTxtDataValidadeDoCartao(dataVencimento);
            pageRecebimentoDireto.informaTxtValorPagamento(valorPagamento);
            pageRecebimentoDireto.informaTxtAgenciaPagamento(agencia);
            pageRecebimentoDireto.informaTxtConta(conta);
            pageRecebimentoDireto.informaTxtnumeroCartaoCheque(numeroDoChequeOuCartao);
            pageRecebimentoDireto.informaTxtPraca(praca);
            pageRecebimentoDireto.selecionarCboTitularidade(titularidade);

        }else{
            Assert.assertFalse("Forma de pagamento não encontada",true);
        }
    }


    @Step
    public void informa_Valor_de_do_pagamento(String valor, String formaDePagamento) {

        if (formaDePagamento.equals("Externo")||formaDePagamento.equals("Eexterno")) {
            pageRecebimentoDireto.informaTxtValorPagamento(valor);

        }else if (formaDePagamento.equals("Desconto")||formaDePagamento.equals("Desconto")) {
            pageRecebimentoDireto.informaTxtValorPagamento(valor);
        } else if (formaDePagamento.equals("Imposto")||formaDePagamento.equals("imposto")) {
            pageRecebimentoDireto.informaTxtValorPagamento(valor);
        }else if (formaDePagamento.equals("Nota Promissoria")||formaDePagamento.equals("nota promissoria")) {
                pageRecebimentoDireto.informaTxtValorPagamento(valor);
        } else {
            Assert.assertFalse("Não foi encontrado o fomra de pagamento ", true);
        }

    }

    @Step
    public void clicar_no_botão_Novo_na_tela_de_Recebimento_Direto() {
        pageRecebimentoDireto.clickBtnNovo();

    }

    @Step
    public void clicar_no_botão_Excluir_na_tela_de_Recebimento_Direto() {
        pageRecebimentoDireto.clickBtnExcluir();
    }

    @Step
    public void clicar_no_botão_Fechar_na_tela_de_Recebimento_Direto() {
        pageRecebimentoDireto.clickBtnFechar();
    }

    @Step
    public void clicar_no_botão_cancelar_pagamento_na_tela_de_Recebimento_Direto() {
        pageRecebimentoDireto.clickBtnCancelarPagamento();
    }

    @Step
    public void informo_o_motivo_de_canelamento_observação_e_confirmo_clicando_no_botão(String motivo, String observacao, String nomebotao) {
        pageRecebimentoDireto.informaTxtMotivoCancelamento(motivo);
        pageRecebimentoDireto.informaTxtObservacaoMotivoCancelamento(observacao);

        if (nomebotao.equals("OK")||nomebotao.equals("ok")||nomebotao.equals("Ok")) {
            pageRecebimentoDireto.clickOkMotivoCancelamento();
        }else{
            Assert.assertFalse("botãonão cadastrado",true);
        }
    }

    @Step
    public void clicar_no_botão_Documento_na_tela_recebimento_Direto() {
        pageRecebimentoDireto.clickBtnDocumento();
    }


    @Step
    public void clicar_no_botão_na_tela_de_Recebimento_direto(String nomeBotao) {
        if (nomeBotao.equals("Parcelar")||nomeBotao.equals("parcelar")){
            pageRecebimentoDireto.clickBtnParcelar();
        }else  if (nomeBotao.equals("Aprovar")||nomeBotao.equals("aprovar")){
            pageRecebimentoDireto.clickBtnAprovar();
        }else{
            Assert.assertTrue("nome do botão não encontrado",false);
        }
    }

    @Step
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_e_Rede_NSU_igual_a(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data, String nsuRede) {
        pageRecebimentoDireto.informaTxtBandeiraDoCartao(bandeiraDoCartao);
        pageRecebimentoDireto.informaTxtConta(ContaChegue);
        pageRecebimentoDireto.informaTxtNumeroDoCartao(numeroDoCartao);
        pageRecebimentoDireto.informaTxtDataValidadeDoCartao(data);
        pageRecebimentoDireto.informaTxtNsuRede(nsuRede);
    }
    @Step
    public void confirmo_que_o_número_NSU_rede_esta_em_todas_as_parcelas() {
        if (pageRecebimentoDireto.validarValorNSUrede1().equals("00111")&& pageRecebimentoDireto.validarValorNSUrede2().equals("00111") &&pageRecebimentoDireto.validarValorNSUrede3().equals("00111")    ){
             Assert.assertTrue(true);

        }else{
            Assert.assertTrue("NSU não encontrado",false);
        }
    }

}
