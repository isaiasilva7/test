package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Fila_de_espera_recepcao_caixa;

import com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.atendimento.atendimento_fila_de_espera_recepcao_caixa.PageAtendimentoFilaDeEsperaRecepcaoCaixa;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.util.Utils.sleep;

public class AtendimentoFilaDeEsperaRecepcaoCaixaSteps {

    PageAtendimentoFilaDeEsperaRecepcaoCaixa pageAtendimentoFilaDeEsperaRecepcaoCaixa;

    @Step
    public void confiramr_que_o_panciente_foi_inserido_na_fila_de_espera() {
        try {
            sleep(2000);
             String pacienteInserido=pageAtendimentoFilaDeEsperaRecepcaoCaixa.capturaraTxtNomePacienteInserido();
             String nomePaciendte=   PageAtendimentoBuscaPaciente.nomePaciente;

             if (pacienteInserido.contains(nomePaciendte)){
                 Assert.assertTrue(nomePaciendte,true);
             }else{
                 Assert.assertTrue("Paciente não encontrado. "+nomePaciendte,false);
             }
        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e);
            Assert.assertTrue("",false);
        }
    }
    @Step
    public void clicar_no_botão_de_encerrar_atendimento() {
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
    }
    @Step
    public void informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento(String nome) {
        String nomePaciendte=  PageAtendimentoBuscaPaciente.nomePaciente;

        if (nome.equals("pacienteAtual")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.inserirTxtNome(nomePaciendte);
        }else{
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.inserirTxtNome(nome);
        }
    }
    @Step
    public void clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento() {
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnDetalhe();
    }

    @Step
    public void clinar_nos_botoes_da_tela_de_Fila_de_espera_recepção(String nomeDoBotao) {
        sleep(2000);
        if (nomeDoBotao.equals("btnChamarProximo")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnChamaProximo();
        }else if (nomeDoBotao.equals("btnChamarSelecionado")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnChamaSelecionado();
        }else if (nomeDoBotao.equals("btnEncerrarAtendimento")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
        }else if (nomeDoBotao.equals("btnDetalhes")) {
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnDetalhe();
        }else if (nomeDoBotao.equals("btnImprimir")) {
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnImprimir();
        }else if (nomeDoBotao.equals("btnAtualizar")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnAtualizarFila();
        }else if (nomeDoBotao.equals("btnFoto")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnFoto();
        }else if (nomeDoBotao.equals("btnCR")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnCR();
        }else if (nomeDoBotao.equals("btnHistorico")) {
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnHistorico();
        }else if (nomeDoBotao.equals("btnMarcacao")) {
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnMarcacao();
        }else if (nomeDoBotao.equals("btnRastraeabilidade")) {
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickbtnRastreabilidade();
        }else{
            Assert.assertFalse("botão não encontrado",true);
            System.out.println("botão não encontrado");
        }
    }

    @Step
    public void clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnTrasferencia();
    }
    @Step
    public void seleciono_medico_na_fila_da_tela_fila_de_espera(String medico) {
        sleep(1000);
        if (medico.equals("ABILIO")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickLbMedicoAbilio();
        }else if (medico.equals("IVISSON LOPES")){
            pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickLblMedicoIvissonLopes();
        } else{
            Assert.assertFalse("Médico não encontrado",true);
            System.out.println("Médico não encontrado");
        }
    }
    @Step
    public void clicar_no_botão_chamar_o_proximo_fila_de_Espera() {
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.clickBtnChamaProximo();
    }

    @Step
    public void informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera(String nomeDoMedico) {
        pageAtendimentoFilaDeEsperaRecepcaoCaixa.inserirFilaMedico(nomeDoMedico);
    }

}
