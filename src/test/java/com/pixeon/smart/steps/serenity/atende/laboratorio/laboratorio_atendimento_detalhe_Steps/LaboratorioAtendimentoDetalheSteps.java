package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_atendimento_detalhe_Steps;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_Atendimento_Detalhe.PageLaboratorioAtendimentoDetalhe;
import net.thucydides.core.annotations.Step;

public class LaboratorioAtendimentoDetalheSteps {
   PageLaboratorioAtendimentoDetalhe pageLaboratorioAtendimentoDetalhe;

   @Step
    public void clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes_no_modulo_Atende_Laboratorio() {
        pageLaboratorioAtendimentoDetalhe.clickBtnRetornarPraFila();
    }
}
