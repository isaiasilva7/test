package com.pixeon.smart.steps.serenity.atende.atendimento.imprimir_Ordem_De_servico;

import com.pixeon.smart.pages.atende.atendimento.imprimir_Ordem_De_servico.PageImprimirOrdemDeServico;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class ImprimirOrdemDeServicoSteps {
    PageImprimirOrdemDeServico pageImprimirOrdemDeServico;
    @Step
    public void clicar_no_botão_na_tela_imprirmir_ordem_de_Serviço(String nomeDoBotao) {
          if (nomeDoBotao.equals("Cancelar")||nomeDoBotao.equals("cancelar")){
              pageImprimirOrdemDeServico.clickBtnCancelar();
          }else if(nomeDoBotao.equals("Guia Tiss")||nomeDoBotao.equals("guia tiss")||nomeDoBotao.equals("Guia tiss")||nomeDoBotao.equals("Guia_Tiss")){
              pageImprimirOrdemDeServico.clickBtnGuiaTiss();
          }else if(nomeDoBotao.equals("Impressora")||nomeDoBotao.equals("impressora")){
              pageImprimirOrdemDeServico.clickBtnImpressora();
          }else if(nomeDoBotao.equals("Qt.De Etiquetas")||nomeDoBotao.equals("Quatidade De Etiquetas")||nomeDoBotao.equals("quatidade de etiquetas")||nomeDoBotao.equals("Quat de etiquetas")){
              pageImprimirOrdemDeServico.clickBtnQtdeDeEtiquetas();
          }else if(nomeDoBotao.equals("OK")||nomeDoBotao.equals("ok")||nomeDoBotao.equals("ok")){
              pageImprimirOrdemDeServico.clickbtnOK();
          }else if(nomeDoBotao.equals("RelatIntern")||nomeDoBotao.equals("Relat. Intern")||nomeDoBotao.equals("relat intern")){
              pageImprimirOrdemDeServico.clickBtnRelatIntern();
          }else if(nomeDoBotao.equals("Visualizar")||nomeDoBotao.equals("visualizar")){
              pageImprimirOrdemDeServico.clickBtnVisualizar();
          }else{
              System.out.println("Botão não encontrado");
              Assert.assertTrue(false);
          }
    }

    @Step
    public void clinar_no_botão_quantidade_de_etiquetas_na_tela_de_imprimir_Ordem_de_servico() {
        pageImprimirOrdemDeServico.clickBtnQtdeDeEtiquetas();
    }

    @Step
    public void clicar_no_botão_Ok_da_tela_impimir_ordem_de_serviço() {
        pageImprimirOrdemDeServico.clickbtnOK();
    }

    @Step
    public void clicar_no_botão_Cancelar_da_tela_imprimir_ordem_de_serviço() {
        pageImprimirOrdemDeServico.clickBtnCancelar();
    }

    @Step
    public void clicar_no_botão_Impressora_da_tela_imprimir_ordem_de_serviço() {
        pageImprimirOrdemDeServico.clickBtnImpressora();
    }

    @Step
    public void clicar_no_botão_Relatorio_internacao_da_tela_impimir_ordem_de_serviço() {
        pageImprimirOrdemDeServico.clickBtnRelatIntern();
    }
}
