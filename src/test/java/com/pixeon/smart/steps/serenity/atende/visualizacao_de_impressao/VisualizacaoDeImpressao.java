package com.pixeon.smart.steps.serenity.atende.visualizacao_de_impressao;

import com.pixeon.smart.pages.atende.visualizacao_de_impressao.PageVisualizacaoDeImpressao;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroOSAtende;
import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroSerieOSAtende;
import static com.pixeon.smart.util.Utils.sleep;

public class VisualizacaoDeImpressao {
    PageVisualizacaoDeImpressao pageVisualizacaoDeImpressao;

    @Step
    public void validar_a_existencia_do_Guia_de_Serviço() {
        pageVisualizacaoDeImpressao.validarAExistenciaDoTituloDaGuia();
    }

    @Step
    public void validar_dados_da_guia_tis(String param) {

        if (param.equals("Numero do prestador")){
             Utils.printPorRegiao(1120,1200,150,200,60,60,50);
             Utils.converteTextImagemEmTXT();
             sleep(1000);
             String query= "Select osm_ctle_cnv  from osm where osm_serie='"+numeroOSAtende+"' and osm_num='"+numeroSerieOSAtende+"'";
             String RetornoQuery= Utils.consultaBanco(query,"osm_ctle_cnv");
             String textoArquivo= Utils.lerArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\imagenTextoDinamica\\out.txt");
             if (textoArquivo.contains(RetornoQuery)){
                 Assert.assertTrue(true);
             }else{
                 Assert.assertTrue(false);
             }

        }else{
            Assert.assertTrue("item não identificado",true);
        }

    }

    @Step
    public void clicar_no_botão_Salvar_Como_na_tela_de_Visualização_de_impressão() {
        pageVisualizacaoDeImpressao.clickBtnSalvarComo();
    }
    @Step
    public void clicar_no_botão_Imprimir_da_tela_de_visualização_de_impressão() {
        pageVisualizacaoDeImpressao.clickBtnImprimir();
    }
    @Step
    public void clicar_no_botão_Fechar_da_tela_de_visualização_de_impressão() {
        pageVisualizacaoDeImpressao.clickBtnFechar();
    }
    @Step
    public void confirmo_a_existencia_do_recibo_na_tela_de_visualização_de_impressão() {
        pageVisualizacaoDeImpressao.validarLblRecibo();
    }

}
