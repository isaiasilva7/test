package com.pixeon.smart.steps.serenity.atende.atendimento.imprimir_Ordem_De_servico;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.atendimento.imprimir_Ordem_De_servico.PageQuantidadeDeEtiquetas;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;

public class QuantidadeDeEtiquetasSteps {
    PageQuantidadeDeEtiquetas pageQuantidadeDeEtiquetas;

    @Step
    public void informar_a_quantidade_igual_a_de_etiqueta_na_tela_quantidade_de_etiquetas(String quantidade) {
        pageQuantidadeDeEtiquetas.informarQuantidadeEtiquetas(quantidade);
    }

    @Step
    public void clicar_no_botão_imprimir_na_tela_quantidade_de_etiquetas() {
        Utils.deletarArquivosDeUmDiretorio("C:\\Smart\\servimp");
        Utils.deletarArquivosDeUmDiretorio("C:\\Smart\\servimp");
        pageQuantidadeDeEtiquetas.clickBtnImprimir();
    }


    @Step
    public void validar_a_quantidade_de_etiqueta_gerada_é_nome_do_Convênio_na_etiqueta_do_paciente(String quantidade, String nome_doConvenio) {
        Utils.abrirPrimeiroArquivoDaPasta("C:\\Smart\\servimp",quantidade, PageAtendimentoBuscaPaciente.nomePaciente);
        Utils.abrirPrimeiroArquivoDaPasta("C:\\Smart\\servimp",quantidade,nome_doConvenio);
    }

    @Step
    public void clicar_no_botão_Ok_da_tela_Quantidade_de_Etiqueta() {
        pageQuantidadeDeEtiquetas.clickBtnOK();
    }

    @Step
    public void clicar_no_botão_Cancelar_da_tela_Quantidade_de_Etiqueta() {
        pageQuantidadeDeEtiquetas.clickBtnCancelar();
    }
}
