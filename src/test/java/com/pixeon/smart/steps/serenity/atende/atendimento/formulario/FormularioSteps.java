package com.pixeon.smart.steps.serenity.atende.atendimento.formulario;


import com.pixeon.smart.pages.atende.atendimento.formulario.PageFormulario;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class FormularioSteps {

    PageFormulario pageFormulario;
    @Step
    public void selecionar_a_teclas_do_teclado(String teclasTeclado) {

        if (teclasTeclado.equals("Shit+Enter")){
            pageFormulario.clicarTecladoShiftEnter();
        }else{
            System.out.println("comando do teclado não encontrado");
            Assert.assertTrue(false);
        }
    }
}
