package com.pixeon.smart.steps.serenity.atende.atendimento.busca_paciente_por_OS;


import com.pixeon.smart.pages.atende.atendimento.busca_paciente_por_OS.PageBuscaPacientePorOS;
import net.thucydides.core.annotations.Step;

public class BuscaPacientePorOsSteps {

    PageBuscaPacientePorOS pageBuscaPacientePorOS;

    @Step
    public void informor_a_numero_de_serie_da_OS_e_confrimo_a_busca_em_informar_o_numero_da_OS_modulo_Atende(String numeroSerieOS) {
        pageBuscaPacientePorOS.inserirtxtImformarNumeroSerieDaOS(numeroSerieOS);
        pageBuscaPacientePorOS.clickBtnOK();
    }


}
