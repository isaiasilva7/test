package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Acompanhante;



import com.pixeon.smart.pages.atende.atendimento.atendimento_Acompanhante.PageAtendimentoAcompanhante;
import net.thucydides.core.annotations.Step;

public class AtendimentoAcompanhanteSteps {
    PageAtendimentoAcompanhante pageAtendimentoAcompanhante;

    @Step
    public void informar_o_parentesco_na_aba_acompanhate(String tipoDeParentesco) {
        pageAtendimentoAcompanhante.informarTxtParentesco(tipoDeParentesco);
    }

    @Step
    public void informar_o_nome_do_Acompanhate_na_aba_acompanhate(String nomeDoAcompanhante) {
        pageAtendimentoAcompanhante.informarTxtNomeDoAcompanhate(nomeDoAcompanhante);
    }

    @Step
    public void informar_a_ultima_data_de_Comparecimento_na_aba_acompanhate(String ultimaDataDeComparecimetno) {
        pageAtendimentoAcompanhante.informarTxtUltimoComparecimento(ultimaDataDeComparecimetno);
    }

    public void informar_a_Observação_na_aba_acompanhate(String observacao) {
        pageAtendimentoAcompanhante.informarTxtObservacao(observacao);
    }
}
