package com.pixeon.smart.steps.serenity.atende.pop_up;

import com.pixeon.smart.pages.atende.pop_up.PagePouUp;
import com.pixeon.smart.util.Utils;
import cucumber.api.java.bs.A;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.sql.SQLOutput;

public class PouUpSteps {
    PagePouUp pagePouUp;
    @Step
    public void clicar_no_botão_do_PouP_Concluir(String nomeDoBotao) {

        if (nomeDoBotao.equals("Sim")||nomeDoBotao.equals("sim")||nomeDoBotao.equals("SIM") ){
            pagePouUp.clicarBtnSim();
        }else if (nomeDoBotao.equals("Não")||nomeDoBotao.equals("não")||nomeDoBotao.equals("nao") ){
            pagePouUp.clicarBtnNao();
        }else if (nomeDoBotao.equals("Ok")||nomeDoBotao.equals("OK")||nomeDoBotao.equals("ok") ){
            pagePouUp.clickBtnOK();
        }else{
            System.out.println("Botão não encontrado");
            Assert.assertTrue(false);
        }
    }


    @Step
    public void o_sistema_não_apresenta_falha_geral() {
        pagePouUp.validarQueExistePopUpFlahaGeral();
    }

    @Step
    public void validar_a_mensagem_de_obrigatoriedade_de_informação_no_campo_no_cadastro_do_paciente(String mensagem_alerta) {

        if (mensagem_alerta.equals("Você deve preencher a data de nascimento do paciente")){
            pagePouUp.validarExistenciapopUpopUpObrigatoriedadeDataNascimento();
        }else if(mensagem_alerta.equals("paciente não gravado. Informe o sexo do paciente")){
            pagePouUp.validarExistenciapopUpObrigatoriedadeInformarSexo();
        }else if(mensagem_alerta.equals("O convênio do Paciente precisa ser informado")){
            pagePouUp.validarExistenciapopUpObrigatoriedadeCboConvenio();
        }else if(mensagem_alerta.equals("não é valido pra o campo Convênio")) {
            pagePouUp.validarExistenciapopUpDadosInvalidaParaConvenio();
        }else if(mensagem_alerta.equals("Digite M - Masculino ou Feminino")){
                pagePouUp.validarExistenciapopSexopInvalido();
        }else if(mensagem_alerta.equals("Data Invalida")){
            pagePouUp.validarExistenciapopUpDataInvalida();
        }else if(mensagem_alerta.equals("CPF Invalido")){
           pagePouUp.validarExistenciaPopUpCPFInvalido();
         }else if(mensagem_alerta.equals("Telefone Invalido")){
            pagePouUp.validarExistenciaPopUpTelefoneInvalido();
        }else {
            Assert.assertTrue("Mensagem não cadastrada", false);
        }
    }

    @Step
    public void medicwareclicar_no_botão_do_PopUp(String nomeDoBotao) {

        if (nomeDoBotao.equals("Sim")||nomeDoBotao.equals("sim") ){
            pagePouUp.clicarBtnSim();
        }else if (nomeDoBotao.equals("Não")||nomeDoBotao.equals("não")||nomeDoBotao.equals("nao") ){
            pagePouUp.clicarBtnNao();
        }else{
            System.out.println("Botão não encontrado");
            Assert.assertTrue(false);
        }
    }

    @Step
    public void o_sistema_alerta_através_de_PoPup_que_o_campo_é_obrigatorio_e_clico_botão_ok() {
       pagePouUp.validarExistenciaPopupAlertaCampoObrigatorio();
        pagePouUp.clickBtnOK();
    }

    // pop up Quantidade recebimento direto
    @Step
    public void informar_a_quantidade_de_parcela_igual_a_clicar_no_botão_da_tela_de_quantidade(String informaQuantidade, String nomeDobtao) {
        pagePouUp.informarTxtQuantidade(informaQuantidade);
            if (nomeDobtao.equals("OK")||nomeDobtao.equals("Ok")) {
                pagePouUp.clicarBtnOKQuantidade();
            }else if (nomeDobtao.equals("Cancelar")||nomeDobtao.equals("cancelar")){
                pagePouUp.clicarBtnCancelarQuantidade();
            }else{
                Assert.assertTrue("Botão não emcontrado",false);
            }
     }
}
