package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_deposito;

import com.pixeon.smart.pages.atende.atendimento.atendimento_deposito.PageAtendimentoDeposito;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class AtendimentoDepositoSteps {
PageAtendimentoDeposito pageAtendimentoDeposito;

    @Step
    public void clicar_no_botão_Novo_na_tela_de_Deposito() {
        pageAtendimentoDeposito.clickBtnNovo();
    }

    @Step
    public void clicar_no_botão_Devolver_na_tela_de_Deposito() {
        pageAtendimentoDeposito.clickBtnbtnDevolver();
    }

    @Step
    public void clicar_no_botão_Visualizar_na_tela_de_Deposito() {
        pageAtendimentoDeposito.clickBtnVisualizar();
    }

    @Step
    public void clicar_no_botão_Cancelar_na_tela_de_Deposito() {
        pageAtendimentoDeposito.clickBtnCancelar();
    }

    @Step
    public void clicar_no_botão_Fechar_na_tela_de_Deposito() {
        pageAtendimentoDeposito.clickBtnFechar();
    }

    @Step
    public void confirmar_que_o_valor_do_Deposito_é(String valorDeposito) {
        if (valorDeposito.equals("500")) {
            pageAtendimentoDeposito.confirmaValorDepositado500();
        }else if (valorDeposito.equals("")) {
            pageAtendimentoDeposito.confirmaValorDepositadoDevolvido();
        }else {
            Assert.assertTrue(" imagem não cadastrada",false);
        }

    }
}
