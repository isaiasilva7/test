package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_XML_de_Enviar_e_retorno;


import com.pixeon.smart.pages.atende.atendimento.atendimento_XML_de_Enviar_e_retorno.PageAtendimentoXmlDeEnvioERetorno;
import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroOSAtende;
import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroSerieOSAtende;
import static com.pixeon.smart.util.Utils.consultaBanco;

public class AtendimentoXmlDeEnvioERetornoSteps {

    PageCommonToAll pageCommonToAll;
    PageAtendimentoXmlDeEnvioERetorno pageAtendimentoXmlDeEnvioERetorno;
    @Step
    public void clicar_no_link_Xml_de_Envio_na_tela_xml_de_Envio_e_Retorno() {
        pageAtendimentoXmlDeEnvioERetorno.clickBtnLinkXmlDeEnvio();
    }
    @Step
    public void clicar_no_link_Xml_de_Retorno_na_tela_xml_de_Envio_e_Retorno() {
        pageAtendimentoXmlDeEnvioERetorno.clickBtnLinkXmlDeRetorno();
    }
    @Step
    public void clicar_no_botão_Salvar_como_na_tela_xml_de_Envio_e_Retorno() {
        pageAtendimentoXmlDeEnvioERetorno.clickBtnSalvarComo();
    }
    @Step
    public void clicar_no_botão_Copiar_como_na_tela_xml_de_Envio_e_Retorno() {
        pageAtendimentoXmlDeEnvioERetorno.clickBtnCopiar();
    }
    @Step
    public void confirmo_que_no_arquivo_xml_na_tag_no_parâmetro_contem_o_valor(String tag, String paramentro, String valor) {


        String retornoxml;
        String retornoxml1;

           // retornoxml1=pageCommonToAll.pegarTextoNoArquivoXML("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado.xml", "paciente","nome");
            retornoxml=pageCommonToAll.pegarTextoNoArquivoXML("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado.xml", tag,paramentro);

        if (valor.equals(retornoxml)){
            Assert.assertTrue(true);
        }else{
            Assert.assertTrue(false);
        }
    }


     @Step
     public void confirmar_o_arquivo_XML_de_retorno_contem(String texto) {

         String query= "SELECT NWA.nwa_xml FROM NWA WHERE NWA_ID=  (SELECT lwl_nwa_id_retorno FROM LWL WHERE  lwl_osm_serie="+numeroOSAtende+" and  LWL_OSM_NUM="+numeroSerieOSAtende+")";
         String retorno= consultaBanco(query,"nwa_xml");

        if (!(retorno.replace("\n","")).contains(texto.replace("'","\""))){
            Assert.assertTrue("texto não encontrado", false);
        }else{
            Assert.assertTrue("texto encontrado"+ texto, true);
        }

    }

    @Step
    public void clicar_no_quando_a_link_Xml_de_Envio_em_que_a_operação_é(String operacao) {

    }
}
