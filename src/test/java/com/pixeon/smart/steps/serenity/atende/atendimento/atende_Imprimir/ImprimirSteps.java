package com.pixeon.smart.steps.serenity.atende.atendimento.atende_Imprimir;


import com.pixeon.smart.pages.atende.atendimento.atende_Imprimir.PageImprimir;
import com.pixeon.smart.pages.atende.atendimento.atende_printer_Setup.PagePrinterSetup;
import net.thucydides.core.annotations.Step;

public class ImprimirSteps {
    PageImprimir pageImprimir;
    PagePrinterSetup pagePrinterSetup;

    @Step
    public void clicar_no_botão_impressora_da_tela_de_imprimir() {
        pageImprimir.informarbtnImpressora();
    }

    @Step
    public void selecionar_a_impressora_e_clicar_no_botão_Ok_da_tela_Printer_Setup(String nomeDaimpressora) {
        pagePrinterSetup.selecionarImpressora(nomeDaimpressora);
        pagePrinterSetup.informarBtnOK();
    }
    @Step
    public void clicar_no_botão_Ok_da_tela_de_impimir() {
        pageImprimir.informarBtnOK();
    }
}
