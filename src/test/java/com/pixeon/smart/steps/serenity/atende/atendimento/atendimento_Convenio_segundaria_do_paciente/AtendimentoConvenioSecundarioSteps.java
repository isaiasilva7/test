package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Convenio_segundaria_do_paciente;

import com.pixeon.smart.pages.atende.atendimento.atendimento_Convenio_segundaria_do_paciente.PageAtendimentoConvenioSecundario;
import net.thucydides.core.annotations.Step;
import static com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente.nomePaciente;


public class AtendimentoConvenioSecundarioSteps {
    PageAtendimentoConvenioSecundario pageAtendimentoConvenioSecundario;

    @Step
    public void informar_o_convênio_Secundario_na_aba_convenio_secundário(String nomeDoConvenioSecundario) {
        pageAtendimentoConvenioSecundario.informarTxtConvenioSecuncario(nomeDoConvenioSecundario);
    }

    @Step
    public void informar_o_Titularidade_na_aba_convenio_secundário(String titularidade) {
        if (titularidade.equals("mesmaTitualirade")){
            pageAtendimentoConvenioSecundario.informarTxtTitularConcenio(nomePaciente);
        }else{
            pageAtendimentoConvenioSecundario.informarTxtTitularConcenio(titularidade);
        }
    }

    @Step
    public void informar_a_matricula_secundária_na_aba_convenio_secundário(String matriculaSecundaria) {
        pageAtendimentoConvenioSecundario.informarTxtMatriculaNaEmpresa(matriculaSecundaria);
    }
    @Step
    public void informar_a_data_de_Validade_na_aba_convenio_secundário(String dataValidade) {
        pageAtendimentoConvenioSecundario.informarTxtDataDeVencimento(dataValidade);
    }
    @Step
    public void informar_o_plano_na_aba_convenio_secundário(String plano) {
        pageAtendimentoConvenioSecundario.informarTxtPlanoConvenio(plano);
    }
    @Step
    public void informar_o_numero_de_matricula_na_empresa_na_aba_convenio_secundário(String matriculaNaEmpresa) {
        pageAtendimentoConvenioSecundario.informarTxtMatriculaNaEmpresa(matriculaNaEmpresa);
    }
    @Step
    public void informar_o_locação_na_aba_convenio_secundário(String locacao) {
        pageAtendimentoConvenioSecundario.informarTxtLocacao(locacao);
    }
    @Step
    public void informar_a_chavesline_na_aba_convenio_secundário(String chaveSLine) {
        pageAtendimentoConvenioSecundario.informarTxtChaveSLine(chaveSLine);
    }
}
