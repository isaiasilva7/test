package com.pixeon.smart.steps.serenity.atende.atendimento.questionario;

import com.pixeon.smart.pages.atende.atendimento.questionario.PageQuestionario;
import net.thucydides.core.annotations.Step;

public class QuestionarioSteps {
 PageQuestionario pageQuestionario;

    @Step
    public void clicar_no_botão_iniciar_da_tela_de_questionario() {
        pageQuestionario.clickBtnIniciar();
    }

    @Step
    public void clicar_no_botão_proximo_da_tela_de_questionario() {
        pageQuestionario.clickBtnProximo();
    }

    @Step
    public void clicar_no_botão_concluir_da_tela_de_questionario() { pageQuestionario.clickBtnConcluir(); }

    @Step
    public void clicar_no_botão_Cancelar_da_tela_de_questionario() {
        pageQuestionario.clickBtnCancelar();
    }
    @Step
    public void clicar_no_botão_Novo_da_tela_de_questionario() {
        pageQuestionario.clickBtnNovo();
    }
    @Step
    public void informar_o_nome_do_questionario_na_tela_de_questionario(String nomeDOQuestinario) {
        pageQuestionario.informarTxtQuestionario(nomeDOQuestinario);
    }
    @Step
    public void informar_a_resposta_na_tela_de_questionario(String resposta) {
        pageQuestionario.informarTxtResposta(resposta);
    }
    @Step
    public void clicar_no_botão_Concluir_da_tela_de_questionario() {
        pageQuestionario.clickBtnConcluir();
    }
    @Step
    public void selecionar_a_lupa_para_visulaizar_da_tela_de_questionario() {
        pageQuestionario.clickBtnLupaVisualizacao();
    }
    @Step
    public void clicar_no_botão_impimir_da_tela_de_questionario() {
        pageQuestionario.clickBtnImprimir();
    }

    public void validar_resposta_do_questionario_na_tela_de_impressão() {
        pageQuestionario.validarAExistenciaDaRespostaDoQuestionario();
    }

}
