package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_Ordem_de_servicos_Steps;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.valorDoServico1Laboratorio;
import static com.pixeon.smart.util.Utils.sleep;

public class LaboratorioOrdemDeServicosSteps {
    PageLaboratorioOrdemDeServico pageLaboratorioOrdemDeServico;

    @Step
    public void informar_dados_do_serviço_da_Primeira_linha_tipo_Código_material_no_modulo_Atende_Laboratorio(String tipo, String codigo, String material) {
        if (!tipo.equals("")){
            pageLaboratorioOrdemDeServico.inserirTxtTipo1(tipo);
        }
        if (!codigo.equals("")){
            pageLaboratorioOrdemDeServico.inserirCodigoDoServico1(codigo);
        }
        if (!material.equals("")){
            pageLaboratorioOrdemDeServico.inserirMaterialDoServico1(material);
        }
    }

    /*
    @Step
    public void clicar_no_botão_Arquivos_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnArquivos();
    }

    @Step
    public void clicar_no_botão_Questionarios_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnQuestionario();
    }

    @Step
    public void clicar_no_botão_Obs_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnObs();
    }

    @Step
    public void clicar_no_botão_reprogramar_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnReprogramar();
    }

    @Step
    public void clicar_no_botão_deposito_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnDeposito();
    }

    @Step
    public void clicar_no_botão_pagamento_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnPagamento();
    }



    @Step
    public void clicar_no_botão_info_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.clicarBtnInfo();
    }

*/

    @Step
    public void capturo_o_valor_do_serviço_no_modulo_Atende_Laboratorio(String capturaValor) {
        if (capturaValor.equals("1")){
            pageLaboratorioOrdemDeServico.capturaValorDoServico1();
        }else {
            //  System.out.println("Servico não Identificado");
            Assert.assertTrue("valor serviço não Identificado",false);
        }
    }


    @Step
    public void validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_modulo_Atende_Laboratorio(String validarValor) {

        String valordoServicoCapturado1 = valorDoServico1Laboratorio;
        Utils.sleep(200);
        if (valordoServicoCapturado1.equals(validarValor)){
            Assert.assertTrue(true);
        }else{
            Assert.assertTrue(false);
        }

    }
    @Step
    public void clicar_no_botão_na_tela_ordem_de_Servico_no_modulo_Atende_Laboratorio(String nomeDoBotao) {

        sleep(200);
        if (nomeDoBotao.equals("Pagamento")||nomeDoBotao.equals("Pagamento")){
            pageLaboratorioOrdemDeServico.clicarBtnPagamento();
        }else if (nomeDoBotao.equals("btnArquivos")||nomeDoBotao.equals("Arquivos")){
            pageLaboratorioOrdemDeServico.clicarBtnArquivos();
        }else if (nomeDoBotao.equals("btnQuestionario")||nomeDoBotao.equals("Questionario")){
            pageLaboratorioOrdemDeServico.clicarBtnQuestionario();
        }else if (nomeDoBotao.equals("btnObs")||nomeDoBotao.equals("Obs")) {
            pageLaboratorioOrdemDeServico.clicarBtnObs();
        }else if (nomeDoBotao.equals("btnReprogramar")||nomeDoBotao.equals("Reprogramar")) {
            pageLaboratorioOrdemDeServico.clicarBtnReprogramar();
        }else if (nomeDoBotao.equals("btnDeposito")||nomeDoBotao.equals("Deposito")){
            pageLaboratorioOrdemDeServico.clicarBtnDeposito();
        }else if (nomeDoBotao.equals("btnPagamento")||nomeDoBotao.equals("Pagamento")){
            pageLaboratorioOrdemDeServico.clicarBtnPagamento();
        }else if (nomeDoBotao.equals("btnInfo")||nomeDoBotao.equals("Info")){
            pageLaboratorioOrdemDeServico.clicarBtnInfo();
        }else{
            Assert.assertFalse("botão não encontrado",true);
            //System.out.println("botão não encontrado");
        }
    }
    @Step
    public void validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico_no_modulo_Atende_Laboratorio() {
        pageLaboratorioOrdemDeServico.confirmaExistenciaDoCampo();
    }
}
