package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_busca_paciente_Steps;

import com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.laboratorio.laboratorio_busca_paciente.PagelaboratorioBuscaPaciente;

import com.pixeon.smart.util.SikuliUtil;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.util.Utils.sleep;

public class LaboratorioBuscaPacienteSteps {

    PagelaboratorioBuscaPaciente pagelaboratorioBuscaPaciente;
    SikuliUtil sikuliUtil;

    @Step
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        pagelaboratorioBuscaPaciente.clickBtnFilaDeEspera();
    }

    @Step
    public void informa_o_item_de_busca_e_busco_no_modulo_Atende_Laboratorio(String itemBusca) {
        pagelaboratorioBuscaPaciente.inserirtxtBuscaPaciente(itemBusca);
        sikuliUtil.buscaAtravesDeAtalho();
    }
    @Step
    public void confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende_Laboratorio() {
        pagelaboratorioBuscaPaciente.clickBtnSim();
    }
    @Step
    public void informar_dados_do_cadasto_do_paciente_no_modulo_Atende_Laboratorio(String sexo, String data_nascimento, String telefone, String convenio, String cPF, String nome_da_mae, String nome_social) {
        pagelaboratorioBuscaPaciente.preencheCampoCadastroPaciente(sexo,data_nascimento,telefone,convenio,cPF,nome_da_mae,nome_social);
    }
    @Step
    public void clicar_no_botão_Adicionar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        pagelaboratorioBuscaPaciente.clickBtnAdicionarNaFila();
    }

    @Step
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende_no_modulo_Atende_Laboratorio() {
        pagelaboratorioBuscaPaciente.clickBtnFilaDeEspera();
    }

    @Step
    public void deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende_Laboratorio() {
        pagelaboratorioBuscaPaciente.validarAConfirmacaoPacienteGravadoComSucesso();
    }
    @Step
    public void clicar_no_botão_na_tela_de_busca_no_modulo_Atende_Laboratorio(String nomeDoBotao) {

            if (nomeDoBotao.equals("btnBuscarPacientePorOS")||nomeDoBotao.equals("Buscar Paciente Por OS") ){
                pagelaboratorioBuscaPaciente.clicarBuscarPacientePorOS();
            }else if (nomeDoBotao.equals("btnTratAmbulatorial")||nomeDoBotao.equals("Trat. Ambulatorial")){
                pagelaboratorioBuscaPaciente.clickBtnTratamentoAmbulatorial();
            }else if (nomeDoBotao.equals("btnGuia")||nomeDoBotao.equals("Guias")){
                pagelaboratorioBuscaPaciente.clickBtnGuia();
            }else if (nomeDoBotao.equals("btnFilaDeEspera")||nomeDoBotao.equals("Fila De Espera")) {
                sleep(1000);
                pagelaboratorioBuscaPaciente.clickBtnFilaDeEspera();
            }else if (nomeDoBotao.equals("btnAtendimento")||nomeDoBotao.equals("Atendimento")) {
                sleep(1000);
                pagelaboratorioBuscaPaciente.clickBtnAtendimento();
            }else if (nomeDoBotao.equals("btnAdicionarNaFila")||nomeDoBotao.equals("Adicionar Na Fila")) {
                sleep(1000);
                pagelaboratorioBuscaPaciente.clickBtnAdicionarNaFila();
            }else if (nomeDoBotao.equals("btnLerCartao")||nomeDoBotao.equals("Ler Cartao")){
                pagelaboratorioBuscaPaciente.clickBtnlerCartao();
            }else if (nomeDoBotao.equals("btnBiometria")||nomeDoBotao.equals("Biometria")){
                pagelaboratorioBuscaPaciente.clickBtnBiometria();
            } else{
                Assert.assertFalse("botão não encontrado",true);
            }
    }

}
