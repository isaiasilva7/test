package com.pixeon.smart.steps.serenity.atende.menu;

import com.pixeon.smart.pages.atende.menu.PageMenu;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

public class MenuSteps {

    PageMenu pageMenu;

    @Step
    public void selecionarMenu(String menu){
        List<String> menuList = new ArrayList<>();
        menuList.add(menu);
   //    menuList.add(subMenu);

        pageMenu.clicarMenu(menuList);
    }
}
