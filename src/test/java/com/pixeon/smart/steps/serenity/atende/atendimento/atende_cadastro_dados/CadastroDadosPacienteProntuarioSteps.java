package com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados;



import com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados.PageCadastroDadosPacienteProntuario;
import com.pixeon.smart.util.Utils;
import cucumber.api.java.pt.Quando;
import net.thucydides.core.annotations.Step;


import static com.pixeon.smart.util.Utils.sleep;

public class CadastroDadosPacienteProntuarioSteps {
    PageCadastroDadosPacienteProntuario pageCadastroDadosPacienteProntuario;

    @Step
    public void confirmo_que_o_número_do_prontuario_não_foi_gerado_na_tela_de_cadasto_do_prontuario() {
        pageCadastroDadosPacienteProntuario.confirmarQueOpronuarioNaoFoiGerado();
    }
    @Step
    public void clicar_no_botão_prontuario_na_tela_de_cadasto_do_prontuario() {
        pageCadastroDadosPacienteProntuario.clickBtnProntuario();
    }
    @Step
    public void confirmo_que_o_número_do_prontuario_foi_gerado_na_tela_de_cadasto_do_prontuario() {
        pageCadastroDadosPacienteProntuario.confirmarQueOpronuarioFoiGerado();
    }

    @Step
    public void informar_o_Peso_na_tela_de_cadastro_do_Prontuario_modulo_atende(String pseo) {
        pageCadastroDadosPacienteProntuario.informarTxtPeso(pseo);
    }
    @Step
    public void informar_o_Alto_na_tela_de_cadastro_do_Prontuario_modulo_atende(String alta) {
        pageCadastroDadosPacienteProntuario.informarTxtAlta(alta);
    }
    @Step
    public void informar_o_Bairro_na_tela_de_cadastro_do_Prontuario_modulo_atende(String bairro) {
        pageCadastroDadosPacienteProntuario.informarTxtBairro(bairro);
    }

    @Step
    public void informar_o_CEP_na_tela_de_cadastro_do_Prontuario_modulo_atende(String cEP) {
        pageCadastroDadosPacienteProntuario.informarTxtCEP(cEP);
    }

    @Step
    public void informar_o_Estado_Civil_na_tela_de_cadastro_do_Prontuario_modulo_atende(String estadoCivil) {
        pageCadastroDadosPacienteProntuario.informarTxtEstadoCivil(estadoCivil);
    }

    @Step
    public void informar_o_Nome_da_Mãe_na_tela_de_cadastro_do_Prontuario_modulo_atende(String nomeDaMae) {
        pageCadastroDadosPacienteProntuario.informarTxtNomeDaMae(nomeDaMae);
    }

    @Step
    public void informar_o_Grupo_Sanguinio_na_tela_de_cadastro_do_Prontuario_modulo_atende(String grupoSanguineo) {
        pageCadastroDadosPacienteProntuario.informarTxtGrupoSanguinio(grupoSanguineo);
    }

    @Step
    public void informar_o_número_do_endereço_na_tela_de_cadastro_do_Prontuario_modulo_atende(String numeroDoEndereco) {
        pageCadastroDadosPacienteProntuario.informarTxtNumeroEndereco(numeroDoEndereco);
    }
    @Step
    public void informar_o_email_na_tela_de_cadastro_do_Prontuario_modulo_atende(String email) {
        pageCadastroDadosPacienteProntuario.informarTxtEmail(email);
    }
    @Step
    public void informar_o_Médico_na_tela_de_cadastro_do_Prontuario_modulo_atende(String medico) {
        pageCadastroDadosPacienteProntuario.informarTxtMedico(medico);
    }
    @Step
    public void informar_o_RG_na_tela_de_cadastro_do_Prontuario_modulo_atende(String rG) {

        if (rG.equals("")){
            pageCadastroDadosPacienteProntuario.informarTxtRG((Utils.getDataHoraMinutoSegundoEMilesimo().replace(":","")).replace(".",""));
        }else {
            pageCadastroDadosPacienteProntuario.informarTxtRG(rG);
        }
    }

    @Step
    public void informar_o_Telefone_Celular_na_tela_de_cadastro_do_Prontuario_modulo_atende(String TelefoneCelular) {
        pageCadastroDadosPacienteProntuario.informarTxtCelular(TelefoneCelular);
    }

    @Step
    public void informar_o_Observação_na_tela_de_cadastro_do_Prontuario_modulo_atende(String observacao) {
        pageCadastroDadosPacienteProntuario.informarTxtObservacao(observacao);
    }
}
