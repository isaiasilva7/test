package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Relatorio_Internacao;


import com.pixeon.smart.pages.atende.atendimento.atendimento_Relatorio_Internacao.PageRelatorioInternacao;
import net.thucydides.core.annotations.Step;

public class RelatorioInternacaoSteps {
    PageRelatorioInternacao pageRelatorioInternacao;
    @Step
    public void selecionar_o_relatorio_Alta_a_pedido_da_tela_de_relatorio_de_Internacao() {
        pageRelatorioInternacao.clickLblAltaDoPedido();
    }

    @Step
    public void clicar_no_botão_visualizar_da_tela_de_relatorio_de_Internacao() {
        pageRelatorioInternacao.clickBtnVisualizar();
    }

}
