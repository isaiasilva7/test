package com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados;


import com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados.PageDadosComplementares;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;

public class DadosComplementaresSteps {
    PageDadosComplementares pageDadosComplementares;

    @Step
    public void informar_nome_do_Pai_na_tela_de_dados_Complementares(String nomeDoPai) {


        if (nomeDoPai.equals("nomePaiAleatorio")) {
            Utils utils =new Utils();
            pageDadosComplementares.informarTxtNomeDoPai("Pai" + utils.nomeAleatorio(5) + " " + utils.nomeAleatorio(5) + " " + utils.nomeAleatorio(4) );
        }else {
            pageDadosComplementares.informarTxtNomeDoPai(nomeDoPai);
        }
    }
    @Step
    public void informar_Apelido_na_tela_de_dados_Complementares(String apelido) {
        pageDadosComplementares.informarTxtApelido(apelido);
    }
    @Step
    public void informar_Religião_na_tela_de_dados_Complementares(String religiao) {
        pageDadosComplementares.informarTxtReligiao(religiao);
    }
    @Step
    public void informar_Cor_na_tela_de_dados_Complementares(String cor) {
        pageDadosComplementares.informarTxtCor(cor);
    }
    @Step
    public void informar_RG_na_tela_de_dados_Complementares(String rg) {
        pageDadosComplementares.informarTxtRG(rg);
    }
    @Step
    public void informar_CEP_na_tela_de_dados_Complementares(String cep) {
        pageDadosComplementares.informarTxtCEP(cep);
    }
    @Step
    public void informar_Número_endereco_na_tela_de_dados_Complementares(String numeroEndereco) {
        pageDadosComplementares.informarTxtNumeroEndereco(numeroEndereco);
    }
    @Step
    public void informar_bairro_na_tela_de_dados_Complementares(String bairro) {
        pageDadosComplementares.informarTxtbairro(bairro);
    }
}
