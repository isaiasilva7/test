package com.pixeon.smart.steps.serenity;

import com.pixeon.smart.pages.PageCalculadora;
import net.thucydides.core.annotations.Step;

import static junit.framework.TestCase.assertTrue;

public class CalculadoraSteps {

    PageCalculadora pageCalculadora;

    @Step
    public void validaCalculadoraAberta() {
        assertTrue(pageCalculadora.validaCalculadoraAberta());
    }

    @Step
    public void zeraCalculadora() {
        pressionaBotao("ce");
    }

    @Step
    public void pressionaBotao(String botao) {
        pageCalculadora.clicaBotao(botao);
    }

    @Step
    public void visualizaResultado(String resultado) {
        assertTrue(pageCalculadora.validaResultado(resultado));
    }
}