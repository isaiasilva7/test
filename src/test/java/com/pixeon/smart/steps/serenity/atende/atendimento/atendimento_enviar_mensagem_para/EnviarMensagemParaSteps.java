package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_enviar_mensagem_para;


import com.pixeon.smart.pages.atende.atendimento.atendimento_enviar_mensagem_para.PageEnviarMensagemPara;
import net.thucydides.core.annotations.Step;

public class EnviarMensagemParaSteps {
    com.pixeon.smart.pages.atende.atendimento.atendimento_enviar_mensagem_para.PageEnviarMensagemPara PageEnviarMensagemPara;

    @Step
    public void clicar_no_botão_Ok_na_tela_Enviar_mensagem_para() {
        PageEnviarMensagemPara.clickBtnOK();
    }

}
