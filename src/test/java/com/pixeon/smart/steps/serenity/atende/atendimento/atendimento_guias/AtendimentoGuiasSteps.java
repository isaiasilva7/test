package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_guias;



import com.pixeon.smart.pages.atende.atendimento.atendimento_guias.PageAtendimentoGuias;
import net.thucydides.core.annotations.Step;
import org.sikuli.script.Key;

public class AtendimentoGuiasSteps {
    PageAtendimentoGuias pageAtendimentoGuias;
    @Step
    public void informar_o_número_da_Guia_na_tela_de_Guias(String numeroDaGuia) {
        pageAtendimentoGuias.informarTxtNumeroDaGuia(numeroDaGuia);
    }

    @Step
    public void informar_o_Data_Início_na_tela_de_Guias(String datInicial) {
        pageAtendimentoGuias.informarTxtDataInicio(datInicial);
    }
    @Step
    public void informar_o_número_de_Dias_na_tela_de_Guias(String numeroDeDias) {
        pageAtendimentoGuias.informarTxtNumeroDDias(numeroDeDias+ Key.TAB);
    }

    @Step
    public void informar_o_Data_Fim_na_tela_de_Guias(String dataFim) {
        pageAtendimentoGuias.informarTxtDataFim(dataFim);
    }
    @Step
    public void informar_a_senha_na_tela_de_Guias(String senha) {
        pageAtendimentoGuias.informarTxtSenha(senha);
    }
    @Step
    public void informar_o_Setor_Executante_na_tela_de_Guias(String setorExecutante) {
        pageAtendimentoGuias.informarTxtSetorExecutante(setorExecutante);
    }
    @Step
    public void informar_o_convenio_na_tela_de_Guias(String convenio) {
        pageAtendimentoGuias.informarTxtConvenio(convenio);
    }
    @Step
    public void clicar_no_botão_Gerar_OS_na_tela_de_Guias() {
        pageAtendimentoGuias.clickBtnGerarOS();
    }
    @Step
    public void clicar_no_botão_Exibir_Apenas_Não_Vencidos_na_tela_de_Guias() {
        pageAtendimentoGuias.clickBtnxibirApenasNaoVencido();
    }
    @Step
    public void clicar_no_botão_Proced_na_tela_de_Guias() {
        pageAtendimentoGuias.clickBtnProcedPac();
    }
}
