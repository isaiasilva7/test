package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_trasferir_paciente_para_fila_definitionSteps;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_trasferir_paciente_para_fila.PageLaboratorioTrasferirPacienteParaFila;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;

public class LaboratorioTrasferirPacienteparaFilaSteps {
    PageLaboratorioTrasferirPacienteParaFila pageLaboratorioTrasferirPacienteParaFila;
    Utils utils;

    public void informo_a_Senha_da_tela_Adicionar_paciente_a_Fila_de_Espera_no_modulo_Atende_Laboratorio(String senha) {
        if (senha.equals("senhaAleatoria")) {
            String SenhaGeradaDaData;
            SenhaGeradaDaData= (utils.getDataHoraMinutoSegundoEMilesimo().replace(":","")).replace(".","");
            pageLaboratorioTrasferirPacienteParaFila.informarTxtSenha(SenhaGeradaDaData);
        }else{
            pageLaboratorioTrasferirPacienteParaFila.informarTxtSenha(senha);
        }
    }

    @Step
    public void clicar_em_botão_adicionar_da_tela_Adicionar_paceinte_a_Fila_de_Espera_no_modulo_Atende_Laboratorio() {
        pageLaboratorioTrasferirPacienteParaFila.clickBtnAdicionarPacienteAFila();
    }

    @Step
    public void informar_a_fila_na_tela_de_trasfêrencia_de_paciente_para_a_fila_no_modulo_Atende_Laboratorio(String fila) {
        pageLaboratorioTrasferirPacienteParaFila.informarTxtFila(fila);
    }

    @Step
    public void clicar_no_botão_Transferir_da_tela_trasferir_paciente_para_a_fila_no_modulo_Atende_Laboratorio() {
        pageLaboratorioTrasferirPacienteParaFila.clickBtnTransfeir();
    }


}
