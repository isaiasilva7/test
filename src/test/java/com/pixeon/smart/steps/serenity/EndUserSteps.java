package com.pixeon.smart.steps.serenity;

import com.pixeon.smart.datamodel.DataDictionary;
import com.pixeon.smart.datamodel.DataWord;
import com.pixeon.smart.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void looks_for(List<DataWord> dataWord) {
        dictionaryPage.enter_keywords(dataWord);
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(List<DataDictionary> dataDictionary) {
    	for (DataDictionary definition : dataDictionary) {
    		assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition.getDefinition())));
		}
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

}