package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_selecionar_nome_do_arquivo;


import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroOSAtende;
import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos.PageLaboratorioOrdemDeServico.numeroSerieOSAtende;
import static com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente.nomePaciente;
import static com.pixeon.smart.util.Utils.sleep;

import com.pixeon.smart.pages.atende.selecionar_nome_do_arquivo.PageSelecionarNomeDoAquivo;
import net.thucydides.core.annotations.Step;

import org.junit.Assert;

import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.Utils;

public class AtendimentoSelecionarNomeDoArquivoSteps {
    PageSelecionarNomeDoAquivo pageSelecionarNomeDoAquivo;

    @Step
    public void informar_o_nome_do_arquivo_Teste_Automatizado_e_clicar_em_Salvar_na_tela_de_Selecione_o_nome_do_arquivo() {
     try{
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");
        sleep(300);
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");
        sleep(200);

        pageSelecionarNomeDoAquivo.informarTxtNomeDoArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado");
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");
        sleep(2000);
        pageSelecionarNomeDoAquivo.clicarBtnSalvar();
       
        if (pageSelecionarNomeDoAquivo.existBtntnSimSubtituitAquivo()){
        	pageSelecionarNomeDoAquivo.clicarBtntnSimSubtituitAquivo();
        	pageSelecionarNomeDoAquivo.clicarBtnSalvar();
        }       
        sleep(800);
     }catch (Exception e){
         Assert.assertFalse(e.getMessage(), true);
     }
    }

    @Step
    public void confirmar_dados_valor_nome_do_paciente_número_da_OS_no_arquivo_com_o_formato(String valorPagmento, String formatoDoArquivo) {

        String resultado= Utils.lerArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado"+formatoDoArquivo);
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");
        sleep(1000);
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");


        if (!valorPagmento.equals("")){
            Assert.assertTrue(resultado.contains(valorPagmento));
        }
        Assert.assertTrue(resultado.contains(nomePaciente));
       // Assert.assertTrue(resultado.contains(numeroOSAtende + "." + numeroSerieOSAtende));

    }
    @Step
    public void confirmar_que_o_nome_do_paciente_a_data_esta_no_arquivo_com_o_formato(String formatoDoArquivo) {
        String resultado= Utils.lerArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado"+formatoDoArquivo);
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");
        sleep(1000);
        Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\");

        Assert.assertTrue(resultado.contains(nomePaciente));
        Assert.assertTrue(resultado.contains(Utils.getDataAtualDiaMesAnoHoraMinutoESegundo().substring(0,10)));
 }

    @Step
    public void informar_o_nome_do_arquivo_e_clicar_em_na_tela_de_Selecione_o_nome_do_arquivo(String caminhoCompletoDoArquivo, String nomeDoBotao) {
        sleep(2000);
        pageSelecionarNomeDoAquivo.informarTxtNomeDoArquivo(caminhoCompletoDoArquivo);

        if (nomeDoBotao.equals("Salvar")){

            pageSelecionarNomeDoAquivo.clicarBtnSalvar();
            sleep(1000);

        }else  if (nomeDoBotao.equals("Abrir")) {
            sleep(2000);
            pageSelecionarNomeDoAquivo.clicarBtnAbrir();
            sleep(1000);
        }else{
            Assert.assertTrue(" botão não cadastrado ",false);
        }
    }
    @Step
    public void escolho_o_formato_do_arquivo_e_clico_no_botão_na_tela_de_Selecionar_o_formato(String formatoDoArquivo, String nomeDobotao) {

        if (formatoDoArquivo.equals("TXT")||formatoDoArquivo.equals("Txt")||formatoDoArquivo.equals("txt")) {
            pageSelecionarNomeDoAquivo.clicarLinkFormatoTxt();
            sleep(1000);
        }else if (formatoDoArquivo.equals("XLS")||formatoDoArquivo.equals("Xls")||formatoDoArquivo.equals("xls")){
            pageSelecionarNomeDoAquivo.clicarLinkFormatoXls();
            sleep(1000);
        }else if (formatoDoArquivo.equals("PSR")||formatoDoArquivo.equals("Psr")||formatoDoArquivo.equals("psr")){
            pageSelecionarNomeDoAquivo.clicarLinkFormatoPsr();
            sleep(1000);
        }else if (formatoDoArquivo.equals("CSV")||formatoDoArquivo.equals("Csv")||formatoDoArquivo.equals("csv")){
            pageSelecionarNomeDoAquivo.clicarLinkFormatoCsv();
            sleep(1000);
        }else if (formatoDoArquivo.equals("HTML")||formatoDoArquivo.equals("Html")||formatoDoArquivo.equals("html")||formatoDoArquivo.equals("HTM")||formatoDoArquivo.equals("Htm")||formatoDoArquivo.equals("htm")){
            pageSelecionarNomeDoAquivo.clicarLinkFormatoHtml();
            sleep(1000);
        }else if (formatoDoArquivo.equals("PDF")||formatoDoArquivo.equals("Pdf")||formatoDoArquivo.equals("pdf")){
            pageSelecionarNomeDoAquivo.clicarLinkFormatoPDF();
            sleep(1000);
        } else{
            Assert.assertTrue(" botão não cadastrado ",false);
        }

        if (nomeDobotao.equals("OK")||nomeDobotao.equals("Ok")||nomeDobotao.equals("ok")){
            pageSelecionarNomeDoAquivo.clicarBtnOK();
            sleep(1000);
        }else{
            Assert.assertTrue(" botão não cadastrado ",false);
        }
    }

    @Step
    public void confirmo_que_os_dados_estão_no_arquivo_como_o_formato(String dados, String formatoDoArquivo) {
        try {

                String frase = dados;
                String array[] = new String[frase.split(";").length];
                array = frase.split(";");
                String resultado=null;

                if (formatoDoArquivo.equals(".txt")||formatoDoArquivo.equals(".TXT")||formatoDoArquivo.equals(".Txt")||formatoDoArquivo.equals(".HTM")||formatoDoArquivo.equals(".HTML")||formatoDoArquivo.equals(".Htm")){
                    sleep(2000);
                    resultado= Utils.lerArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado"+formatoDoArquivo.toUpperCase());
                    Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo");
                }else if (formatoDoArquivo.equals(".csv")||formatoDoArquivo.equals(".CSV")||formatoDoArquivo.equals(".Csv")) {
                    PageCommonToAll pageCommonToAll = new PageCommonToAll();
                    resultado = pageCommonToAll.leArquivoCSVeTXT("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado" + formatoDoArquivo.toUpperCase());
                    Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo");
                }else if (formatoDoArquivo.equals(".csv")||formatoDoArquivo.equals(".CSV")||formatoDoArquivo.equals(".Csv")) {
                    PageCommonToAll pageCommonToAll = new PageCommonToAll();
                    resultado = pageCommonToAll.leArquivoCSVeTXT("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado" + formatoDoArquivo.toUpperCase());
                    Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo");
                }else if (formatoDoArquivo.equals(".PSR")||formatoDoArquivo.equals(".psr")) {
                    PageCommonToAll pageCommonToAll = new PageCommonToAll();
                    resultado = pageCommonToAll.leArquivoCSVeTXT("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado" + formatoDoArquivo.toUpperCase());
                    Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo");
                    Utils.deletarArquivosDeUmDiretorio("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo");
                }else if (formatoDoArquivo.equals(".pdf")||formatoDoArquivo.equals(".Pdf")||formatoDoArquivo.equals(".PDF")){
                    PageCommonToAll pageCommonToAll =new PageCommonToAll();
                    resultado= pageCommonToAll.extraiTextoDoPDF("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado"+formatoDoArquivo.toUpperCase());
                    resultado=(resultado).replace("\r\n", " ");// \r\n quebra de linha
                }else if (formatoDoArquivo.equals(".xml")||formatoDoArquivo.equals(".Pdf")||formatoDoArquivo.equals(".XML")){
                    PageCommonToAll pageCommonToAll =new PageCommonToAll();
                    sleep(2000);
                    resultado= Utils.lerArquivo("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\repositorioDeArquivo\\Teste Automatizado"+formatoDoArquivo.toUpperCase());
                    resultado=(resultado).replace("\r\n", " ");// \r\n quebra de linha
                }else{
                   Assert.assertFalse("formato não identificado",true);
                }

                for (int i = 0; i < array.length; i++) {
                    if (array[i].equals("nomePacienteAleatorio")){
                        Assert.assertTrue(resultado.contains(nomePaciente));
                    }else if (array[i].equals("DataAtual")||array[i].equals("dataAtual")){
                       if (resultado.contains( Utils.getDataAtualDiaMesAnoHoraMinutoESegundo().substring(0,6) + Utils.getDataAtualDiaMesAnoHoraMinutoESegundo().substring(8,10))||resultado.contains(Utils.getDataAtualDiaMesAnoHoraMinutoESegundo().substring(0,10)) ){
                            Assert.assertTrue("Data encontrada no arquivo ", true);
                       }else {
                           Assert.assertTrue("Data encontrada no arquivo ", false);
                       }
                    }else if (array[i].equals("Numero do prestador")){
                        String query= "Select osm_ctle_cnv  from osm where osm_serie='"+numeroOSAtende+"' and osm_num='"+numeroSerieOSAtende+"'";
                        String RetornoQuery= Utils.consultaBanco(query,"osm_ctle_cnv");
                        Assert.assertTrue(resultado.contains(RetornoQuery));

                    }else if (array[i].contains("valor=")) {
                        String array1[] = new String[array[i].split("=").length];
                        array1 = array[i].split("=");
                        Assert.assertTrue(resultado.contains(array1[1]));
                  }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
