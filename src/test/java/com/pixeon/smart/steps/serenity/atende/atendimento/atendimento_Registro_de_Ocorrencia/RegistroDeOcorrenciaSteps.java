package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Registro_de_Ocorrencia;

import com.pixeon.smart.pages.atende.atendimento.atendimento_Registro_de_Ocorrencia.PageRegistroDeOcorrencia;
import net.thucydides.core.annotations.Step;


public class RegistroDeOcorrenciaSteps {

    PageRegistroDeOcorrencia pageRegistroDeOcorrencia;

    @Step
    public void clicar_no_botão_Ok_na_tela_Registro_de_Ocorrencia() {
        pageRegistroDeOcorrencia.clickBtnOK();
    }
    @Step
    public void clicar_no_botão_Anônimo_na_tela_Registro_de_Ocorrencia() {
        pageRegistroDeOcorrencia.clickBtnAnonimo();
    }
    @Step
    public void informar_a_origem_a_unidade_o_setor_a_categoria_o_tipo_e_a_descrição_na_tela_Registro_de_Ocorrencia(String origem, String unidade, String setor, String categoria, String tipo, String descricao) {

        if (!origem.equals("")){
            pageRegistroDeOcorrencia.informarTxorigem(origem);
        }
        if (!unidade.equals("")){
            pageRegistroDeOcorrencia.informarTxUnidade(unidade);
        }
        if (!setor.equals("")){
            pageRegistroDeOcorrencia.informarTxSetor(setor);
        }
        if (!categoria.equals("")){
            pageRegistroDeOcorrencia.informarTxCategoria(categoria);
        }
        if (!tipo.equals("")){
            pageRegistroDeOcorrencia.informarTxtTipo(tipo);
        }

        if (!descricao.equals("")){
            pageRegistroDeOcorrencia.informarTxtDescricao(descricao);
        }

    }
}
