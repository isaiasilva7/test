package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Dados_do_item;


import com.pixeon.smart.pages.atende.atendimento.atendimento_Dados_do_item.PageAtendimentoDadosDoItem;
import net.thucydides.core.annotations.Step;

public class AtendimentoDadosDoItemSteps {
    PageAtendimentoDadosDoItem pageAtendimentoDadosDoItem;
    @Step
    public void clicar_botão_Alterar_Data_Do_Resultados_na_tela_Dados_do_Item() {
        pageAtendimentoDadosDoItem.clickBtnAterarDataDoResultado();
    }

    @Step
    public void clicar_botão_Diligenciamento_na_tela_Dados_do_Item() {
        pageAtendimentoDadosDoItem.clickBtnDeligenciamento();
    }

    @Step
    public void clicar_botão_Informações_na_tela_Dados_do_Item() {
        pageAtendimentoDadosDoItem.clickBtnInformacoes();
    }
    @Step
    public void clicar_botão_OK_na_tela_Dados_do_Item() {
        pageAtendimentoDadosDoItem.clickBtnOK();
    }
    @Step
    public void clicar_botão_Fechar_na_tela_Dados_do_Item() {
        pageAtendimentoDadosDoItem.clickBtnFechar();
    }
}
