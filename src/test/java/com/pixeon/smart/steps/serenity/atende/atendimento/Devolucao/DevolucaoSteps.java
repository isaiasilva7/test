package com.pixeon.smart.steps.serenity.atende.atendimento.Devolucao;


import com.pixeon.smart.pages.atende.atendimento.Devolucao.PageDevolucao;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class DevolucaoSteps {
    PageDevolucao pageDevolucao;

    @Step
    public void clicar_no_botão_OK_da_tela_Devolucao() {
           pageDevolucao.clickBtnOK();
    }

    @Step
    public void informo_o_responsavel_e_confirmo_clicando_no_botão_ok(String nomeDoRepsoanvel) {
        pageDevolucao.informaReponsavelDevolucao(nomeDoRepsoanvel);
        pageDevolucao.clickBtnOKReponsavel();
    }
}
