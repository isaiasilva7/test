package com.pixeon.smart.steps.serenity.fature.commonToAll;

import org.junit.Assert;

import com.pixeon.smart.pages.atende.login.PageLogin;
import com.pixeon.smart.pages.fature.PageCronogramaFaturamento;
import com.pixeon.smart.pages.fature.commonToAll.PageCommonToAll;

import net.thucydides.core.annotations.Step;

public class CommonToAllSteps {

	PageLogin pageLogin;
	PageCronogramaFaturamento pageCronograma;
	PageCommonToAll pageCommonToAll;
	
	 @Step
	public void que_tenho_acesso_a_aplicação_Smart_Fature() {	
		
		 try{
			pageCommonToAll.abrirModulo("FATURE125");
          if (pageLogin.validaAtendeAberta()){
			   pageLogin.inserirUsuario("medicware");
			   pageLogin.inserirSenha("1");
			   pageLogin.clickOK();
			   // Assert.assertTrue(pageLogin.validarMensagemAlerta());
			   pageLogin.clickbtnNaoDaMessagem();
			   pageCronograma.clickBtnCronogramaFaturamento();
			   
		   }else {
          	Assert.assertFalse("erro ao abrir a aplicação",true);
		   }


		 }catch(Exception e){
			 System.out.println(e);
		 }
	}

	@Step
	public void acesso_o_menu_na_Home_do_modulo_Fature(String menu) {

	}
}
