package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_Dados_Cadastrais_do_Paciente;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_Dados_Cadastrais_do_Paciente.PageLaboratorioCadastroDadosPacienteProntuario;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class LaboratorioDadosCadastraisDoPacienteSteps {
    PageLaboratorioCadastroDadosPacienteProntuario pageLaboratorioCadastroDadosPacienteProntuario;

    @Step
    public void clicar_nos_botões_na_tela_Dados_Cadastrais_do_paciente_no_modulo_Atende_Laboratorio(String botoes) {

        if (botoes.equals("Mais")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnMais();
        }else if (botoes.equals("Conv. Secuncadário")||botoes.equals("Conv. Secuncadario")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnConvenioSecundario();
        }else if (botoes.equals("Deficiencias")||botoes.equals("Deficiências")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnDeficiencias();
        }else if (botoes.equals("Prontuário")||botoes.equals("Prontuario")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnProntuario();
        }else if (botoes.equals("Acompanhante")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnAcompanhante();
        }else if (botoes.equals("RN")||botoes.equals("Rn")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnRn();
        }else if (botoes.equals("Cadastror Biometria")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnBiometria();
        }else if (botoes.equals("OK")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnBiometria();
        }else if (botoes.equals("Cancelar")){
            pageLaboratorioCadastroDadosPacienteProntuario.clickBtnBiometria();
        }else{
            Assert.assertTrue("botão não cadasrado",false);
        }
    }

}
