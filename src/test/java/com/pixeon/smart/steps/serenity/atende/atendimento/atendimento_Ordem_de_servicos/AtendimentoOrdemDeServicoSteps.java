package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Ordem_de_servicos;

import com.pixeon.smart.pages.atende.atendimento.atendimento_Ordem_de_servicos.PageAtendimentoOrdemDeServico;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.pages.atende.atendimento.atendimento_Ordem_de_servicos.PageAtendimentoOrdemDeServico.valorDoServico1;


public class AtendimentoOrdemDeServicoSteps {

    PageAtendimentoOrdemDeServico pageAtendimentoOrdemDeServico;

    @Step
    public void o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_tela_de_Ordem_de_serviço_na_busca_no_modulo_Atende() {
        pageAtendimentoOrdemDeServico.nomeDoPacientepresente();
    }

    @Step
    public void informar_dados_do_serviço(String codigo, String quantidade, String nome_do_responsavel_coleta_material) {
        pageAtendimentoOrdemDeServico.inserirCodigoDoServico1(codigo);
        pageAtendimentoOrdemDeServico.inserirQuantidadeServico1(quantidade);

        if (!nome_do_responsavel_coleta_material.equals("")){
            pageAtendimentoOrdemDeServico.inserirResponsavelColetaDeMaterial(nome_do_responsavel_coleta_material);
        }
    }

    @Step
    public void capturo_o_numero_da_OS() {
        pageAtendimentoOrdemDeServico.capturaNumeroOS();
    }


    @Step
    public void capturo_o_valor_do_serviço(String identificacaoDoServico) {
        if (identificacaoDoServico.equals("1")){
            pageAtendimentoOrdemDeServico.capturaValorDoServico1();
        }else {
          //  System.out.println("Servico não Identificado");
            Assert.assertTrue("Servico não Identificado",false);
        }
    }

    @Step
    public void validar_o_valor_do_exame_na_tela_ordem_de_serviço_do_paciente_no_módulo_Atende(String valorExame) {
       String valordoServicoCapturado1 = valorDoServico1;
        Utils.sleep(200);
       if (valordoServicoCapturado1.equals(valorExame)){
           Assert.assertTrue(true);
       }else{
           Assert.assertTrue(false);
       }
    }

    @Step
    public void informar_dados_da_OS_tipo_Atendimento_e_Setor(String tipoAtendimento, String setor) {
        pageAtendimentoOrdemDeServico.selecionaCBoTipoAtendiemnto(tipoAtendimento);
        pageAtendimentoOrdemDeServico.inserirSetorOS(setor);
    }

    @Step
    public void clicar_no_botão_deposito_tela_de_ordem_de_serviço() {
        pageAtendimentoOrdemDeServico.clickBtnDeposito();
    }
    @Step
    public void clicar_no_botão_Adiconar_na_Fila_tela_de_Ordem_de_servico_no_modulo_Atende() {
        pageAtendimentoOrdemDeServico.clickBtnAdicionarNaFilla();
    }

    @Step
    public void clicar_no_botão_Fila_de_Espera_tela_de_Ordem_de_servico_no_modulo_Atende() {
        pageAtendimentoOrdemDeServico.clickbtnFilaDeEspera();
    }

    @Step
    public void clicar_no_botão_pagamento_da_tela_ordem_de_servico() {
        pageAtendimentoOrdemDeServico.inserirTipoDeServico("");
        pageAtendimentoOrdemDeServico.clickBtnPagamento();
    }

    @Step
    public void validar_que_o_status_da_fatura_esta_como_Faturado_na_tela_ordem_de_servico() {
        pageAtendimentoOrdemDeServico.validarStatusFaturado();
    }

    @Step
    public void validar_que_o_status_da_fatura_esta_como_aberto_na_tela_ordem_de_servico() {
        pageAtendimentoOrdemDeServico.validarStatusAberto();
    }
    @Step
    public void clicar_botão_Documento_da_tela_oremdem_de_Serviço_do_modulo_atende() {
        pageAtendimentoOrdemDeServico.clickBtnDocumentos();
    }
    @Step
    public void clicar_botão_anexar_o_documento_da_tela_oremdem_de_Serviço_do_modulo_atende() {
        pageAtendimentoOrdemDeServico.clickBtnAnexarDocumento();
    }
    @Step
    public void clicar_no_botão_atendimento_da_tela_Ordem_de_servico() {
        pageAtendimentoOrdemDeServico.clickBtnAtendimento();
    }
    @Step
    public void clicar_no_botão_registro_de_Ocorrencia_na_tela_de_Ordem_de_Serviço() {
        pageAtendimentoOrdemDeServico.clickBtnRegistroOcorrencia();
    }
    @Step
    public void clicar_no_botão_Ocorrencia_na_tela_de_Ordem_de_Serviço() {
        pageAtendimentoOrdemDeServico.clickBtnOcorrencia();
    }
    @Step
    public void informar_dados_do_serviço_da_Primeira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        if (!tipo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtTipoAtendimento01(tipo); }
        if (!codigo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtcodigoDoServico01(codigo); }
        if (!setor.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtSetor01(setor); }
    }

    @Step
    public void informar_dados_do_serviço_da_Segunda_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        if (!tipo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtTipoAtendimento02(tipo); }
        if (!codigo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtcodigoDoServico02(codigo); }
        if (!setor.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtSetor02(setor);}
    }

    @Step
    public void informar_dados_do_serviço_Terceira_linha_tipo_Código_Setor_na_tela_Ordem_de_serviços(String tipo, String codigo, String setor) {
        if (!tipo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtTipoAtendimento03(tipo); }
        if (!codigo.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtcodigoDoServico03(codigo); }
        if (!setor.equals("")) { pageAtendimentoOrdemDeServico.inserirTxtSetor03(setor); }

    }
    @Step
    public void seleciono_o_Serviço_via_tecla_de_atalho_do_teclado(String telclaAtalho) {
        Utils utils =new  Utils();
        utils.teclaDeAtalho(telclaAtalho);
    }
}



