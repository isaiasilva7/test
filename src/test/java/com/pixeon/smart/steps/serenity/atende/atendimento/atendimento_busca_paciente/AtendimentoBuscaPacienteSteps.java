package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_busca_paciente;

import com.pixeon.smart.datamodel.atende.atendimento_busca_paciente.DataAtendeAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.SikuliUtil;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import java.util.List;

import static com.pixeon.smart.util.Utils.sleep;

public class AtendimentoBuscaPacienteSteps {
    PageAtendimentoBuscaPaciente pageAtendimentoBuscaPaciente;
    PageCommonToAll pageCommonToAll;
    //  Utils utils;
    SikuliUtil sikuliUtil;

    @Step
    public void informa_o_item_de_busca_e_busco_no_modulo_Atende(String itemDeBusca) {
        pageAtendimentoBuscaPaciente.inserirtxtBuscaPaciente(itemDeBusca);
        sikuliUtil.buscaAtravesDeAtalho();
    }

    @Step
    public void o_sistema_retorna_o_paciente_o_paciente_ROBO_TESTE_AUTOMATIZADO_na_busca_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.pacientePresente();
    }

    @Step
    public void que_seleciono_busca_paciente_por_Os_no_Atendimento_busca_modulo_Atende() {
        pageAtendimentoBuscaPaciente.clicarBuscarPacientePorOS();
    }

    @Step
    public void confirmo_que_desejo_incluir_novo_paciente_no_atendimento_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.clickBtnSim();
    }

    @Step
    public void informar_dados_do_cadasto_do_paciente(List<DataAtendeAtendimentoBuscaPaciente> dataAtendeAtendimentoBuscaPaciente) {
        pageAtendimentoBuscaPaciente.preencheCampoCadastroPaciente(dataAtendeAtendimentoBuscaPaciente);
    }

    @Step
    public void gravo_as_informacoes() {
        pageAtendimentoBuscaPaciente.clickBtnGravar();
    }

    @Step
    public void deve_apresentar_a_mensagem_Paciente_gravado_com_sucesso_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.validarAConfirmacaoPacienteGravadoComSucesso();
    }

    @Step
    public void informar_dados_do_cadasto_do_paciente(String sexo, String data_nascimento, String telefone, String convenio, String cPF, String nome_da_mae, String nome_social) {
        pageAtendimentoBuscaPaciente.preencheCampoCadastroPaciente(sexo,data_nascimento,telefone, convenio, cPF, nome_da_mae, nome_social);
    }

    @Step
    public void seleciono_o_botao_Tratamento_Ambulatorial_na_tela_atendimento_busca_paciente_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.clickBtnTratamentoAmbulatorial();
    }


    @Step
    public void clicar_no_botão_Adiconar_na_Fila_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.clickBtnAdicionarNaFila();
    }
    @Step
    public void clicar_no_botão_Fila_de_Espera_tela_de_cadastro_do_paciente_no_modulo_Atende() {
        pageAtendimentoBuscaPaciente.clickBtnFilaDeEspera();
    }

    @Step
    public void clicar_nos_botoes_na_tela_de_busca(String nomeDoBotao) {
        sleep(1000);
        if (nomeDoBotao.equals("btnBuscarPacientePorOS")){
            pageAtendimentoBuscaPaciente.clicarBuscarPacientePorOS();
        }else if (nomeDoBotao.equals("btnTratAmbulatorial")){
            pageAtendimentoBuscaPaciente.clickBtnTratamentoAmbulatorial();
        }else if (nomeDoBotao.equals("btnGuia")||nomeDoBotao.equals("Guias")){
            pageAtendimentoBuscaPaciente.clickBtnGuia();
        }else if (nomeDoBotao.equals("btnFilaDeEspera")) {
            sleep(1000);
            pageAtendimentoBuscaPaciente.clickBtnFilaDeEspera();
        }else if (nomeDoBotao.equals("btnAtendimento")) {
            sleep(1000);
            pageAtendimentoBuscaPaciente.clickBtnAtendimento();
        }else if (nomeDoBotao.equals("btnAdicionarNaFila")) {
            sleep(1000);
            pageAtendimentoBuscaPaciente.clickBtnAdicionarNaFila();
        }else if (nomeDoBotao.equals("btnLerCartao")){
                pageAtendimentoBuscaPaciente.clickBtnlerCartao();
        }else if (nomeDoBotao.equals("btnBiometria")){
            pageAtendimentoBuscaPaciente.clickBtnBiometria();
        } else{
            Assert.assertFalse("botão não encontrado",true);
        }
    }
}
