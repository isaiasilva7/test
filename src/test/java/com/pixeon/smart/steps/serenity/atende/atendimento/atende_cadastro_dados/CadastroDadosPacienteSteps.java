package com.pixeon.smart.steps.serenity.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados.PageCadastroDadosPaciente;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.util.Utils.sleep;

public class CadastroDadosPacienteSteps {
    PageCadastroDadosPaciente pageCadastroDadosPaciente;

    @Step
    public void clicar_nos_botoes_na_tela_cadastro_do_paciente(String nomeDoBotao) {
        sleep(1000);
        if (nomeDoBotao.equals("Prontuario")||nomeDoBotao.equals("Prontuário")||nomeDoBotao.equals("btnProntuario")){
            pageCadastroDadosPaciente.clickBtnProntuario();
        }else if (nomeDoBotao.equals("Questionario")||nomeDoBotao.equals("Questionário")||nomeDoBotao.equals("btnQuestionario")){
            pageCadastroDadosPaciente.clickbtnQuestionario();
        }else if (nomeDoBotao.equals("Questionario")||nomeDoBotao.equals("Foto")||nomeDoBotao.equals("btnFoto")||nomeDoBotao.equals("foto")){
            pageCadastroDadosPaciente.clickbtnQuestionario();
        }else if (nomeDoBotao.equals("Arq")||nomeDoBotao.equals("btnArq")||nomeDoBotao.equals("arq")){
            pageCadastroDadosPaciente.clickBtnArquivo();
        } else if (nomeDoBotao.equals("Guias 1")||nomeDoBotao.equals("btnGuia1")||nomeDoBotao.equals("guia 1")){
            pageCadastroDadosPaciente.clickbtnGuia1();
        }else if (nomeDoBotao.equals("Convenio Secundário")||nomeDoBotao.equals("Conv. Secundário")||nomeDoBotao.equals("conv. secundário")){
            pageCadastroDadosPaciente.clickBtnConvenioSecundario();
        } else if (nomeDoBotao.equals("Acompanhante")||nomeDoBotao.equals("acompanhantes")||nomeDoBotao.equals("Acompanhantes")||nomeDoBotao.equals("btnAcompanhantes")){
            pageCadastroDadosPaciente.clickBtnCadastroAcompanhante();
        } else if (nomeDoBotao.equals("Dados Complmentares")||nomeDoBotao.equals("dados domplementares")){
            pageCadastroDadosPaciente.clickBtnDadosComplementares();
        } else if (nomeDoBotao.equals("Trat. Ambulatorial")||nomeDoBotao.equals("trat. ambulatorial")||nomeDoBotao.equals("Tratamento Ambulatorial")){
            pageCadastroDadosPaciente.clickBtnCadastroAcompanhante();
        } else if (nomeDoBotao.equals("Cadastro biometria")||nomeDoBotao.equals("Cadastro biometria")){
            pageCadastroDadosPaciente.clickBtnBiometria();
        } else if (nomeDoBotao.equals("Guias")||nomeDoBotao.equals("Guias")){
            pageCadastroDadosPaciente.clickBtnGuia();
        }else if (nomeDoBotao.equals("Fila de Espera")||nomeDoBotao.equals("Fila de Espera")){
            pageCadastroDadosPaciente.clickBtnFilaDeEspera();
        }else if (nomeDoBotao.equals("Adionionar na fila")||nomeDoBotao.equals("Adionionar na fila")){
            pageCadastroDadosPaciente.clickBtnAdiconarNaFila();
        }else{
            Assert.assertFalse("botão não encontrado",true);
        }


    }
}
