package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_recebimento_Direto_Steps;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_recebimento_Direto.PageLaboratorioRecebimentoDireto;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class LaboratorioRecebimentoDiretoSteps {
    PageLaboratorioRecebimentoDireto pageLaboratorioRecebimentoDireto;

    @Step
    public void selecionar_a_forma_de_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String formaDePagamento) {
        Utils.sleep(500);
        if (formaDePagamento.equals("A Receber")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboAreceber();
        }else if (formaDePagamento.equals("Boleto")||formaDePagamento.equals("boleto")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboBoleto();
        }else if (formaDePagamento.equals("Cheque")||formaDePagamento.equals("cheque")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboCheque();
        }else if (formaDePagamento.equals("Epécie")||formaDePagamento.equals("Especie")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickcboEspece();
        }else if (formaDePagamento.equals("Externo")||formaDePagamento.equals("externo")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboExterno();
        }else if (formaDePagamento.equals("Desconto")||formaDePagamento.equals("Desconto")) {
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboDesconto();
        }else if (formaDePagamento.equals("Cartão")||formaDePagamento.equals("cartão")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboCartao();
        }else if (formaDePagamento.equals("Imposto")||formaDePagamento.equals("imposto")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickscrollFormaDEPagamento();
            pageLaboratorioRecebimentoDireto.clickscrollFormaDEPagamento();
            pageLaboratorioRecebimentoDireto.clickcboImposto();
        }else if (formaDePagamento.equals("Nota Promissoria")||formaDePagamento.equals("nota promissoria")) {
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickscrollFormaDEPagamento();
            pageLaboratorioRecebimentoDireto.clickscrollFormaDEPagamento();
            pageLaboratorioRecebimentoDireto.clickCboNotaPromissoria();
        }else if (formaDePagamento.equals("Cheque pré Datado")||formaDePagamento.equals("Cheque Pré-Datado")){
            pageLaboratorioRecebimentoDireto.clickcboFormaDePagamento();
            pageLaboratorioRecebimentoDireto.clickCboChequePreDatado();
        }else{
            Assert.assertFalse("Forma de pagamento não cadastrada",true);
        }
    }

    @Step
    public void clicar_no_botão_OK_da_tela_recebimento_direto_no_modulo_Atende_Laboratorio() {
        pageLaboratorioRecebimentoDireto.clickBtnOK();
    }

    @Step
    public void validar_que_o_botão_cancelar_Pagamento_esta_na_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio() {
        pageLaboratorioRecebimentoDireto.clickBtnCancelarPagamento();
    }
    @Step
    public void informa_Valor_do_forma_de_pagamento_data_de_vencimento_do_cheque_agencia_conta_numero_do_chegue_praça_titularidade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String valorPagamento, String fomraDePagamento, String dataVencimento, String agencia, String conta, String numeroDoChequeOuCartao, String praca, String titularidade) {
        if (fomraDePagamento.equals("Cheque")||fomraDePagamento.equals("Cheque pré Datado")){
            pageLaboratorioRecebimentoDireto.informaTxtDataValidadeDoCartao(dataVencimento);
            pageLaboratorioRecebimentoDireto.informaTxtValorPagamento(valorPagamento);
            pageLaboratorioRecebimentoDireto.informaTxtAgenciaPagamento(agencia);
            pageLaboratorioRecebimentoDireto.informaTxtConta(conta);
            pageLaboratorioRecebimentoDireto.informaTxtnumeroCartaoCheque(numeroDoChequeOuCartao);
            pageLaboratorioRecebimentoDireto.informaTxtPraca(praca);
            pageLaboratorioRecebimentoDireto.selecionarCboTitularidade(titularidade);

        }else{
            Assert.assertFalse("Forma de pagamento não encontada",true);
        }
    }
    @Step
    public void informa_Valor_de_do_pagamento_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String valor, String formaDePagamento) {
        if (formaDePagamento.equals("Externo")||formaDePagamento.equals("Eexterno")) {
            pageLaboratorioRecebimentoDireto.informaTxtValorPagamento(valor);
        }else if (formaDePagamento.equals("Desconto")||formaDePagamento.equals("Desconto")) {
            pageLaboratorioRecebimentoDireto.informaTxtValorPagamento(valor);
        } else if (formaDePagamento.equals("Imposto")||formaDePagamento.equals("imposto")) {
            pageLaboratorioRecebimentoDireto.informaTxtValorPagamento(valor);
        }else if (formaDePagamento.equals("Nota Promissoria")||formaDePagamento.equals("nota promissoria")) {
            pageLaboratorioRecebimentoDireto.informaTxtValorPagamento(valor);
        } else {
            Assert.assertFalse("Não foi encontrado o forma de pagamento ", true);
        }
    }

    @Step
    public void informa_bandeira_do_cartão_conta_Chegue_número_do_cartão_data_de_validade_da_tela_de_Recebimento_Direto_no_modulo_Atende_Laboratorio(String bandeiraDoCartao, String ContaChegue, String numeroDoCartao, String data) {
        pageLaboratorioRecebimentoDireto.informaTxtBandeiraDoCartao(bandeiraDoCartao);
        pageLaboratorioRecebimentoDireto.informaTxtConta(ContaChegue);
        pageLaboratorioRecebimentoDireto.informaTxtNumeroDoCartao(numeroDoCartao);
        pageLaboratorioRecebimentoDireto.informaTxtDataValidadeDoCartao(data);
    }

}
