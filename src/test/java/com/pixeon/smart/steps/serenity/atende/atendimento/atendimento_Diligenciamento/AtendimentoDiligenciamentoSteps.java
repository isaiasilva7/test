package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Diligenciamento;

import com.pixeon.smart.pages.atende.atendimento.atendimento_Diligenciamento.PageAtendimentoDiligenciamento;
import com.pixeon.smart.pages.atende.pop_up.PagePouUp;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import javax.xml.bind.SchemaOutputResolver;

public class AtendimentoDiligenciamentoSteps {
    PageAtendimentoDiligenciamento pageAtendimentoDiligenciamento;

    @Step
    public void clicar_na_Aba_Diligenciamento_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaDiligenciamento();
    }
    @Step
    public void clicar_na_Aba_Status_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaStatus();
    }
    @Step
    public void clicar_na_Aba_Rastrabilidade_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaRastreabilidade();
    }
    @Step
    public void clicar_na_Aba_Faturamento_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaFaturamento();
    }
    @Step
    public void clicar_no_botão_OK_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickBtnbtnOK();
    }
    @Step
    public void clicar_no_botão_Cancelar_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickBtnCancelar();
    }
    @Step
    public void clicar_no_botão_Fechar_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickBtnFechar();
    }

    @Step
    public void clicar_na_Aba_Documento_da_OS_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaDocumentos();
    }

    @Step
    public void clicar_na_Aba_Comunicação_WS_da_OS_na_tela_Diligenciamento() {
        pageAtendimentoDiligenciamento.clickAbaComunicacaoWS();
    }

    @Step
    public void clicar_no_quando_a_link_Xml_de_Envio_em_que_a_operação_é(String operacao) {
         pageAtendimentoDiligenciamento.clickScroll();
         pageAtendimentoDiligenciamento.clickScroll();
          pageAtendimentoDiligenciamento.clickScroll();
         pageAtendimentoDiligenciamento.clickScroll();

        if (operacao.equals("Requisição Alvoro")||operacao.equals("EnviarRequisicaoAlvaro")){
            pageAtendimentoDiligenciamento.clickLblRequisicaoAlvaro();

        }else if (operacao.equals("Soliciação_Procedimento")||operacao.equals("Solicitação_Procedimemto")||operacao.equals("PROCEDIMENTOS")||operacao.equals("SOLICITACAO_PROCEDIMENTOS")){
            pageAtendimentoDiligenciamento.clickLblProcedimento();
        }else if (operacao.equals("ReceberAtendimento")||operacao.equals("ReceberAtendimento")){
            pageAtendimentoDiligenciamento.clickLblReceberAtendimentoDbDiagnotico();
        }else if (operacao.equals("EnviarAmostras")||operacao.equals("Enviar Amostras")){
            pageAtendimentoDiligenciamento.clickLblEnviarAmostrDbDiagnotico();
        }else if (operacao.equals("getPeDido")||operacao.equals("get PeDido")){//Heremes Pardini
            pageAtendimentoDiligenciamento.clickLblgetPedido();
        }

        else{
            Assert.assertFalse("Operação não encontra",true);
        }
    }

    @Step
    public void dá_um_click_duplo_foram_do_item_e_valdiar_se_o_sistema_não_apresenta_falha_geral() {
        pageAtendimentoDiligenciamento.clickScroll();
        pageAtendimentoDiligenciamento.clickScroll();
        pageAtendimentoDiligenciamento.clickScroll();
        pageAtendimentoDiligenciamento.clickScroll();
        pageAtendimentoDiligenciamento.clickLblRetorno();

        PagePouUp pagePouUp = new PagePouUp();
        pagePouUp.validarQueExistePopUpFlahaGeral();


    }

}
