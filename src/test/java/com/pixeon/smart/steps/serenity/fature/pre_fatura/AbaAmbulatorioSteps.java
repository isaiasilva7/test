package com.pixeon.smart.steps.serenity.fature.pre_fatura;


import org.junit.Assert;

import com.pixeon.smart.pages.fature.pre_fatura.PagePreFatureAbaAmbulatorio;

import net.thucydides.core.annotations.Step;

public class AbaAmbulatorioSteps {
	
	PagePreFatureAbaAmbulatorio pagePreFatureAbaAmbulatorio;
	
	@Step
	public void clico_na_aba(String aba) {		
		if(aba.equals("Ambulatório")) {
			pagePreFatureAbaAmbulatorio.clickAbaAmbulatorio();			
		}
		else {
			 Assert.assertFalse("Aba não encontrada",true);
		}
	}
	
	//Recebe por parametro "pre_fature.feature" o nomr fo paciente
	@Step
	public void busco_pelo_paciente(String nome_paciente) {
		pagePreFatureAbaAmbulatorio.informarTxtBuscaPaciente(nome_paciente);
		pagePreFatureAbaAmbulatorio.clickBotaoBuscar();
	}
	
	@Step
	public void clico_no_botão_e_verifico_o_cabeçalho_da_pré_fatura(String botao) {
		pagePreFatureAbaAmbulatorio.clickBotaoAvancar();
	}
	
	@Step
	public void clico_no_botão(String btnProcessar) {
		pagePreFatureAbaAmbulatorio.clickBotaoProcessar();
	}
	
	@Step
	public void seleciono_o_periodo_atual_de_a(String periodoInicial, String periodoFinal) {
		pagePreFatureAbaAmbulatorio.informarPeriodoInicial(periodoInicial);
		pagePreFatureAbaAmbulatorio.informarPeriodoFinal(periodoFinal);
	}
	
	@Step	
	public void clico_no_botão_do_módulo_Fature(String btnGravar) {
		pagePreFatureAbaAmbulatorio.clickBotaoGravar();
	}
	
	@Step
	public void clico_no_botão_para_gerar_uma_nova_pré_fatura(String btnNovo) {
		pagePreFatureAbaAmbulatorio.clickBotaoNovo();
	}
	
	@Step	
	public void nenhuma_OS_gerada_no_periodo_é_exibida() {
		pagePreFatureAbaAmbulatorio.JanelaSemOS();
	}
	
	@Step
	public void confirmo_em_o_processamento_da_pré_fatura(String simProcessar) {
		pagePreFatureAbaAmbulatorio.clickSimProcessar();
	}

	@Step
	public void uma_mensagem_informando_que_nenhum_item_foi_encontrado_para_esta_pré_fatura_é_exibida(String mensagem) {
		pagePreFatureAbaAmbulatorio.TxtValidacaoMensagem();
	}
	
	@Step
	public void seleciono_um_periodo_de_trinta_dias_de_a(String periodoInicial, String periodoFinal) {
		pagePreFatureAbaAmbulatorio.informarPeriodoInicial(periodoInicial);
		pagePreFatureAbaAmbulatorio.informarPeriodoFinal(periodoFinal);
	}
	
	@Step
	public void visualizo_uma_lista_de_OS_geradas_no_periodo_pesquisado() {
		pagePreFatureAbaAmbulatorio.ExisteOS();
	}
	
	@Step
	public void a_pré_fatura_é_processada_e_uma_mensagem_informando_que_o_é_exibido() {
		pagePreFatureAbaAmbulatorio.MsgProcessoConcluido();
	}
	
	@Step
	public void clico_no_botao_da_aba_ambulatorio_da_tela_de_pre_fature_do_modulo_fature(String nomeBotao) {
		if(nomeBotao.equals("OK")) {
			pagePreFatureAbaAmbulatorio.ClickBotaoOK();			
		} 
		else if (nomeBotao.equals("Processar")) {
			pagePreFatureAbaAmbulatorio.clickBotaoProcessar();
		}
		else if (nomeBotao.equals("Gravar")) {
			pagePreFatureAbaAmbulatorio.clickBotaoGravar();
		}
		else if (nomeBotao.equals("Buscar Por OS")) {
			pagePreFatureAbaAmbulatorio.clickBotaoBuscarPorOS();
		}		
		else {
			 Assert.assertFalse("Botão não encontrado",true);
		}
	}
	
	@Step
	public void sou_redirecionado_para_a_aba_de_onde_visualizo_a_pré_fatura_criada() {
		pagePreFatureAbaAmbulatorio.AbaLogDeProcessamento();
	}
	
	@Step
	public void informo_a_OS(String numeroOs) {
		pagePreFatureAbaAmbulatorio.informarTxtNumeroOs(numeroOs);
	}
	
	@Step
	public void clico_no_botao_da_tela_de_informar_numero_da_OS(String botaoOk) {
		pagePreFatureAbaAmbulatorio.clickBotaoOKInformarNumeroOS();
	}
	
	
}
