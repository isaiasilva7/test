package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_selecione_o_servico;

import com.pixeon.smart.pages.atende.atendimento.atendimento_selecione_o_servico.PageAtendimentoSelecioneOServico;
import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.sql.SQLOutput;
import static com.pixeon.smart.util.Utils.sleep;

public class AtendimentoSelecioneOServicoSteps {
    PageAtendimentoSelecioneOServico pageAtendimentoSelecioneOServico;
    @Step
    public void seleciono_a_busca_por_informo_confirmo_través_enter_do_telcado_e_clico_em_OK(String busca, String texto) {
        pageAtendimentoSelecioneOServico.clicarBtnbtnScroll();
        if ("1-Código".equals(busca)){
            pageAtendimentoSelecioneOServico.clicarCboCodigo();

        }else{
          Assert.assertTrue("busca pelo não encontrado", false);
        }
        pageAtendimentoSelecioneOServico.informarTexto(texto);

        Utils.teclaDeAtalho("Enter");
        pageAtendimentoSelecioneOServico.clicarBtnOK();

    }

}
