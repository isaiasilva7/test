package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_fila_de_espera_recepcao_caixa_Steps;

import com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente.PageAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.atende.laboratorio.laboratorio_busca_paciente.PagelaboratorioBuscaPaciente;
import com.pixeon.smart.pages.atende.laboratorio.laboratorio_fila_de_espera_recepcao_caixa.PageLaboratorioFilaDeEsperaRecepcaoCaixa;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static com.pixeon.smart.pages.atende.laboratorio.laboratorio_busca_paciente.PagelaboratorioBuscaPaciente.nomePacienteLaboratorio;
import static com.pixeon.smart.util.Utils.sleep;

public class LaboratorioFilaDeEsperaRecepcaoCaixaSteps {

   PageLaboratorioFilaDeEsperaRecepcaoCaixa pageLaboratorioFilaDeEsperaRecepcaoCaixa;
    @Step
    public void clicar_no_botão_de_encerrar_no_modulo_Atende_Laboratorio() {
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
    }

    @Step
    public void informa_o_nome_do_Médico_para_consulta_a_Fila_na_tela_de_fila_de_Espera_no_modulo_Atende_Laboratorio(String nomeMedico) {
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.inserirFilaMedico(nomeMedico);
    }

    @Step
    public void clicar_no_botão_de_encerrar_atendimento_no_modulo_Atende_Laboratorio() {
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
    }

    @Step
    public void informar_o_nome_do_paciente_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio(String nome) {
        String nomePaciendte=  nomePacienteLaboratorio;
        if (nome.equals("pacienteAtual")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.inserirTxtNome(nomePaciendte);
        }else{
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.inserirTxtNome(nome);
        }
    }
    @Step
    public void clicar_no_botão_detalhes_na_tela_fila_de_espera_atendimento_no_modulo_Atende_Laboratorio() {
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnDetalhe();
    }
    @Step
    public void clicar_no_botão_Trasfêrencia_tela_de_cadastro_do_paciente_no_modulo_Atende_Laboratorio() {
        pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnTrasferencia();
    }

    @Step
    public void confirmar_que_o_panciente_foi_inserido_na_fila_de_espera_no_modulo_Atende_Laboratorio() {
        try {
            sleep(2000);
            String pacienteInserido=pageLaboratorioFilaDeEsperaRecepcaoCaixa.capturaraTxtNomePacienteInserido();
            String nomePaciendte=   nomePacienteLaboratorio;

            if (pacienteInserido.contains(nomePaciendte)){
                Assert.assertTrue(nomePaciendte,true);
            }else{
                Assert.assertTrue("Paciente não encontrado. "+nomePaciendte,false);
            }
        } catch (Exception e) {
            // TODO: handle exception
           // System.err.println(e);
            Assert.assertTrue(e.getMessage(),false);
        }

    }

    @Step
    public void clicar_no_botão_na_tela_de_Fila_de_espera_recepção_no_modulo_Atende_Laboratorio(String nomeDoBotao) {
        sleep(200);
        if (nomeDoBotao.equals("btnChamarProximo")||nomeDoBotao.equals("Chamar Proximo")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnChamaProximo();
        }else if (nomeDoBotao.equals("btnChamarSelecionado")||nomeDoBotao.equals("Chamar Selecionado")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnChamaSelecionado();
        }else if (nomeDoBotao.equals("btnEncerrarAtendimento")||nomeDoBotao.equals("Encerrar Atendimento")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnEncerrarAtendimento();
        }else if (nomeDoBotao.equals("btnDetalhes")||nomeDoBotao.equals("Detalhes")) {
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnDetalhe();
        }else if (nomeDoBotao.equals("btnImprimir")||nomeDoBotao.equals("Imprimir")) {
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnImprimir();
        }else if (nomeDoBotao.equals("btnAtualizar")||nomeDoBotao.equals("Atualizar")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnAtualizarFila();
        }else if (nomeDoBotao.equals("btnFoto")||nomeDoBotao.equals("Foto")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnFoto();
        }else if (nomeDoBotao.equals("btnCR")||nomeDoBotao.equals("CR")){
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnCR();
        }else if (nomeDoBotao.equals("btnHistorico")||nomeDoBotao.equals("Historico")) {
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnHistorico();
        }else if (nomeDoBotao.equals("btnMarcacao")||nomeDoBotao.equals("Marcacao")) {
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickBtnMarcacao();
        }else if (nomeDoBotao.equals("btnRastraeabilidade")||nomeDoBotao.equals("Rastraeabilidade")) {
            pageLaboratorioFilaDeEsperaRecepcaoCaixa.clickbtnRastreabilidade();
        }else{
            Assert.assertFalse("botão não encontrado",true);
            System.out.println("botão não encontrado");
        }
    }

}
