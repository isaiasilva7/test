package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_motivo;


import com.pixeon.smart.pages.atende.atendimento.atendimento_motivo.PageMotivo;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class MotivoSteps {
    PageMotivo pageMotivo;

    @Step
    public void infarma_o_motivo_a_observação_e_clicar_no_botão_da_tela_de_Motivo(String motivo, String observacao, String nomeDobotao) {
        pageMotivo.informaTxtMotivo(motivo);
        pageMotivo.informaTxtMotivo(observacao);
        if (nomeDobotao.equals("OK")||nomeDobotao.equals("Ok")){
            pageMotivo.clickBtnOK();
        }else if (nomeDobotao.equals("Cancelar")||nomeDobotao.equals("cancelar")) {
            pageMotivo.clickBtnCancelar();
        }else{
            Assert.assertFalse("botão não cadastrado",true);
        }
    }
}
