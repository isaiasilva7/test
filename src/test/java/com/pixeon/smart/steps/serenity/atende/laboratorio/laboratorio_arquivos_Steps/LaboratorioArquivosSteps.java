package com.pixeon.smart.steps.serenity.atende.laboratorio.laboratorio_arquivos_Steps;

import com.pixeon.smart.pages.atende.laboratorio.laboratorio_arquivos_Steps.PageLaboratorioArquivos;
import net.thucydides.core.annotations.Step;

public class LaboratorioArquivosSteps {
  PageLaboratorioArquivos pageLaboratorioArquivos;

    @Step
    public void clicar_botão_anexar_o_documento_da_tela_Arquivo_no_modulo_Atende_Laboratorio() {
        pageLaboratorioArquivos.clickBtnAnexar();
    }

    @Step
    public void clicar_no_botão_fechar_da_tela_Arquivo_no_modulo_Atende_Laboratorio() {
        pageLaboratorioArquivos.clickBtnFechar();
    }
}
