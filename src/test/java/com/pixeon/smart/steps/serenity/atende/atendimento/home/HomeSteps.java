package com.pixeon.smart.steps.serenity.atende.atendimento.home;

import com.pixeon.smart.pages.atende.atendimento.home.PageHome;
import com.pixeon.smart.pages.fature.PageFatureHome;

import net.thucydides.core.annotations.Step;

public class HomeSteps {

  PageHome pageHome;
  PageFatureHome fatureHome;

  @Step
   public void acesso_o_menu_no_Home_do_modulo_Atende(String menu) {
      pageHome.selecionarMenu(menu);
  }
 
  @Step
  public void acesso_o_menu_no_Home_do_modulo_Fature(String menu) {
	  fatureHome.selecionarMenu(menu);
  }
  
}
