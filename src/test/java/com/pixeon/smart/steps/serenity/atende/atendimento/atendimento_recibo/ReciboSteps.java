package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_recibo;

import com.pixeon.smart.pages.atende.atendimento.atendimento_recibo.PageRecibo;
import com.pixeon.smart.util.Utils;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class ReciboSteps {

PageRecibo pageRecibo;

    @Step
    public void selecionar_o_documento_na_tela_recebimento_Direto(String nomedoDocumento) {
        if (nomedoDocumento.equals("MEDICWARE")) {
            Utils.sleep(1000);
            pageRecibo.clickBtnScroolDocumento();
            pageRecibo.clickBtnScroolDocumento();
            Utils.sleep(400);
            pageRecibo.clickLblDocumentoMedicware();
        }else{
            Assert.assertFalse("Documento não configurado",true);

        }
    }

    @Step
    public void clicar_no_botão_Visualizar_na_tela_de_Recibo() {
        pageRecibo.clickBtnVisualizar();
    }

}
