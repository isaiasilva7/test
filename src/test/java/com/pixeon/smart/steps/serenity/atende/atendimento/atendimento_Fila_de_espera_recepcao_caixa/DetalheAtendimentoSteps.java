package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_Fila_de_espera_recepcao_caixa;

import com.pixeon.smart.pages.atende.atendimento.atendimento_fila_de_espera_recepcao_caixa.PageAtendimentoDetalhe;
import net.thucydides.core.annotations.Step;

public class DetalheAtendimentoSteps {

    PageAtendimentoDetalhe pageAtendimentoDetalhe;

    @Step
    public void clicar_no_botão_Retorna_pra_Fila_na_tela_Detalhes() {
        pageAtendimentoDetalhe.clickBtnRetornarPraFila();
    }
}
