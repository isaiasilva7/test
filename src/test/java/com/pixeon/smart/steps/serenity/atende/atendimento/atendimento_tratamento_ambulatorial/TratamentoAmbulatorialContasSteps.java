package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_tratamento_ambulatorial;


import com.pixeon.smart.pages.atende.atendimento.atendimento_tratamento_ambulatorial.PageTratamentoAmbulatorialContas;
import net.thucydides.core.annotations.Step;

public class TratamentoAmbulatorialContasSteps {

    PageTratamentoAmbulatorialContas pageTratamentoAmbulatorialContas;

    @Step
    public void clicar_na_GuiaTis_do_modulo_Atende() {
        pageTratamentoAmbulatorialContas.clickBtnGuiaTiss();
    }

}
