package com.pixeon.smart.steps.serenity.atende.atendimento.atendimento_tratamento_ambulatorial;


import com.pixeon.smart.pages.atende.atendimento.atendimento_tratamento_ambulatorial.PageAtendementoTratamentoAmbulatorial;
import net.thucydides.core.annotations.Step;


public class AtendimentoTratamentoAmbulatorialSteps {

    PageAtendementoTratamentoAmbulatorial pageAtendementoTratamentoAmbulatorial;

    @Step
    public void gravar_o_Tratamento_Ambulatorial_no_modulo_Atende() {
        pageAtendementoTratamentoAmbulatorial.ativarArearTratamentoAmbulatorialEGravar();
    }
}
