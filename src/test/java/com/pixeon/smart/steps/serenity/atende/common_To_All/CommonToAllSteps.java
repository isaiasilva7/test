package com.pixeon.smart.steps.serenity.atende.common_To_All;

import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.Utils;
import com.pixeon.util.UtilProperties;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.sikuli.script.App;

import static com.pixeon.smart.util.Utils.sleep;

public class CommonToAllSteps {
    PageCommonToAll pageCommonToAll;

    @Step
    public void clicar_no_botão_imprirmir_do_modulo_Atende() {
        pageCommonToAll.clickBtnImpirmir();
    }
    @Step
    public void clicar_no_botão_gravar_do_modulo_Atende() {
        sleep(1000);
        pageCommonToAll.clickbtnGravar();
    }

    @Step
    public void clicar_no_botão_cancelar_do_modulo_Atende() {
        pageCommonToAll.clickbtnCancelar();
    }
    @Step
    public void clicar_no_botão_novo_do_modulo_Atende() {
        pageCommonToAll.clickBnNovo();
    }
    @Step
    public void clicar_no_botão_buscar_do_modulo_Atende() {
        pageCommonToAll.clickbtnBuscar();
    }
    @Step
    public void clicar_no_botão_fechar_do_modulo_Atende() {
        pageCommonToAll.clickBtnFechar();
    }
    @Step
    public void clicar_no_botão_retornar_do_modulo_Atende() {
        pageCommonToAll.clickBtnRetornar();
    }
    @Step
    public void clicar_no_botão_avancar_do_modulo_Atende() {
        pageCommonToAll.clickbtnAvancar();
    }
    @Step
    public void clicar_no_botão_zoom_do_modulo_Atende() {
        pageCommonToAll.clickBtnZoom();
    }

    @Step
    public void clicar_no_botão_avancar_por_vezes_do_modulo_Atende(String vezes) {
        pageCommonToAll.clickbtnAvancar(vezes);
    }
    @Step
    public void clicar_na_tecla_do_teclado_no_modulo_Atende(String tecla) {
        Utils.teclaDeAtalho(tecla);
        sleep(1000);
    }


    @Step
    public void clicar_nos_botoes(String nomeDoBotao) {
        sleep(1000);
        if (nomeDoBotao.equals("btnGravar")){
            pageCommonToAll.clickbtnGravar();
        }else if (nomeDoBotao.equals("btnCancelar")||nomeDoBotao.equals("btncancelar")){
            pageCommonToAll.clickbtnCancelar();
        }else if (nomeDoBotao.equals("btnBusca")||nomeDoBotao.equals("btnbusca")){
            pageCommonToAll.clickbtnBuscar();
        }else if (nomeDoBotao.equals("btnAvancar")) {
            pageCommonToAll.clickbtnAvancar();
        }else if (nomeDoBotao.equals("btnImprimir")) {
            pageCommonToAll.clickBtnImpirmir();
        }else if (nomeDoBotao.equals("btnZoom")){
            pageCommonToAll.clickBtnZoom();
        }else if (nomeDoBotao.equals("btnRetornar")){
            pageCommonToAll.clickBtnRetornar();

        }else if (nomeDoBotao.equals("btnExcluir")){
            pageCommonToAll.clickBtnExcluir();
        } else{
            System.out.println("botão não encontrado");
            Assert.assertFalse("botão não encontrado",true);
        }
    }

    @Step
    public void fechar_modulo_Atende() {
        pageCommonToAll.moduloAtende("Atende125");
    }

    @Step
    public void fechar_a_tela_via_comando_ESC_do_teclado() {
        pageCommonToAll.comandoESCTeclado();
    }

    @Step
    public void o_sistema_apresenta_a_mensagem_Gravação_realizada_com_sucesso() {
        pageCommonToAll.validarMensagem();
    }
    @Step
    public void clicar_no_botão_Novo_no_modulo_Atende() {
        pageCommonToAll.clickBnNovo();
    }
}
