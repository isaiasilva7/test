package com.pixeon.smart;

import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions( plugin = { "pretty", 
							 "html:Relatorio",
							 "json:Relatorio/cucumber.json",
							 "junit:Relatorio/cucumber.xml" }, 
				  monochrome = true,
				  features = {"src/test/resources/features"}, 
				  tags = {"@ambulatorial_paciente"}, 
				  glue = {"com.pixeon.smart"},
				  dryRun = false
)

public class DefinitionTestSuite {
	
	
}

//glue = {"com.pixeon.smart"}, 