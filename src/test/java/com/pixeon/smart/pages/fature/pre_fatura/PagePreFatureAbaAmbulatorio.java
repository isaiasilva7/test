package com.pixeon.smart.pages.fature.pre_fatura;

import org.junit.Assert;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

public class PagePreFatureAbaAmbulatorio extends BasePage {
	
	   @FindBy(image = "fature_menu_arquivo_abrir_prefature", x=20, y=5)
       private SikuliElement menu_preFature;
	
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio")
	   private SikuliElement abaAmbulatorio;

	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Busca_Paciente")
	   private SikuliElement txtBuscaPaciente;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_Busca_Paciente")
	   private SikuliElement btnBuscaPaciente;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_Avancar")
	   private SikuliElement btnAvancar;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_Gravar")
	   private SikuliElement btnGravar;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_Novo")
	   private SikuliElement btnNovo;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Periodo", x=50, y=5)
	   private SikuliElement dtPeriodoInicial;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Periodo", x=180, y=5)
	   private SikuliElement dtPeriodoFinal;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_NenhumaOS")
	   private SikuliElement janelaSemOS;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_Processar")
	   private SikuliElement btnProcessar;

	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_SimProcessamentoFatura")
	   private SikuliElement btnSimProcessar;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_ProcessamentoTxtMensagem")
	   private SikuliElement TxtValidacaoMensagem;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_ComOS")
	   private SikuliElement SetorPosto2;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_SetaDeSelecao")
	   private SikuliElement SetaSelecao;

	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_MsgProcessoConcluido")
	   private SikuliElement MsgProcessoConcluido;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_OkProcessoConcluido")
	   private SikuliElement btnOkProcessoConcluido;
	   
	   @FindBy(image = "Pre_Fature_Aba_LogDeProcessamento")
	   private SikuliElement LogDeProcessamento;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_Botao_BuscarPorOs")
	   private SikuliElement btnBuscarPorOS;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_SerieOS")
	   private SikuliElement txtSerieOS;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_NumeroOS")
	   private SikuliElement txtNumeroOS;
	   
	   @FindBy(image = "Pre_Fature_Aba_Ambulatorio_BotaoOkInfNumeroOS")
	   private SikuliElement btnOKInfNumeroOS;
	   
	   
	      
	   public void clickAbrirPreFature() {
	        Utils.sleep(1000);
	        if ( menu_preFature.exists(10)){
	        	 menu_preFature.click();	        	
	        }else{
	            Assert.assertTrue("Menu Pré-Fature não encontrado", false);
	        }
	   }
	   
	    public void clickAbaAmbulatorio(){
	        Utils.sleep(1000);
	        if ( abaAmbulatorio.exists(10)){
	        	 abaAmbulatorio.click();	        	
	        }else{
	            Assert.assertTrue("Aba não encontrada", false);
	        }
	    }
	    
	    public void informarTxtBuscaPaciente(String buscaPaciente){
	        Utils.sleep(1000);
	        if ( txtBuscaPaciente.exists(10)){
	        	 txtBuscaPaciente.type(buscaPaciente);       	
	        }else{
	            Assert.assertTrue("Campo Busca não encontrado", false);
	        }
	    }
	    
	    public void informarTxtSerieOs(String serieOS){
	        Utils.sleep(1000);
	        if ( txtSerieOS.exists(10)){
	        	 txtSerieOS.type(serieOS);     
	        }else{
	            Assert.assertTrue("Campo 'Série da OS' não foi encontrado", false);
	        }
	    }
	    
	    public void informarTxtNumeroOs(String numeroOs){
	        Utils.sleep(1000);
	        if ( txtNumeroOS.exists(10)){   	
	        	 txtNumeroOS.type(numeroOs);   
	        }else{
	            Assert.assertTrue("Campo Busca não encontrado", false);
	        }
	    } 
	    
	    public void clickBotaoBuscar(){
	        Utils.sleep(2000);
	        if ( btnBuscaPaciente.exists(15)){
	        	 btnBuscaPaciente.click();	        	
	        }else{
	            Assert.assertTrue("Botão Buscar não encontrado", false);
	        }
	    }
	    
	    public void clickBotaoGravar(){
	        Utils.sleep(2000);
	        if ( btnGravar.exists(10)){
	        	 btnGravar.click();	        	
	        }else{
	            Assert.assertTrue("Botão Gravar não encontrado", false);
	        }
	    }
	    
	    public void clickBotaoNovo(){
	        Utils.sleep(2000);
	        if ( btnNovo.exists(10)){
	        	 btnNovo.click();	        	
	        }else{
	            Assert.assertTrue("Botão Novo não encontrado", false);
	        }
	    }  
	    
	    public void clickBotaoAvancar(){
	        Utils.sleep(2000);
	        if ( btnAvancar.exists(10)){
	        	 btnAvancar.click();	        	
	        }else{
	            Assert.assertTrue("Botão Avançar não encontrado", false);
	        }
	    }

	    public void informarPeriodoInicial(String periodoInicial){
	        Utils.sleep(1000);
	        if ( dtPeriodoInicial.exists(10)){
	        	 dtPeriodoInicial.type(periodoInicial);       	
	        }else{
	            Assert.assertTrue("Periodo Inicial não encontrado", false);
	        }
	    }
	    
	    public void informarPeriodoFinal(String periodoFinal){
	        Utils.sleep(1000);
	        if ( dtPeriodoFinal.exists(10)){
	        	 dtPeriodoFinal.type(periodoFinal);       	
	        }else{
	            Assert.assertTrue("Periodo Final não encontrado", false);
	        }
	    }
	    
	    public void JanelaSemOS(){
	        Utils.sleep(1000);
	        if ( janelaSemOS.exists(10)){
	        	 janelaSemOS.click();	 
	        }else{
	            Assert.assertTrue("Janela sem OS não encontrada ou não exibida", false);
	        }
	    }
	    
	    public void clickBotaoProcessar(){
	        Utils.sleep(2000);
	        if ( btnProcessar.exists(10)){
	        	 btnProcessar.click();		
	        }else{
	            Assert.assertTrue("Botão Processar não encontrado", false);
	        }
	    }
	    
	    public void clickBotaoBuscarPorOS(){
	        Utils.sleep(2000);
	        if ( btnBuscarPorOS.exists(10)){
	        	 btnBuscarPorOS.click();		
	        }else{
	            Assert.assertTrue("Botão 'Buscar Por OS' não encontrado", false);
	        }
	    }
	    
	    public void clickBotaoOKInformarNumeroOS(){
	        Utils.sleep(2000);
	        if ( btnOKInfNumeroOS.exists(10)){
	        	 btnOKInfNumeroOS.click();		
	        }else{
	            Assert.assertTrue("Botão 'OK' da tela 'Informar o numero da OS' não foi encontrado", false);
	        }
	    }
	    
	    public void clickSimProcessar(){
	        Utils.sleep(2000);
	        if ( btnSimProcessar.exists(10)){
	        	 btnSimProcessar.click();		
	        }else{
	            Assert.assertTrue("Opção: SIM do processamento da fatura não encontrado", false);
	        }
	    }
	    
	    public void TxtValidacaoMensagem(){
	        Utils.sleep(2000);
	        if (TxtValidacaoMensagem.exists(10)){
	        	TxtValidacaoMensagem.click();		
	        }else{
	            Assert.assertTrue("Houve algum erro: a mensagem esperada é: 'Nenhum item foi encontrado para esta pré-fatura' ", false);
	        }
	    }
	    
	    public void ExisteOS(){
	        Utils.sleep(1000);
	        if ( SetorPosto2.exists(10) && SetaSelecao.exists() ){
	        	 //Não faz nada;
	        }else{
	            Assert.assertTrue("Nenhuma OS encontrada no periodo especificado!", false);
	        }
	    }

	    public void MsgProcessoConcluido(){
	        Utils.sleep(1000);
	        if ( MsgProcessoConcluido.exists(10)){
	        	 MsgProcessoConcluido.click();		
	        }else{
	            Assert.assertTrue("Houve algum erro: a mensagem: 'Processo concluido' não foi exibida", false);
	        }
	    }
	    
	    public void ClickBotaoOK(){
	        Utils.sleep(1000);
	        if ( btnOkProcessoConcluido.exists(10)){
	        	 btnOkProcessoConcluido.click();		
	        }else{
	            Assert.assertTrue("Houve algum erro: o botão Ok não foi exibido", false);
	        }
	    }
	    
	    public void AbaLogDeProcessamento(){
	        Utils.sleep(1000);
	        if ( LogDeProcessamento.exists(10)){
	        	 LogDeProcessamento.click();		
	        }else{
	            Assert.assertTrue("Houve algum erro: nao foi possivel redirecionar para a aba de Log de Processamento", false);
	        }
	    }
	    
}
