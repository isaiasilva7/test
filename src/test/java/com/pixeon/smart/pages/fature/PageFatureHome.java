package com.pixeon.smart.pages.fature;


import com.pixeon.smart.pages.BasePage;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

public class PageFatureHome  extends BasePage {

    @FindBy(image = "fature_menu_arquivo", x=20, y=5)
    private SikuliElement menu_Arquivo;

    @FindBy(image = "fature_menu_arquivo_abrir", x=20, y=5)
    private SikuliElement menu_Abrir;

    @FindBy(image = "fature_menu_arquivo_abrir_prefature", x=20, y=5)
    public SikuliElement menu_prefature;
    
    public void selecionarMenu(String menu) {
        try{

            String frase = menu;
            String array[] = new String[frase.split(";").length];
            array = frase.split(";");

            for (int i = 0; i < array.length; i++) {
              //System.out.println(array[i]);
                switch( array[i] ) {
                    case "Arquivo":
                        menu_Arquivo.exists();
                        menu_Arquivo.doubleClick();
                        break;
                    case "Abrir":
                        menu_Abrir.exists();
                        menu_Abrir.doubleClick();
                        break;
                    case "Pré-Fatura":
                    	menu_prefature.exists();
                    	menu_prefature.doubleClick();
                        break;
                    default:
                        System.out.printf("Você digitou uma operação inválida.");
                }
            }
        }catch(Exception e){
            System.out.println("Erro ao esperar a imagem ou imagem não localizada." +e);
        }
    }
}




