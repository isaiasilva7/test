package com.pixeon.smart.pages.fature;

import org.junit.Assert;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

public class PageCronogramaFaturamento extends BasePage{

    @FindBy(image = "msgCronogramaFaturamentoOk", x=5, y=5)
    private SikuliElement btnOkCronograma;

    public void clickBtnCronogramaFaturamento(){
        Utils.sleep(500);
        btnOkCronograma.exists(6);
        Assert.assertTrue(btnOkCronograma.exists());
        btnOkCronograma.click();
    }

}
