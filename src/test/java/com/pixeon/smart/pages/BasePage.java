package com.pixeon.smart.pages;

import com.pixeon.smart.util.CustomDriver;
import io.github.marcoslimaqa.sikulifactory.SikuliFactory;
import net.thucydides.core.pages.PageObject;

public class BasePage extends PageObject {

    public BasePage() {
        SikuliFactory.initElements(CustomDriver.getInstance(), this);
    }
}
