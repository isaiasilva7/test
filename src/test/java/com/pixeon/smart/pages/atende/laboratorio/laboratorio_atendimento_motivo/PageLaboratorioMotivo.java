package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_motivo;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLaboratorioMotivo extends BasePage {



    @FindBy(image = "atendimento_btn_cancelar_motivo", x=2, y=5)
    private SikuliElement btnCancelar;


    @FindBy(image = "atendimento_btn_ok_motivo", x=2, y=5)
    private SikuliElement btnOK;


    @FindBy(image = "atendimento_txt_motivo_motivo", x=2, y=5)
    private SikuliElement txtMotivo;

    @FindBy(image = "atendimento_txt_observacao_motivo", x=2, y=5)
    private SikuliElement txtObservacao;


    public void clickBtnOK(){
        Utils.sleep(2000);
        btnOK.exists();
        Assert.assertTrue(btnOK.exists(1));
        btnOK.click();
        Utils.sleep(1000);
    }

    public void clickBtnCancelar(){
        Utils.sleep(200);
        Assert.assertTrue(btnCancelar.exists(6));
        btnCancelar.click();
        Utils.sleep(300);
    }

    public void informaTxtMotivo(String motivo){
        Utils.sleep(1000);
        txtMotivo.exists();
        Assert.assertTrue(txtMotivo.exists(1));
        txtMotivo.type(motivo);
        Utils.sleep(300);
    }

    public void informaTxtObservacao(String observacao){
        Utils.sleep(200);
        txtObservacao.exists();
        Assert.assertTrue(txtObservacao.exists(1));
        txtObservacao.type(observacao);
        Utils.sleep(300);
    }

}
