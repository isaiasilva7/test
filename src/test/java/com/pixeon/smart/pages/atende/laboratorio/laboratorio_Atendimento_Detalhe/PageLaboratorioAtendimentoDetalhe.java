package com.pixeon.smart.pages.atende.laboratorio.laboratorio_Atendimento_Detalhe;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLaboratorioAtendimentoDetalhe extends BasePage {
    @FindBy(image = "atendimento_btn_retornar pra_fila_detalhes", x=10, y=5)
    private SikuliElement btnRetornarPraFila;

    public void clickBtnRetornarPraFila() {
        Assert.assertTrue(btnRetornarPraFila.exists());
        btnRetornarPraFila.click();
        Utils.sleep(300);
    }

}
