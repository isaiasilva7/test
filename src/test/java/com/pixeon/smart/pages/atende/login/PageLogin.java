package com.pixeon.smart.pages.atende.login;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLogin extends BasePage {

	
	@FindBy(image = "atende_login_txtUsuario", x=20, y=5)
    private SikuliElement txrUsuario;

    @FindBy(image = "atende_login_txtSenha")
    private SikuliElement txtSenha;

    @FindBy(image = "atende_login_btnOk")
    private SikuliElement btnOK;


    @FindBy(image = "atende_login_btnCancelar")
    private SikuliElement btnCancelar;


    @FindBy(image = "atende_mensagem_correio")
    private SikuliElement memsagemAlerta;

    @FindBy(image = "atende_mensagem_btnNao")
    private SikuliElement btnNaoDaMessagem;


    public boolean validaAtendeAberta() {
        return txrUsuario.exists(5);
    }

    public boolean validarMensagemAlerta() {
        return memsagemAlerta.exists();
    }


   public void inserirUsuario(String usuario ){
      try {
       //   Thread.sleep(500);
          Utils.sleep(00);
        //  SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\login\\atende_login_txtUsuario.png", 30);

          txrUsuario.exists();
      Assert.assertTrue(txrUsuario.exists(8));
          txrUsuario.type(usuario);
      }catch (Exception e){
          e.getMessage();
      }
   }

    public void inserirSenha(String senha ){
        txtSenha.exists();
        txtSenha.type(senha);
    }

    public void clickOK(){
        btnOK.click();
    }

    public void clickCancelar(){
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }

    public void clickbtnNaoDaMessagem(){
        Assert.assertTrue(btnNaoDaMessagem.exists());
        btnNaoDaMessagem.click();
    }


}
