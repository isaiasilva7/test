package com.pixeon.smart.pages.atende.laboratorio.laboratorio_recebimento_Direto;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.CustomDriver;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;



public class PageLaboratorioRecebimentoDireto extends BasePage {
    @FindBy(image = "atende_btn_fechar_recebimento_direto", x=20, y=5)
    private SikuliElement btnRecebimentoDireto;

    @FindBy(image = "atende_btn_novo_recebimento", x=20, y=5)
    private SikuliElement btnNovo;

    @FindBy(image = "atende_btn_Excluir_recebimento_direto", x=20, y=5)
    private SikuliElement btnExcluir;

    @FindBy(image = "atende_btn_Documento_recebimento_direto", x=2, y=5)
    private SikuliElement btnDocumento;

    @FindBy(image = "atende_btn_Fechar_recebimento_direto", x=20, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atendimento_btn_Ok_recebimento_direto", x=5, y=2)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_Cbo_a_receber_Forma_de_Pagamento_recibimento_direto", x=2, y=5)
    private SikuliElement cboAreceber;

    @FindBy(image = "atendimento_Cbo_Boleto_Forma_de_Pagamento_recibimento_direto", x=2, y=5)
    private SikuliElement cboBoleto;

    @FindBy(image = "atendimento_Cbo_Cartao_Forma_de_Pagamento_recibimento_direto", x=2, y=5)
    private SikuliElement cbocartao;

    @FindBy(image = "atendimento_Cbo_Espece_Forma_de_Pagamento_recibimento_direto", x=2, y=5)
    private SikuliElement cboEspece;

    @FindBy(image = "atendimento_Cbo_Forma_de_Pagamento_recibimento_direto", x=30, y=30)
    private SikuliElement cboFormaDePagamento;

    @FindBy(image = "atendimento_chk_confirmacao_pagamento_recibimento_direto", x=30, y=10)
    private SikuliElement chkPagamentoConfirmado;

    @FindBy(image = "atendimento_Cbo_cheque_Forma_de_Pagamento_recibimento_direto", x=5, y=5)
    private SikuliElement cboCheque;

    @FindBy(image = "atendimento_Cbo_cheque_Forma_de_Pagamento_recibimento_direto_1", x=5, y=5)
    private SikuliElement cboCheque1;

    @FindBy(image = "atendimento_Cbo_desconto_Forma_de_Pagamento_recibimento_direto", x=10, y=5)
    private SikuliElement cboDesconto;

    @FindBy(image = "atendimento_Cbo_Externo_Forma_de_Pagamento_recibimento_direto", x=10, y=5)
    private SikuliElement cboExterno;

    @FindBy(image = "atendimento_Cbo_imposto_Forma_de_Pagamento_recibimento_direto", x=10, y=5)
    private SikuliElement cboimposto;

    @FindBy(image = "atendimento_Cbo_Nota_promissoria_Forma_de_Pagamento_recibimento_direto", x=10, y=5)
    private SikuliElement cboNotaPromissoria;

    @FindBy(image = "atendimento_Cbo_cheque_pre_Forma_de_Pagamento_recibimento_direto", x=10, y=5)
    private SikuliElement cboChequePreDatado;

    @FindBy(image = "atendimento_Scroll_forma_De_pagamento_recebimento_direto", x=2, y=2)
    private SikuliElement scrollFormaDEPagamento;


    @FindBy(image = "atendimento_txt_tipo_recebimento_direto", x=10, y=5)
    private SikuliElement txtTipo;

    @FindBy(image = "atendimento_txt_valorTotal_Recebimento_direto", x=20, y=5)
    private SikuliElement txtValorTotal;

    @FindBy(image = "atendimento_txt_bandeira_do_cartaão_recebimento_direto", x=5, y=5)
    private SikuliElement txtBandeiraDoCartao;

    @FindBy(image = "atendimento_txt_conta_cheque_recebimento_direto", x=5, y=40)
    private SikuliElement txtContaChegue;

    @FindBy(image = "atendiemtno_txt_numero_doCartao_recebimento_direto", x=5, y=5)
    private SikuliElement txtNumeroDoCartao;

    @FindBy(image = "atendimento_txt_data_vencimento_Recebimento_direto", x=5, y=40)
    private SikuliElement txtDataValidadeDoCartao;

    @FindBy(image = "atendimento_txt_data_vencimento_praca_Recebimento_direto", x=5, y=30)
    private SikuliElement txtDataValidadeDoCartao1;


    @FindBy(image = "atendimento_txt_valor_pagamento.png", x=5, y=40)
    private SikuliElement txtValorPagamento;

    @FindBy(image = "atendimento_txt_agencia_cheque_recebimento_direto", x=5, y=40)
    private SikuliElement txtAgenciaPagamento;
    @FindBy(image = "atendimento_txt_conta_cheque_recebimento_direto", x=5, y=40)
    private SikuliElement txtContaPagamento;

    @FindBy(image = "atendimento_txt_data_cheque_Recebimento_direto", x=5, y=40)
    private SikuliElement txtDataPagamento;

    @FindBy(image = "atendimento_txt_numero_cheque_recebimento_direto", x=5, y=4)
    private SikuliElement txtnumeroCartaoCheque;


    @FindBy(image = "atendimento_txt_praca_do_cheque_recebimento_direto",  x=10, y=5)
    private SikuliElement txtPraca;


    @FindBy(image = "atendimento_txt_valor_cheque_Recebimento_direto", x=5, y=20)
    private SikuliElement CboTitularidade;

    @FindBy(image = "atendimento_btn_Ok_motivo_cancelamento", x=5, y=5)
    private SikuliElement btnOkMotivoCancelamento;

    @FindBy(image = "atendemento_txt_observacao_motivo_do_cancelamento", x=5, y=5)
    private SikuliElement txObservacaoMotivoCancelamento;

    @FindBy(image = "atendimento_txt_motivo_canelamento_pagamento", x=5, y=5)
    private SikuliElement txMotivoCancelamento;

    @FindBy(image = "atendimento_recebimento_btn_Aprovar_desconto", x=5, y=5)
    private SikuliElement btnAprovar;

    @FindBy(image = "atendimento_recebimento_btn_parcela", x=5, y=5)
    private SikuliElement btnParcelar;

    @FindBy(image = "atendimento_txt_NSU_rede_recebimento_direto", x=5, y=25)
    private SikuliElement txtNsuRede;

    @FindBy(image = "atendimento_txt_NSU_rede_recebimento_direto", x=5, y=60)
    private SikuliElement txtNsuRede2;

    @FindBy(image = "atendimento_txt_NSU_rede_recebimento_direto", x=5, y=88)
    private SikuliElement txtNsuRede3;

    //atende_laboratorio_recebimento_Direto
    @FindBy(image = "laboratorio_recebimento_direto_btn_cancelar_pagamento", x=10, y=5)
    private SikuliElement btnCancelarPagamento;


    public String validarValorNSUrede3(){
        String valor;
        Utils.sleep(300);
        txtDataValidadeDoCartao1.click();
        txtDataValidadeDoCartao1.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        txtNsuRede3.click();
        txtNsuRede3.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        return  valor;
    }

    public String validarValorNSUrede2(){
        String valor;
        Utils.sleep(300);
        txtDataValidadeDoCartao1.click();
        txtDataValidadeDoCartao1.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        txtNsuRede2.click();
        txtNsuRede2.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        return  valor;
    }

    public String validarValorNSUrede1(){
        String valor;
        Utils.sleep(300);
        txtDataValidadeDoCartao1.click();
        txtDataValidadeDoCartao1.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        txtNsuRede.click();
        txtNsuRede.doubleClick();
        valor = Utils.capturadoTextoSelecionado();
        return  valor;
    }

    public void informaTxtNsuRede(String nsuRede){
        Utils.sleep(200);
        if ( txtNsuRede.exists(3)){
            txtNsuRede.type(nsuRede);
        }else{
            Assert.assertTrue( "caixa de texto não encontrada", false);
        }
    }

    public void clickBtnAprovar(){
        if (btnAprovar.exists(3)){
            btnAprovar.click();
        }else{
            Assert.assertTrue(false);
        }
    }


    public void clickBtnParcelar(){
        if (btnParcelar.exists(3)){
            btnParcelar.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnRecebimentoDireto(){
        Assert.assertTrue(btnRecebimentoDireto.exists());
        btnRecebimentoDireto.click();
    }

    public void clickBtnNovo(){
        Assert.assertTrue(btnNovo.exists());
        btnNovo.click();
    }

    public void clickBtnExcluir(){
        btnExcluir.exists();
        Assert.assertTrue(btnExcluir.exists());
        btnExcluir.click();
    }

    public void clickBtnFechar(){
        btnFechar.exists();
        Assert.assertTrue(btnFechar.exists());
        btnFechar.click();
    }

    public void clickBtnDocumento(){
        Utils.sleep(1000);
        btnDocumento.exists();
        Assert.assertTrue(btnDocumento.exists());
        btnDocumento.click();
    }

    public void clickBtnCancelarPagamento(){
        Utils.sleep(100);
       if ( btnCancelarPagamento.exists(6)){
           btnCancelarPagamento.doubleClick();
       }
        Utils.sleep(200);
    }

    public void clickBtnOK(){
        Utils.sleep(2000);
        Assert.assertTrue(btnOK.exists(6));
        btnOK.click();
        Utils.sleep(4000);
    }


    public void clickCboAreceber(){
        Utils.sleep(400);
        Assert.assertTrue(cboAreceber.exists());
        cboAreceber.click();
    }


    public void clickCboBoleto(){
        Utils.sleep(100);
        Assert.assertTrue(cboBoleto.exists());
        cboBoleto.click();
    }

    public void clickCboCartao(){
        Utils.sleep(100);
        Assert.assertTrue(cbocartao.exists());
        cbocartao.click();
    }

    public void clickcboEspece(){
        Utils.sleep(100);
        Assert.assertTrue(cboEspece.exists());
        cboEspece.click();
    }

    public void clickcboImposto(){
        Utils.sleep(100);
        Assert.assertTrue(cboimposto.exists());
        cboimposto.click();
    }


    public void clickcboFormaDePagamento(){
        Utils.sleep(100);
       if (cboFormaDePagamento.exists(10)){
        //   Assert.assertTrue(cboFormaDePagamento.exists(2));
           cboFormaDePagamento.click();
        }else {
           Assert.assertTrue(false);
       }


    }


    public void clickChkPagamentoConfirmado(){
        Utils.sleep(100);
        Assert.assertTrue(chkPagamentoConfirmado.exists());
    }

    public void clickCboDesconto(){
        Utils.sleep(100);
        Assert.assertTrue(cboDesconto.exists());
        cboDesconto.click();
    }

    public void clickCboCheque(){
        Utils.sleep(500);
        if (cboCheque.exists()){
            cboCheque.click();
        }else{
            cboCheque1.click();
        }
    }

    public void clickCboChequePreDatado(){
        Utils.sleep(100);
        cboChequePreDatado.exists();
        Assert.assertTrue(cboChequePreDatado.exists());
        cboChequePreDatado.click();

    }



    public void clickCboExterno(){
        Utils.sleep(100);
        cboExterno.exists();
        Assert.assertTrue(cboExterno.exists());
        cboExterno.click();

    }

    public void clickscrollFormaDEPagamento(){
        Utils.sleep(100);
        scrollFormaDEPagamento.exists();
        Assert.assertTrue(scrollFormaDEPagamento.exists());
        scrollFormaDEPagamento.click();
    }



    public void clickCboNotaPromissoria(){
        Utils.sleep(100);
        cboNotaPromissoria.exists();
        Assert.assertTrue( cboNotaPromissoria.exists());
        cboNotaPromissoria.click();
    }

    public void validarExistenciaBtnCancelarPagamento(){
        Utils.sleep(100);
        if ( btnCancelarPagamento.exists(8)){
            Assert.assertTrue( true);
        }else{
            Assert.assertTrue( false);
        }


    }

    public void informaTxtTipo(String tipo){
        Utils.sleep(1000);
        txtTipo.exists();
        Assert.assertTrue( txtTipo.exists());
        txtTipo.type(tipo);
    }

    public void informaTxtValorTotal(String ValorTotal){
        Utils.sleep(100);
        Assert.assertTrue( txtValorTotal.exists());
        txtValorTotal.type(ValorTotal);
    }


    public void informaTxtBandeiraDoCartao(String bandeiraDoCartao){
        Utils.sleep(100);
        Assert.assertTrue( txtBandeiraDoCartao.exists());
        txtBandeiraDoCartao.type(bandeiraDoCartao);
    }


    public void informaTxtConta(String numeroConta){
        Utils.sleep(100);
        Assert.assertTrue( txtContaChegue.exists());
        txtContaChegue.type(numeroConta);
    }

    public void informaTxtNumeroDoCartao(String numeroDoCartao){
        Utils.sleep(200);
        txtNumeroDoCartao.click();
        txtNumeroDoCartao.click();
        txtNumeroDoCartao.click();
        txtNumeroDoCartao.click();
         Assert.assertTrue( txtNumeroDoCartao.exists());
        txtNumeroDoCartao.type(numeroDoCartao);
    }

    public void informaTxtDataValidadeDoCartao(String DataValidadeDoCartao){
        Utils.sleep(100);
       // Assert.assertTrue( txtDataValidadeDoCartao.exists());
        txtDataValidadeDoCartao1.type(DataValidadeDoCartao);
    }


    public void informaTxtValorPagamento(String valorDoPagamento){
        Utils.sleep(100);
        txtValorPagamento.exists();
        Assert.assertTrue( txtValorPagamento.exists());
        txtValorPagamento.type(valorDoPagamento);
    }


    public void informaTxtAgenciaPagamento(String numeroDaAgencia){
        Utils.sleep(100);
        txtAgenciaPagamento.exists();
   //     Assert.assertTrue( txtAgenciaPagamento.exists());
        txtAgenciaPagamento.type(numeroDaAgencia);
    }

    public void informaTxtContaPagamento(String numeroDaConta){
        Utils.sleep(100);
        txtContaPagamento.exists();
   //     Assert.assertTrue( txtContaPagamento.exists());
        txtContaPagamento.type(numeroDaConta);
    }


    public void informaTxtDataPagamento(String datapagamento){
        Utils.sleep(100);
        txtDataPagamento.exists();
     //   Assert.assertTrue( txtDataPagamento.exists());
        txtDataPagamento.type(datapagamento);
    }

    public void informaTxtnumeroCartaoCheque(String numeroCartaoCheque){
        Utils.sleep(100);
        txtnumeroCartaoCheque.exists();
      Assert.assertTrue( txtnumeroCartaoCheque.exists());
        txtnumeroCartaoCheque.type(numeroCartaoCheque);
    }

    public void informaTxtPraca(String numeroPraca){
        Utils.sleep(100);
        txtPraca.exists();
        txtPraca.click();
       Assert.assertTrue( txtPraca.exists());
        txtPraca.type(numeroPraca);
    }

    public void selecionarCboTitularidade(String titularidade){
        Utils.sleep(100);
        CboTitularidade.exists();
       Assert.assertTrue( CboTitularidade.exists());
        CboTitularidade.click();

       if (titularidade.equals("mesma")||titularidade.equals("Mesma")){
           System.out.println("ok");
       }else{
           Assert.assertFalse("ainda não foi implemntado com outra titularidade", false);
       }

    }


    public void informaTxtMotivoCancelamento(String motivoCancelamento){
        Utils.sleep(100);
        txMotivoCancelamento.exists();
        Assert.assertTrue( txMotivoCancelamento.exists());
        txMotivoCancelamento.type(motivoCancelamento);
    }


    public void informaTxtObservacaoMotivoCancelamento(String observacao){
        Utils.sleep(100);
        txObservacaoMotivoCancelamento.exists();
        Assert.assertTrue( txObservacaoMotivoCancelamento.exists());
        txObservacaoMotivoCancelamento.type(observacao);
    }


    public void clickOkMotivoCancelamento(){
        Utils.sleep(100);
        btnOkMotivoCancelamento.exists();
        Assert.assertTrue(btnOkMotivoCancelamento.exists());
        btnOkMotivoCancelamento.click();

    }





}
