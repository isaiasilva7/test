package com.pixeon.smart.pages.atende.pop_up;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PagePouUp extends BasePage {

    @FindBy(image = "atede_Pop_up_Falha_Geral", x=20, y=5)
    private SikuliElement popUpFalhaGeral;

    @FindBy(image = "atende_btn_nao_Pop_up", x=20, y=5)
    private SikuliElement btnNao;

    @FindBy(image = "atende_btn_Sim_pop_up", x=2, y=3)
    private SikuliElement btnSim;

    @FindBy(image = "atende_btn_Sim1_pop_up", x=2, y=3)
    private SikuliElement btnSim1;

    @FindBy(image = "atende_btn_Sim3_pop_up", x=2, y=3)
    private SikuliElement btnSim3;

    @FindBy(image = "atende_btn_Sim4_pop_up", x=2, y=3)
    private SikuliElement btnSim4;

    @FindBy(image = "atende_Pop_concluir", x=20, y=5)
    private SikuliElement popUpConcluir;


    @FindBy(image = "atende_popup_alerta_informar_txtSexo_obrigatorio", x=20, y=5)
    private SikuliElement popUpObrigatoriedadeSexoPaciente;

    @FindBy(image = "atende_popup_mensagem_alerta_informar_CboConvenio_obrigatorio", x=20, y=5)
    private SikuliElement popUpObrigatoriedadeCboConvenio;

    @FindBy(image = "atende_popup_mensagem_alerta_informar_dataNascimento_obrigatorio", x=20, y=5)
    private SikuliElement popUpObrigatoriedadeDataNascimento;

    @FindBy(image = "atende_popup_alerta_informar_txtSexo_obrigatorio", x=20, y=5)
    private SikuliElement popUpObrigatoriedadeInformarSexo;

    @FindBy(image = "atende_popup_data_Invalida", x=20, y=5)
    private SikuliElement popUpDataInvalida;

    @FindBy(image = "atende_popup_btn_Ok", x=20, y=5)
    private SikuliElement btnOk;

    @FindBy(image = "atende_popup_btn_Ok1", x=20, y=5)
    private SikuliElement btnOk1;

    @FindBy(image = "atende_poup_alert_nao_e_valido_para_convenio", x=20, y=5)
    private SikuliElement popUpDadosInvalidaParaConvenio;

    @FindBy(image = "atende_popup_mensagem_alerta_sexo", x=20, y=5)
    private SikuliElement popUpSexoInvalido;

    @FindBy(image = "atende_popup_CPF_Invalido", x=20, y=5)
    private SikuliElement popUpCPFInvalido;

    @FindBy(image = "atende_popup_telefone_Invalido", x=20, y=5)
    private SikuliElement popUpTelefoneInvalido;

    @FindBy(image = "atende_alerta_campo_obrigatorio", x=20, y=5)
    private SikuliElement popUpAlertaCampoObrigatorio;

    @FindBy(image = "atende_alerta_1_campo_obrigatorio", x=20, y=5)
    private SikuliElement popUpAlertaCampoObrigatorio1;

    @FindBy(image = "atendimento_popup_btn_cancelar", x=20, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_popup_btn_ok_quantidade", x=5, y=5)
    private SikuliElement btnOKQuantidade;

    @FindBy(image = "atendimento_popup_btn_ok_quantidade", x=60, y=5)
    private SikuliElement btnCancelarQuantidade;

    @FindBy(image = "atendimento _Popup_Quantidade_txt_quntidade", x=20, y=5)
    private SikuliElement txtQuantidade;

    public void clicarBtnOKQuantidade() {
        if ( btnOKQuantidade.exists(5)){
            btnOKQuantidade.click();
        }else{
            Assert.assertTrue("botão não encontrado",false);
        }
    }

    public void clicarBtnCancelarQuantidade() {
        if ( btnCancelarQuantidade.exists(5)){
            btnCancelarQuantidade.click();
        }else{
            Assert.assertTrue("botão não encontrado",false);
        }
    }

    public void clicarBtnCancelar() {
        if ( btnCancelar.exists(5)){
            btnCancelar.click();
        }else{
            Assert.assertTrue("botão não encontrado",false);
        }
    }

    public void informarTxtQuantidade(String quantidade) {
        if ( txtQuantidade.exists(5)){
            txtQuantidade.type(quantidade);
        }else{
            Assert.assertTrue("Caixa de texto quantidade não encontrado",false);
        }
    }



    public void validarQueExistePopUpFlahaGeral()
    {
        Utils.sleep(800);
        if (popUpFalhaGeral.exists(5)){
            Assert.assertTrue(false);
        }else{
            Assert.assertTrue(true);
        }
    }

    public void validarQueExistePopUpconcluir() {
        boolean popUpConcluirExiste;
        popUpConcluirExiste=popUpConcluir.exists();
        Assert.assertTrue(popUpConcluirExiste);
    }

    public void clicarBtnSim() {
        Utils.sleep(8000);
       // btnSim.exists(6);
        if (btnSim.exists(50)||btnSim1.exists(50)||btnSim3.exists(50)||btnSim4.exists(50)){
            if (btnSim.exists(2)){
                btnSim.click();
            }else if (btnSim1.exists(2)){
               btnSim1.click();
           }else if (btnSim3.exists(2)){
                btnSim3.click();
            }else if (btnSim4.exists(2)){
                btnSim4.click();
            }
        }else{
            Assert.assertTrue(false);
        }
    }
    public void clicarBtnNao() {
        Assert.assertTrue(btnNao.exists());
        btnNao.click(); }

    public void validarExistenciaPopupObrigatoriedadeSexoPaciente() {
        Assert.assertTrue(popUpObrigatoriedadeSexoPaciente.exists());
    }

    public void validarExistenciaPopupAlertaCampoObrigatorio() {
        Utils.sleep(1000);
        popUpAlertaCampoObrigatorio.exists(8);
       if  (popUpAlertaCampoObrigatorio.exists(8)){
           Assert.assertTrue(popUpAlertaCampoObrigatorio.exists());
       }else{
           Assert.assertTrue(popUpAlertaCampoObrigatorio1.exists());
       }
    }

    public void validarExistenciapopUpObrigatoriedadeCboConvenio() {
        Assert.assertTrue(popUpObrigatoriedadeCboConvenio.exists());
    }
    public void validarExistenciapopUpopUpObrigatoriedadeDataNascimento() {
        Assert.assertTrue(popUpObrigatoriedadeDataNascimento.exists());
    }
    public void validarExistenciapopUpObrigatoriedadeInformarSexo() {

        Assert.assertTrue(popUpObrigatoriedadeInformarSexo.exists());
    }
    public void validarExistenciapopUpDataInvalida() {
        Assert.assertTrue(popUpDataInvalida.exists());
    }
    public void validarExistenciapopUpDadosInvalidaParaConvenio() {
        Assert.assertTrue(popUpDadosInvalidaParaConvenio.exists());
    }
    public void validarExistenciapopSexopInvalido() {
        Assert.assertTrue(popUpSexoInvalido.exists());
    }
    public void validarExistenciaPopUpTelefoneInvalido() {
        Assert.assertTrue(popUpTelefoneInvalido.exists());
    }
    public void validarExistenciaPopUpCPFInvalido() {
        Assert.assertTrue(popUpCPFInvalido.exists());
    }

    public void clickBtnOK() {
        Utils.sleep(200);
        if (btnOk.exists(6)){
            Assert.assertTrue(btnOk.exists());
            btnOk.click();
        }else{
            if (btnOk1.exists()){
                btnOk1.click();
            }else {
                Assert.assertTrue(btnOk1.exists());
            }
        }
        Utils.sleep(100);
    }


}
