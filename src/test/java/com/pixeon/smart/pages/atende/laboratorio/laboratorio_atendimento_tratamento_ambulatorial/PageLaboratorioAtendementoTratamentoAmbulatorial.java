package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_tratamento_ambulatorial;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

import javax.rmi.CORBA.Util;

import static com.pixeon.smart.util.Utils.sleep;

public class PageLaboratorioAtendementoTratamentoAmbulatorial extends BasePage {

    @FindBy(image = "atendimento_tratamento_ambulatorial_selecionar")
    private SikuliElement ativarAreTratamentoAmbulatorial;

    @FindBy(image = "atende_atendimento_mensagem_tratamento_gravado_com_sucesso")
    private SikuliElement lblMensagemTratamentoGravadoComSucesso;



    public void ativarArearTratamentoAmbulatorialEGravar(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\atendimento_tratamento_ambulatorial\\atendimento_tratamento_ambulatorial_selecionar", 30);
        sleep(3000);
        if (ativarAreTratamentoAmbulatorial.exists(30)){
            Assert.assertTrue( ativarAreTratamentoAmbulatorial.exists());
        }

        ativarAreTratamentoAmbulatorial.click();
        PageCommonToAll pageCommonToAll= new PageCommonToAll();
        pageCommonToAll.clickbtnGravar();
        pageCommonToAll.clickbtnGravar();
        pageCommonToAll.clickbtnGravar();
        pageCommonToAll.clickbtnGravar();
        sleep(1800);

        if(lblMensagemTratamentoGravadoComSucesso.exists(6)){
            Assert.assertTrue(lblMensagemTratamentoGravadoComSucesso.exists());
        }else{
            Assert.assertTrue(false);
        }


    }


}
