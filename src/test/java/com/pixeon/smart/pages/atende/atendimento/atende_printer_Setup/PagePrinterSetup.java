package com.pixeon.smart.pages.atende.atendimento.atende_printer_Setup;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PagePrinterSetup extends BasePage {
    @FindBy(image = "atende_printer_Setup_bln_scroll", x=5, y=5)
    private SikuliElement btnScroll;

    @FindBy(image = "atendimento_printer_Setup_btnOK", x=4, y=5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_Printer_Setup_lbl_Print_to_PDF", x=4, y=5)
    private SikuliElement lblPrintToPDF;

    @FindBy(image = "atendimento_Printer_Setup_lbl_Print_to_PDF_1", x=4, y=5)
    private SikuliElement lblPrintToPDF1;

    public void informarBtnOK (){
        Utils.sleep(500);
        btnOK.exists(2);
        Assert.assertTrue(btnOK.exists());
        btnOK.click();
    }


    public void selecionarImpressora (String nomeDaImpressora) {
        Utils.sleep(500);
        btnScroll.exists(2);
        Assert.assertTrue(btnScroll.exists());

        for (int i = 0; i < 10; i++) {
            if (nomeDaImpressora.equals("Print to PDF")) {
                if (lblPrintToPDF.exists()) {
                    lblPrintToPDF.click();
                    break;
                } else if(lblPrintToPDF1.exists()) {
                    lblPrintToPDF1.click();
                    break;

                }else {
                    btnScroll.click();
                }
            } else {
                Assert.assertFalse("Impressora não encontrada.", true);
            }
        }
    }


    public void clickLblPrintToPDF(){
        Utils.sleep(500);
        if (lblPrintToPDF.exists(5)){
            Assert.assertTrue(lblPrintToPDF.exists());
            lblPrintToPDF.click();
        }else{
            if (lblPrintToPDF1.exists(1)){
                lblPrintToPDF1.click();
            }else{
                Assert.assertTrue(lblPrintToPDF1.exists());
            }
        }
    }
}
