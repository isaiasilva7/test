package com.pixeon.smart.pages.atende.laboratorio.laboratorio_busca_paciente_por_OS;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.SikuliUtil;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.sikuli.script.Key;

public class PageLaboratorioBuscaPacientePorOS extends BasePage {

    @FindBy(image = "atende_numero_de_serie_OS", x=30, y=30)
    private SikuliElement txtImformarNumeroSerieDaOS;

     @FindBy(image = "atendimento_BtnOK", x=5, y=5)
     private SikuliElement btnOK;


    public void inserirtxtImformarNumeroSerieDaOS(String txt_Busca_Paciente ){
        SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\busca_paciente_por_OS\\atende_numero_de_serie_OS.png", 30);

        txtImformarNumeroSerieDaOS.type(txt_Busca_Paciente + Key.TAB);
    }

    public void clickBtnOK(){
        btnOK.click();
    }


    }
