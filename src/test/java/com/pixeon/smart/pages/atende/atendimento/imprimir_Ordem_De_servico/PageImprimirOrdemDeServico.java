package com.pixeon.smart.pages.atende.atendimento.imprimir_Ordem_De_servico;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageImprimirOrdemDeServico  extends BasePage {

    @FindBy(image = "atende_btn_cancelar_imprimir_OS")
    private SikuliElement btnCancelar;

    @FindBy(image = "atende_btn_Guia_Tiss_imprimir_OS")
    private SikuliElement btnGuiaTiss;

    @FindBy(image = "atende_btn_Impressora_imprimir_OS")
    private SikuliElement btnImpressora;

    @FindBy(image = "atende_btn_oK_imprimir_OS")
    private SikuliElement btnOK;

    @FindBy(image = "atende_btn_quantidade_De_etiqueta_impirmir_OS")
    private SikuliElement btnQtdeDeEtiquetas;

    @FindBy(image = "atende_btn_Relat_intern_imprimir_OS")
    private SikuliElement btnRelatIntern;

    @FindBy(image = "atendimento_busca_btnGravar")
    private SikuliElement btnGrava;

    @FindBy(image = "atende_btn_Visualizar_Atendimento")
    private SikuliElement btnVisualizar;


    public void clickBtnVisualizar(){
        Utils.sleep(2000);
        Assert.assertTrue(btnVisualizar.exists());
        btnVisualizar.click();
    }


    public void clickBtnCancelar(){
        Utils.sleep(3000);
        if (btnCancelar.exists(50)){
            Assert.assertTrue(btnCancelar.exists());
            btnCancelar.click();
        }else{
            Assert.assertTrue(false);
        }


    }

    public void clickBtnGuiaTiss(){
        if ( btnGuiaTiss.exists(5)){
            btnGuiaTiss.click();
        }else{
            Assert.assertTrue(false);
        }

    }
    public void clickBtnImpressora(){
       // Assert.assertTrue(btnImpressora.exists());

        if (btnImpressora.exists(5)){
            btnImpressora.click();
        }else{
            Assert.assertTrue(false);
        }


    }
    public void clickbtnOK(){
        Utils.sleep(2000);
        Assert.assertTrue(btnOK.exists(5));
        btnOK.click();
        Utils.sleep(2000);
    }

    public void clickBtnQtdeDeEtiquetas() {
        Utils.sleep(2000);
        Assert.assertTrue(btnQtdeDeEtiquetas.exists(6));
        btnQtdeDeEtiquetas.click();
        Utils.sleep(500);
    }


    public void clickBtnRelatIntern(){
        Utils.sleep(2000);
        btnRelatIntern.exists(10);
        Assert.assertTrue(btnRelatIntern.exists());
        btnRelatIntern.click();
    }
}
