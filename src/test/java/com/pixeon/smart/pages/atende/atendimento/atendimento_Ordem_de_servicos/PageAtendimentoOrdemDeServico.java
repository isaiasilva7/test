package com.pixeon.smart.pages.atende.atendimento.atendimento_Ordem_de_servicos;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

import org.junit.Assert;
import org.sikuli.script.Key;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

public class PageAtendimentoOrdemDeServico extends BasePage {

    @FindBy(image = "atendimento_ordem_de_servico_nome_paciente", x = 3, y = 5)
    private SikuliElement lblNomeDoPaciente;

    @FindBy(image = "atende_atendimento_ordem_servico_nome_paciente_laboratorio", x = 3, y = 5)
    private SikuliElement lblNomeDoPacientelaboratorio;

    @FindBy(image = "atende_atendimento_Ch_servico_1", x = 3, y = 5)
    private SikuliElement ChServico1;

    @FindBy(image = "atende_atendimento_codigo_do_servico_1", x = 3, y = 5)
    private SikuliElement txtcodigoDoServico1;

    //Tipo de atendimento
    @FindBy(image = "atendimento_Ordem_de_servico_txt_Tipo", x = 2, y = 18)
    private SikuliElement txtTipoAtendiemnto01;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_Tipo", x = 2, y = 50)
    private SikuliElement txtTipoAtendiemnto02;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_Tipo", x = 2, y = 80)
    private SikuliElement txtTipoAtendiemnto03;

    // txt codigo Do Servicos
    @FindBy(image = "atendimento_Ordem_de_servico_txt_codigo", x = 2, y = 40)
    private SikuliElement txtcodigoDoServico01;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_codigo", x = 2, y = 70)
    private SikuliElement txtcodigoDoServico02;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_codigo", x = 1, y = 80)
    private SikuliElement txtcodigoDoServico03;

    //Setor
    @FindBy(image = "atendimento_Ordem_de_servico_txt_Setor", x = 6, y = 18)
    private SikuliElement txtSetor01;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_Setor", x = 6, y = 50)
    private SikuliElement txtSetor02;

    @FindBy(image = "atendimento_Ordem_de_servico_txt_Setor", x = 3, y = 80)
    private SikuliElement txtSetor03;

    //Senha
    @FindBy(image = "atendimento_Ordem_de_servico_txt_Senha", x = 8, y = 18)
    private SikuliElement txtSenha01;
    @FindBy(image = "atendimento_Ordem_de_servico_txt_Senha", x = 8, y = 40)
    private SikuliElement txtSenha02;
    @FindBy(image = "atendimento_Ordem_de_servico_txt_Senha", x = 8, y = 60)
    private SikuliElement txtSenha03;

    @FindBy(image = "atende_atendimento_coleta_de_material_1", x = 8, y = 5)
    private SikuliElement txtcoletaDeMaterial1;

    @FindBy(image = "atende_atendimento_medico_servico_1", x = 3, y = 5)
    private SikuliElement txtmedicoServico1;

    @FindBy(image = "atende_atendimento_quantidade_servico_1", x = 3, y = 5)
    private SikuliElement txtquantidadeServico1;

    @FindBy(image = "atende_atendimento_senha_servico_1", x = 3, y = 5)
    private SikuliElement txtsenhaServico1;

    @FindBy(image = "atende_atendimento_valor_servico_1", x = 3, y = 20)
    private SikuliElement txtvalorServico1;

    @FindBy(image = "atende_txt_numero_os", x = 30, y = 5)
    private SikuliElement txtNumeroOs;

    @FindBy(image = "atende_txt_serie_os", x = 1, y = 5)
    private SikuliElement txtNumeroDeSerieOS;

    @FindBy(image = "atende_tipo_de_atendimento_OS", x = 100, y = 5)
    private SikuliElement CboTipoDeAtendimentoOS;

    @FindBy(image = "atende_tipo_de_atendimento_cirugia", x = 10, y = 5)
    private SikuliElement cboSelecioarTipoDeAtendimentoCirugia;

    @FindBy(image = "atende_tipo_de_atendimento_emergencial", x = 5, y = 5)
    private SikuliElement cboSelecionarTipoDeAtendimentoemergencial;

    @FindBy(image = "atende_informar_o_setor_Os", x = 27, y = 5)
    private SikuliElement txtSetorOS;

    @FindBy(image = "atende_bnt_pagamento_OS")
    private SikuliElement btnPagamento;

    @FindBy(image = "atende_btn_deposito_OS")
    private SikuliElement btnDeposito;

    @FindBy(image = "atendimento_btn_Adicionar_na_filla_OS")
    private SikuliElement btnAdicionarNaFilla;

    @FindBy(image = "atendimento_btn_Fila_de_espera_OS")
    private SikuliElement btnFilaDeEspera;

    @FindBy(image = "atendimento_Status_da_fatura_ordem_servico")
    private SikuliElement lblStatusFaturado;

    @FindBy(image = "atendimento_Status_da_fatura_aberto_ordem_servico")
    private SikuliElement lblStatusAberto;


    @FindBy(image = "atendimento_txt_tipo_codigo_ordem_de_servico", x = 5, y = 10)
    private SikuliElement txtTipoDeServico;

    @FindBy(image = "atendimento_btn_documento_ordem_de_servico", x = 5, y = 10)
    private SikuliElement btnDocumentos;

    @FindBy(image = "atendimento_btn_anexar_documento-ordem_de_servico", x = 5, y = 2)
    private SikuliElement btnAnexarDocumento;


    @FindBy(image = "atendimento_Ordem_de_servicos_btn_Registro_De_Ocorrencia", x = 5, y = 2)
    private SikuliElement btnRegistroDeOcorrencia;

    @FindBy(image = "atendimento_Ordem_de_servicos_btn_Ocorrencia", x = 5, y = 2)
    private SikuliElement btnOcorrencia;

    @FindBy(image = "atendimento_Ordem_de_servicos_btn_Observacao", x = 5, y = 2)
    private SikuliElement btnObservacao;

    @FindBy(image = "atendimento_Ordem_de_servicos_Btn_documentos", x = 5, y = 2)
    private SikuliElement btnDocumentos1;

    @FindBy(image = "atendimento_Ordem_de_servicos_btn_Diligenciamento", x = 5, y = 2)
    private SikuliElement btnDiligenciamento;

    @FindBy(image = "atendimento_Ordem_de_servicos_btn_Atendimento", x = 5, y = 2)
    private SikuliElement btnAtendimento;

    @FindBy(image = "atendimento_Ordem_de_servicos_btn_itens_os.", x = 5, y = 2)
    private SikuliElement btnItens;



    static public String numeroOSAtende;
    static public String numeroSerieOSAtende;
    static public String valorDoServico1;

    public void nomeDoPacientepresente() {
        if (lblNomeDoPaciente.exists(3)) {
            // SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\Atendimento_busca\\atendimento_ordem_de_servico_nome_paciente.png", 30);
            Assert.assertTrue(lblNomeDoPaciente.exists());
        } else if (lblNomeDoPacientelaboratorio.exists()) {
            Assert.assertTrue(lblNomeDoPacientelaboratorio.exists());
        } else {
            Assert.assertTrue(false);
        }
    }

    public void inserirChServico(String ChServico) {
        ChServico1.type(ChServico + Key.TAB);
    }


    public void inserirCodigoDoServico1(String codigoDoServicoUm) {
        Utils.sleep(1000);
        txtcodigoDoServico1.exists(2);
        txtcodigoDoServico1.type(codigoDoServicoUm + Key.TAB);
    }

    public void inserirQuantidadeServico1(String quantidadeServico) {
        txtquantidadeServico1.exists();
        txtquantidadeServico1.type(quantidadeServico + Key.TAB);
    }


    public void clickBtnDocumentos() {
        Utils.sleep(500);
        btnDocumentos.exists();
        btnDocumentos.click();
    }


    public void clickBtnRegistroOcorrencia() {
        Utils.sleep(500);
        btnRegistroDeOcorrencia.exists();
        btnRegistroDeOcorrencia.doubleClick();
    }

    public void clickBtnObservacao() {
        Utils.sleep(500);
        btnObservacao.exists();
        btnObservacao.click();
    }

    public void clickBtnDocumentos1() {
        Utils.sleep(500);
        btnDocumentos1.exists();
        btnDocumentos1.click();
    }

    public void clickBtnDiligenciamento() {
        Utils.sleep(500);
        btnDiligenciamento.exists();
        btnDiligenciamento.click();
    }

    public void clickBtnAtendimento() {
        Utils.sleep(500);
        btnAtendimento.exists();
        btnAtendimento.click();
    }


    public void clickBtnItensDeOs() {
        Utils.sleep(500);
        btnItens.exists();
        btnItens.click();
    }

    public void clickBtnAnexarDocumento() {
        Utils.sleep(500);
        btnAnexarDocumento.exists();
        btnAnexarDocumento.click();
    }

    public void clickBtnOcorrencia() {
        Utils.sleep(500);
        btnOcorrencia.exists();
        btnOcorrencia.click();
    }


    public void inserirMedicoServico1(String medicoServicoUm) {
        txtmedicoServico1.type(medicoServicoUm + Key.TAB);
    }

    public void inserirResponsavelColetaDeMaterial(String responsavelColetaDeMaterial) {
        Utils.sleep(4000);
        txtcoletaDeMaterial1.exists(6);
        txtcoletaDeMaterial1.type(responsavelColetaDeMaterial + Key.TAB);
        // Utils.sleep(200);
    }


    public String capturaNumeroOS() {
    	Utils.sleep(2500);
        txtNumeroDeSerieOS.exists(5);

        if (txtNumeroDeSerieOS.exists(5)){
            txtNumeroDeSerieOS.click();
            txtNumeroDeSerieOS.doubleClick();
            numeroSerieOSAtende = "";
            numeroSerieOSAtende = Utils.capturadoTextoSelecionado();
            Utils.sleep(2000);
            txtNumeroOs.doubleClick();
            numeroOSAtende = "";
            numeroOSAtende = Utils.capturadoTextoSelecionado();
        }else{
            Assert.assertTrue(true);
        }

        return numeroOSAtende + "." + numeroSerieOSAtende;
    }


    public String capturaValorDoServico1() {
        Utils.sleep(2200);
      try {
          if (txtvalorServico1.exists(10)){
              txtvalorServico1.doubleClick();
              Utils.sleep(1000);
              valorDoServico1 = "";
              valorDoServico1 = "";
              valorDoServico1 = Utils.capturadoTextoSelecionado();
              return valorDoServico1;
          }else{
              Utils.sleep(5000);
              txtvalorServico1.doubleClick();
              Utils.sleep(2000);
              valorDoServico1 = "";
              valorDoServico1 = "";
              valorDoServico1 = Utils.capturadoTextoSelecionado();
          }

      }catch (Exception e){
          Assert.assertTrue(false);
      }

        return valorDoServico1;

    }

    public void selecionaCBoTipoAtendiemnto(String tipoAtendimento) {  	
		 try {
			 if (tipoAtendimento.equals("Assistencial")||tipoAtendimento.equals("")){
                 System.out.println("ok");
            }else{
                 Utils.sleep(800);
			     CboTipoDeAtendimentoOS.exists(6);
                 CboTipoDeAtendimentoOS.doubleClick();
                
                 if (tipoAtendimento.equals("Emergência")||tipoAtendimento.equals("Emergencia")||tipoAtendimento.equals("emergencia")){

                         cboSelecionarTipoDeAtendimentoemergencial.exists();
                         cboSelecionarTipoDeAtendimentoemergencial.click();

                }else if (tipoAtendimento.equals("Cirugia")||tipoAtendimento.equals("cirugia")) {

                         cboSelecioarTipoDeAtendimentoCirugia.exists();
                         cboSelecioarTipoDeAtendimentoCirugia.click();
                }else{
                    System.out.println("tipode atendiemnto não encontrado");
                    Assert.assertTrue(false);
                }                 
           }
		} catch (Exception e) {
			// TODO: handle exception
			 System.err.println(e);
			  Assert.assertTrue(false);
		}

    }

    public void inserirSetorOS(String setor) {
    	try {
    	    if (setor.equals("")){

            }else {
                Utils.sleep(5000);
                txtSetorOS.exists();
                txtSetorOS.doubleClick();
                txtSetorOS.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
                txtSetorOS.doubleClick();
                txtSetorOS.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
                txtSetorOS.doubleClick();
                txtSetorOS.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
                txtSetorOS.doubleClick();
                txtSetorOS.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
                txtSetorOS.doubleClick();
                txtSetorOS.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
                txtSetorOS.type(setor);
            }
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
    }

    public void clickBtnPagamento(){
        Utils.sleep(500);
        Assert.assertTrue(btnPagamento.exists(3));
        btnPagamento.click();
        
    }
    public void clickBtnDeposito(){
        Assert.assertTrue(btnDeposito.exists());
        btnDeposito.click();
    }

    public void clickBtnAdicionarNaFilla(){
        Assert.assertTrue(btnAdicionarNaFilla.exists());
        btnAdicionarNaFilla.click();
    }

    public void clickbtnFilaDeEspera(){
        Assert.assertTrue(btnFilaDeEspera.exists());
        btnFilaDeEspera.click();
    }


    public void validarStatusFaturado(){
        Utils.sleep(2000);
        Assert.assertTrue(lblStatusFaturado.exists());
    }

    public void validarStatusAberto(){
        Utils.sleep(2000);
        Assert.assertTrue(lblStatusAberto.exists());
    }


    public void inserirTipoDeServico(String tipo){
        Utils.sleep(2000);
        Assert.assertTrue(txtTipoDeServico.exists(5));
        txtTipoDeServico.click();

        if (tipo.equals("")){

        }else{
            txtTipoDeServico.type(Key.DELETE+tipo);
        }
    }


    public void inserirTxtcodigoDoServico01(String codigo){
        Utils.sleep(2000);
        if (txtcodigoDoServico01.exists(5)){
            txtcodigoDoServico01.click();
            txtcodigoDoServico01.type(Key.DELETE+codigo+Key.DELETE);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtcodigoDoServico02(String codigo){
        Utils.sleep(1000);
        if (txtcodigoDoServico02.exists(5)){
            txtcodigoDoServico02.click();
            txtcodigoDoServico01.click();
            txtcodigoDoServico02.type(codigo);
        }else{
            Assert.assertTrue(false);
        }
    }


    public void inserirTxtcodigoDoServico03(String codigo){
        Utils.sleep(2000);
        if (txtcodigoDoServico03.exists(5)){
            txtcodigoDoServico03.click();
            txtcodigoDoServico03.type(Key.DELETE+codigo+Key.TAB);
        }else{
            Assert.assertTrue(false);
        }
    }


    public void inserirTxtSetor01(String tipo){
        Utils.sleep(2000);
        if (txtSetor01.exists(5)){
            txtSetor01.click();
            txtSetor01.doubleClick();
            txtSetor01.type(tipo);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtSetor02(String tipo){
        Utils.sleep(2000);
        if (txtSetor02.exists(5)){
            txtSetor02.click();
            txtSetor02.doubleClick();
            txtSetor02.type(tipo);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtSetor03(String tipo){
        Utils.sleep(2000);
        if (txtSetor03.exists(5)){
            txtSetor03.click();
            txtSetor03.type(Key.DELETE+tipo);
        }else{
            Assert.assertTrue(false);
        }
    }


    public void inserirTxtTipoAtendimento01(String tipo){
        Utils.sleep(2000);
        if (txtTipoAtendiemnto01.exists(5)){
            txtTipoAtendiemnto01.click();
            txtTipoAtendiemnto01.type(tipo+Key.TAB);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtTipoAtendimento02(String tipo){
        Utils.sleep(2000);
        if (txtTipoAtendiemnto02.exists(5)){
            txtTipoAtendiemnto02.click();
            txtTipoAtendiemnto02.click();
            txtTipoAtendiemnto02.type(tipo+Key.TAB);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtTipoAtendimento03(String tipo){
        Utils.sleep(2000);
        if (txtTipoAtendiemnto03.exists(5)){
            txtTipoAtendiemnto03.click();
            txtTipoAtendiemnto03.click();
            txtTipoAtendiemnto03.type(tipo+Key.TAB);
        }else{
            Assert.assertTrue(false);
        }
    }



    public void inserirTxtSenha01(String tipo){
        Utils.sleep(2000);
        if (txtSenha01.exists(5)){
            txtSenha01.click();
            txtSenha01.type(Key.DELETE+tipo);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtSenha02(String tipo){
        Utils.sleep(2000);
        if (txtSenha02.exists(5)){
            txtSenha02.click();
            txtSenha02.type(Key.DELETE+tipo);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void inserirTxtSenha03(String tipo){
        Utils.sleep(2000);
        if (txtSenha03.exists(5)){
            txtSenha03.click();
            txtSenha03.type(Key.DELETE+tipo);
        }else{
            Assert.assertTrue(false);
        }
    }




}
