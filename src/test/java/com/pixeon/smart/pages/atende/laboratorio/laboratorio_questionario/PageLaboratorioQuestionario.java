package com.pixeon.smart.pages.atende.laboratorio.laboratorio_questionario;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

import java.sql.SQLOutput;

public class PageLaboratorioQuestionario extends BasePage {

    @FindBy(image = "atende_questionario_btn_inicio", x=20, y=5)
    private SikuliElement btnIniciar;

    @FindBy(image = "atende_questionario_btn_anterior", x=20, y=5)
    private SikuliElement btnAnterior;

    @FindBy(image = "atende_questionario_btn_proximo", x=20, y=5)
    private SikuliElement btnProximo;

    @FindBy(image = "atende_questionario_btn_cancelar", x=20, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atende_questionario_btn_concluir", x=20, y=5)
    private SikuliElement btnConcluir;

    @FindBy(image = "atende-questionario_txt_resposta", x=40, y=5)
    private SikuliElement txtResposta;

    @FindBy(image = "atende-questionario_txt_resposta_1", x=40, y=5)
    private SikuliElement txtResposta1;

    @FindBy(image = "atende_questionario_btn_imprimir", x=20, y=5)
    private SikuliElement btnImprimir;

    @FindBy(image = "atende_questionario_btn_fechar", x=20, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atende_questionario_bnt_Novo", x=20, y=5)
    private SikuliElement btnNovo;

    @FindBy(image = "atende_questionario_txt_qestionario", x=20, y=5)
    private SikuliElement txtQuestionario;

    @FindBy(image = "atende_questionario_btn_lupa_visualizacao_qestionario", x=3, y=5)
    private SikuliElement btnLupaVisualizacao;

    @FindBy(image = "atende_questionario_lbl_resposta_Do_Questionario_na_tela_de_impresao", x=3, y=5)
    private SikuliElement LblRespostaQuestionario;

    public void clickBtnIniciar(){
       // SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\questinario\\atende_questionario_btn_inicio.png", 10);
        Utils.sleep(2000);
        btnIniciar.exists(5);
        Assert.assertTrue(btnIniciar.exists());
        btnIniciar.click();
    }

    public void clickBtnAnterior(){
        Assert.assertTrue(btnAnterior.exists());
        btnAnterior.click();
    }

    public void clickBtnCancelar(){
        Utils.sleep(2000);
        Assert.assertTrue(btnCancelar.exists(3));
        btnCancelar.click();
    }

    public void clickBtnConcluir(){
        Assert.assertTrue(btnConcluir.exists());
        btnConcluir.click();
    }

    public void clickBtnProximo(){
        Assert.assertTrue(btnProximo.exists());
        btnProximo.click();
    }

    public void informarTxtResposta(String resposta) {
        txtResposta.exists(6);
        txtResposta1.exists(2);
        if (txtResposta1.exists()) {
            Assert.assertTrue(txtResposta1.exists());
            txtResposta1.type(resposta);
        } else{
            txtResposta.exists(6);
            Assert.assertTrue(txtResposta.exists());
            txtResposta.type(resposta);
       }
    }

    public void informarTxtQuestionario(String questionario){
        Assert.assertTrue(txtQuestionario.exists());
        txtQuestionario.type(questionario);
    }


    public void clickBtnImprimir(){
        Assert.assertTrue(btnImprimir.exists());
        btnImprimir.click();
    }

    public void clickBtnFechar(){
        btnFechar.exists(3);
        Assert.assertTrue(btnFechar.exists());
        btnFechar.click();
    }

    public void clickBtnNovo(){
        Utils.sleep(2000);
        btnNovo.exists(3);
        Assert.assertTrue(btnNovo.exists());
        btnNovo.click();
    }

    public void clickBtnLupaVisualizacao(){
        btnLupaVisualizacao.exists(3);
        Assert.assertTrue(btnLupaVisualizacao.exists());
        btnLupaVisualizacao.click();
    }

    public void validarAExistenciaDaRespostaDoQuestionario(){
        Utils.sleep(2000);
        LblRespostaQuestionario.exists(3);
        Assert.assertTrue(LblRespostaQuestionario.exists());
    }


}
