package com.pixeon.smart.pages.atende.atendimento.atendimento_busca_paciente;

import com.gargoylesoftware.htmlunit.html.Keyboard;
import com.pixeon.smart.datamodel.atende.atendimento_busca_paciente.DataAtendeAtendimentoBuscaPaciente;
import com.pixeon.smart.pages.BasePage;

import com.pixeon.smart.util.DataHelper;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

import javax.rmi.CORBA.Util;
import java.sql.SQLOutput;
import java.util.List;
import java.util.SortedMap;

public class PageAtendimentoBuscaPaciente extends BasePage {

    @FindBy(image = "Atende_Txt_busca_paciente", x=35, y=5)
    private SikuliElement txtBuscaPaciente;

    @FindBy(image = "atende_resultado_busca_paciente", x=35, y=5)
    private SikuliElement LblBuscaPaciente;

    @FindBy(image = "atende_atendimenteo_buscar_paciente_por_OS", x=35, y=5)
    private SikuliElement btnBuscarParcientePorOS;

    @FindBy(image = "atende_atendimento_busca_por_os", x=35, y=5)
    private SikuliElement btnBuscarPorOS;

    @FindBy(image = "atende_btnSim1_incluir_novo_paciente", x=5, y=5)
    private SikuliElement btnSim1;

    @FindBy(image = "atende_btnSim2_incluir_novo_paciente", x=5, y=5)
    private SikuliElement btnSim2;

    @FindBy(image = "atendimento_Btn_Sim_adiconar_paciente", x=5, y=5)
    private SikuliElement btnSim3;

    @FindBy(image = "atende_atendimento_busca_txtSexo")
    private SikuliElement txtsexo;

    @FindBy(image = "atende_atendimento_busca_txtDataNascimento",  x=-2)
    private SikuliElement txtDataNascimento;

    @FindBy(image = "atende_atendimento_busca_txtTelefone", y=20)
    private SikuliElement txtTelefone;

    @FindBy(image = "atende_atendimento_busca_txtConvenio", x=-1, y=5)
    private SikuliElement txtConvenio;

    @FindBy(image = "atende_atendimento_busca_txtCPF", x=5, y=5)
    private SikuliElement txtCPF;

    @FindBy(image = "atende_atendimento_busca_txtNomeDaMae", x=5, y=5)
    private SikuliElement txtNomeDaMae;

    @FindBy(image = "atende_atendimento_busca_txtNomeSocial", x=5, y=5)
    private SikuliElement txtNomeSocial;

    @FindBy(image = "atende_atendimento_paciente_gravado_com_sucesso")
    private SikuliElement lblPacienteGravadoComSucesso;

    @FindBy(image = "atende_atendimento_busca_btnTratamento_ambulatorial")
    private SikuliElement btnTratamentoAmbulatorial;

    @FindBy(image = "atende_atendimento_btn_adicionar_na_fila_cadastro_paciente")
    private SikuliElement btnAdicionarNaFila;

    @FindBy(image = "atende_atendimento_btn_Atendimento_cadastro_paciente")
    private SikuliElement btnAtendimento;

    @FindBy(image = "atende_atendimento_btn_Fila_de_espera_cadastro_paciente")
    private SikuliElement btnFilaDeEspera;

    @FindBy(image = "atende_atendimento_btn_guia_cadastro_paciente")
    private SikuliElement btnGuia;

    @FindBy(image = "atende_btnGravar")
    private SikuliElement btnGravar;

    @FindBy(image = "atende_atendimento_btn_biaometria_busca")
    private SikuliElement btnBiometria;

    @FindBy(image = "atende_atendimento_Btn_ler_cartao_busca")
    private SikuliElement btnlerCartao;

    @FindBy(image = "atendimento_msg_alerta_paciente_não_foi_encontrado")
    private SikuliElement msgAlertaPacienteNaoEncontrado;



    Utils utils =new Utils();
    static public String nomePaciente;

    public void inserirtxtBuscaPaciente(String txt_Busca_Paciente ) {
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\Atendimento_busca\\Atende_Txt_busca_paciente.png", 30);
        Utils.sleep(1500);
      if (txtBuscaPaciente.exists(50)){
          if (txt_Busca_Paciente.equals("nomePacienteAleatorio")) {
              //  Utils utils =new Utils();
              nomePaciente = "";
              nomePaciente = "ROBO " + utils.nomeAleatorio(5) + " " + utils.nomeAleatorio(5) + " " + utils.nomeAleatorio(4) + " ROBO";
              Assert.assertTrue(txtBuscaPaciente.exists());
              txtBuscaPaciente.type(nomePaciente + Key.TAB);
          } else if (txt_Busca_Paciente.equals("PacienteNaMemoria")){
              txtBuscaPaciente.type(nomePaciente + Key.TAB);

          }else{
              txtBuscaPaciente.type(txt_Busca_Paciente + Key.TAB);
          }
      }else {
          Assert.assertFalse("XT busca não esta presente", true);
      }


    }

    public void clickBtnGravar(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(2000);
        btnGravar.exists(6);
        Assert.assertTrue(btnGravar.exists());
        btnGravar.click();
    }

    public void clicarBuscarPacientePorOS (){

        if (btnBuscarParcientePorOS.exists()){
           SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\Atendimento_busca\\Atende_Atendiemnteo_buscar_paciente_por_OS.png", 3);
            Assert.assertTrue(btnBuscarParcientePorOS.exists());
            btnBuscarParcientePorOS.click();
        }else if ( btnBuscarPorOS.exists()) {
            btnBuscarPorOS.click();
        }else{
            Assert.assertTrue(false);
        }

    }

    public void pacientePresente(){
        Utils.sleep(1000);
        if ( LblBuscaPaciente.exists(30)){
           Assert.assertTrue(LblBuscaPaciente.exists());
        }else{
            Assert.assertTrue("paciente não encontrado", false);
        }
    }

    public void clickBtnSim(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\Atendimento_busca\\atende_mensagem_paciente_nao_foi_incluido.png", 30);
        btnSim1.exists(2);



            if (!msgAlertaPacienteNaoEncontrado.exists(15)){
                Utils.sleep(1000);
            }

       // msgAlertaPacienteNaoEncontrado.click();
       // Utils.sleep(1000);


        if (btnSim1.exists(3)) {
            btnSim1.doubleClick();
        } else {
            if (btnSim2.exists(1)) {
                btnSim2.doubleClick();
            } else {
                if ( btnSim3.exists(1)) {
                    btnSim3.doubleClick();
                } else {
                    Assert.assertTrue("Botão sim não encontrado", false);
                }
            }
        }

    }




    public void preencheCampoCadastroPaciente(List<DataAtendeAtendimentoBuscaPaciente> dataAtendeAtendimentoBuscaPaciente) {
        DataAtendeAtendimentoBuscaPaciente data= DataHelper.getData(dataAtendeAtendimentoBuscaPaciente);
        // Sexo
        if (data.getSexo() != null) {
               Assert.assertTrue( txtsexo.exists());
               txtsexo.type(data.getSexo()+Key.TAB);
        }

        //data_nascimento
        if (data.getData_nascimento() != null) {
            txtDataNascimento.type(data.getData_nascimento() );
        }

        //telefone
        if (data.getTelefone() != null) {
            txtTelefone.type(data.getTelefone());
        }

        //convenio
        if (data.getConvenio() != null) {
            txtConvenio.type(data.getConvenio());
        }

        //CPF
        if (data.getcPF() != null) {
            if (data.getcPF().equals("CPF_Gerado_Aleatorio")) {
                txtCPF.type(utils.geraCPF());
            }else{
                txtCPF.type(data.getcPF());
            }

        }

        //Nome da Mae
        if (data.getNome_da_mae() != null) {
            txtNomeDaMae.type(data.getNome_da_mae());
        }

        //Nome social
        if (data.getNome_social() != null) {
            txtNomeSocial.type(data.getNome_social());
        }

    }

    public void validarAConfirmacaoPacienteGravadoComSucesso(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\Atendimento_busca\\atende_atendimento_paciente_gravado_com_sucesso.png", 30);
        lblPacienteGravadoComSucesso.exists();
        lblPacienteGravadoComSucesso.exists(6);
        Assert.assertTrue(lblPacienteGravadoComSucesso.exists());
    }

    public void preencheCampoCadastroPaciente(String sexo, String data_nascimento, String telefone, String convenio, String cPF, String nome_da_mae, String nome_social){

        if (!sexo.equals("")) {
            txtsexo.type(sexo+Key.TAB);
        }

        //data_nascimento
        if (!data_nascimento.equals("")) {
            txtDataNascimento.type(data_nascimento);
        }

        //telefone
        if (!telefone.equals("")) {
        //    txtTelefone.click();
            txtTelefone.type(telefone);
        }

        //convenio
        if (!convenio.equals("")) {
           // txtConvenio.doubleClick();
            Utils.sleep(100);
            txtConvenio.exists();txtConvenio.doubleClick();
            txtConvenio.type(Key.BACKSPACE + Key.BACKSPACE+ Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE  );
            Utils.sleep(200);
            txtConvenio.doubleClick();
            txtConvenio.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE+ Key.DELETE);
            Utils.sleep(300);
            txtConvenio.type(Key.BACKSPACE +Key.DELETE + convenio+Key.TAB);
            //txtConvenio.type(Key.TAB);
        }

        //CPF
        if (!cPF.equals("")) {
                if (cPF.equals("CPF_Gerado_Aleatorio")) {
                    //Utils utils =new Utils();
                //    txtCPF.click();
                    txtCPF.type(utils.geraCPF());
                }else{
                    txtCPF.type(cPF);
                }
        }

        //Nome da Mae
        if (!nome_da_mae.equals("")) {
            if (nome_da_mae.equals("nome_da_mae_Gerado_Aleatorio")) {
              //  txtNomeDaMae.click();
                txtNomeDaMae.type(utils.nomeAleatorio(4)+" "+utils.nomeAleatorio(4));
            }else{
                txtNomeDaMae.type(nome_da_mae);
            }
        }

        //Nome social
        if (!nome_social.equals("")) {
            if (nome_social.equals("nome_social_Gerado_Aleatorio")) {
                txtNomeSocial.type(utils.nomeAleatorio(5));
            }else{
                txtNomeSocial.type(nome_social);
            }
        }

    }


    public void clickBtnTratamentoAmbulatorial(){
        Utils.sleep(1000);
        if (btnTratamentoAmbulatorial.exists(6)){
           // Assert.assertTrue(btnTratamentoAmbulatorial.exists());
            if ( (btnTratamentoAmbulatorial.exists(2))){
                btnTratamentoAmbulatorial.click();
                Utils.sleep(1000);

                if ((btnTratamentoAmbulatorial.exists(2))){
                   btnTratamentoAmbulatorial.click();
                   Utils.sleep(1000);
               }

            }else{
                Assert.assertFalse("botão Tra. Ambulatorial não encontrado", true);
            }
        }else{
            Assert.assertFalse("botão Tra. Ambulatorial não encontrado", true);
        }
    }

    public void clickBtnAdicionarNaFila(){
        Assert.assertTrue(btnAdicionarNaFila.exists(5));
        btnAdicionarNaFila.click();
    }

    public void clickBtnAtendimento(){
        Assert.assertTrue(btnAtendimento.exists(5));
        btnAtendimento.click();
    }

    public void clickBtnFilaDeEspera(){
        Utils.sleep(300);
      if (btnFilaDeEspera.exists(5)){
          btnFilaDeEspera.click();
      }else{
          Assert.assertTrue("botão não encontrado",true);
      }



    }
    public void clickBtnGuia(){
        Assert.assertTrue(btnGuia.exists(5));
        btnGuia.click();
    }

    public void clickBtnBiometria(){
        Utils.sleep(1000);
        Assert.assertTrue(btnBiometria.exists(6));
        btnBiometria.click();
    }

    public void clickBtnlerCartao(){
        Utils.sleep(1000);
        Assert.assertTrue(btnlerCartao.exists(6));
        btnlerCartao.click();
    }

}
