package com.pixeon.smart.pages.atende.laboratorio.laboratorio_Dados_Cadastrais_do_Paciente;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioCadastroDadosPacienteProntuario extends BasePage {

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_mais")
    private SikuliElement btnMais;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_voltar")
    private SikuliElement btnVoltar;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_Convenio_Secundario")
    private SikuliElement btnConvenioSecundario;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_deficiencia")
    private SikuliElement btnDeficiencias;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_prontuario")
    private SikuliElement btnProntuario;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_acompanhante")
    private SikuliElement btnAcompanhante;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_Rn")
    private SikuliElement btnRN;

   // botão tela principal
    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_cadastro_biometria_2")
    private SikuliElement btnBiometria;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_OK")
    private SikuliElement btnOK;

    @FindBy(image = "atende_Laboratorio_dados_cadastrais_btn_cancelar")
    private SikuliElement btnCancelar;


    public void clickBtnMais() {
        if (btnMais.exists(6)){
            btnMais.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnVoltar() {
        if (btnVoltar.exists(6)){
            btnVoltar.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnConvenioSecundario() {
        if (btnConvenioSecundario.exists(6)){
            btnConvenioSecundario.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnDeficiencias() {
        if (btnDeficiencias.exists(6)){
            btnDeficiencias.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnProntuario() {
        if (btnProntuario.exists(6)){
            btnProntuario.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnAcompanhante() {
        if (btnAcompanhante.exists(6)){
            btnAcompanhante.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnRn() {
        if (btnRN.exists(6)){
            btnRN.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnBiometria() {
        if (btnBiometria.exists(6)){
            btnBiometria.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnOK() {
        if (btnOK.exists(6)){
            btnOK.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }

    public void clickBtnCancelar() {
        if (btnCancelar.exists(6)){
            btnCancelar.click();
        }else{
            Assert.assertTrue("botão não encontrado", false);
        }
    }


}
