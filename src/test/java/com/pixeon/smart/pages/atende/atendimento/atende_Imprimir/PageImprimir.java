package com.pixeon.smart.pages.atende.atendimento.atende_Imprimir;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageImprimir extends BasePage {
    @FindBy(image = "atende_imprimir_btn_Cancelar", x=4, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atende_imprimir_btn_Impressora", x=4, y=5)
    private SikuliElement btnImpressora;

    @FindBy(image = "atende_imprimir_btn_Ok", x=4, y=5)
    private SikuliElement btnOK;

    public void informarBtnCancelar (){
        Utils.sleep(500);
        btnCancelar.exists(2);
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }

    public void informarbtnImpressora (){
        Utils.sleep(500);
        btnImpressora.exists(2);
        Assert.assertTrue(btnImpressora.exists());
        btnImpressora.click();
    }

    public void informarBtnOK (){
        Utils.sleep(500);
        btnOK.exists(2);
        Assert.assertTrue(btnOK.exists());
        btnOK.click();
    }
}
