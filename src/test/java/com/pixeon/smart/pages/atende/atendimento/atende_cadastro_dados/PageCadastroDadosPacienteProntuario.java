package com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import javafx.scene.control.Tab;
import org.junit.Assert;
import org.sikuli.script.Key;

import java.sql.SQLOutput;

import static org.sikuli.script.Commands.exists;

public class PageCadastroDadosPacienteProntuario extends BasePage {

    @FindBy(image = "atendimento_lbl_prontuario_cadastro_prontuario", x=30, y=5)
    private SikuliElement lblProntuario;

    @FindBy(image = "atendimento_btn_prontuario_cadastro_prontuario", x=0, y=5)
    private SikuliElement btnProntuario;

    @FindBy(image = "atendimento_lbl_paciente_ja_possou _numero_prontuariocadastro_prontuario", x=5, y=5)
    private SikuliElement lblJaPossuiNumeroDoProntuario;

    @FindBy(image = "atendimento_txt_observacao_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtObservacao;

    @FindBy(image = "atendimento_txt_numero_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtNumeroEndereco;

    @FindBy(image = "atendimento_txt_bairro_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtBairro;

    @FindBy(image = "atendimento_txt_Cidade_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtCidade;

    @FindBy(image = "atendimento_txt_medico_dados _cadastrais_prontuario", x=50, y=5)
    private SikuliElement txtMedico;

    @FindBy(image = "atendimento_txt_CEP_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtCEP;

    @FindBy(image = "atendimento_txt_estado_civil_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtEstadoCivil;

    @FindBy(image = "atendimento_txt_altura_dados_cadastrai_prontuario", x=30, y=5)
    private SikuliElement txtAlta;

    @FindBy(image = "atendimento_txt_peso_dados_cadastrai_prontuario", x=30, y=5)
    private SikuliElement txtPeso;
    
    @FindBy(image = "atendimento_txt_e-mail_dados_cadastrais", x=30, y=5)
    private SikuliElement txtEmail;

    @FindBy(image = "atendimento_txt_Grupo_sanguineo_dados _cadastrais_prontuario", x=50, y=5)
    private SikuliElement txtGrupoSanguinio;

    @FindBy(image = "atendimento_txt_nome_da_mae_dados _cadastrais_prontuario", x=30, y=5)
    private SikuliElement txtNomeDaMae;

    @FindBy(image = "atendimento_txt_Celular_dados_cadastrai_prontuario", x=30, y=5)
    private SikuliElement txtCelular;
    
    @FindBy(image = "atendimento_txt_RG_dados_cadastrai_prontuario", x=30, y=5)
    private SikuliElement txtRG;



    public void informarTxtPeso (String peso ){
        Utils.sleep(500);
        txtPeso.exists(2);
        txtPeso.click();
        Assert.assertTrue(txtPeso.exists());
        txtPeso.type(peso);

    }


    public void informarTxtRG (String rG ){
        Utils.sleep(500);
        txtRG.exists(2);
        Assert.assertTrue(txtRG.exists());
        txtRG.type(rG);
    }

    public void informarTxtCelular (String celular ){
        Utils.sleep(500);
        txtCelular.exists(2);
        Assert.assertTrue(txtCelular.exists());
        txtCelular.type(celular+ Key.TAB);
    }

    public void informarTxtObservacao (String observacao ){
        Utils.sleep(500);
        txtObservacao.exists(2);
        Assert.assertTrue(txtObservacao.exists());
        txtObservacao.type(observacao);
    }

    public void informarTxtNumeroEndereco (String numeroEndereco ){
        Utils.sleep(500);
        txtNumeroEndereco.exists(2);
        Assert.assertTrue(txtNumeroEndereco.exists());
        txtNumeroEndereco.type(numeroEndereco);
    }

    public void informarTxtBairro (String bairro ){
        Utils.sleep(500);
        txtBairro.exists(2);
        Assert.assertTrue(txtBairro.exists());
        txtBairro.type(bairro);
    }


    public void informarTxtCidade (String cidade ){
        Utils.sleep(500);
        txtCidade.exists(2);
        Assert.assertTrue(txtCidade.exists());
        txtCidade.type(cidade);
    }

    public void informarTxtMedico (String medico ){
        Utils.sleep(500);
        txtMedico.exists(2);
        Assert.assertTrue(txtMedico.exists());
        txtMedico.type(medico+ Key.TAB);
    }

    public void informarTxtCEP (String cEP ){
        Utils.sleep(500);
        txtCEP.exists(2);
        Assert.assertTrue(txtCEP.exists());
        txtCEP.type(cEP+ Key.TAB);
    }


    public void informarTxtEstadoCivil (String estadoCivil ){
        Utils.sleep(500);
        txtEstadoCivil.exists(2);
        Assert.assertTrue(txtEstadoCivil.exists());
        txtEstadoCivil.type(estadoCivil+ Key.TAB);
    }


    public void informarTxtAlta (String alta ){
        Utils.sleep(500);
        txtAlta.exists(2);
        Assert.assertTrue(txtAlta.exists());
        txtAlta.type(alta+ Key.TAB);
    }


    public void informarTxtEmail (String email ){
        Utils.sleep(500);
        txtEmail.exists(2);
        Assert.assertTrue(txtEmail.exists());
        txtEmail.type(email+ Key.TAB);
    }

    public void informarTxtGrupoSanguinio (String grupoSanguinio ){
        Utils.sleep(500);
        txtGrupoSanguinio.exists(2);
        Assert.assertTrue(txtGrupoSanguinio.exists());
        txtGrupoSanguinio.type(grupoSanguinio+ Key.TAB);
    }

    public void informarTxtNomeDaMae (String nomeDaMae ){
        Utils.sleep(500);
        txtNomeDaMae.exists(2);
        Assert.assertTrue(txtNomeDaMae.exists());
        txtNomeDaMae.type(nomeDaMae+ Key.TAB);
    }



    public void confirmarQueOpronuarioNaoFoiGerado(){
        Utils.sleep(500);
        lblProntuario.exists(6);
        Assert.assertTrue(lblProntuario.exists());
    }




    public void confirmarQueOpronuarioFoiGerado(){
        Utils.sleep(2000);
       // float similaridade= (lblJaPossuiNumeroDoProntuario.getSimilarity0to100());
        Assert.assertTrue(lblJaPossuiNumeroDoProntuario.exists());
    }


    public void clickBtnProntuario() {
        Utils.sleep(500);
        btnProntuario.exists(6);
        Assert.assertTrue(btnProntuario.exists());
        btnProntuario.click();
    }
}
