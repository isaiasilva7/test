package com.pixeon.smart.pages.atende.laboratorio.laboratorio_Devolucao;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLaboratorioDevolucao extends BasePage {



    @FindBy(image = "atendimento_btn_Ok_Devolver", x=2, y=5)
    private SikuliElement btnOK;


    @FindBy(image = "atendiemnto_btn_ok_repsoanvel_devovler", x=2, y=5)
    private SikuliElement btnOKReponsavel;


    @FindBy(image = "atendimento_txt_reponsavel_devolver", x=2, y=5)
    private SikuliElement txtReponsavel;


    public void clickBtnOK(){
        Utils.sleep(2000);
        Assert.assertTrue(btnOK.exists(6));
        if (btnOK.exists(10)){
            Assert.assertTrue(btnOK.exists());
            btnOK.click();
        }else{
            Assert.assertTrue(false);
        }

        Utils.sleep(3000);
    }

    public void clickBtnOKReponsavel(){
        Utils.sleep(2000);
        btnOKReponsavel.exists();
        Assert.assertTrue(btnOKReponsavel.exists(1));
        btnOKReponsavel.click();
        Utils.sleep(1000);
    }

    public void informaReponsavelDevolucao(String responsavelDevolucao){
        Utils.sleep(2000);
        txtReponsavel.exists();
        Assert.assertTrue(txtReponsavel.exists(1));
        txtReponsavel.type(responsavelDevolucao);
        Utils.sleep(1000);
    }
}
