package com.pixeon.smart.pages.atende.atendimento.atendimento_Registro_de_Ocorrencia;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageRegistroDeOcorrencia extends BasePage {

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_cbo_Cliente", x = 3, y = 5)
    private SikuliElement cboCliente;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_CBO_prioridade", x = 3, y = 5)
    private SikuliElement cboPrioridade;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_btn_OK", x = 5, y = 5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_btn_anonimo", x = 3, y = 5)
    private SikuliElement btnAnonimo;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_descricao", x = 15, y = 30)
    private SikuliElement txtDescricao;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_OS_Numero", x = 50, y = 5)
    private SikuliElement txtOsNumero;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_OS_Numero", x = 20, y = 5)
    private SikuliElement txtOsSerie;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_natureza", x = 15, y = 5)
    private SikuliElement txtNatureza;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_Grau", x = 15, y = 5)
    private SikuliElement txtGrau;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_doc", x = 15, y = 5)
    private SikuliElement txtDoc;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_tipo", x = 30, y = 10)
    private SikuliElement txtTipo;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_unidade", x = 15, y = 5)
    private SikuliElement txtUnidade;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_Origem", x = 40, y = 5)
    private SikuliElement txtOrigem;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_Setor", x = 30, y = 10)
    private SikuliElement txtSetor;

    @FindBy(image = "atendimento_Registro_de_Ocorrencia_txt_categoria", x = 40, y = 5)
    private SikuliElement txtcategoria;



    public void clickBtnOK() {
        btnOK.exists();
        Assert.assertTrue(btnOK.exists());
        btnOK.click();

    }

    public void clickCboCliente() {
        cboCliente.exists();
        Assert.assertTrue(cboCliente.exists());
        cboCliente.click();
    }

    public void clickCboPrioridade() {
        cboCliente.exists();
        Assert.assertTrue(cboCliente.exists());
        cboPrioridade.click();
    }

    public void clickBtnAnonimo() {
        btnAnonimo.exists(5);
        Assert.assertTrue(btnAnonimo.exists());
        btnAnonimo.click();
    }

    public void informarTxtDescricao (String descricacao ){
        Utils.sleep(1000);
        txtDescricao.exists(2);
        txtDescricao.click();
        Assert.assertTrue(txtDescricao.exists());
        txtDescricao.click();
        txtDescricao.click();
        txtDescricao.click();
        txtDescricao.type(descricacao);

    }

    public void informarTxtosNumero (String osNumero ){
        Utils.sleep(500);
        txtOsNumero.exists(2);
        txtOsNumero.click();
        txtOsNumero.click();
        txtOsNumero.click();
        txtOsNumero.click();
        Assert.assertTrue(txtOsNumero.exists());
        txtOsNumero.type(osNumero);
    }

    public void informarTxtOsSerie (String osSerie ){
        Utils.sleep(500);
        txtOsSerie.exists(2);
        txtOsSerie.click();
        txtOsSerie.click();
        txtOsSerie.click();
        Assert.assertTrue(txtOsSerie.exists());
        txtOsSerie.type(osSerie);
    }

    public void informarTxtnatureza (String natureza ){
        Utils.sleep(500);
        txtNatureza.exists(2);
        txtNatureza.click();
        txtNatureza.click();
        txtNatureza.click();
        txtNatureza.click();
        Assert.assertTrue(txtNatureza.exists());
        txtNatureza.type(natureza);
    }

    public void informarTxtGrau (String grau ){
        Utils.sleep(500);
        txtGrau.exists(2);
        txtGrau.click();
        txtGrau.click();
        txtGrau.click();
        Assert.assertTrue(txtGrau.exists());
        txtGrau.type(grau);
    }

    public void informarTxtDoc (String doc ){
        Utils.sleep(500);
        txtDoc.exists(2);
        txtDoc.doubleClick();
        txtDoc.type( Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE);
        txtOrigem.doubleClick();
        txtOrigem.type( Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE);
        Assert.assertTrue(txtDoc.exists());
        txtDoc.type(doc);
    }

    public void informarTxtTipo (String tipo ){
        Utils.sleep(500);
        txtTipo.exists(2);
        txtTipo.click();
        txtTipo.click();
        txtTipo.click();
        txtTipo.click();
        Assert.assertTrue(txtTipo.exists());
        txtTipo.type(tipo+Key.TAB);
    }

    public void informarTxUnidade (String txUnidade ){
        Utils.sleep(500);
        txtUnidade.exists(2);
        txtUnidade.click();
        Assert.assertTrue(txtUnidade.exists());
        txtUnidade.type(txUnidade);
    }

    public void informarTxorigem (String origem ){
        Utils.sleep(500);
        txtOrigem.exists(2);
        txtOrigem.doubleClick();
        txtOrigem.type( Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE+Key.DELETE);
        txtOrigem.doubleClick();
        txtOrigem.type( Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE);
        Assert.assertTrue(txtOrigem.exists());
        txtOrigem.type(origem+ Key.DELETE+Key.BACKSPACE);
    }

    public void informarTxSetor (String Setor ){

        if (txtSetor.exists(3)){
            txtSetor.click();
            txtSetor.click();
            txtSetor.click();
            Assert.assertTrue(txtSetor.exists());
            txtSetor.type(Setor);
        }else {
            Utils.sleep(800);
            txtSetor.exists(2);
            txtSetor.click();
            txtSetor.click();
            txtSetor.click();
            Assert.assertTrue(txtSetor.exists());
            txtSetor.type(Setor);
        }



    }


    public void informarTxCategoria (String categoria ){
        Utils.sleep(500);
        txtcategoria.exists(2);
        txtcategoria.click();
        Assert.assertTrue(txtcategoria.exists());
        txtcategoria.type(categoria);
    }


}
