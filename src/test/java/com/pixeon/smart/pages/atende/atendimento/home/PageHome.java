package com.pixeon.smart.pages.atende.atendimento.home;

import com.pixeon.smart.pages.BasePage;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

public class PageHome  extends BasePage {

    @FindBy(image = "atende_home_menu_Arquivo", x=20, y=5)
    private SikuliElement menu_Arquivo;

    @FindBy(image = "atende_home_menu_Abrir", x=20, y=5)
    private SikuliElement menu_Abrir;

    @FindBy(image = "atende_home_menu_atendiemnto", x=20, y=5)
    private SikuliElement menu_atendimento;

    @FindBy(image = "atende_home_menu_laboratorio", x=20, y=5)
    private SikuliElement menu_laboratorio;

    public void selecionarMenu(String menu) {
        try{

            String frase = menu;
            String array[] = new String[frase.split(";").length];
            array = frase.split(";");

            for (int i = 0; i < array.length; i++) {
              //System.out.println(array[i]);
                switch( array[i] ) {
                    case "Arquivo":
                        menu_Arquivo.exists();
                        menu_Arquivo.doubleClick();
                        break;
                    case "Abrir":
                        menu_Abrir.exists();
                        menu_Abrir.doubleClick();
                        break;
                    case "Atendimento":
                        menu_atendimento.exists();
                        menu_atendimento.doubleClick();
                        break;
                    case "Laboratório":
                        menu_laboratorio.exists();
                        menu_laboratorio.doubleClick();
                        break;
                    case "Laboratorio":
                        menu_laboratorio.exists();
                        menu_laboratorio.doubleClick();
                        break;
                    default:
                        System.out.printf("Você digitou uma operação inválida.");
                }
            }
        }catch(Exception e){
            System.out.println("Erro ao espera a imagem." +e);
        }
    }
}




