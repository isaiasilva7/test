package com.pixeon.smart.pages.atende.common_To_All;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Assert;
import org.sikuli.script.App;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class PageCommonToAll extends BasePage {

    @FindBy(image = "atende_btnAcesso")
    private SikuliElement btnAcesso;

    @FindBy(image = "atende_btnAvancar")
    private SikuliElement btnAvancar;

    @FindBy(image = "atende_btnBuscar")
    private SikuliElement btnBuscar;

    @FindBy(image = "atende_btnBuscar")
    private SikuliElement btnBuscar1;

    @FindBy(image = "atende_btnCancelar")
    private SikuliElement btnCancelar;

    @FindBy(image = "atende_btnDocumentoOnline")
    private SikuliElement btnDocumentoOnline;

    @FindBy(image = "atende_btnExcluir")
    private SikuliElement btnExcluir;

    @FindBy(image = "atende_btnFechar")
    private SikuliElement btnFechar;

    @FindBy(image = "atende_btnGravar")
    private SikuliElement btnGravar;

    @FindBy(image = "atende_btnImprimir")
    private SikuliElement btnImprimir;

    @FindBy(image = "atende_btnNovo")
    private SikuliElement btnNovo;

    @FindBy(image = "Atende_btnRetornar")
    private SikuliElement btnRetornar;

    @FindBy(image = "atende_btnZoom")
    private SikuliElement btnZoom;

    @FindBy(image = "atende_atendimento_mensagem_tratamento_gravado_com_sucesso")
    private SikuliElement lblMensagem;


    public void validarMensagem(){
        Utils.sleep(100);

      if (lblMensagem.exists(8)){
          Assert.assertTrue(true);
      }else{
          Assert.assertTrue(false);
      }



    }

    public void clickBtnFechar(){
        btnFechar.click();
    }

    public void clickBnNovo(){
        btnNovo.click();
    }


    public void clickbtnBuscar(){
        Utils.sleep(200);
        if (btnBuscar.exists()) {
            Assert.assertTrue(btnBuscar.exists());
            btnBuscar.click();
        }else if (btnBuscar1.exists()){
            Assert.assertTrue(btnBuscar1.exists());
            btnBuscar1.click();
        }else{
            Assert.assertTrue(false);
        }

    }

    public void clickbtnGravar(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(2000);
      if ( btnGravar.exists(6)){
          btnGravar.click();
       //   Assert.assertTrue(btnGravar.exists());
      }else{
          Assert.assertTrue(false);
      }
    }


    public void clickBtnExcluir()
    {
        Utils.sleep(100);
        Assert.assertTrue(btnExcluir.exists());
        btnExcluir.click();
    }

    public void clickbtnCancelar(){
        Utils.sleep(100);
        btnCancelar.exists();
        btnCancelar.click();
    }

    public void clickBtnImpirmir(){
        Utils.sleep(1500);
          if (btnImprimir.exists(3)){
              Assert.assertTrue(btnImprimir.exists());
              Utils.sleep(100);
              btnImprimir.click();
          }else{
              Assert.assertTrue(false);
          }
    }

    public void clickBtnRetornar(){
        Utils.sleep(1500);
      if (btnRetornar.exists(2)){
        Assert.assertTrue(btnRetornar.exists());
        btnRetornar.click();
      }else {
          Assert.assertTrue(btnRetornar.exists());
      }
        Utils.sleep(500);
    }


    public void clickbtnAvancar(){
        btnAvancar.exists(2);
        Assert.assertTrue(btnAvancar.exists());
        btnAvancar.click();
        Utils.sleep(100);
    }

    public void clickbtnAvancar(String quantidadeDeVezes){
          try {
                int quantidade=Integer.valueOf(quantidadeDeVezes);
                if (quantidade<0 ){
                    quantidade=1;
                }else if (quantidade==0 ) {
                    quantidade = 1;
                }
                for (int i = 0; i <quantidade; i++){
                    Utils.sleep(2000);
                    btnAvancar.exists(4);
                    btnAvancar.click();
                }
          }catch (Exception e){
              System.out.println(e);
              Assert.assertTrue(false);
          }
    }


    public void clickBtnZoom(){
        Utils.sleep(300);
        if ( btnZoom.exists(5)){
            Assert.assertTrue(btnZoom.exists());
            btnZoom.click();
            Utils.sleep(400);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickbtnAcesso()
    {   Assert.assertTrue(btnAcesso.exists());
        btnAcesso.click();
    }

    public void moduloAtende(String nomeDoModulo)
    {
        try{
            Process p = Runtime.getRuntime().exec("taskkill /F /IM  "+nomeDoModulo+".exe");
            Utils.sleep(6000);
        }catch(Exception e){
            System.out.println(e);
            Assert.assertFalse("Modulo não encontrado", true);
        }
    }

    public static void abrirModulo(String nomeDoModulo)
    {
        try{
            Utils.sleep(300);
            Process w = Runtime.getRuntime().exec("taskkill /F /IM  dllhost.exe");
            Process j = Runtime.getRuntime().exec("taskkill /F /IM  PrintIsolationHost.exe");
            Process p = Runtime.getRuntime().exec("taskkill /F /IM  "+nomeDoModulo+".exe");

            Process z = Runtime.getRuntime().exec("taskkill /F /IM  smart_util.exe");
            Utils.sleep(2000);
           // Process Z  = Runtime.getRuntime().exec("taskkill /F /IM  "+nomeDoModulo+".exe");
          //  Utils.sleep(500);
            App.open("C:/Smart/APLIC60/"+nomeDoModulo+".exe");
            Utils.sleep(300);
        }catch(Exception e){
            System.out.println(e);
            Assert.assertFalse("Modulo não encontrado", true);
        }
    }

    public void comandoESCTeclado(){
        Utils.sleep(4000);
        try {
            Robot robot1=new Robot();
            robot1.keyPress(KeyEvent.VK_ESCAPE);
            Utils.sleep(3000);
        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -: " + e.getMessage());
            Assert.assertTrue(false);
        }
    }

    /**
   //  * extraiTextoDoPDF
  //   * @param caminho
   //  * @return
   //  */

 public String extraiTextoDoPDF(String caminho) {

        File file = new File(caminho);
        String todoConteudoDoPdf = null;
        String parsedText="";
        try {
         
        	File pdfFile = new File(caminho);
        	PDDocument pdDoc = PDDocument.load(pdfFile);
        	PDFTextStripper pdfStripper = new PDFTextStripper();
        	parsedText = pdfStripper.getText(pdDoc);
        	pdDoc.close();
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertFalse("Erro no objeto" ,true);
        }
        return parsedText;

    }

    /**
     *  leArquivoCSVeTXT
     * @param arquivo
     * @return
     */
    public  String leArquivoCSVeTXT(String arquivo)  {
        StringBuilder bufSaida = new StringBuilder();
        try {
            File file = new File(arquivo);
            if (! file.exists()) {
                return null;
            }
            BufferedReader br = new BufferedReader(new FileReader(arquivo));
            String linha;
            while( (linha = br.readLine()) != null ){
                bufSaida.append(linha).append("\n");
            }
            br.close();
        } catch (Exception e) {
        	  Assert.assertFalse("Erro no objeto" ,true);

        }
        return bufSaida.toString();
  }

    /**
     *experarObjetoExistir
     * @param objeto
     */

  public void experarObjetoExistir(SikuliElement objeto, SikuliElement objeto1)  {
        try {
            for (int i = 0; i <31; i++) {
                if  (objeto.exists()){
                    break;
                }else {
                    Utils.sleep(2000);
                    if (i==20){
                        objeto1.click();
                    }
                    if (i==6){
                        objeto.click();
                    }
                }
            }
        } catch (Exception e) {
           Assert.assertFalse("Erro no objeto",true);
        }
    }

    public  String pegarTextoNoArquivoXML(String url,String tag, String paramentro){
        String retorno=null;
        try {
            File inputFile = new File(url);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;
            dBuilder = dbFactory.newDocumentBuilder();
            Document documento = dBuilder.parse(inputFile);
            documento.getDocumentElement().normalize();
          //  System.out.println("Root element :" + documento.getDocumentElement().getNodeName());
            NodeList nList = documento.getElementsByTagName(tag);
            for (int temp = 0; temp < nList.getLength(); temp++) {
                 Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    retorno= eElement.getAttribute(paramentro);
                }
            }
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.assertTrue("Erro no arquivo",false);
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            Assert.assertTrue("Erro no arquivo",false);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.assertTrue("Erro no arquivo",false);
        }
        return retorno;
    }



}
