package com.pixeon.smart.pages.atende.selecionar_nome_do_arquivo;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

import org.junit.Assert;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

public class PageSelecionarNomeDoAquivo extends BasePage {

    @FindBy(image = "atendimento_txt_nome_do_arquivo_selecionar_o_nome_do_arquivo", x=5, y=5)
    private SikuliElement txtNomeDoArquivo;

    @FindBy(image = "atendimento_txt_nome_do_arquivo_selecionar_o_nome_do_arquivo", x=10, y=5)
    private SikuliElement txtNomeDoArquivo2;

    @FindBy(image = "atendimento_txt_nome1_do_arquivo_selecionar_o_nome_do_arquivo", x=5, y=5)
    private SikuliElement txtNomeDoArquivo1;

    @FindBy(image = "atendimento_txt_nome3_do_arquivo_selecionar_o_nome_do_arquivo",  y=-5)
    private SikuliElement txtNomeDoArquivo3;

    @FindBy(image = "atendimento_nome_do_arquivo_selecionar_nome_do_arquivo_txt_nome4", x=60, y=5)
    private SikuliElement txtNomeDoArquivo4;

    @FindBy(image = "atendimento_btn_Salvar_Selecionar_o_nome_de_arquivo", x=5, y=5)
    private SikuliElement btnSalvar;

    @FindBy(image = "atendimento_btn_Abrir_Selecionar_o_nome_de_arquivo", x=5, y=5)
    private SikuliElement btnAbrir;

    @FindBy(image = "atendimento_btn_Abrir1_Selecionar_o_nome_de_arquivo", x=5, y=5)
    private SikuliElement btnAbrir1;

    @FindBy(image = "atendimento_btn_OK_Selecionar_o_formato_do_arquivo", x=5, y=5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_link_txt_selecionar_o_formato", x=5, y=5)
    private SikuliElement linkFormatoTxt;

    @FindBy(image = "atendimento_link_xls_selecinar_o_formato", x=5, y=5)
    private SikuliElement linkFormatoXls;

    @FindBy(image = "atendimento_link_html_selecionar_o_formato", x=5, y=5)
    private SikuliElement  linkFormatohtml;

    @FindBy(image = "atendimento_link_csv_selecionar_o_formato", x=5, y=5)
    private SikuliElement  linkFormatoCsv;

    @FindBy(image = "atendimento_link_PDF_selecionar_o_formato", x=5, y=5)
    private SikuliElement  linkFormatoPDF;

    @FindBy(image = "atendimento_link_psr_selecionar_o_formato", x=5, y=5)
    private SikuliElement  linkFormatoPsr;


    @FindBy(image = "atendimento_btn_sim_confirmar_subistituicao_do_arquivo", x=5, y=5)
    private SikuliElement  btnSimSubtituitAquivo;
   
    public boolean existBtntnSimSubtituitAquivo(){ 
        return btnSimSubtituitAquivo.exists();
    }
    
    public void clicarBtntnSimSubtituitAquivo(){
        btnSimSubtituitAquivo.click();
    }


    public void informarTxtNomeDoArquivo(String nomeDoArquivo){
        Utils.sleep(1500);
       if (txtNomeDoArquivo4.exists(8)) {
          // Assert.assertTrue(txtNomeDoArquivo1.exists(5));
          // txtNomeDoArquivo1.type(nomeDoArquivo);
           txtNomeDoArquivo4.type(nomeDoArquivo);

       }else if  ((txtNomeDoArquivo.exists(2))){
           // Assert.assertTrue(txtNomeDoArquivo.exists());
           txtNomeDoArquivo.click();
           txtNomeDoArquivo.type(nomeDoArquivo);
        } else if  ((txtNomeDoArquivo2.exists(2))) {
           //Assert.assertTrue(txtNomeDoArquivo2.exists(5));
           txtNomeDoArquivo2.click();
           txtNomeDoArquivo2.type(nomeDoArquivo);
       }else if  ((txtNomeDoArquivo3.exists(5))) {
          /// Assert.assertTrue(txtNomeDoArquivo3.exists(5));
           txtNomeDoArquivo3.click();
           txtNomeDoArquivo3.type(nomeDoArquivo);

       }else{
           Assert.assertTrue(false);


       }
    }

    public void clicarBtnSalvar(){
        Assert.assertTrue(btnSalvar.exists(5));
        btnSalvar.click();
    }

    public void clicarBtnOK(){
        Assert.assertTrue(btnOK.exists(5));
        btnOK.click();
    }


    public void clicarBtnAbrir(){
        if (btnAbrir.exists(5)) {
            Assert.assertTrue(btnAbrir.exists(5));
            btnAbrir.click();
        }else{
            Assert.assertTrue(btnAbrir1.exists(5));
            btnAbrir1.click();
        }

    }

    public void clicarLinkFormatoTxt(){
        Assert.assertTrue(linkFormatoTxt.exists(5));
        linkFormatoTxt.click();
    }


    public void clicarLinkFormatoXls(){
        Assert.assertTrue(linkFormatoXls.exists(5));
        linkFormatoXls.click();
    }

    public void clicarLinkFormatoHtml(){
        Assert.assertTrue(linkFormatohtml.exists(5));
        linkFormatohtml.click();
    }


    public void clicarLinkFormatoCsv(){
        Assert.assertTrue(linkFormatoCsv.exists(5));
        linkFormatoCsv.click();
    }


    public void clicarLinkFormatoPDF(){
        Assert.assertTrue(linkFormatoPDF.exists(5));
        linkFormatoPDF.click();
    }

    public void clicarLinkFormatoPsr(){
        Assert.assertTrue(linkFormatoPsr.exists(5));
        linkFormatoPsr.click();
    }




}
