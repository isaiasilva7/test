package com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.pages.atende.common_To_All.PageCommonToAll;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageCadastroDadosPaciente extends BasePage {

    @FindBy(image = "atendimento_btn_cadastror_biometria", x=5, y=5)
    private SikuliElement btnBiometria;

    @FindBy(image = "attendimento_btn_foto_cadastro", x=0, y=5)
    private SikuliElement btnFoto;

    @FindBy(image = "atendimento_btn_arquivo_cadastro", x=3, y=5)
    private SikuliElement btnArquivo;

    @FindBy(image = "atendimento_cadastro_btn_prontuario", x=2, y=5)
    private SikuliElement btnProntuario;

    @FindBy(image = "atendimento_cadastro_btn_Questionario", x=10, y=5)
    private SikuliElement btnQuestionario;

    @FindBy(image = "atendimento_btn_guia1_cadastro", x=1, y=5)
    private SikuliElement btnGuia1;

    @FindBy(image = "atendimento_btn_Conv_secundario_cadastro", x=10, y=5)
    private SikuliElement btnConvenioSecundario;

    @FindBy(image = "atendimento_btn_acompanhante_cadastro", x=5, y=5)
    private SikuliElement btnCadastroAcompanhante;

    @FindBy(image = "atendimento_btn_Dados_complementares_cadastro", x=10, y=2)
    private SikuliElement btnDadosComplementares;

    @FindBy(image = "atendimento_btn_Dados_complementares_1_cadastro", x=-20, y=5)
    private SikuliElement btnDadosComplementares1;

    @FindBy(image = "atendimento_btn_Tratamento_ambulatorial_cadastro", x=5, y=5)
    private SikuliElement btnTratamentoAmbulatorial;

    @FindBy(image = "atendimento_btn_Guia_cadastro", x=5, y=5)
    private SikuliElement btnGuia;

    @FindBy(image = "atendimento_btn_fila_de_espera_cadastro", x=5, y=5)
    private SikuliElement btnFilaDeEspera;

    @FindBy(image = "atendimento_btn_adicionar_na_fila_cadastro", x=5, y=5)
    private SikuliElement btnAdiconarNaFila;

    @FindBy(image = "atende_LBL_questionario", x=20, y=5)
    private SikuliElement lblQuestinario;

    @FindBy(image = "atendiemtno_txt_convenio_secundaria_do_paciente_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtConvenioSecuncario;

    @FindBy(image = "atendimento_Txt_apelido_1_dados_complementares", x=60, y=5)
    private SikuliElement txtApelidoAbaDadosComplmentares;


    public void clickBtnBiometria(){
        Utils.sleep(500);
        btnBiometria.exists(6);
        Assert.assertTrue(btnBiometria.exists());
        btnBiometria.click();
    }

    public void clickBtnFoto(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnFoto.exists(6);
        Assert.assertTrue(btnFoto.exists());
        btnFoto.click();
    }

    public void clickBtnArquivo(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnArquivo.exists(6);
        Assert.assertTrue(btnArquivo.exists());
        btnArquivo.click();
    }

    public void clickBtnProntuario(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnArquivo.exists(6);
        Assert.assertTrue(btnArquivo.exists());
        btnArquivo.click();
    }

    public void clickbtnQuestionario(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnQuestionario.exists(6);
        Assert.assertTrue(btnQuestionario.exists());
        btnQuestionario.click();

        for (int i = 0; i <31; i++) {
           if  (lblQuestinario.exists()){
               break;
           }else {
               Utils.sleep(2000);
               if (i==20){
                   btnQuestionario.click();
               }
               if (i==6){
                   btnQuestionario.click();
               }
           }

        }
    }

    public void clickbtnGuia1(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnGuia1.exists(6);
        Assert.assertTrue(btnGuia1.exists());
        btnGuia1.click();
    }

    public void clickBtnConvenioSecundario(){
        Utils.sleep(500);
        btnConvenioSecundario.exists(6);
        Assert.assertTrue(btnConvenioSecundario.exists());
        btnConvenioSecundario.click();
        for (int i = 0; i <31; i++) {
            if  (txtConvenioSecuncario.exists()){
                break;
            }else {
                Utils.sleep(2000);
                if (i==10){
                    btnConvenioSecundario.click();
                }
                if (i==15){
                    btnConvenioSecundario.click();
                }
                if (i==20){
                    btnConvenioSecundario.click();
                }
                if (i==6){
                    btnConvenioSecundario.click();
                }
            }

        }

    }

    public void clickBtnCadastroAcompanhante(){
        Utils.sleep(500);
        btnCadastroAcompanhante.exists(6);
        Assert.assertTrue(btnCadastroAcompanhante.exists());
        btnCadastroAcompanhante.click();
        Utils.sleep(500);
        btnCadastroAcompanhante.click();
        Utils.sleep(500);
        btnCadastroAcompanhante.click();
    }

    public void clickBtnDadosComplementares(){
      //  SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnDadosComplementares.exists(6);
        Assert.assertTrue(btnDadosComplementares.exists());
        btnDadosComplementares1.click();
        for (int i = 0; i <31; i++) {
            if  (txtApelidoAbaDadosComplmentares.exists()){
                break;
            }else {
                Utils.sleep(2000);
                if (i==10){
                    btnDadosComplementares.click();
                }
                if (i==15){
                    btnDadosComplementares.click();
                }
                if (i==20){
                    btnDadosComplementares.click();
                }
                if (i==6){
                    btnDadosComplementares.click();
                }
            }
        }
    }

    public void clickbtnTratamentoAmbulatorial(){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\common_To_All\\atende_btnGravar.png", 5);
        Utils.sleep(500);
        btnTratamentoAmbulatorial.exists(6);
        Assert.assertTrue(btnTratamentoAmbulatorial.exists());
        btnTratamentoAmbulatorial.click();
        Utils.sleep(100);
    }

    public void clickBtnGuia(){
        Utils.sleep(500);
        btnGuia.exists(6);
        Assert.assertTrue(btnGuia.exists());
        btnGuia.click();
    }

    public void clickBtnAdiconarNaFila(){
        Utils.sleep(500);
        btnAdiconarNaFila.exists(6);
        Assert.assertTrue(btnAdiconarNaFila.exists());
        btnAdiconarNaFila.click();
    }

    public void clickBtnFilaDeEspera(){
        Utils.sleep(500);
        btnFilaDeEspera.exists(6);
        Assert.assertTrue(btnFilaDeEspera.exists());
        btnFilaDeEspera.click();
    }

}
