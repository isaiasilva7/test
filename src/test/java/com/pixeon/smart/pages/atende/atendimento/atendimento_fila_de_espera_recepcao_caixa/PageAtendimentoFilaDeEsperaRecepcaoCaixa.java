package com.pixeon.smart.pages.atende.atendimento.atendimento_fila_de_espera_recepcao_caixa;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

import org.junit.Assert;
import org.sikuli.script.Key;

public class PageAtendimentoFilaDeEsperaRecepcaoCaixa extends BasePage {


    @FindBy(image = "atende_atendimento_btn_chama_Proximo_Fila_de_espera", x=10, y=5)
    private SikuliElement btnChamaProximo;

    @FindBy(image = "atende_atendimento_btn_chama_Selecionado_Fila_de_espera", x=10, y=5)
    private SikuliElement btnChamaSelecionado;

    @FindBy(image = "atende_atendimento_btn_Encerrar_Atendiemtno_Fila_de_espera", x=10, y=3)
    private SikuliElement btnEncerrarAtendiemtno;

    @FindBy(image = "atende_atendimento_txt_nome_do Paciente_Fila_de_espera", x=30, y=50)
    private SikuliElement txtNomePacienteInserido;

    @FindBy(image = "atende_atendimento_txt_nome_Fila_de_espera", x=25, y=5)
    private SikuliElement txtNome;

    @FindBy(image = "atende_atendimento_btn_Atualizar_Fila_de_espera", x=10, y=5)
    private SikuliElement btnAtualizarFila;

    @FindBy(image = "atende_atendimento_btn_CR_Fila_de_espera", x=10, y=5)
    private SikuliElement btnCR;

    @FindBy(image = "atende_atendimento_btn_Detalhe_Fila_de_espera", x=10, y=5)
    private SikuliElement btnDetalhe;

    @FindBy(image = "atende_atendimento_btn_Foto_Fila_de_espera", x=10, y=5)
    private SikuliElement btnFoto;

    @FindBy(image = "atende_atendimento_btn_Imprimir_Fila_de_espera", x=10, y=5)
    private SikuliElement btnImprimir;

    @FindBy(image = "atende_atendimento_btn_Trasferencia_Fila_de_espera", x=10, y=5)
    private SikuliElement btnTrasferencia;

    @FindBy(image = "atendimento_lb_medico_Abilio_Fila", x=10, y=5)
    private SikuliElement lblMedicoAbilio;

    @FindBy(image = "atendimento_lb_medico_Ivisson_lopes_Fila", x=10, y=5)
    private SikuliElement lblMedicoIvissonLopes;


    @FindBy(image = "atende_atendimento_btn_Historico_Fila_de_espera", x=10, y=5)
    private SikuliElement btnHistorico;

    @FindBy(image = "atende_atendimento_btn_Marcacao_Fila_de_espera", x=10, y=5)
    private SikuliElement btnMarcacao;

    @FindBy(image = "atende_atendimento_btn_Rastreabilidade_Fila_de_espera", x=10, y=5)
    private SikuliElement btnRastreabilidade;

    @FindBy(image = "atendimento_txt_Fila_medica_fila_de_espera", x = 40, y = 5)
    private SikuliElement txtFilaMedico;


    public void inserirFilaMedico(String medico) {
        Assert.assertTrue(txtFilaMedico.exists());
        txtFilaMedico.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtFilaMedico.type(Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE);
        txtFilaMedico.type(medico + Key.TAB);

    }

    public void inserirTxtNome(String nome) {
        Assert.assertTrue(txtNome.exists());
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(nome + Key.TAB);


    }

    public String  capturaraTxtNomePacienteInserido() {
        String valorcaprurado;

        //Utils utils = new Utils();
        Utils.sleep(2000);
        Assert.assertTrue(txtNomePacienteInserido.exists());
        txtNomePacienteInserido.click();
        valorcaprurado = Utils.capturadoTextoSelecionado();
        return valorcaprurado;
    }

    public void clickBtnEncerrarAtendimento() {
        Utils.sleep(200);
        Assert.assertTrue(btnEncerrarAtendiemtno.exists());
        btnEncerrarAtendiemtno.click();
    }

    public void clickBtnChamaSelecionado() {
        Utils.sleep(500);
        btnChamaSelecionado.exists();
        Assert.assertTrue(btnChamaSelecionado.exists());
        btnChamaSelecionado.click();
    }

    public void clickBtnChamaProximo() {
        Assert.assertTrue(btnChamaProximo.exists());
        btnChamaProximo.click();
    }

    public void clickBtnAtualizarFila() {
        Assert.assertTrue(btnAtualizarFila.exists());
        btnAtualizarFila.click();
    }

    public void clickBtnCR() {
        Utils.sleep(200);
        Assert.assertTrue(btnCR.exists());
        btnCR.click();
    }

    public void clickBtnDetalhe() {
        Assert.assertTrue(btnDetalhe.exists());
        btnDetalhe.click();

    }

    public void clickBtnFoto() {
        Assert.assertTrue(btnFoto.exists());
        btnFoto.click();
    }

    public void clickBtnImprimir() {
        Assert.assertTrue(btnImprimir.exists());
        btnImprimir.click();
    }

    public void clickLbMedicoAbilio() {
        Assert.assertTrue(lblMedicoAbilio.exists());
        lblMedicoAbilio.doubleClick();
    }

    public void clickBtnTrasferencia() {
        btnTrasferencia.exists(5);
        Assert.assertTrue(btnTrasferencia.exists());
        btnTrasferencia.click();
    }


    public void clickLblMedicoIvissonLopes() {
        Assert.assertTrue(lblMedicoIvissonLopes.exists());
        lblMedicoIvissonLopes.doubleClick();
    }

    public void clickBtnMarcacao() {
        Assert.assertTrue(btnMarcacao.exists());
        btnMarcacao.doubleClick();
    }

    public void clickbtnRastreabilidade() {
        Assert.assertTrue(btnRastreabilidade.exists());
        btnRastreabilidade.doubleClick();
    }

    public void clickBtnHistorico() {
        Assert.assertTrue(btnHistorico.exists());
        btnHistorico.doubleClick();
    }


}
