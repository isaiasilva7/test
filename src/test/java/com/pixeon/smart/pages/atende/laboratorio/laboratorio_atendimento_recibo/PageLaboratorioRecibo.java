package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_recibo;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioRecibo extends BasePage {

    @FindBy(image = "atendimento_aba_filtro_recibo", x = 3, y = 5)
    private SikuliElement abaFiltro;

    @FindBy(image = "atendimento_aba_opcao_recibo", x = 3, y = 5)
    private SikuliElement abaOpcoes;

    @FindBy(image = "atendimento_aba_outros_Relatorios_recibo", x = 3, y = 5)
    private SikuliElement abaOutrosRelatoios;

    @FindBy(image = "atendimento_btn__nota_fiscal_medico_recibo", x = 3, y = 5)
    private SikuliElement btnNotaFiscal;

    @FindBy(image = "atendimento_btn_cancelar_recibo", x = 3, y = 5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_btn_visualizar_recibo", x = 3, y = 5)
    private SikuliElement btnVisualizar;

    @FindBy(image = "atendimento_lbl_documento_medicware_documentos_recibo", x = 3, y = 5)
    private SikuliElement lblDocumentoMedicware;


    @FindBy(image = "atendimento_scroll_documentos_recibo",x = 2, y = 2)
    private SikuliElement btnScroolDocumento;


    public void clickAbaFiltro() {
        abaFiltro.exists();
        Assert.assertTrue(abaFiltro.exists());
        abaFiltro.click();
    }

    public void clickLblDocumentoMedicware() {
        Utils.sleep(100);
        lblDocumentoMedicware.exists(2);
        Assert.assertTrue(lblDocumentoMedicware.exists());
        lblDocumentoMedicware.click();

    }

    public void clickBtnScroolDocumento() {
        Utils.sleep(1000);
        btnScroolDocumento.exists(2);
       // Assert.assertTrue(btnScroolDocumento.exists());
        btnScroolDocumento.click();

    }


    public void clickBtnVisualizar() {
        btnVisualizar.exists();
        Assert.assertTrue(btnVisualizar.exists());
        btnVisualizar.click();

    }
}
