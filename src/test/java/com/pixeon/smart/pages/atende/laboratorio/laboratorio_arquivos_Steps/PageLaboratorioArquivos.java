package com.pixeon.smart.pages.atende.laboratorio.laboratorio_arquivos_Steps;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLaboratorioArquivos extends BasePage {

    @FindBy(image = "atende_laboratorio_arquivos_btn_anexar")
    private SikuliElement btnAnexar;

    @FindBy(image = "atende_laboratorio_arquivos_btn_grupo")
    private SikuliElement btnGrupo;

    @FindBy(image = "atende_laboratorio_arquivos_btn_dataFlow")
    private SikuliElement btnDataFlow;

    @FindBy(image = "atende_laboratorio_arquivos_btn_Enviar")
    private SikuliElement btnEnviar;

    @FindBy(image = "atende_laboratorio_arquivos_btn_Destaque")
    private SikuliElement btnDestaque;

    @FindBy(image = "atende_laboratorio_arquivos_btn_Excluir")
    private SikuliElement btnExluir;

    @FindBy(image = "atende_laboratorio_arquivos_btn_observacao")
    private SikuliElement btnObservacao;

    @FindBy(image = "atende_laboratorio_arquivos_btn_salvar_como")
    private SikuliElement btnSalvarCom;

    @FindBy(image = "atende_laboratorio_arquivos_btn_Visualizar")
    private SikuliElement btnVisualizar;

    @FindBy(image = "atende_laboratorio_arquivos_btn_Fechar")
    private SikuliElement btnFechar;


    public void clickBtnAnexar() {
        if (btnAnexar.exists(5)){
            btnAnexar.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnFechar() {
        if (btnFechar.exists(5)){
            btnFechar.click();
        }else{
            Assert.assertTrue(false);
        }
    }
}
