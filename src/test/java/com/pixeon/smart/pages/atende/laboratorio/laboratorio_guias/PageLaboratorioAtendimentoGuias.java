package com.pixeon.smart.pages.atende.laboratorio.laboratorio_guias;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioAtendimentoGuias extends BasePage {


    @FindBy(image = "atendimento_Guia_btn_Proced_APAC", x=5, y=5)
    private SikuliElement btnProcedPac;

    @FindBy(image = "atendimento_Guia_btn_Exibir_apenas_nao_vencida", x=5, y=5)
    private SikuliElement btnExibirApenasNaoVencido;

    @FindBy(image = "atendimento_Guia_btn_Gerar_OS", x=5, y=5)
    private SikuliElement btnGerarOS;

    @FindBy(image = "atendimento_Guia_txt_Medico", x=5, y=20)
    private SikuliElement txtMedico;

    @FindBy(image = "atendimento_Guia_txt_Liberacao", x=5, y=20)
    private SikuliElement txtLiberar;

    @FindBy(image = "atendimento_Guia_txt_atendente", x=5, y=20)
    private SikuliElement txtAtendente;

    @FindBy(image = "atendimento_Guia_txt_Convenio", x=5, y=20)
    private SikuliElement txtConvenio;

    @FindBy(image = "atendimento_Guia_txt_Qt_Aut", x=5, y=20)
    private SikuliElement txtQtAut;

    @FindBy(image = "atendimento_Guia_txt_quantidade_sol", x=5, y=20)
    private SikuliElement txtQuatidadeSol;

    @FindBy(image = "atendimento_Guia_txt_Setor_executante", x=5, y=20)
    private SikuliElement txtSetorExecutante;

    @FindBy(image = "atendimento_Guia_txt_Senha", x=5, y=20)
    private SikuliElement txtSenha;

    @FindBy(image = "atendimento_Guia_txt_Data_inicio", x=5, y=20)
    private SikuliElement txtDataInicio;

    @FindBy(image = "atendimento_Guia_txt_Data_Fim", x=5, y=20)
    private SikuliElement txtDataFim;

    @FindBy(image = "atendimento_Guia_txt_Num_D_Dias", x=5, y=20)
    private SikuliElement txtNumeroDDias;

    @FindBy(image = "atendimento_Guia_txt_procedimento", x=5, y=20)
    private SikuliElement txtProcedimento;

    @FindBy(image = "atendimento_Guia_txt_numero_da_guia", x=5, y=20)
    private SikuliElement txtnumeroDaGuia;


    public void clickBtnProcedPac(){
        btnProcedPac.exists();
        Assert.assertTrue(btnProcedPac.exists());
        btnProcedPac.click();
    }

    public void clickBtnxibirApenasNaoVencido(){
        Utils.sleep(800);
        btnExibirApenasNaoVencido.exists();
        Assert.assertTrue(btnExibirApenasNaoVencido.exists());
        btnExibirApenasNaoVencido.doubleClick();
    }

    public void clickBtnGerarOS(){
        btnGerarOS.exists();
        Assert.assertTrue(btnGerarOS.exists());
        btnGerarOS.click();
    }

    public void informarTxtMedico(String medico){
        Utils.sleep(500);
        txtMedico.exists(6);
        Assert.assertTrue(txtMedico.exists());
        txtMedico.click();
        txtMedico.type(medico);
    }

    public void informarTxtLiberar(String liberar){
        Utils.sleep(500);
        txtLiberar.exists(6);
        Assert.assertTrue(txtLiberar.exists());
        txtLiberar.click();
        txtLiberar.type(liberar);
    }

    public void informarTxtAtendente(String atendente){
        Utils.sleep(500);
        txtAtendente.exists(6);
        Assert.assertTrue(txtAtendente.exists());
        txtAtendente.click();
        txtAtendente.type(atendente);
    }

    public void informarTxtConvenio(String convenio){
        Utils.sleep(500);
        txtConvenio.exists(6);
        Assert.assertTrue(txtConvenio.exists());
        txtConvenio.click();
        txtConvenio.type(Key.BACKSPACE+Key.BACKSPACE+ Key.BACKSPACE+Key.DELETE+Key.DELETE+ Key.DELETE+Key.DELETE+ Key.DELETE+convenio);
    }

    public void informarTxtQtAut(String qtAut){
        Utils.sleep(500);
        txtQtAut.exists(6);
        Assert.assertTrue(txtQtAut.exists());
        txtQtAut.click();
        txtQtAut.type(qtAut);
    }

    public void informarTxtQuatidadeSol(String quatidadeSol){
        Utils.sleep(500);
        txtQuatidadeSol.exists(6);
        Assert.assertTrue(txtQuatidadeSol.exists());
        txtQuatidadeSol.click();
        txtQuatidadeSol.type(quatidadeSol);
    }

    public void informarTxtSetorExecutante(String setorExecutante){
        Utils.sleep(500);
        txtSetorExecutante.exists(6);
        Assert.assertTrue(txtSetorExecutante.exists());
        txtSetorExecutante.click();
        txtSetorExecutante.type(setorExecutante);
    }


    public void informarTxtDataInicio(String dataInicio){
        Utils.sleep(500);
        txtSenha.exists(6);
        Assert.assertTrue(txtDataInicio.exists());
        txtDataInicio.click();
        txtDataInicio.type(dataInicio);
    }

    public void informarTxtDataFim(String dataFim){
        Utils.sleep(500);
        txtDataFim.exists(6);
        Assert.assertTrue(txtDataFim.exists());
        txtDataFim.click();
        txtDataFim.type(dataFim);
    }

    public void informarTxtNumeroDDias(String numeroDDias){
        Utils.sleep(500);
        txtNumeroDDias.exists(6);
        Assert.assertTrue(txtNumeroDDias.exists());
        txtNumeroDDias.click();
        txtNumeroDDias.type(numeroDDias);
    }

    public void informarTxtProcedimento(String numeroDDias){
        Utils.sleep(500);
        txtProcedimento.exists(6);
        Assert.assertTrue(txtProcedimento.exists());
        txtProcedimento.click();
        txtProcedimento.type(numeroDDias);
    }

    public void informarTxtSenha(String senha){
        Utils.sleep(500);
        txtSenha.exists(6);
        Assert.assertTrue(txtSenha.exists());
        txtSenha.click();
        txtSenha.type(senha);
    }


    public void informarTxtNumeroDaGuia(String numeroDaGuia){
        Utils.sleep(500);
        txtnumeroDaGuia.exists(6);
        Assert.assertTrue(txtnumeroDaGuia.exists());
        txtnumeroDaGuia.click();
        txtnumeroDaGuia.type(Key.BACKSPACE+Key.BACKSPACE+ Key.BACKSPACE+Key.DELETE+Key.DELETE+ Key.DELETE+Key.DELETE+ Key.DELETE+numeroDaGuia);
    }


}
