package com.pixeon.smart.pages.atende.atendimento.formulario;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

import java.awt.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;

import static com.pixeon.smart.util.Utils.sleep;

public class PageFormulario extends BasePage {

    @FindBy(image = "atende_btn_cancelar_formulario")
    private SikuliElement btnCancelar;

    @FindBy(image = "atende_btn_OK_formulario")
    private SikuliElement btnOK;

    public void clicarBtnCancelar() {
        boolean presente;
        presente=btnCancelar.exists(5);
        if (presente==true){
            btnCancelar.click();
            Assert.assertTrue(presente);
        }else{
            System.out.println("botão não encontrado");
            Assert.assertTrue(false);
        }
    }
    public void clicarBtnOK() {
        boolean presente;
        presente=btnOK.exists(5);
        if (presente==true){
            btnOK.click();
            Assert.assertTrue(presente);
        }else{
            System.out.println("botão não encontrado");
            Assert.assertTrue(false);
        }
    }

    public void clicarTecladoShiftEnter() {
        Robot  robot = null;
        try {
            sleep(2000);
            robot = new Robot();
            robot.setAutoWaitForIdle(true);
            robot.keyPress(KeyEvent.VK_SHIFT);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_SHIFT);
            robot.keyRelease(KeyEvent.VK_ESCAPE);
            robot.keyRelease(KeyEvent.VK_ESCAPE);

        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
}
