package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_Ordem_de_servicos_atendimentos;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioAtendimentoOrdemDeServicoAtendimentos extends BasePage {

    @FindBy(image = "atendimento_Ordem_de_servicos_atendimentos_seta", x = 3, y = 5)
    private SikuliElement lblSeta;


    public void clickLblSeta() {
        Utils.sleep(500);
        lblSeta.exists();
        lblSeta.click();
    }

}
