package com.pixeon.smart.pages.atende.laboratorio.laboratorio_Dados_do_item;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioAtendimentoDadosDoItem extends BasePage {

    @FindBy(image = "atendimento_Dados_do_item_btn_Alterar_data_do_reultado", x=4, y=5)
    private SikuliElement btnAterarDataDoResultado;

    @FindBy(image = "atendimento_Dados_do_item_btn_Deligenciamento", x=4, y=5)
    private SikuliElement btnDeligenciamento;

    @FindBy(image = "atendimento_Dados_do_item_btn_Fechar", x=4, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atendimento_Dados_do_item_btn_informacoes", x=4, y=5)
    private SikuliElement btnInformacoes;

    @FindBy(image = "atendimento_Dados_do_item_btn_OK", x=4, y=5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_Dados_do_item_txt_Cod_Amostra", x=30, y=5)
    private SikuliElement txtCodAmostra;

    @FindBy(image = "atendimento_Dados_do_item_txt_intrucao", x=30, y=5)
    private SikuliElement txtInstrucao;


    public void clickBtnAterarDataDoResultado(){
          Utils.sleep(2000);
        if ( btnAterarDataDoResultado.exists(6)){
             Assert.assertTrue(btnAterarDataDoResultado.exists());
            btnAterarDataDoResultado.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnDeligenciamento(){
        Utils.sleep(3000);
        if ( btnDeligenciamento.exists(6)){
            Assert.assertTrue(btnDeligenciamento.exists());
            btnDeligenciamento.click();
            Utils.sleep(500);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnFechar(){
        Utils.sleep(2000);
        if ( btnFechar.exists(6)){
            Assert.assertTrue(btnFechar.exists());
            btnFechar.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnInformacoes(){
        Utils.sleep(2000);
        if ( btnInformacoes.exists(6)){
            Assert.assertTrue(btnInformacoes.exists());
            btnInformacoes.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnOK(){
        Utils.sleep(2000);
        if ( btnOK.exists(6)){
            Assert.assertTrue(btnOK.exists());
            btnOK.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void informarTxtCodAmostra (String codAmostra){
        Utils.sleep(200);
        if ( txtCodAmostra.exists(6)){
            Assert.assertTrue(txtCodAmostra.exists());
            txtCodAmostra.type(Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+ Key.DELETE +Key.DELETE +codAmostra);
        }else{
            Assert.assertTrue(false);
        }
    }

    public void informarTxtInstrucao (String instrucao){
        Utils.sleep(200);
        if ( txtInstrucao.exists(6)){
            Assert.assertTrue(txtInstrucao.exists());
            txtInstrucao.type(Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+Key.BACKSPACE+ Key.DELETE +Key.DELETE +instrucao);
        }else{
            Assert.assertTrue(false);
        }
    }

}
