package com.pixeon.smart.pages.atende.atendimento.atendimento_XML_de_Enviar_e_retorno;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;


public class PageAtendimentoXmlDeEnvioERetorno extends BasePage {
    @FindBy(image = "atendimento_XML_de_Envio_e_Retorno_Link_xml_de_envio", x=4, y=5)
    private SikuliElement linkXmlDeEnvio;

    @FindBy(image = "atendimento_XML_de_Envio_e_Retorno_Link_xml_de_Retorno", x=4, y=5)
    private SikuliElement linkXmlDeRetorno;

    @FindBy(image = "atendimento_XML_de_Envio_e_Retorno_btn_salvar_como", x=4, y=5)
    private SikuliElement btnSalvarComo;

    @FindBy(image = "atendimento_XML_de_Envio_e_Retorno_btn_Copiar", x=4, y=5)
    private SikuliElement btnCopiar;


    public void clickBtnLinkXmlDeRetorno (){
        Utils.sleep(200);
        if (linkXmlDeRetorno.exists(8)){
            Assert.assertTrue(linkXmlDeRetorno.exists());
            linkXmlDeRetorno.click();
        }else{
            Assert.assertTrue(true);
        }
    }

    public void clickBtnLinkXmlDeEnvio (){
        Utils.sleep(200);
        if (linkXmlDeEnvio.exists(8)){
            Assert.assertTrue(linkXmlDeEnvio.exists());
            linkXmlDeEnvio.click();
        }else{
            Assert.assertTrue(true);
        }
    }

    public void clickBtnCopiar (){
        Utils.sleep(1000);
        if (btnCopiar.exists(8)){
            Assert.assertTrue(btnCopiar.exists());
            btnCopiar.click();
        }else{
            Assert.assertTrue(true);
        }
    }

    public void clickBtnSalvarComo(){
        Utils.sleep(1000);
        if (btnSalvarComo.exists(8)){
            Assert.assertTrue(btnSalvarComo.exists());
            btnSalvarComo.click();
            Utils.sleep(1000);
        }else{
            Assert.assertTrue(true);
        }
    }



}
