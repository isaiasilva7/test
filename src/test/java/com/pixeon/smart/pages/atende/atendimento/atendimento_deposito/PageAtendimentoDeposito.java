package com.pixeon.smart.pages.atende.atendimento.atendimento_deposito;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageAtendimentoDeposito extends BasePage {


    @FindBy(image = "atendimento_btn_cancelar_Deposito", x=5, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_btn_Devolver_Deposito", x=5, y=5)
    private SikuliElement btnDevolver;

    @FindBy(image = "Atendimento_btn_fechar_deposito", x=5, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atendimento_btn_Novo_Deposito", x=5, y=5)
    private SikuliElement btnNovo;

    @FindBy(image = "atendimento_btn_Visualizar_deposito", x=5, y=5)
    private SikuliElement btnVisualizar;

    @FindBy(image = "atendimento_valor_do_Deposito_500", x=5, y=5)
    private SikuliElement lblValorDepositado500;


    @FindBy(image = "atendimento_valor_do_Deposito_devolvido", x=5, y=5)
    private SikuliElement lblValorDepositadoDevolvido;

    public void clickBtnCancelar(){

        btnCancelar.exists();
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }
    public void clickBtnbtnDevolver(){
        Utils.sleep(800);
        btnDevolver.exists();
        Assert.assertTrue(btnDevolver.exists());
        btnDevolver.doubleClick();
    }

    public void clickBtnNovo(){
        btnNovo.exists();
        Assert.assertTrue(btnNovo.exists());
        btnNovo.click();
    }

    public void clickBtnFechar(){
        btnFechar.exists();
        Assert.assertTrue( btnFechar.exists());
        btnFechar.click();
    }

    public void clickBtnVisualizar(){
        btnVisualizar.exists();
        Assert.assertTrue( btnVisualizar.exists());
        btnVisualizar.click();
    }

    public void confirmaValorDepositado500(){

        Utils.sleep(800);
        lblValorDepositado500.exists();
        Assert.assertTrue( lblValorDepositado500.exists());
    }

    public void confirmaValorDepositadoDevolvido(){

       if (lblValorDepositadoDevolvido.exists(8)){
           Assert.assertTrue( lblValorDepositadoDevolvido.exists());
       }else{
           Assert.assertTrue( "Resultado errado",false);
       }

    }




}
