package com.pixeon.smart.pages.atende.atendimento.atendimento_trasferir_paciente_para_fila;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

import javax.swing.*;

public class PageAtendimentoTrasferirPacienteParaFila extends BasePage {

    @FindBy(image = "atendimento_Btn_Adicionar_paciente_a_fila", x = 3, y = 5)
    private SikuliElement btnAdicionarPacienteAFila;

    @FindBy(image = "atendimento_Btn_cancelar_Adicionar_paciente_a_fila", x = 3, y = 5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_Txt_Fila_adicionar_Paciente_a_Fila", x = 40, y = 5)
    private SikuliElement txtFila;

    @FindBy(image = "atendimento_Txt_Reponsavel_Adicionar_Paciente_a_Fila", x = 40, y = 5)
    private SikuliElement txtReponsavel;

    @FindBy(image = "atendimento_Txt_Senha_Adicionar_Paciente_a_Fila", x = 10, y = 5)
    private SikuliElement txtSenha;

    @FindBy(image = "atendimento_Btn_Transfeir_transferir_paciente_para_Fila", x = 10, y = 5)
    private SikuliElement btnTransfeir;





    public void clickBtnAdicionarPacienteAFila(){
        btnAdicionarPacienteAFila.exists(5);
        Assert.assertTrue(btnAdicionarPacienteAFila.exists());
        btnAdicionarPacienteAFila.click();
    }

    public void clickBtnCancelar(){
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }

    public void clickBtnTransfeir(){
        Assert.assertTrue(btnTransfeir.exists());
        btnTransfeir.click();
    }

    public void informarTxtFila(String fila){
        Utils.sleep(1000);
        Assert.assertTrue(txtFila.exists());
        txtFila.doubleClick();
        txtFila.type(fila);
    }

    public void informartxtReponsavel(String reponsavel){
        Assert.assertTrue(txtReponsavel.exists());
        txtReponsavel.doubleClick();
        txtReponsavel.type(reponsavel);
    }
    public void informarTxtSenha(String senha){
    	try {
    	    Utils.sleep(3000);
    	    btnAdicionarPacienteAFila.exists(6);
  		   Assert.assertTrue(txtSenha.exists());
    	        txtSenha.doubleClick();
    	        txtSenha.type(senha);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(senha);
            Assert.assertTrue(txtSenha.exists());
		}
    }


}
