package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_Acompanhante;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

import java.sql.SQLOutput;

public class PageLaboratorioAtendimentoAcompanhante extends BasePage {

    @FindBy(image = "atendimento_Acompanhante_txt_Nome_do_acompanhate", x=5, y=2)
    private SikuliElement txtNomeDoAcompanhate;

    @FindBy(image = "atendimento_Acompanhante_txt_observacao", x=4, y=5)
    private SikuliElement txtObservacao;

    @FindBy(image = "atendimento_Acompanhante_txt_paretensco", x=-10, y=2)
    private SikuliElement txtParentesco;

    @FindBy(image = "atendimento_Acompanhante_txt_ultimo_comparecimento", x=10, y=13)
    private SikuliElement txtUltimoComparecimento;



    public void informarTxtNomeDoAcompanhate(String nomeDoAcompanhate){
        Utils.sleep(500);
        txtNomeDoAcompanhate.exists(6);
        txtNomeDoAcompanhate.click();
        txtNomeDoAcompanhate.click();
        Assert.assertTrue(txtNomeDoAcompanhate.exists());
        txtNomeDoAcompanhate.type(nomeDoAcompanhate);
    }

    public void informarTxtObservacao(String observacao){
        Utils.sleep(500);
        txtObservacao.exists(6);
        txtObservacao.click();
        Assert.assertTrue(txtObservacao.exists());
        txtObservacao.type(observacao);
    }

    public void informarTxtParentesco(String parentesco){
        Utils.sleep(100);
        txtParentesco.exists(6);
        txtParentesco.click();
        txtParentesco.click();
        Assert.assertTrue(txtParentesco.exists());
        txtParentesco.type(parentesco);
    }

    public void informarTxtUltimoComparecimento(String ultimoComparecimento){
        Utils.sleep(500);
        txtUltimoComparecimento.exists(6);
        txtUltimoComparecimento.click();
        Assert.assertTrue(txtUltimoComparecimento.exists());
        txtUltimoComparecimento.type(ultimoComparecimento);
    }

}
