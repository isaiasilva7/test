package com.pixeon.smart.pages.atende.atendimento.atendimento_selecione_o_servico;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageAtendimentoSelecioneOServico extends BasePage {

    @FindBy(image = "atendimento_selecione_O_servico_txt_texto", x=5, y=2)
    private SikuliElement txtNomeDoArquivo;

    @FindBy(image = "atendimento_selecione_O_servico_Scroll")
    private SikuliElement btnScroll;

    @FindBy(image = "atendimento_selecione_O_servico_btn_OK", x=5, y=5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_selecione_O_servico_Cbo_codigo_")
    private SikuliElement CboCodigo;


    public void informarTexto(String texto){
        Utils.sleep(100);
        if (txtNomeDoArquivo.exists(3)){
            txtNomeDoArquivo.click();
            txtNomeDoArquivo.type(texto);
        }else{
            Assert.assertTrue("Cbo Código não encontrado", false);
        }
    }

    public void clicarCboCodigo(){
        Utils.sleep(100);
        if (CboCodigo.exists(3)){
            CboCodigo.click();
        }else{
            Assert.assertTrue("Cbo Código não encontrado", false);
        }
    }

    public void clicarBtnOK(){
        Utils.sleep(100);
      if (btnOK.exists(3)){
          btnOK.click();
      }else{
          Assert.assertTrue("botão não encontrado", false);
      }
    }

    public void clicarBtnbtnScroll(){
        Utils.sleep(100);
        if (btnScroll.exists(3)){
            btnScroll.click();
        }else{
            Assert.assertTrue("Scroll não encontrado", false);
        }
    }

}
