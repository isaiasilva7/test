package com.pixeon.smart.pages.atende.laboratorio.laboratorio_Ordem_de_servicos;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioOrdemDeServico extends BasePage {



    //txt Codigo
    @FindBy(image = "atende_atendimento_codigo_do_servico_1", x = 3, y = 20)
    private SikuliElement txtcodigoDoServico1;
    //txtmaterial
    @FindBy(image = "atende_laboratorio_ordem_de_servico_txt_Material", x = 3, y = 20)
    private SikuliElement txtMaterial;
    // txt Tipo
    @FindBy(image = "atende_laboratorio_ordem_de_servico_txt_tipo", x = 3, y = 20)
    private SikuliElement txtTipo;
    //txt valor
    @FindBy(image = "atende_laboratorio_ordem_de_servico_txt_Valor", x = 3, y = 23)
    private SikuliElement txtValor;


    //btn Depostio
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_deposito", x = 3, y = 2)
    private SikuliElement btnDeposito;
    //btn pagamento
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_pagamento", x = 3, y = 2)
    private SikuliElement btnPagamento;
    //btn arquivos
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_arquivo", x = 3, y = 2)
    private SikuliElement btnArquivos;
    //btn questionarios
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_Questionario", x = 3, y = 2)
    private SikuliElement btnQuestionario;
    //btn info
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_info", x = 3, y = 2)
    private SikuliElement btnInfo;
    //btn Obs
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_Obs", x = 3, y = 2)
    private SikuliElement btnObs;
    //btn Reprogramar
    @FindBy(image = "atende_laboratorio_ordem_de_servico_btn_reprogramar", x = 3, y = 2)
    private SikuliElement btnReprogramar;

    //flag
    @FindBy(image = "atende_laboratorio_ordem_de_servico_flag_faturado")
    private SikuliElement flagFaturado;


    static public String numeroOSAtende;
    static public String numeroSerieOSAtende;
    static public String valorDoServico1Laboratorio;


    public String capturaValorDoServico1() {
        try {
            Utils.sleep(6000);
            if (txtValor.exists(3)){
                txtValor.doubleClick();
                valorDoServico1Laboratorio = "";
                valorDoServico1Laboratorio = "";
                valorDoServico1Laboratorio = Utils.capturadoTextoSelecionado();
               // return valorDoServico1Laboratorio;
            }else{
                Utils.sleep(5000);
                txtValor.doubleClick();
                Utils.sleep(2000);
                valorDoServico1Laboratorio = "";
                valorDoServico1Laboratorio = "";
                valorDoServico1Laboratorio = Utils.capturadoTextoSelecionado();
            }
        }catch (Exception e){
            Assert.assertTrue(false);
        }
        return valorDoServico1Laboratorio;
    }

    //Codigo
    public void inserirCodigoDoServico1(String codigoDoServicoUm) {
        Utils.sleep(1000);
        txtcodigoDoServico1.exists(2);
        txtcodigoDoServico1.type(codigoDoServicoUm + Key.TAB+ Key.TAB);
    }


    //Codigo
    public void inserirMaterialDoServico1(String material) {
        Utils.sleep(1000);
       if (txtMaterial.exists(2)){
           txtMaterial.type(material + Key.TAB);
       }else{
           Assert.assertFalse("txt material não encontrado",true);
       }
    }

   //txtTipo
   public void inserirTxtTipo1(String tipo) {
     //  Utils.sleep(1000);
       if (txtTipo.exists(6)){
           txtTipo.click();
           txtTipo.type(tipo + Key.TAB);
       }else{
           Assert.assertFalse("txt material não encontrado",true);
       }
   }

    //btn Deposito
    public void clicarBtnDeposito() {
        Utils.sleep(500);
        if (btnDeposito.exists(4)){
            btnDeposito.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }

    //btn Pagamento
    public void clicarBtnPagamento() {
        PageLaboratorioOrdemDeServico pageLaboratorioOrdemDeServico=new PageLaboratorioOrdemDeServico();
        pageLaboratorioOrdemDeServico.inserirTxtTipo1("");
        Utils.sleep(500);
        if (btnPagamento.exists(5)){
            btnPagamento.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }


    //btn Pagamento
    public void clicarBtnArquivos() {
        Utils.sleep(500);
        if (btnArquivos.exists(4)){
            btnArquivos.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }

    //btn questionarios
    public void clicarBtnQuestionario() {
        Utils.sleep(500);
        if (btnQuestionario.exists(4)){
            btnQuestionario.click();
            Utils.sleep(200);
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }

    //btn questionarios
    public void clicarBtnInfo() {
        Utils.sleep(500);
        if (btnInfo.exists(4)){
            btnInfo.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }

    //btn Obs
    public void clicarBtnObs() {
        Utils.sleep(500);
        if (btnObs.exists(4)){
            btnObs.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }

    //btn Reprogramar
    public void clicarBtnReprogramar() {
        Utils.sleep(500);
        if (btnReprogramar.exists(4)){
            btnReprogramar.click();
        }else{;
            Assert.assertFalse("btn Deposito não encontrado",true);
        }
    }


    //Faturado
    public void confirmaExistenciaDoCampo() {
        Utils.sleep(500);
        if (flagFaturado.exists(4)){
            Assert.assertTrue("",true);
        }else{
            Assert.assertTrue("flag Faturado não encontrado",false);
        }
    }
}
