package com.pixeon.smart.pages.atende.laboratorio.laboratorio_imprimir_Ordem_De_servico;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioQuantidadeDeEtiquetas extends BasePage {


    @FindBy(image = "atendimento_bt_impimir_quantidade_etiquetas")
    private SikuliElement btnImprimir;

    @FindBy(image = "atendimento_btn_cancelar_etiquetas")
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_txt_quantidade_etiquetas", x=10, y=10)
    private SikuliElement txtQuantidade;

    @FindBy(image = "atendimento_btn_ok_quantidade_de_etiqueta", x=10, y=10)
    private SikuliElement btnOK;


    public void clickBtnCancelar(){
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }

    public void clickBtnOK(){
        Assert.assertTrue(btnOK.exists());
        btnOK.click();
    }

    public void clickBtnImprimir(){
        Assert.assertTrue(btnImprimir.exists());
        btnImprimir.click();
    }

    public void informarQuantidadeEtiquetas(String quantidade){
        Utils.sleep(2000);
        txtQuantidade.exists(5);
        Assert.assertTrue(txtQuantidade.exists());
        txtQuantidade.click();
        txtQuantidade.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtQuantidade.type(quantidade);

    }
}

