package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_Diligenciamento;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

import java.sql.SQLOutput;

public class PageLaboratorioAtendimentoDiligenciamento extends BasePage {


    @FindBy(image = "atendimento_Diligenciamento_aba_Diligenciamento", x=5, y=5)
    private SikuliElement abaDiligenciamento;

    @FindBy(image = "atendimento_Diligenciamento_aba_Status", x=5, y=5)
    private SikuliElement abaStatus;

    @FindBy(image = "atendimento_Diligenciamento_aba_Rastraebilidade", x=5, y=5)
    private SikuliElement abaRastreabilidade;

    @FindBy(image = "atendimento_Diligenciamento_aba_Faturamento", x=5, y=5)
    private SikuliElement abaFaturamento;

    @FindBy(image = "atendimento_Diligenciamento_aba_Documentos_da_OS", x=5, y=5)
    private SikuliElement abaDocumentos;

    @FindBy(image = "atendimento_Diligenciamento_aba_Comunicacao_WS", x=5, y=5)
    private SikuliElement abaComunicacaoWS;

    @FindBy(image = "atendimento_Diligenciamento_btn_Cancelar", x=5, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_Diligenciamento_btn_fechar", x=5, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atendimento_Diligenciamento_btn_OK", x=5, y=5)
    private SikuliElement btnOK;

    @FindBy(image = "atendimento_aba_comunicacao_ws_scroll")
    private SikuliElement scroll;

    @FindBy(image = "atendimento_aba_comunicacao_WS_XML_enviado", x=5, y=20)
    private SikuliElement lblXMLEnviado1;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_procedimento", x=350)
    private SikuliElement LblProcedimento;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_RequisicaoAlvaro", x=350)
    private SikuliElement LblRequisicaoAlvaro;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_Staus_autorizacao", x=350)
    private SikuliElement LblStausautorizacao;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_ReceberAtendimento_DB_DIAGNOSTICOS", x=350)
    private SikuliElement LblReceberAtendimentoDbDiagnotico;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_EnviarAmostra_DB_DIAGNOSTICOS", x=380)
    private SikuliElement LblEnviarAmostra;

    @FindBy(image = "atendimento_diligenciamento_comunicacao_ws_Lbl_getPedido_HermesPardini", x=380)
    private SikuliElement LblgetPedido;

    public void clickLblgetPedido(){
        if (LblgetPedido.exists(6)){
            Assert.assertTrue(LblgetPedido.exists());
            LblgetPedido.click();
            LblgetPedido.doubleClick();
        }else {
            Assert.assertTrue(false);
        }
    }
    public void clickLblReceberAtendimentoDbDiagnotico(){
        if (LblReceberAtendimentoDbDiagnotico.exists(6)){
            Assert.assertTrue(LblReceberAtendimentoDbDiagnotico.exists());
            LblReceberAtendimentoDbDiagnotico.click();
            LblReceberAtendimentoDbDiagnotico.doubleClick();
        }else {
            Assert.assertTrue(false);
        }
    }


    public void clickLblEnviarAmostrDbDiagnotico(){
        if (LblEnviarAmostra.exists(6)){
            Assert.assertTrue(LblEnviarAmostra.exists());
            LblEnviarAmostra.click();
            LblEnviarAmostra.doubleClick();
        }else {
            Assert.assertTrue(false);
        }
    }


    public void clickLblStausAutorizacao(){
        if (LblStausautorizacao.exists(6)){
            Assert.assertTrue(LblStausautorizacao.exists());
            LblStausautorizacao.click();
            LblStausautorizacao.doubleClick();
        }else {
            Assert.assertTrue(false);
        }
    }

    public void clickLblRequisicaoAlvaro(){
        if (LblRequisicaoAlvaro.exists(6)){
            Assert.assertTrue(LblRequisicaoAlvaro.exists());
            LblRequisicaoAlvaro.click();
            LblRequisicaoAlvaro.doubleClick();

        }else {
            Assert.assertTrue(false);
        }
    }

    public void clickLblProcedimento(){
        if (LblProcedimento.exists(6)){
            Assert.assertTrue(LblProcedimento.exists());
            LblProcedimento.click();
            LblProcedimento.doubleClick();
        }else {
            Assert.assertTrue(false);
        }
    }


    public void clickXMLEnviado1(){
        if (lblXMLEnviado1.exists(6)){
            Assert.assertTrue(lblXMLEnviado1.exists());
            lblXMLEnviado1.click();
            lblXMLEnviado1.doubleClick();

        }else {
            Assert.assertTrue(false); 
        }
    }


    public void clickScroll(){
        if(scroll.exists(3)){
            Assert.assertTrue(scroll.exists());
            scroll.click();;
        }else{
            Assert.assertTrue("scroll não encontrado" ,false);
        }
    }

    public void clickAbaDiligenciamento(){

        if(abaDiligenciamento.exists(2)){
            abaDiligenciamento.exists();
            Assert.assertTrue(abaDiligenciamento.exists());
            abaDiligenciamento.click();
        }else{
            Assert.assertTrue(false);
        }

    }

    public void clickAbaStatus(){
        abaStatus.exists();
        Assert.assertTrue(abaStatus.exists());
        abaStatus.click();
    }

    public void clickAbaRastreabilidade(){
        abaRastreabilidade.exists();
        Assert.assertTrue(abaRastreabilidade.exists());
        abaRastreabilidade.click();
    }

    public void clickAbaFaturamento(){
        abaFaturamento.exists();
        Assert.assertTrue(abaFaturamento.exists());
        abaFaturamento.click();
    }

    public void clickAbaComunicacaoWS(){
        abaComunicacaoWS.exists();
        Assert.assertTrue(abaComunicacaoWS.exists());
        abaComunicacaoWS.click();
    }

    public void clickAbaDocumentos(){
        abaDocumentos.exists();
        Assert.assertTrue(abaDocumentos.exists());
        abaDocumentos.click();
    }

    public void clickBtnbtnOK(){
        btnOK.exists();
        Assert.assertTrue(btnOK.exists());
        btnOK.click();
    }

    public void clickBtnFechar(){
        btnFechar.exists();
        Assert.assertTrue(btnFechar.exists());
        btnFechar.click();
    }

    public void clickBtnCancelar(){
        btnCancelar.exists();
        Assert.assertTrue(btnCancelar.exists());
        btnCancelar.click();
    }


}
