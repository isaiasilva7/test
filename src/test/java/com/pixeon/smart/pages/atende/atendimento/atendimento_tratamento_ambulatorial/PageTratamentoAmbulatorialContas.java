package com.pixeon.smart.pages.atende.atendimento.atendimento_tratamento_ambulatorial;

import com.pixeon.smart.pages.BasePage;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

public class PageTratamentoAmbulatorialContas extends BasePage {

    @FindBy(image = "atendemento_tratamento_Ambulatorial_contas_btnGuiaTiss")
    private SikuliElement btnGuiaTiss;

    public void clickBtnGuiaTiss(){
        btnGuiaTiss.click();
    }


}
