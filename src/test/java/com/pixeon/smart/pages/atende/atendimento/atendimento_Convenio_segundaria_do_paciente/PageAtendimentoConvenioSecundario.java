package com.pixeon.smart.pages.atende.atendimento.atendimento_Convenio_segundaria_do_paciente;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageAtendimentoConvenioSecundario extends BasePage {

    @FindBy(image = "atendiemtno_txt_chave_S_L_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtChaveSLine;

    @FindBy(image = "atendiemtno_txt_convenio_secundaria_do_paciente_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtConvenioSecuncario;

    @FindBy(image = "atendiemtno_txt_data_validade_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtDataDeVencimento;

    @FindBy(image = "atendiemtno_txt_lotacao_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtLocacao;

    @FindBy(image = "atendiemtno_txt_matricula_secundaria_aba_convenio_secundario", x=60, y=5)
    private SikuliElement txtMatriculaNaEmpresa;

    @FindBy(image = "atendiemtno_txt_Plano_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtPlanoConvenio;

    @FindBy(image = "atendiemtno_txt_titular_aba_convenio_secundario", x=40, y=5)
    private SikuliElement txtTitularConcenio;



    public void informarTxtChaveSLine(String ChaveSLine){
        Utils.sleep(500);
        txtChaveSLine.exists(6);
        Assert.assertTrue(txtChaveSLine.exists());
        txtChaveSLine.type(ChaveSLine+Key.TAB);
    }

    public void informarTxtConvenioSecuncario(String convenioSecuncario){
        Utils.sleep(500);
        txtConvenioSecuncario.exists(6);
        txtConvenioSecuncario.click();
        Assert.assertTrue(txtConvenioSecuncario.exists());
        txtConvenioSecuncario.type(convenioSecuncario+ Key.TAB);
    }

    public void informarTxtDataDeVencimento(String dataDeVencimento){
        Utils.sleep(500);
        txtDataDeVencimento.exists(6);
        txtDataDeVencimento.click();
        Assert.assertTrue(txtDataDeVencimento.exists());
        txtDataDeVencimento.type(dataDeVencimento+ Key.TAB);
    }

    public void informarTxtLocacao(String locacao){
        Utils.sleep(500);
        txtLocacao.exists(6);
        txtLocacao.click();
        Assert.assertTrue(txtLocacao.exists());
        txtLocacao.type(locacao+ Key.TAB);
    }

    public void informarTxtMatriculaNaEmpresa(String matriculaNaEmpresa){
        Utils.sleep(500);
        txtMatriculaNaEmpresa.exists(6);
        txtMatriculaNaEmpresa.click();
        Assert.assertTrue(txtMatriculaNaEmpresa.exists());
        txtMatriculaNaEmpresa.type(matriculaNaEmpresa+ Key.TAB+Key.TAB);
    }

    public void informarTxtPlanoConvenio(String planoConvenio){
        Utils.sleep(500);
        txtPlanoConvenio.click();
        txtPlanoConvenio.exists(6);
        Assert.assertTrue(txtPlanoConvenio.exists());
        txtPlanoConvenio.type(planoConvenio+Key.TAB);
    }

    public void informarTxtTitularConcenio(String titularConcenio){
        Utils.sleep(500);
        txtTitularConcenio.exists(6);
        Assert.assertTrue(txtTitularConcenio.exists());
        txtTitularConcenio.click();
        txtTitularConcenio.type(titularConcenio+Key.TAB);
    }

}
