package com.pixeon.smart.pages.atende.atendimento.atendimento_Relatorio_Internacao;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageRelatorioInternacao extends BasePage {

    @FindBy(image = "atendimento_aba_filtro_relatorio_internacao", x = 3, y = 5)
    private SikuliElement abaFiltro;

    @FindBy(image = "atendimento_aba_opcao_relatorio_internacao", x = 3, y = 5)
    private SikuliElement abaOpcoes;

    @FindBy(image = "atendimento_aba_outros_relatorios_relatorio_internacao", x = 3, y = 5)
    private SikuliElement abaOutrosRelatoios;


    @FindBy(image = "atendimento_btn_cancelar_relatorio_internacao", x = 3, y = 5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_btn_visualizar_relatorio_internacao", x = 3, y = 5)
    private SikuliElement btnVisualizar;

    @FindBy(image = "atendimento_lbl_documento_Alta_A_Pedido_documentos_relatorio_internacao", x = 3, y = 5)
    private SikuliElement lblAltaDoPedido;


    @FindBy(image = "atendimento_scroll_documentos_relatorio_internacao",x = 2, y = 2)
    private SikuliElement btnScroolDocumento;


    public void clickAbaFiltro() {
        abaFiltro.exists();
        Assert.assertTrue(abaFiltro.exists());
        abaFiltro.click();
    }

    public void clickLblAltaDoPedido() {
        Utils.sleep(100);
        lblAltaDoPedido.exists(2);
        Assert.assertTrue(lblAltaDoPedido.exists());
        lblAltaDoPedido.click();
    }

    public void clickBtnScroolDocumento() {
        Utils.sleep(1000);
        btnScroolDocumento.exists(2);
       // Assert.assertTrue(btnScroolDocumento.exists());
        btnScroolDocumento.click();

    }


    public void clickBtnVisualizar() {
        btnVisualizar.exists();
        Assert.assertTrue(btnVisualizar.exists());
        btnVisualizar.click();

    }
}
