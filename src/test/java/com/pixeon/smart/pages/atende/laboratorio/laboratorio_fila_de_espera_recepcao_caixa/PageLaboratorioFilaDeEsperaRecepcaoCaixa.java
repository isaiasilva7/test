package com.pixeon.smart.pages.atende.laboratorio.laboratorio_fila_de_espera_recepcao_caixa;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;

import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;

import org.junit.Assert;
import org.sikuli.script.Key;

public class PageLaboratorioFilaDeEsperaRecepcaoCaixa extends BasePage {

    @FindBy(image = "atende_atendimento_btn_Encerrar_Atendiemtno_Fila_de_espera", x=10, y=3)
    private SikuliElement btnEncerrarAtendiemtno;

    @FindBy(image = "atende_atendimento_txt_nome_Fila_de_espera", x=25, y=5)
    private SikuliElement txtNome;

    @FindBy(image = "atende_atendimento_btn_Detalhe_Fila_de_espera", x=10, y=5)
    private SikuliElement btnDetalhe;

    @FindBy(image = "atende_atendimento_btn_Trasferencia_Fila_de_espera", x=10, y=5)
    private SikuliElement btnTrasferencia;

    @FindBy(image = "atende_atendimento_txt_nome_do Paciente_Fila_de_espera", x=30, y=50)
    private SikuliElement txtNomePacienteInserido;

    @FindBy(image = "atendimento_txt_Fila_medica_fila_de_espera", x = 40, y = 5)
    private SikuliElement txtFilaMedico;



    @FindBy(image = "atende_atendimento_btn_chama_Proximo_Fila_de_espera", x=10, y=5)
    private SikuliElement btnChamaProximo;

    @FindBy(image = "atende_atendimento_btn_chama_Selecionado_Fila_de_espera", x=10, y=5)
    private SikuliElement btnChamaSelecionado;


    @FindBy(image = "atende_atendimento_btn_Atualizar_Fila_de_espera", x=10, y=5)
    private SikuliElement btnAtualizarFila;

    @FindBy(image = "atende_atendimento_btn_CR_Fila_de_espera", x=10, y=5)
    private SikuliElement btnCR;

    @FindBy(image = "atende_atendimento_btn_Foto_Fila_de_espera", x=10, y=5)
    private SikuliElement btnFoto;

    @FindBy(image = "atende_atendimento_btn_Imprimir_Fila_de_espera", x=10, y=5)
    private SikuliElement btnImprimir;

    @FindBy(image = "atendimento_lb_medico_Abilio_Fila", x=10, y=5)
    private SikuliElement lblMedicoAbilio;

    @FindBy(image = "atendimento_lb_medico_Ivisson_lopes_Fila", x=10, y=5)
    private SikuliElement lblMedicoIvissonLopes;

    @FindBy(image = "atende_atendimento_btn_Historico_Fila_de_espera", x=10, y=5)
    private SikuliElement btnHistorico;

    @FindBy(image = "atende_atendimento_btn_Marcacao_Fila_de_espera", x=10, y=5)
    private SikuliElement btnMarcacao;

    @FindBy(image = "atende_atendimento_btn_Rastreabilidade_Fila_de_espera", x=10, y=5)
    private SikuliElement btnRastreabilidade;





  public void clickBtnEncerrarAtendimento() {
      Utils.sleep(200);
      if (btnEncerrarAtendiemtno.exists(6)){
          btnEncerrarAtendiemtno.click();
      }else{
          Assert.assertTrue(false);
      }
  }

    public void inserirTxtNome(String nome) {
        Assert.assertTrue(txtNome.exists());
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtNome.doubleClick();
        txtNome.type(nome + Key.TAB);
    }

    public void clickBtnDetalhe() {
        if (btnDetalhe.exists(4)){
            btnDetalhe.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnTrasferencia() {
        if (btnTrasferencia.exists(4)){
            btnTrasferencia.click();
        }else{
            Assert.assertTrue(false);
        }
    }


    public String  capturaraTxtNomePacienteInserido() {
        String valorcaprurado;
        //Utils utils = new Utils();
        Utils.sleep(2000);
        Assert.assertTrue(txtNomePacienteInserido.exists());
        txtNomePacienteInserido.click();
        valorcaprurado = Utils.capturadoTextoSelecionado();
        return valorcaprurado;
    }

    public void inserirFilaMedico(String medico) {
        Assert.assertTrue(txtFilaMedico.exists());
        txtFilaMedico.type(Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE + Key.DELETE);
        txtFilaMedico.type(Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE);
        txtFilaMedico.type(medico + Key.TAB);
    }

    public void clickBtnChamaSelecionado() {
        Utils.sleep(500);
        btnChamaSelecionado.exists();
        Assert.assertTrue(btnChamaSelecionado.exists());
        btnChamaSelecionado.click();
    }

    public void clickBtnChamaProximo() {
       if (btnChamaProximo.exists(4)) {
           btnChamaProximo.click();
       }else{
           Assert.assertTrue(false);
       }
    }

    public void clickBtnAtualizarFila() {
        if (btnAtualizarFila.exists(4)) {
            btnAtualizarFila.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnCR() {
        if (btnCR.exists(4)) {
            btnCR.click();
        }else{
            Assert.assertTrue(false);
        }
    }



    public void clickBtnFoto() {
        if (btnFoto.exists(4)) {
            btnFoto.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickBtnImprimir() {
        if (btnImprimir.exists(4)) {
            btnImprimir.click();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickLbMedicoAbilio() {
        if (lblMedicoAbilio.exists(4)) {
            lblMedicoAbilio.click();
        }else{
            Assert.assertTrue(false);
        }
    }



    public void clickLblMedicoIvissonLopes() {
        if (lblMedicoIvissonLopes.exists(4)) {
            lblMedicoIvissonLopes.doubleClick();
        }else{
            Assert.assertTrue(false);
        }
    }
    public void clickBtnMarcacao() {
        if (btnMarcacao.exists(4)) {
            btnMarcacao.doubleClick();
        }else{
            Assert.assertTrue(false);
        }
    }

    public void clickbtnRastreabilidade() {
        if (btnRastreabilidade.exists(4)) {
            btnRastreabilidade.doubleClick();
        }else{
            Assert.assertTrue(false);
        }
    }


    public void clickBtnHistorico() {
        if (btnHistorico.exists(4)) {
            btnHistorico.doubleClick();
        }else{
            Assert.assertTrue(false);
        }
    }



}
