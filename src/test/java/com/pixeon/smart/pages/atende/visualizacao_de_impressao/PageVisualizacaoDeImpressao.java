package com.pixeon.smart.pages.atende.visualizacao_de_impressao;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageVisualizacaoDeImpressao extends BasePage {
    @FindBy(image = "atende_titulo_guia_tiss", x=20, y=5)
    private SikuliElement lblTituloGuiaTiss;

    @FindBy(image = "atendimento_btn_Salvar_Como_impressao", x=5, y=5)
    private SikuliElement btnSalvarComo;

    @FindBy(image = "atendiemento_btn_imprimir_visualizacao_impressao", x=5, y=5)
    private SikuliElement btnImprimir;

    @FindBy(image = "atendiemento_btn_imprimir1_visualizacao_impressao", x=5, y=5)
    private SikuliElement btnImprimir1;

    @FindBy(image = "atendiemento_btn_Fechar_visualizacao_impressao", x=5, y=5)
    private SikuliElement btnFechar;

    @FindBy(image = "atendimento_lbl_recibo_visualizacao", x=5, y=5)
    private SikuliElement lblRecibo;



    public void validarAExistenciaDoTituloDaGuia(){
        Boolean presente;
        presente=lblTituloGuiaTiss.exists(6);
        if (presente==true){
            Assert.assertTrue(presente);
        }else{
            System.out.println("Titulo não encontrado");
            Assert.assertTrue(false);
        }
    }

    public void clickBtnSalvarComo(){
        btnSalvarComo.exists();
        btnSalvarComo.exists(5);
        btnSalvarComo.click();
    }

    public void clickBtnImprimir(){

      if (btnImprimir.exists(2)) {
          btnImprimir.click();
      }else  if (btnImprimir1.exists()){
          btnImprimir1.click();
      }else {
          Assert.assertFalse("botão não encontrado",true);
      }

    }

    public void clickBtnFechar(){
        Utils.sleep(1500);
        btnFechar.exists();
        Assert.assertTrue(btnFechar.exists(5));
        btnFechar.click();
    }


    public void validarLblRecibo(){
        Utils.sleep(1500);
        lblRecibo.exists();
        Assert.assertTrue(lblRecibo.exists(5));
    }

}
