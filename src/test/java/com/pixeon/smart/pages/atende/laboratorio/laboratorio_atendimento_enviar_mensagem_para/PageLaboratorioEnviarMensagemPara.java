package com.pixeon.smart.pages.atende.laboratorio.laboratorio_atendimento_enviar_mensagem_para;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;

public class PageLaboratorioEnviarMensagemPara extends BasePage {


    @FindBy(image = "atendimento_enviar_mensagem_para_btn_Remover", x=5, y=5)
    private SikuliElement btnRemover;

    @FindBy(image = "atendimento_enviar_mensagem_para_btn_remover_todos", x=5, y=5)
    private SikuliElement btnRemoverTodos;

    @FindBy(image = "atendimento_enviar_mensagem_para_txt_nome", x=5, y=5)
    private SikuliElement txtNome;

    @FindBy(image = "atendimento_enviar_mensagem_para_btn_adicionar", x=5, y=5)
    private SikuliElement btnAdicionar;

    @FindBy(image = "atendimento_enviar_mensagem_para_btn_adicionar_todos", x=5, y=5)
    private SikuliElement btnAdicionarTodos;

    @FindBy(image = "atendimento_enviar_mensagem_para_btn_Cancelar", x=5, y=5)
    private SikuliElement btnCancelar;

    @FindBy(image = "atendimento_enviar_mensagem_para_Lbl_estoque", x=5, y=5)
    private SikuliElement LblEstoque;

    @FindBy(image = "atendimento_enviar_mensagem_para_btn_OK", x=5, y=5)
    private SikuliElement btnOK;

    public void clickBtnOK(){
        btnOK.exists();
        Assert.assertTrue(btnOK.exists());
        btnOK.click();
    }

    public void clickBtnEstoque(){
        LblEstoque.exists();
        Assert.assertTrue( LblEstoque.exists());
        LblEstoque.click();
    }

    public void clickBtnCancelar(){
        btnCancelar.exists();
        Assert.assertTrue( btnCancelar.exists());
        btnCancelar.click();
    }

    public void clickBtnRemover(){
        btnRemover.exists();
        Assert.assertTrue( btnRemover.exists());
        btnRemover.click();
    }

    public void clickBtnRemoverTodos(){
        btnRemoverTodos.exists();
        Assert.assertTrue( btnRemoverTodos.exists());
        btnRemoverTodos.click();
    }

    public void informarTxtNome(String nome){
        txtNome.exists();
        Assert.assertTrue( txtNome.exists());
        txtNome.type(nome);
    }

    public void clickBtnAdicionar(){
        btnAdicionar.exists();
        Assert.assertTrue( btnAdicionar.exists());
        btnAdicionar.click();
    }

    public void clickBtnAdicionarTodos(){
        btnAdicionarTodos.exists();
        Assert.assertTrue( btnAdicionarTodos.exists());
        btnAdicionarTodos.click();
    }

}
