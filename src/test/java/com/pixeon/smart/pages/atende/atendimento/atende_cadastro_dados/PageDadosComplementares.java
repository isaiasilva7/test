package com.pixeon.smart.pages.atende.atendimento.atende_cadastro_dados;

import com.pixeon.smart.pages.BasePage;
import com.pixeon.smart.util.SikuliUtil;
import com.pixeon.smart.util.Utils;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.junit.Assert;
import org.sikuli.script.Key;

public class PageDadosComplementares extends BasePage {
    @FindBy(image = "atendimento_Txt_nome_do_pai_1_dados_complementares", x=50, y=5)
    private SikuliElement txtNomeDoPai;

    @FindBy(image = "atendimento_Txt_Cor_1_dados_complementares", x=30, y=5)
    private SikuliElement txtCor;

    @FindBy(image = "atendimento_Txt_apelido_1_dados_complementares", x=60, y=5)
    private SikuliElement txtApelido;

    @FindBy(image = "atendimento_Txt_telefone_1_dados_complementares", x=30, y=5)
    private SikuliElement txtTelefone;

    @FindBy(image = "atendimento_Txt_Cidade_1_dados_complementares", x=30, y=5)
    private SikuliElement txtCidade;

    @FindBy(image = "atendimento_Txt_RG_1_dados_complementares", x=20, y=5)
    private SikuliElement txtRG;

    @FindBy(image = "atendimento_Txt_CEP_1_dados_complementares", x=30, y=5)
    private SikuliElement txtCep;

    @FindBy(image = "atendimento_Txt_numero_endereco_1_dados_complementares", x=30, y=5)
    private SikuliElement txtnumeroEndereco;

    @FindBy(image = "atendimento_Txt_bairro_1_dados_complementares", x=30, y=5)
    private SikuliElement txtbairro;

    @FindBy(image = "atendimento_Txt_religiao_1_dados_complementares", x=40, y=5)
    private SikuliElement txtReligiao;


    public void informarTxtNomeDoPai (String nomeDoPai ){
        //SikuliUtil.esperaObjetosImagem("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\atende\\atende_cadastro_dados\\cadastroDadosComplementares\\atendimento_Txt_nome_do_pai_1_dados_complementares.png", 20);
        Utils.sleep(4000);
       if (txtNomeDoPai.exists(10)){
           txtNomeDoPai.click();
           txtNomeDoPai.type(nomeDoPai);
           Utils.sleep(1000);

       }else{
           Assert.assertTrue(false);
       }




    }


    public void informarTxtApelido (String apelido ){
        Utils.sleep(500);
        txtApelido.exists(2);
        txtApelido.click();
        Assert.assertTrue(txtApelido.exists());
        txtApelido.type(apelido);
    }

    public void informarTxtCor (String cor ){
        Utils.sleep(500);
        txtCor.exists(2);
        Assert.assertTrue(txtCor.exists());
        txtCor.type(cor+ Key.TAB);
    }

    public void informarTxtTelefone (String telefone ){
        Utils.sleep(500);
        txtTelefone.exists(2);
        Assert.assertTrue(txtTelefone.exists());
        txtTelefone.type(telefone+ Key.TAB);
    }

    public void informarTxtCidade (String cidade ){
        Utils.sleep(500);
        txtCidade.exists(2);
        Assert.assertTrue(txtCidade.exists());
        txtCidade.type(cidade+ Key.TAB);
    }

    public void informarTxtRG (String rg ){
        Utils.sleep(500);
        txtRG.exists(2);
        Assert.assertTrue(txtRG.exists());
        txtRG.type(rg+ Key.TAB);
    }


    public void informarTxtCEP (String cep ){
        Utils.sleep(500);
        txtCep.exists(2);
        Assert.assertTrue(txtCep.exists());
        txtCep.type(cep+ Key.TAB);
    }

    public void informarTxtNumeroEndereco (String numeroEndereco ){
        Utils.sleep(500);
        txtnumeroEndereco.exists(2);
        Assert.assertTrue(txtCep.exists());
        txtnumeroEndereco.type(numeroEndereco);
    }

    public void informarTxtbairro (String bairro ){
        Utils.sleep(500);
        txtbairro.exists(2);
        Assert.assertTrue(txtCep.exists());
        txtbairro.type(bairro+ Key.TAB);
    }

    public void informarTxtReligiao(String religiao ){
        Utils.sleep(500);
        txtReligiao.exists(2);
        txtReligiao.click();
        Assert.assertTrue(txtReligiao.exists());
        txtReligiao.type(religiao);
    }

}
