package com.pixeon.smart.pages;

import com.pixeon.smart.util.CustomDriver;
import io.github.marcoslimaqa.sikulifactory.FindBy;
import io.github.marcoslimaqa.sikulifactory.SikuliElement;
import org.sikuli.script.FindFailed;

public class PageCalculadora extends BasePage {

    @FindBy(image = "calculadora")
    private SikuliElement calculadora;

    @FindBy(image = "calculadora_resultado")
    private SikuliElement btn_resultado;

    @FindBy(image = "calculadora_igual")
    private SikuliElement btn_igual;

    @FindBy(image = "calculadora_soma", y = 25)
    private SikuliElement btn_soma;

    @FindBy(image = "calculadora_subtracao", y = -25)
    private SikuliElement btn_subtracao;


    public boolean validaCalculadoraAberta() {
        return calculadora.exists();
    }

    public void clicaBotao(String botao) {
        if (botao.equals("soma"))
            btn_soma.click();
        else if (botao.equals("subtracao"))
            btn_subtracao.click();
        else {
            try {
                CustomDriver.getInstance().click("calculadora_".concat(botao));
            } catch (FindFailed findFailed) {
                findFailed.printStackTrace();
            }
        }
    }

    public boolean validaResultado(String resultado) {
        btn_igual.click();

        return btn_resultado.getText().equals(resultado);
    }
}