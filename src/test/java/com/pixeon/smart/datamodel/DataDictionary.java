package com.pixeon.smart.datamodel;

public class DataDictionary {

	private String definition;

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}
}
