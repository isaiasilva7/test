package com.pixeon.smart.datamodel.atende.atendimento_ordem_de_servico;

public class DataAtendeAtendimentoOrdemDeServico {

    private String codigo1DoServico;
    private String quantidad1DoServico;
    private String resposnavelColetaMaterial1;

    public String getCodigo1DoServico() {
        return codigo1DoServico;
    }

    public void setCodigo1DoServico(String codigo1DoServico) {
        this.codigo1DoServico = codigo1DoServico;
    }

    public String getQuantidad1DoServico() {
        return quantidad1DoServico;
    }

    public void setQuantidad1DoServico(String quantidad1DoServico) {
        this.quantidad1DoServico = quantidad1DoServico;
    }

    public String getResposnavelColetaMaterial1() {
        return resposnavelColetaMaterial1;
    }

    public void setResposnavelColetaMaterial1(String resposnavelColetaMaterial1) {
        this.resposnavelColetaMaterial1 = resposnavelColetaMaterial1;
    }
}
