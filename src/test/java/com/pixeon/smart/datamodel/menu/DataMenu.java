package com.pixeon.smart.datamodel.menu;

public class DataMenu {

    private String menu;

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}
