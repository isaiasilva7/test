package com.pixeon.smart.datamodel;

public class DataWord {

	private String word;

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
}
