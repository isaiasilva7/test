package com.pixeon.smart;

import com.pixeon.smart.util.CustomDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	private static boolean desktop;

	@Before
	public void beforeScenario(Scenario scenario) {
		desktop = scenario.getSourceTagNames().contains("@desktop");
	}

	@After
	public void afterScenario() {
		CustomDriver.killDriver();
	}

	public static boolean isDesktop() {
		return desktop;
	}

}
