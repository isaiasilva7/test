package com.pixeon.smart.util;

import net.bytebuddy.asm.Advice;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.sikuli.script.Key;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.script.Sikulix;
import org.sikuli.hotkey.Keys;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Key;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;



import java.awt.datatransfer.Clipboard;
import java.sql.SQLOutput;

import static com.pixeon.smart.util.Utils.sleep;
import static org.sikuli.script.App.getClipboard;
import static org.sikuli.script.Commands.click;

public class SikuliUtil {

	/**
	 * LOG
	 */
	final static Logger logger = Logger.getLogger(SikuliUtil.class);



	//esperaObjetoDeImagem
	public static void  esperaObjetosImagem(String urlImagem, int sec) {
		try{
          //"C:\\FrameworkJava\\DemoSikuli\\Imagens\\sete.png"
			Screen screen =new Screen();
			Pattern imagem=new Pattern(urlImagem);
			screen.wait(imagem, sec);
		}catch(Exception e){
			System.out.println("Erro ao espera a imagem." +e);
		}
	}

	//
	public static void  buscaAtravesDeAtalho() {
		try{
			Screen screen =new Screen();
			screen.type(Key.F4);
		}catch(Exception e){
			System.out.println("Erro ao atalho Buscar F4" +e);
		}
	}



}
