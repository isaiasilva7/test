package com.pixeon.smart.util;

import com.pixeon.util.UtilProperties;
import org.apache.log4j.Logger;
import org.sikuli.basics.Settings;
import org.sikuli.script.App;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Screen;

import java.io.IOException;

public class CustomDriver {


    private CustomDriver() {
    }

    /**
     * LOG
     */
    final static Logger logger = Logger.getLogger(SikuliUtil.class);

    private static Screen driver;
    private static App sikuliApp;
    private static String pathImage;
    private static String pathApp;

    private static void init() {
        try {
            driver = new Screen();
            pathImage = UtilProperties.getSerenityProperty("desktop.image.path");
            pathApp = UtilProperties.getSerenityProperty("desktop.application.path");

            for (String directory : Utils.getSubDirectoriesNames(pathImage)) {
                ImagePath.add(directory);
            }

            Settings.MoveMouseDelay = 0.07F;
            Utils.waitThread(2000);
            sikuliApp = App.open(pathApp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Screen getInstance() {
        if (driver == null) {
            init();
            return driver;
        } else {
            return driver;
        }
    }

    public static void killDriver() {
        if (sikuliApp != null) {
            driver = null;
            try {
                Runtime.getRuntime().exec("TASKKILL /F /IM " + UtilProperties.getSerenityProperty("desktop.application.processname"));
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
            sikuliApp = null;
        }
    }

}