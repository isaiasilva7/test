package com.pixeon.smart.util;


import com.pixeon.smart.Hooks;

import com.pixeon.util.UtilProperties;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.model.DataTable;
import net.thucydides.core.model.ScenarioOutcomeCounter;
import net.thucydides.core.model.Story;
import net.thucydides.core.model.TestOutcome;
import net.thucydides.core.steps.ExecutedStepDescription;
import net.thucydides.core.steps.StepFailure;
import net.thucydides.core.steps.StepListener;
import net.thucydides.core.steps.StepPublisher;
import org.apache.log4j.Logger;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class BaseStepListener implements StepListener {
    /**
     * LOG
     */
    final static Logger logger = Logger.getLogger(BaseStepListener.class);



    @Override
    public void testSuiteStarted(Class<?> storyClass) {

    }

    @Override
    public void testSuiteStarted(Story story) {

    }

    @Override
    public void testSuiteFinished() {

    }



    @Override
    public void testStarted(String description) {

    }

    @Override
    public void testStarted(String description, String id) {

    }

    @Override
    public void testFinished(TestOutcome result) {

    }

    @Override
    public void testRetried() {

    }

    @Override
    public void stepStarted(ExecutedStepDescription description) {

    }

    @Override
    public void skippedStepStarted(ExecutedStepDescription description) {

    }

    @Override
    public void stepFailed(StepFailure failure) {

        String screenshotsForFailures = UtilProperties.getSerenityProperty("sikuli.take.screenshots.forfailures");

     // if (screenshotsForFailures != null && screenshotsForFailures.equalsIgnoreCase("true")) {
        if (screenshotsForFailures != null){
           if (Hooks.isDesktop()) {
                try {

                    String fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
                    String pathFile = UtilProperties.getSerenityProperty("path.screenshot.sikuli");
                    File[] files;
                    Path pathRelativeDirectory;

                    if (pathFile != null && !pathFile.isEmpty()) {
                        pathRelativeDirectory = Paths.get(pathFile);
                        files = new File(pathRelativeDirectory.toString()).listFiles();

                        if (!Files.exists(pathRelativeDirectory)) {
                            Files.createDirectories(pathRelativeDirectory);
                        }

                        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                        ImageIO.write(image, "png", new File(pathFile + fileName));
                        Serenity.recordReportData().withTitle("Evidências").downloadable().fromFile(Paths.get(pathFile + fileName));

                        if (files != null && files.length > 0) {
                            for (File file : files) {
                                file.delete();
                            }
                        }

                    } else {
                        pathRelativeDirectory = Paths.get(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/"));
                        files = new File(pathRelativeDirectory.toString()).listFiles();

                        if (!Files.exists(pathRelativeDirectory)) {
                            Files.createDirectories(pathRelativeDirectory);
                        }

                        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                        ImageIO.write(image, "png", new File(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/") + fileName));
                        Serenity.recordReportData().withTitle("Evidências").downloadable().fromFile(Paths.get(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/") + fileName));


                        if (files != null && files.length > 0) {
                            for (File file : files) {
                                file.delete();
                            }
                        }
                    }
                } catch (IOException e) {
                    logger.error(e.getMessage());
                } catch (AWTException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }


    @Override
    public void lastStepFailed(StepFailure failure) {

    }

    @Override
    public void stepIgnored() {

    }

    @Override
    public void stepPending() {

    }

    @Override
    public void stepPending(String message) {

    }

    @Override
    public void stepFinished() {
        String screenshotsForFailures = UtilProperties.getSerenityProperty("sikuli.take.screenshots.forfailures");

        if (screenshotsForFailures != null && screenshotsForFailures.equalsIgnoreCase("false")) {
        //if (screenshotsForFailures != null ) {
            if (Hooks.isDesktop()) {
                try {

                    String fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
                    String pathFile = UtilProperties.getSerenityProperty("path.screenshot.sikuli");
                    File[] files;
                    Path pathRelativeDirectory;

                    if (pathFile != null && !pathFile.isEmpty()) {
                        pathRelativeDirectory = Paths.get(pathFile);
                        files = new File(pathRelativeDirectory.toString()).listFiles();

                        if (!Files.exists(pathRelativeDirectory)) {
                            Files.createDirectories(pathRelativeDirectory);
                        }

                        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                        ImageIO.write(image, "png", new File(pathFile + fileName));
                        Serenity.recordReportData().withTitle("Evidências").downloadable().fromFile(Paths.get(pathFile + fileName));

                        if (files != null && files.length > 0) {
                            for (File file : files) {
                                file.delete();
                            }
                        }

                    } else {
                        pathRelativeDirectory = Paths.get(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/"));
                        files = new File(pathRelativeDirectory.toString()).listFiles();

                        if (!Files.exists(pathRelativeDirectory)) {
                            Files.createDirectories(pathRelativeDirectory);
                        }

                        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                        ImageIO.write(image, "png", new File(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/") + fileName));
                        Serenity.recordReportData().withTitle("Evidências").downloadable().fromFile(Paths.get(System.getProperty("user.dir").concat("/").concat("src/test/resources/imagens/") + fileName));


                        if (files != null && files.length > 0) {
                            for (File file : files) {
                                file.delete();
                            }
                        }
                    }
                } catch (IOException e) {
                    logger.error(e.getMessage());
                } catch (AWTException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }

    @Override
    public void testFailed(TestOutcome testOutcome, Throwable cause) {

    }

    @Override
    public void testIgnored() {

    }

    @Override
    public void testSkipped() {

    }

    @Override
    public void testPending() {

    }

    @Override
    public void testIsManual() {

    }

    @Override
    public void notifyScreenChange() {

    }

    @Override
    public void useExamplesFrom(DataTable table) {
    }

    @Override
    public void addNewExamplesFrom(DataTable table) {


    }

    @Override
    public void exampleStarted(Map<String, String> data) {

    }

    @Override
    public void exampleFinished() {

    }

    @Override
    public void assumptionViolated(String message) {

    }

    @Override
    public void testRunFinished() {

    }
}
