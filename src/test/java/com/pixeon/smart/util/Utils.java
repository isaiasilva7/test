package com.pixeon.smart.util;

import java.awt.AWTException;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.sikuli.script.Screen;

public class Utils {

    public static List<String> getSubDirectoriesNames(String path) {
        List<File> files = listFiles(new File(path));
        List<String> directories = new ArrayList<>();

        for (File f : files) {
            directories.add(f.getAbsolutePath());
        }

        return directories;
    }

    public static List<File> listFiles(File directory) {
        List<File> files = new ArrayList<>();
        listFiles(files, directory);
        return files;
    }

    private static void listFiles(List<File> files, File directory) {
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                files.add(file);
                listFiles(files, file);
            }
        }
    }

    public static void waitThread(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




    /**
     * informarTextoEmJanelaDoWindowsEconfiramr
     * @author Gilson
     * @param texto
     */
    public static void informarTextoEmJanelaDoWindowsEconfiramr(String texto) {
        try {
            Thread.sleep(3000);
            int length = texto.length();
            for (int i = 0; i < length; i++) {
                typeCaracterTeclado(texto.charAt(i));
            }
            Thread.sleep(3000);
            Robot robot1=new Robot();
            robot1.keyPress(KeyEvent.VK_ENTER);
            robot1.keyRelease(KeyEvent.VK_ENTER);

        } catch (Exception e) {
            Logger.getLogger("StaleElementReferenceException -: " + e.getMessage());
            Assert.assertTrue(false);
        }
    }


    /**
     * typeCaracterTeclado
     * @author Gilson
     * @param character
     */


    public static void typeCaracterTeclado(char character) {
        Robot robot;
        try {
            robot = new Robot();

            switch (character) {

                case 'a':
                    robot.keyPress(KeyEvent.VK_A);
                    break;

                case 'b':
                    robot.keyPress(KeyEvent.VK_B);
                    break;

                case 'c':
                    robot.keyPress(KeyEvent.VK_C);
                    break;

                case 'd':
                    robot.keyPress(KeyEvent.VK_D);
                    break;

                case 'e':
                    robot.keyPress(KeyEvent.VK_E);
                    break;

                case 'f':
                    robot.keyPress(KeyEvent.VK_F);
                    break;

                case 'g':
                    robot.keyPress(KeyEvent.VK_G);
                    break;

                case 'h':
                    robot.keyPress(KeyEvent.VK_H);
                    break;

                case 'i':
                    robot.keyPress(KeyEvent.VK_I);
                    break;

                case 'j':
                    robot.keyPress(KeyEvent.VK_J);
                    break;

                case 'k':
                    robot.keyPress(KeyEvent.VK_K);
                    break;

                case 'l':
                    robot.keyPress(KeyEvent.VK_L);
                    break;

                case 'm':
                    robot.keyPress(KeyEvent.VK_M);
                    break;

                case 'n':
                    robot.keyPress(KeyEvent.VK_N);
                    break;

                case 'o':
                    robot.keyPress(KeyEvent.VK_O);
                    break;

                case 'p':
                    robot.keyPress(KeyEvent.VK_P);
                    break;

                case 'q':
                    robot.keyPress(KeyEvent.VK_Q);
                    break;

                case 'r':
                    robot.keyPress(KeyEvent.VK_R);
                    break;

                case 's':
                    robot.keyPress(KeyEvent.VK_S);
                    break;

                case 't':
                    robot.keyPress(KeyEvent.VK_T);
                    break;

                case 'u':
                    robot.keyPress(KeyEvent.VK_U);
                    break;

                case 'v':
                    robot.keyPress(KeyEvent.VK_V);
                    break;

                case 'w':
                    robot.keyPress(KeyEvent.VK_W);
                    break;

                case 'x':
                    robot.keyPress(KeyEvent.VK_X);
                    break;

                case 'y':
                    robot.keyPress(KeyEvent.VK_Y);
                    break;

                case 'z':
                    robot.keyPress(KeyEvent.VK_Z);
                    break;

                case 'A':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_A);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'B':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_B);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'C':

                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_C);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'D':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_D);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'E':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_E);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'F':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_F);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'G':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_G);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'H':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_H);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'I':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_I);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'J':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_J);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'K':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_K);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'L':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_L);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'M':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_M);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'N':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_N);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'O':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_O);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'P':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_P);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'Q':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_Q);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'R':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_R);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'S':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_S);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'T':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_T);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'U':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_U);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'V':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_V);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'W':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_W);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'X':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_X);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'Y':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_Y);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case 'Z':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_Z);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case '`':
                    robot.keyPress(KeyEvent.VK_BACK_QUOTE);

                    robot.keyRelease(KeyEvent.VK_SHIFT);

                    break;

                case '0':
                    robot.keyPress(KeyEvent.VK_0);
                    break;

                case '1':
                    robot.keyPress(KeyEvent.VK_1);
                    break;

                case '2':
                    robot.keyPress(KeyEvent.VK_2);
                    break;

                case '3':
                    robot.keyPress(KeyEvent.VK_3);
                    break;

                case '4':
                    robot.keyPress(KeyEvent.VK_4);
                    break;

                case '5':
                    robot.keyPress(KeyEvent.VK_5);
                    break;

                case '6':
                    robot.keyPress(KeyEvent.VK_6);
                    break;

                case '7':
                    robot.keyPress(KeyEvent.VK_7);
                    break;

                case '8':
                    robot.keyPress(KeyEvent.VK_8);
                    break;

                case '9':
                    robot.keyPress(KeyEvent.VK_9);
                    break;

                case '-':
                    robot.keyPress(KeyEvent.VK_MINUS);
                    break;

                case '=':
                    robot.keyPress(KeyEvent.VK_EQUALS);
                    break;

                case '~':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_BACK_QUOTE);

                    break;

                case '!':
                    robot.keyPress(KeyEvent.VK_EXCLAMATION_MARK);
                    break;

                case '@':
                    robot.keyPress(KeyEvent.VK_AT);
                    break;

                case '#':
                    robot.keyPress(KeyEvent.VK_NUMBER_SIGN);
                    break;

                case '$':
                    robot.keyPress(KeyEvent.VK_DOLLAR);
                    break;

                case '%':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_5);

                    break;

                case '^':
                    robot.keyPress(KeyEvent.VK_CIRCUMFLEX);
                    break;

                case '&':
                    robot.keyPress(KeyEvent.VK_AMPERSAND);
                    break;

                case '*':
                    robot.keyPress(KeyEvent.VK_ASTERISK);
                    break;

                case '(':
                    robot.keyPress(KeyEvent.VK_LEFT_PARENTHESIS);
                    break;

                case ')':
                    robot.keyPress(KeyEvent.VK_RIGHT_PARENTHESIS);
                    break;

                case '_':
                    robot.keyPress(KeyEvent.VK_UNDERSCORE);
                    break;

                case '+':
                    robot.keyPress(KeyEvent.VK_PLUS);
                    break;

                case '\t':
                    robot.keyPress(KeyEvent.VK_TAB);
                    break;

                case '\n':
                    robot.keyPress(KeyEvent.VK_ENTER);
                    break;

                case '[':
                    robot.keyPress(KeyEvent.VK_OPEN_BRACKET);
                    break;

                case ']':
                    robot.keyPress(KeyEvent.VK_CLOSE_BRACKET);
                    break;

                case '\\':
                    robot.keyPress(KeyEvent.VK_BACK_SLASH);
                    break;

                case '{':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_OPEN_BRACKET);

                    break;

                case '}':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_CLOSE_BRACKET);
                    break;

                case '|':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_BACK_SLASH);
                    break;

                case ';':
                    robot.keyPress(KeyEvent.VK_SEMICOLON);
                    break;

                case ':':
                    robot.keyPress(KeyEvent.VK_COLON);
                    break;

                case '\'':
                    robot.keyPress(KeyEvent.VK_QUOTE);
                    break;

                case '"':
                    robot.keyPress(KeyEvent.VK_QUOTEDBL);
                    break;

                case ',':
                    robot.keyPress(KeyEvent.VK_COMMA);
                    break;

                case '<':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_COMMA);
                    break;

                case '.':
                    robot.keyPress(KeyEvent.VK_PERIOD);
                    break;

                case '>':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_PERIOD);
                    break;

                case '/':
                    robot.keyPress(KeyEvent.VK_SLASH);
                    break;

                case '?':
                    robot.keyPress(KeyEvent.VK_SHIFT);

                    robot.keyPress(KeyEvent.VK_SLASH);
                    break;

                case ' ':
                    robot.keyPress(KeyEvent.VK_SPACE);
                    break;

                default:

                    throw new IllegalArgumentException("Não é possível digitar o caractere " + character);
            }

        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String geraCPF() {
        int digito1 = 0, digito2 = 0, resto = 0;
        String  nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();
        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);


        int soma = n9*2 + n8*3 + n7*4 + n6*5 + n5*6 + n4*7 + n3*8 + n2*9 + n1*10;
        int valor = (soma / 11)*11;
        digito1 = soma-valor;
        //Primeiro resto da divisão por 11.
        resto = (digito1 % 11);

        if(digito1 < 2){
            digito1 = 0;
        }
        else {
            digito1 = 11-resto;
        }

        int soma2 = digito1 * 2 + n9*3 + n8*4 + n7*5 + n6*6 + n5*7 + n4*8 + n3*9 + n2*10 + n1*11;
        int valor2 = (soma2 / 11)*11;
        digito2 = soma2-valor2;

        //Primeiro resto da divisão por 11.
        resto = (digito2 % 11);
        if(digito2 < 2){
            digito2 = 0;
        }
        else {
            digito2 = 11-resto;
        }

        //Conctenando os numeros
        // numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3) +"." + String.valueOf(n4) +
        //                      String.valueOf(n5) + String.valueOf(n6) +"."+ String.valueOf(n7) +String.valueOf(n8)  +
        //                     String.valueOf(n9)+"-";

        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3)  + String.valueOf(n4) +
                String.valueOf(n5) + String.valueOf(n6) + String.valueOf(n7) +String.valueOf(n8)  +
                String.valueOf(n9);

        //Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
        numeroGerado = numerosContatenados+nDigResult;
        //System.out.println("CPF Gerado " + numeroGerado);
        return numeroGerado;
    }//fim do metodo geraCPF


    /**
     * Gerar nome aleatorio com um tamanho determinado
     * @return
     */
    private static Random rand = new Random();
    private static char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    public  String nomeAleatorio (int tamanho) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i <tamanho+1; i++) {
            int ch = rand.nextInt (letras.length);
            sb.append (letras [ch]);
        }
        return sb.toString();
    }


    /**
     * Gerar número aleatorio com um tamanho determinado
     * @return
     */
    public  String numeroAleatorio (int tamanho) {
        String numeroAleatorioFim = "";
        for (int i = 0; i < 8; i++) {
            int numAleatorio = (int)(Math.random() * 10 ) + 1;
            numeroAleatorioFim += String.valueOf(numAleatorio);
        }
        return numeroAleatorioFim;
    }
    private static Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    /**capturar Texto Da Area De Transferencia (parte 1)
     * @author:
     * @return String
     * @param
     */
    private static String capturarTextoDaAreaDeTransferencia() throws UnsupportedFlavorException, IOException {
        try {
            Utils utils =new Utils();
            if (clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor)) {
                utils.sleep(100);
                return clipboard.getData(DataFlavor.stringFlavor).toString();//Retorna o texto na Área de Transferência.
            }else {
                sleep(100);
                return "";//Não há texto na Área de Transferência, então Retorna uma String vazia.
            }
        } catch (IllegalStateException e) {
            System.out.println("\n> A Área de Transferência está indisponível neste instante: "+e);
            sleep(100); //"Thread.sleep(100)": aguardamos 100ms para depois tentar ler a Clipboard novamente.
            return capturarTextoDaAreaDeTransferencia(); //Recursividade: Tentamos ler a Clipboard novamente, até conseguir.
        }
    }


    /** sleep (espera)
     * @author:
     * @return millissegundos
     * @param
     */

    public static void sleep(long millissegundos) {
        try {
            Thread.sleep(millissegundos);
        } catch (InterruptedException e) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    /** texto Capturado (parte 2)
     * @author:
     * @return String
     * @param
     */
    public  static String  capturadoTextoSelecionado (){
        Robot robot;
        String texto = null;
        try {
            sleep(1000);
            robot = new Robot();
            robot.setAutoWaitForIdle(true);
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_C);
            robot.keyPress(KeyEvent.VK_C);
            robot.keyRelease(KeyEvent.VK_C);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            texto=capturarTextoDaAreaDeTransferencia();

        } catch (AWTException | UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return texto;
    }


    /** get Data Atual Dia Mes Ano
     * @author:
     * @return
     * @param
     */
    public String  getDataAtualDiaMesAno() {
        Date dataAtual = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = format.format(dataAtual);
        return dataFormatada;
    }
    /** get Data Atual Dia Mes Ano Hora Minuto E Segundo
     * @author:
     * @return
     * @param
     */
    public static  String  getDataAtualDiaMesAnoHoraMinutoESegundo() {
    	String dataFormatada = null;

	    	  Date dataAtual = new Date();
	          SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	           dataFormatada = format.format(dataAtual);  

        return dataFormatada;
    }



    /** get Data Hora Minuto E Segundo
     * @author:
     * @return
     * @param
     */
    public static String  getDataHoraMinutoESegundo() {
        Date dataAtual = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
        String dataFormatada = format.format(dataAtual);
        return dataFormatada;
    }

    /** get Data Hora Minuto E Segundo
     * @author:
     * @return
     * @param
     */
    public static String  getDataHoraMinutoSegundoEMilesimo() {
        Date dataAtual = new Date();
        SimpleDateFormat format = new  SimpleDateFormat("hh:mm:ss.SSS");
        String dataFormatada = format.format(dataAtual);
        return dataFormatada;
    }


    /** gera Numero Atraves Da Data Atual
     * @author: Gilson
     * @return
     * @param
     */
    public static String  geraNumeroAtravesDaDataAtual() {
        Date dataAtual = new Date();
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy hh:mm:ss");
        //rs.replace("s","h")
        String numeroGeradoAtravesDaData = ((format.format(dataAtual)).replace(":","")).replace(" ","");
        return numeroGeradoAtravesDaData;
    }

    /**
     * @author: Gilson
     * @param caminhoUrl
     */
    public static void deletarArquivosDeUmDiretorio(String caminhoUrl)  {
        //File file = new File("C:\\Smart\\servimp");
        File file = new File(caminhoUrl);
        File afile[] = file.listFiles();
        int i = 0;

        try {
        	for (int j = afile.length; i < j; i++) {
                File arquivos = afile[i];
                //System.out.println(arquivos.getName());
                Thread.sleep(50);
                arquivos.delete();
            }
           
		} catch (Exception e) {
			 Assert.assertTrue(false);
		}
        
    }

    
    /**
     * @author: Gilson
     * @param caminhoUrl
     */

    public static String capituranomeNomeDoPrimieroArquivo(String caminhoUrl)  {
        String nomeCompleto=null;
        try {
            File file = new File(caminhoUrl);
            File afile[] = file.listFiles();
            int i = 0;

            for (int j = afile.length; i < j; i++) {
                File arquivos = afile[i];
                nomeCompleto=(String.valueOf(arquivos)).replace("'\'","'/'");
                //System.out.println(nomeCompleto);
                break;
            }

        } catch (Exception e) {
            // TODO: handle exception
            //   System.out.println("nok");
            Assert.assertTrue(false);
        }

        return nomeCompleto;
    }

    /**
     *
     * @param caminhoUrl
     * @param quantidade
     * @param paramento2
     */
    public static void abrirPrimeiroArquivoDaPasta(String caminhoUrl,  String quantidade, String paramento2)  {

        try {
            sleep(2000);
            BufferedReader br = new BufferedReader(new FileReader(capituranomeNomeDoPrimieroArquivo(caminhoUrl)));
            int quantidadeLinha=0;
            //String nome=null;
            Boolean param=false;

            while(br.ready()){
                String linha = br.readLine();
                if (linha.contains("|")){
                    quantidadeLinha=quantidadeLinha+1;
                }
                if (linha.contains(paramento2)){
                    param=true;
                }
               //  System.out.println(linha);
            }

            if (quantidadeLinha==Integer.parseInt(quantidade) &&  param.equals(true) ){
                Assert.assertTrue(true);
            }else{
                Assert.assertTrue(false);
            }

        } catch (Exception e) {
            // TODO: handle exception
            Assert.assertFalse(e.getMessage(),true);
        }
    }


    public static String lerArquivo(String ulr) {
        String tudo = "";
        BufferedReader br;

        try {
            br = new BufferedReader(new FileReader(ulr));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            tudo = sb.toString();
            br.close();
        }

        catch (Exception e)   {
            e.printStackTrace();
        }
        return tudo;
    }



    /** Método paraconverte Text Imagem Em TXT
     * referencia: https://github.com/tesseract-ocr/  or  https://pt.osdn.net/projects/sfnet_tesseract-ocr-alt/downloads/tesseract-ocr-3.02-win32-portable.zip/ or
     * @author:
     * @return
     * @param
     */

    public static void converteTextImagemEmTXT() {
        try {
            Robot robot = new Robot();
            // Call the tesseract.exe OCR C:\repositorio
            Process process = new ProcessBuilder("C:\\repositorios\\smart-desktop-automated-tests\\lib\\Tesseract-OCR\\tesseract.exe","C:/repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\imagenTextoDinamica\\printDinomicoRegiao.jpg","C:/repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\imagenTextoDinamica\\out").start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    /**Método para print Por Regiao
     * @author:
     * @return
     * @param
     * @throws AWTException
     * @throws IOException
     */

    public static void  printPorRegiao(int possicaoX1,int possicaoX2, int possicaoY1,int possicaoY2, int altuaRegiao, int larguraRegiao, int tempos) {
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] screens = ge.getScreenDevices();
            Rectangle allScreenBounds = new Rectangle();
            for (GraphicsDevice screen : screens) {
                Rectangle screenBounds = screen.getDefaultConfiguration().getBounds();
                allScreenBounds.width += larguraRegiao;
                allScreenBounds.height =altuaRegiao ;
                allScreenBounds.x=Math.min(possicaoX1, possicaoX2);
                allScreenBounds.y=Math.min(possicaoY1, possicaoY2);
            }
            Robot robot = new Robot();
            BufferedImage bufferedImage = robot.createScreenCapture(allScreenBounds);
            File file = new File("C:\\repositorios\\smart-desktop-automated-tests\\src\\test\\resources\\sikuli-images\\imagenTextoDinamica\\printDinomicoRegiao.jpg");
            if(!file.exists())
                file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            ImageIO.write( bufferedImage, "jpg", fos );
        } catch (Exception e) {
            // TODO: handle exception
            e.getMessage();
            e.printStackTrace();
        }

    }

    public static String consultaBanco( String query,String coluna){
       //query= "Select smm_ctle_cnv  from smm where smm_pac_reg=(Select pac_reg from pac  where Pac_nome='ROBO ABSORA HTGLGI CGOGB ROBO')"
       //coluna= "smm_ctle_cnv"
        Connection con = null;
        String valorObtidoDaconsultanovo = null;
        try {
          try {

          	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
              String connectionUrl = "jdbc:sqlserver://10.71.5.250:1433;databaseName=Smart_Automacao";
              con = DriverManager.getConnection(connectionUrl, "sa", "tacanha");
              Statement stmt = con.createStatement();
              ResultSet rs;

              rs = stmt.executeQuery(query);
              while ( rs.next() ) {
                  String valorObtidoDaconsulta = rs.getString(coluna);
                  if (!(valorObtidoDaconsulta==null)){
                      valorObtidoDaconsultanovo=	valorObtidoDaconsulta;
                      break;
                  }
              }
              con.close();
		} catch (Exception e) {
			// TODO: handle exception
   			System.out.println(e);
			Assert.assertFalse("Erro no sql ", true);
		}

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }
        System.out.println(valorObtidoDaconsultanovo);
        return  valorObtidoDaconsultanovo;
    }



    public static void teclaDeAtalho(String nomeDoAtalho) {
        try {
            Robot robot = new Robot();
            switch (nomeDoAtalho) {
                case "F1":
                    sleep(100);
                    robot.keyPress(KeyEvent.VK_F1);
                    robot.keyRelease(KeyEvent.VK_F1);
                    break;
                case "F2":
                    sleep(100);
                    robot.keyPress(KeyEvent.VK_F2);
                    robot.keyRelease(KeyEvent.VK_F2);
                    break;
                case "F3":
                    sleep(100);
                    robot.keyPress(KeyEvent.VK_F3);
                    robot.keyRelease(KeyEvent.VK_F3);
                    break;
                case "F4":
                    sleep(100);
                    robot.keyPress(KeyEvent.VK_F4);
                    robot.keyRelease(KeyEvent.VK_F4);
                    break;
                case "F5":
                    sleep(100);
                    robot.keyPress(KeyEvent.VK_F5);
                    robot.keyRelease(KeyEvent.VK_F5);
                    break;
                case "F6":
                    robot.keyPress(KeyEvent.VK_F6);
                    robot.keyRelease(KeyEvent.VK_F6);
                    break;
                case "F7":
                    robot.keyPress(KeyEvent.VK_F7);
                    robot.keyRelease(KeyEvent.VK_F7);
                    break;
                case "F8":
                    robot.keyPress(KeyEvent.VK_F8);
                    robot.keyRelease(KeyEvent.VK_F8);
                    break;
                case "F9":
                    robot.keyPress(KeyEvent.VK_F9);
                    robot.keyRelease(KeyEvent.VK_F9);
                    break;
                case "F10":
                    robot.keyPress(KeyEvent.VK_F10);
                    robot.keyRelease(KeyEvent.VK_F10);
                    break;
                case "F11":
                    robot.keyPress(KeyEvent.VK_F11);
                    robot.keyRelease(KeyEvent.VK_F11);
                    break;
                case "F12":
                    robot.keyPress(KeyEvent.VK_F12);
                    robot.keyRelease(KeyEvent.VK_F12);
                    break;
                case "Ctr+Enter":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case "CTRL+ENTER":
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_ENTER);
                    break;
                case "Shft+Enter":
                    robot.keyPress(KeyEvent.VK_SHIFT);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_SHIFT);
                    robot.keyRelease(KeyEvent.VK_ENTER);;
                    break;
                case "Enter":
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);;
                    break;
                default:
                    Assert.assertFalse("Atalho não cadastrado", true);

            }
        }catch(Exception e){
            Assert.assertFalse("Tecla De atalho não encontrada", true);
        }

    }
}
